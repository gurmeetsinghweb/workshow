<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

/**
 * Description of VSHelper
 *
 * @author Genx Sharna @ GenX Infotech
 */
class VSHelper
{

    public static function getSelectedCity($attribute = Null)
    {

        if (\Yii::$app->id !== 'app-frontend') {
            return Null;
        }

        $deafult_city = [
            'id' => 1287,
            'slug' => 'india',
            'name' => 'India'
        ];

        // check selected city
        $selected_city = \Yii::$app->request->cookies->getValue('selected_city', False);
        if (!$selected_city) {
            $user_selected_city_info = $deafult_city;
        } else {
            $user_selected_city_info = unserialize(base64_decode($selected_city));
        }

        if ($attribute) {
            return $user_selected_city_info[$attribute];
        } else {
            return $user_selected_city_info;
        }
    }

    public static function setCityBasedURL($route, $params = [], $route_only = False,$selected_city = True)
    {
        $final_route="";
        if($selected_city)
        {    
            // selected city base url change self::getSelectedCity('slug') . '/' . $route;
        $final_route = self::getSelectedCity('slug') . '/' . $route;
        }
        else{
            $final_route =  $route;
        }
        if (isset($params['slug'])) {
            $final_route .= '/' . $params['slug'];
            if (isset($params['slug1'])) {
                $final_route .= '/' . $params['slug1'];
                unset($params['slug1']);
            }
            if (isset($params['slug2'])) {
                $final_route .= '/' . $params['slug2'];
                unset($params['slug2']);
            }
            
            unset($params['slug']);
            if (isset($params['others'])) {

                $final_route .= "?category%5B%5D=" . $params['others'];
            }
        }

        if ($route_only) {
            return $final_route;
        }
        return \yii\helpers\Url::toRoute($final_route, $params, True);
    }
    
    // non city based
    public static function setNonCityBasedURL($route, $params = [], $route_only = False)
    {
        $final_route =  $route;
        if (isset($params['slug'])) {
            $final_route .= '/' . $params['slug'];
            if (isset($params['slug1'])) {
                $final_route .= '/' . $params['slug1'];
                unset($params['slug1']);
            }
            if (isset($params['slug2'])) {
                $final_route .= '/' . $params['slug2'];
                unset($params['slug2']);
            }
            
            unset($params['slug']);
            if (isset($params['others'])) {

                $final_route .= "?category%5B%5D=" . $params['others'];
            }
        }

        if ($route_only) {
            return $final_route;
        }
        return \yii\helpers\Url::toRoute($final_route, $params, True);
    }
    

    public static function setDealBasedURL($route, $params = [], $route_only = False)
    {
        $final_route = "/deal/";
        if (isset($params['slug'])) {
            $final_route .= $params['slug'];
            unset($params['slug']);
        }

        if ($route_only) {
            return $final_route;
        }
        return \yii\helpers\Url::toRoute($final_route, $params, True);
    }

    public static function accessCheck($role, $web_user = Null)
    {
        if (!$web_user) {
            $web_user = \Yii::$app->user;
        }

        if (!$web_user->can($role)) {
            throw new \yii\web\ForbiddenHttpException();
        }
    }

}
