<?php

namespace common\components;

use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * Description of TNTBaseModel
 *
 */
class GenxBaseModel extends \yii\db\ActiveRecord
{

    STATIC $_CACHE = [];

    const STATUS_DELETED = 0;
    const STATUS_DISABLED = 1;
    const STATUS_REJECTED = 2;
    const STATUS_CANCLED = 3;
    const STATUS_INCOMPLETE = 4;
    const STATUS_PENDINGAPPROVAL = 6;
    const STATUS_APPROVED = 5;
    const STATUS_REDEEMED = 9;
    const STATUS_ACTIVE = 10;
    const FEATURED_ENABLE = 1;
    const FEATURED_DISABLE = 0;

    private static $filter_status = True;

    public function rules($rules = array())
    {
        $default_rules = [
            [['status', 'created_at', 'updated_at'], 'integer',],
            [['status'], 'default', 'value' => self::STATUS_ACTIVE],
        ];

        if (is_array($rules)) {
            foreach ($default_rules as $rule) {
                $rules[] = $rule;
            }
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    public function getCreatedTime()
    {
        return date('F j, Y H:i', $this->created_at);
    }

    public function getCreatedDate()
    {
        return date('F j, Y', $this->created_at);
    }

    public function getUpdatedTime()
    {
        return date('F j, Y H:i', $this->updated_at);
    }

    public static function getStatusDropdown($options = '')
    {
        $default_options = [
            self::STATUS_ACTIVE => "Active",
            self::STATUS_DISABLED => "In-active",
        ];
        if (isset($options['filter_options'])) {
            return ArrayHelper::merge($default_options, (array) $options['filter_options']);
        } else {
            return $default_options;
        }
    }

    public static function getFeaturedDropdown($options = '')
    {
        $default_options = [
            self::FEATURED_DISABLE => "In-Active",
            self::FEATURED_ENABLE => "Active",
        ];
        if (isset($options['filter_options'])) {
            return ArrayHelper::merge($default_options, (array) $options['filter_options']);
        } else {
            return $default_options;
        }
    }

    public function canApprove($column = 'admin_approved')
    {
        return (\Yii::$app->user->isAdmin());
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        try {
            // log performer id
            if ($this->hasAttribute('performed_id') && !\Yii::$app->user->isGuest) {
                $this->performed_id = \Yii::$app->user->getId();
            }
            return parent::save($runValidation, $attributeNames);
        } catch (\Exception $e) {
            $this->addError('save', $e->getMessage());
        }
    }

    public function update($runValidation = true, $attributeNames = null)
    {
        // log performer id
        if ($this->hasAttribute('performed_id') && !\Yii::$app->user->isGuest) {
            $this->performed_id = \Yii::$app->user->getId();
        }
        return parent::update($runValidation, $attributeNames);
    }

    public function delete()
    {

        if (\Yii::$app->user->isGuest) {
            throw new \yii\web\ForbiddenHttpException();
        }

        if (\Yii::$app->user->can(\common\models\User::ROLE_SYSADMIN)) {
            return parent::delete();
        }

        if (isset($this->status)) {
            $this->status = self::STATUS_DELETED;
            $deleted_string = "DELETED_" . time() . "_";

            if (isset($this->username)) {
                $this->username = $deleted_string . $this->username;
            }

            if (isset($this->email)) {
                $this->email = $deleted_string . $this->email;
            }

            if (isset($this->slug)) {
                $this->slug = $deleted_string . $this->slug;
            }

            return $this->save(False);
        }

        return False;
    }

    public function getURL($action = 'view', $only_route = False, $full_url = False)
    {
        $route = [$action, "id" => $this->id, 'slug' => ((isset($this->slug) ? $this->slug : ''))];
        if ($only_route) {
            return $route;
        }

        return \yii\helpers\Url::toRoute($route, $full_url);
    }

    public function getActivateURL()
    {
        $route = ['activate-model', "id" => $this->id, 'return_url' => \yii\helpers\Url::current()];
        return \yii\helpers\Url::toRoute($route);
    }

    public function getDeactivateURL()
    {
        $route = ['deactivate-model', "id" => $this->id, 'return_url' => \yii\helpers\Url::current()];
        return \yii\helpers\Url::toRoute($route);
    }

    /**
     * @inheritdoc
     * @return ActiveQuery the newly created [[ActiveQuery]] instance.
     */
    public static function find($active_only = True)
    {
        $find = parent::find();
        $table_name = (new $find->modelClass())->tableName();
        if ($active_only) {
            $find->andWhere(['!=', $table_name . '.status', self::STATUS_DELETED]);
            if (self::$filter_status && \Yii::$app->id == 'app-frontend') {
                $find->where([$table_name . '.status' => self::STATUS_ACTIVE]);
            }
        }

        return $find;
    }

    public static function enableStatusFilter()
    {
        self::$filter_status = False;
    }

    /**
     * 
     * Udated as Follows from parent to maintain compatibility
     * if(isset ($condition['status'])) {
     *       $query = static::find(False);
     *   } else {
     *       $query = static::find(True);
     *   }
     * 
     * @param type $condition
     * @return type
     * @throws InvalidConfigException
     */
    protected static function findByCondition($condition)
    {
        if (isset($condition['status'])) {
            $query = static::find(False);
        } else {
            $query = static::find(True);
        }

        if (!ArrayHelper::isAssociative($condition)) {
            // query by primary key
            $primaryKey = static::primaryKey();
            if (isset($primaryKey[0])) {
                $pk = $primaryKey[0];
                if (!empty($query->join) || !empty($query->joinWith)) {
                    $pk = static::tableName() . '.' . $pk;
                }
                $condition = [$pk => $condition];
            } else {
                throw new InvalidConfigException('"' . get_called_class() . '" must have a primary key.');
            }
        }

        return $query->andWhere($condition);
    }

    protected function __loggedinUser()
    {
        return \Yii::$app->user;
    }

    public function canView()
    {
        
    }

    public function canCreate()
    {
        
    }

    public function canUpdate()
    {
        // if is admin, then Update
        if ($this->__loggedinUser()->isAdmin()) {
            return True;
        }

        if ($this->hasAttribute('user_id')) {
            $owner_id = $this->user_id;
        }

        // if is owner, then Update
        if ($this->__loggedinUser()->getID() == $owner_id) {
            return True;
        }

        return False;
    }

    public function canEnable()
    {
        
    }

    public function canSearch()
    {
        
    }

}
