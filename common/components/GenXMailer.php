<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\components;

class GenXMailer
{

    public static function send($to, $subject, $body)
    {
        if (is_array($body) && (!isset($body['view']) || !isset($body['vars']))) {
            throw new \yii\web\BadRequestHttpException("Invalid mail Args : " . json_encode($body));
        }


        if (!is_array($body)) {
            $message = $body;
        } else if (is_array($body)) {
            $message = \Yii::$app->getView()->render('@common/mail/' . $body['view'], $body['vars']);
        }

        return mail($to, $subject, $message, self::getHtmlHeaders());
    }

    public static function getHtmlHeaders()
    {
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= "From: " . \Yii::$app->params['supportEmail'] . " <" . \Yii::$app->params['supportEmail'] . ">\r\n";

        return $headers;
    }

    /**
     * 
     * @return GenXMailer
     */
    public static function start()
    {
        return (new GenXMailer());
    }

    /**
     * 
     * @return \yii\swiftmailer\Mailer
     */
    public function sendSignup($user)
    {
        $vars = [
            'view' => ['html' => 'signup-html'],
            'params' => ['user' => $user],
            'to_address' => $user->email,
            'subject' => "Great " . $user->name . "! You have successfully Registered, Please Activate Your Account"
        ];

        return $this->__swiftSend($vars);
    }
    
    public function sendSignupGuest($user,$password)
    {
        $vars = [
            'view' => ['html' => 'signupguest-html'],
            'params' => ['user' => $user,'password'=>$password],
            'to_address' => $user->email,
            'subject' => "Great " . $user->name . "! You have successfully Registered"
        ];

        return $this->__swiftSend($vars);
    }

    /**
     * 
     * @return \yii\swiftmailer\Mailer
     */
    public function sendUserInvoice($order, $filename)
    {
        $vars = [
            'view' => ['html' => 'userinvoice-html'],
            'params' => ['order' => $order],
            'to_address' => $order->user->email,
            'subject' => "You have placed order no : $order->id on Classifr.com",
            'attach' => $filename
        ];

        //GenXHelper::c($order);
        //GenXHelper::c($order->cart);
        return $this->__swiftSend($vars);
    }

    // merchant Stock7087570875 
    public function sendMerchantStockInvoice($deal, $cart)
    {
        $vars = [
            'view' => ['html' => 'merchantstock-html'],
            'params' => ['deals' => $deal, 'cart' => $cart],
            'to_address' => $deal['deal']->user->email,
            'subject' => "Well Done! Your " . $deal['deal']->title . " Classifr was recently bought on Classifr Store",
        ];

        //GenXHelper::c($order);
        //GenXHelper::c($order->cart);
        return $this->__swiftSend($vars);
    }

    // 
    // send request for Enquiry cancel
    public function sendRequestForCartCancel($cart, $request, $to_email = NULL)
    {
        $vars = [
            'view' => ['html' => 'customerdealcancelrequest-html'],
            'params' => ['cart' => $cart, 'request' => $request],
            'to_address' => ($to_email) ? $to_email : $cart->user->email,
            'subject' => "Request ID $request->id - Your request to cancel Classifr ID $cart->id has been received",
        ];

        //GenXHelper::c($order);
        //GenXHelper::c($order->cart);
        return $this->__swiftSend($vars);
    }
    
    // Enquiry cancel request accept and send to customer
       
    public function sendCustomerClassifrCancelRequestAccept($cart, $request, $to_email = NULL)
    {
        $vars = [
            'view' => ['html' => 'customerdealcancelaccept-html'],
            'params' => ['cart' => $cart, 'request' => $request],
            'to_address' => ($to_email) ? $to_email : $cart->user->email,
            'subject' => "Classifr Cancellation Request ID $request->id Has been Approved",
        ];

        //GenXHelper::c($order);
        //GenXHelper::c($order->cart);
        return $this->__swiftSend($vars);
    }
    // Enquiry cancel request reject and send to customer 
     public function sendCustomerClassifrCancelRequestReject($cart, $request, $to_email = NULL)
    {
        $vars = [
            'view' => ['html' => 'customerdealcanceldecline-html'],
            'params' => ['cart' => $cart, 'request' => $request],
            'to_address' => ($to_email) ? $to_email : $cart->user->email,
            'subject' => "Classifr Cancellation Request ID $request->id Has been Rejected",
        ];

        //GenXHelper::c($order);
        //GenXHelper::c($order->cart);
        return $this->__swiftSend($vars);
    }

    


    // merchant Redeem 
    public function sendMerchantAfterRedeemed($cart, $to_email = NULL)
    {
        $vars = [
            'view' => ['html' => 'sendmerchantredeemed-html'],
            'params' => ['cart' => $cart],
            'to_address' => ($to_email) ? $to_email : $cart->merchant->email,
            'subject' => "Congratulations! " . $cart->user->name . " has redeemed the Enquiry Coupon at your outlet.",
        ];

        //GenXHelper::c($order);
        //GenXHelper::c($order->cart);
        return $this->__swiftSend($vars);
    }

    // user redeemed
    // merchant Redeem 
    public function sendCustomerAfterRedeemed($cart, $to_email = NULL)
    {
        $vars = [
            'view' => ['html' => 'sendcustomerredeemed-html'],
            'params' => ['cart' => $cart],
            'to_address' => ($to_email) ? $to_email : $cart->merchat->email,
            'subject' => "Congratulations! " . $cart->user->name . " your Coupon has redeemed successfully.",
        ];

        //GenXHelper::c($order);
        //GenXHelper::c($order->cart);
        return $this->__swiftSend($vars);
    }

    // Contact Us 
    public function sendCountactUs($enquiry)
    {
        $vars = [
            'view' => ['html' => 'ContactUs-html'],
            'params' => ['enquiry' => $enquiry],
            'to_address' => \Yii::$app->params['supportEmail'],
            'subject' => "Contact  " . $enquiry->email_id . " on Classifr.com",
        ];

        //GenXHelper::c($order);
        //GenXHelper::c($order->cart);
        return $this->__swiftSend($vars);
    }

    // Promote Classifr Business 
    public function sendPromoteVoucher($enquiry, $to_email = NULL)
    {
        $vars = [
            'view' => ['html' => 'promotcoupon-html'],
            'params' => ['enquiry' => $enquiry],
            'to_address' => ($to_email) ? $to_email : \Yii::$app->params['supportEmail'],
            'subject' => "Enquiry New Business  " . $enquiry->email_id . " on Classifr.com",
        ];

        //GenXHelper::c($order);
        //GenXHelper::c($order->cart);
        return $this->__swiftSend($vars);
    }

    // Work with Us 
    public function sendWorkFile($enquiry, $filename = NULL)
    {
        $vars = [
            'view' => ['html' => 'workwithus-html'],
            'params' => ['enquiry' => $enquiry],
            'to_address' => \Yii::$app->params['supportEmail'],
            'subject' => "Work with us " . $enquiry->email_id . " on Classifr.com",
            'attach' => $filename,
        ];


        return $this->__swiftSend($vars);
    }
// Desire your voucher
    
    public function sendDesireAVoucher($enquiry, $filename = NULL)
    {
        $vars = [
            'view' => ['html' => 'desireavoucher-html'],
            'params' => ['enquiry' => $enquiry],
            'to_address' =>  \Yii::$app->params['supportEmail'],
            'subject' => "Desire a Classifr" . $enquiry->title . " on Classifr.com",
            'attach' => $filename,
        ];


        return $this->__swiftSend($vars);
    }

    
    public function sendCouponInvoice($coupon, $filename, $to_email = Null)
    {
        $vars = [
            'view' => ['html' => 'usercoupon-html'],
            'params' => ['coupon' => $coupon],
            'to_address' => (($to_email) ? $to_email : $coupon->cart->user->email),
            'subject' => "Your Purchase at Classifr Store " . $coupon->cart->merchant->company->title . " Your Classifr id : #$coupon->id is ready ",
            'attach' => $filename
        ];

        //GenXHelper::c($order);
        //GenXHelper::c($order->cart);
        return $this->__swiftSend($vars);
    }

    public function sendCouponInfoToAdmin($coupon, $to_email = Null)
    {
        $vars = [
            'view' => ['html' => 'usercoupon-html'],
            'params' => ['coupon' => $coupon],
            'to_address' => (($to_email) ? $to_email : $coupon->cart->user->email),
            'subject' => "Your enquiry at Classifr Store with Merchant " . $coupon->cart->merchant->company->title ,
        ];

        //GenXHelper::c($order);
        //GenXHelper::c($order->cart);
        return $this->__swiftSend($vars);
    }

    // send Gift Invoice
    public function sendGift($gift, $filename, $to_email = Null)
    {
        $vars = [
            'view' => ['html' => 'sendgift-html'],
            'params' => ['gift' => $gift],
            'to_address' => $to_email,
            'subject' => "Wow! You have received a Gift Classifr from $gift->gift_from",
            'attach' => $filename
        ];

        //GenXHelper::c($order);
        //GenXHelper::c($order->cart);
        return $this->__swiftSend($vars);
    }

    /**
     * 
     * @return \yii\swiftmailer\Mailer
     */
    public function sendReEmailVerification($user)
    {
        $vars = [
            'view' => ['html' => 're-email-validation'],
            'params' => ['user' => $user],
            'to_address' => $user->email,
            'subject' => "Please verify your email to login"
        ];

        return $this->__swiftSend($vars);
    }

    // send verification code
    public function sendMerchanttoPublish($deal)
    {

        $vars = [
            'view' => ['html' => 'merchantdeal-validation-html'],
            'params' => ['deal' => $deal],
            'to_address' => \Yii::$app->params['deal_publish_email'],//$deal->user->email,
            'subject' => $deal->title . "Please Activate your deal Classifr Store",
           // 'cc'=> \Yii::$app->params['deal_publish_email']
        ]; 


        return $this->__swiftSend($vars);
    
        
    }
    //Send Email to Admin For Merchant Deal Published
    public function sendAdmintonotifydealPublish($deal)
    {

        $vars = [
            'view' => ['html' => 'merchantdealpublished-notifytoadmin-html'],
            'params' => ['deal' => $deal],
            'to_address' =>\Yii::$app->params['deal_publish_email'],
            'subject' => $deal->title . "Merchant live This Deal.",
            //'cc'=> \Yii::$app->params['deal_publish_email']
        ]; 


        return $this->__swiftSend($vars);
    
        
    }
    
    // send to merchant if not received
    public function sendMannualEmailtoMerchanttoPublish($deal,$to_email)
    {

        $vars = [
            'view' => ['html' => 'merchantdeal-validation-html'],
            'params' => ['deal' => $deal],
            'to_address' => $to_email,
            'subject' => $deal->title . "Please Activate your deal Classifr Store",
           // 'cc'=> \Yii::$app->params['deal_publish_email']
        ]; 


        return $this->__swiftSend($vars);
    
        
    }
    
    
    // send associate to view deal
     public function sendAssociateToViewPublish($deal)
    {

        $vars = [
            'view' => ['html' => 'merchantdeal-viewassociate-html'],
            'params' => ['deal' => $deal],
            'to_address' => ((\Yii::$app->params['static_email_for_deal_publish']) ? \Yii::$app->params['deal_publish_email'] : $deal->user->email),
            'subject' => $deal->title . " Please View deal and send email to merchant for activation Classifr Store"
        ];


        return $this->__swiftSend($vars);
    }
    
      // send newsletter to view deal
     public function sendNewsletterSubscribedUser($deal,$to_email = Null,$subject)
    {

        $vars = [
            'view' => ['html' => 'newsletter-subscribed-html'],
            'params' => ['deals' => $deal,'email'=>$to_email],
            'to_address' => $to_email,
            'subject' => $subject
        ];


        return $this->__swiftSend($vars);
    }
     public function sendExpireDealNotification($deal, $Merchant_detail, $to_email = Null,$subject)
    {

        $vars = [
            'view' => ['html' => 'dealExpiredNotificationForMerchant'],
            'params' => ['deals' => $deal,'email'=>$to_email,'merchant_detail'=>$Merchant_detail],
            'to_address' => $to_email,
            'subject' => $subject
        ];


        return $this->__swiftSend($vars);
    }

    /**
     * 
     * @return \yii\swiftmailer\Mailer
     */
    public function sendPasswordReset($user)
    {
        $vars = [
            'view' => ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
            'params' => ['user' => $user],
            'to_address' => $user->email,
            'subject' => "Password Reset"
        ];

        return $this->__swiftSend($vars);
    }

    /**
     * 
     * @return \yii\swiftmailer\Mailer
     */
    private function __swiftSend($vars)
    {
        if (GenXHelper::isLocal()) {
            return True;
        }

        $sendfile = \Yii::$app->mailer->compose($vars['view'], $vars['params'])
                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->params['supportName']])
                ->setTo($vars['to_address'])
                ->setSubject($vars['subject']);
        if(isset($vars['cc']))
        {
            $sendfile->setCc($vars['cc']);
        }  
        if (isset($vars['attach'])) {
            $sendfile->attach($vars['attach']);
        }
        
        return $sendfile->send();
    }

}
