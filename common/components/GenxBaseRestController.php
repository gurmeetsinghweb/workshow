<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

use yii\filters\Cors;

/**
 * Description of GenxBaseRestController
 *
 * @author Acer
 */
class GenxBaseRestController extends \yii\rest\Controller
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
        ];

        $behaviors['authenticator'] = [
            'class' => GenXParamAuth::className(),
            'except' => ['options'],
        ];

//        $behaviors['authenticator'] = [
//            'class' => GenXCompositeAuth::className(),
//            'authMethods' => [
//                'classes' => [
//                    GenXParamAuth::className(),
//                    GenXBearerAuth::className()
//                ],
//                'params' => [
//                    'except' => ['options', 'search'],
//                ]
//            ],
//        ];

        return $behaviors;
    }

}
