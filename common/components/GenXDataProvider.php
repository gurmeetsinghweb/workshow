<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

/**
 * Description of GenXDataProvider
 *
 * @author Genx
 */
class GenXDataProvider extends \yii\data\ActiveDataProvider
{

    public function __construct($config = array())
    {
        if (!isset($config['pagination'])) {
            $config['pagination'] = ['pageSize' => 20];
        }
        if (!isset($config['sort'])) {
            $config['sort'] = ['defaultOrder' => [
                    'id' => SORT_DESC,
            ]];
        }
        return parent::__construct($config);
    }

}
