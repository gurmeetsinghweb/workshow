<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

use linslin\yii2\curl;

class GoogleApiEx
{

    public $action_url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCpPCdHAqfn6D90C_QnKz3-W4ulMuw13E8';
    
    public $addresss = Null;
    public $use_cache = True;
    protected $_parsed_elements = [];

    public function __construct($address)
    {
        
        $this->action_url= $this->action_url."&address=$address";
        //$this->registration_number = $registraion_number;
        //$this->post_XML_doc = str_replace('{{__REGISTRATION_NUMBER__}}', $this->registration_number, $this->post_XML_doc);
    }

    public function setJson($json)
    {
        $this->_parsed_elements = json_decode($json, True);
    }

    public function getJson()
    {
        return $this->_parsed_elements;
    }

    public function getInfo()
    {
          $this->_parseUrl();
          
        $this->setJson($this->_parsed_elements);
        //GenXHelper::c($this->_parsed_elements);
        return $this->_parsed_elements;
    }

    public function getParsedInfo()
    {
        $raw_data = $this->getInfo();
       
        return $return;
    }

    
    protected function _parseUrl()
    {
        $file_cache = new \yii\caching\FileCache();
        if (!$this->use_cache) {
            $data = False;
        } else {
            $data = False;
        }
        if (!$data) {
            try {
                $curl = new curl\Curl();
                $curl
                        ->setOption(CURLOPT_HTTPHEADER, array(
                            'Content-type: application/json',
                            'Content-length: 0'
                        ))
                        ->setOption(CURLOPT_RETURNTRANSFER, TRUE)
                        ->setOption(CURLOPT_MAXREDIRS, 100)
                        ->setOption(CURLOPT_SSL_VERIFYPEER, FALSE)
                        ->get($this->action_url);
                
                $data = $curl->response; 
               

            } catch (\Exception $e) {
                echo $e->getMessage();
                exit;
                $data = False;
            }
            if ($data) {
//                try {
//                    
//                } catch (Exception $e) {
//                    
//                }
            }
        }
        $this->_parsed_elements = $data;
        return $data;
    }

    protected function _parseStr($str)
    {
        return strtolower(trim($str));
    }


    public function __call($name, $arguments)
    {
        if (\yii\helpers\StringHelper::startsWith($name, 'get')) {
            $attribute_name = strtolower(substr($name, 3));
            if (isset($this->_parsed_elements[$attribute_name])) {
                return $this->_parseStr($this->_parsed_elements[$attribute_name]);
            }
        }

        return "";
    }

}
