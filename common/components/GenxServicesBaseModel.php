<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

trait GenxServicesBaseModel
{

    public function initSearch($params, $defaults)
    {
        if (!isset($params['search'])) {
            $params['search'] = [];
        }

        $params['search'] = \yii\helpers\ArrayHelper::merge($defaults, $params['search']);

        return $params;
    }

    public function baseSearch($params, $search_options)
    {

        // if its options request just return blank array
        if (\Yii::$app->getRequest()->getMethod() === 'OPTIONS') {
            return [];
        }

        $query = self::find();
        $dataProvider = new \common\components\GenXDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => ((isset($params['pageSize'])) ? $params['pageSize'] : 10),
            ],
        ]);

        if (isset($search_options['SELECT'])) {
            $query->select($search_options['SELECT']);
        }
        
        if (isset($search_options['HAVING'])) {
            $query->having($search_options['HAVING']);
        }
        if (isset($search_options['orderBy'])) {
            $query->orderBy($search_options['orderBy']);
        }
        
        if (isset($search_options['join']) && is_array($search_options['join']) && sizeof($search_options['join'])) {
            //foreach ($search_options['join'] as $join) {
                $query->join($search_options['join']["tbl_type"], $search_options['join']['table_name'], $search_options['join']['params']);
//                var_dump($search_options['join']);
//                exit();
            //}
        }
        
//        if (is_null($params['search']['status']) || empty($params['search']['status'])) {
//            $query->andWhere(["status" => self::STATUS_ACTIVE]);
//        } else {
//            $query->andWhere(["status" => $params['search']['status']]);
//        }

        if (is_array($search_options['andWhereFilter']) && sizeof($search_options['andWhereFilter'])) {
            foreach ($search_options['andWhereFilter'] as $andWhereFilter) {
                $query->andFilterWhere($andWhereFilter);
            }
        }
        
        
        
        if (isset($params['sort']) && sizeof($params['sort'])) {
            $sortArray = [
                'defaultOrder' => [
                    key($params['sort']) => ($params['sort'][key($params['sort'])] ? SORT_DESC : SORT_ASC)
                ],
                'attributes' => (array_merge((array) (new self)->attributes(), ['distance']))
            ];

            $dataProvider->setSort($sortArray);
        }

        return $dataProvider;
    }

}
