<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;


use yii\web\UrlRule;
/**
 * Description of GenXUrlRule
 *
 * @author Genx Sharna @ GenX Infotech
 */
class GenXUrlRule extends UrlRule
{
    public function createUrl($manager, $route, $params)
    {
        parent::createUrl($manager, $route, $params);
    }
    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();
        if (preg_match('%^(\w+)(/(\w+))?$%', $pathInfo, $matches)) {
            GenXHelper::c($matches);exit;
            // check $matches[1] and $matches[3] to see
            // if they match a manufacturer and a model in the database
            // If so, set $params['manufacturer'] and/or $params['model']
            // and return ['car/index', $params]
        }
        return parent::parseRequest($manager, $request);
    }
}
