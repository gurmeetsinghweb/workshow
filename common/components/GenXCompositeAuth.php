<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

/**
 * Description of GenXCompositeAuth
 *
 * @author Acer
 */
class GenXCompositeAuth extends \yii\filters\auth\CompositeAuth
{

    /**
     * @inheritdoc
     */
    public function authenticate($user, $request, $response)
    {
        foreach ($this->authMethods['classes'] as $i => $auth) {
            if (!$auth instanceof AuthInterface) {
                $this->authMethods['classes'][$i] = $auth = \Yii::createObject($auth, $this->authMethods['params']);
                if (!$auth instanceof \yii\filters\auth\AuthInterface) {
                    throw new InvalidConfigException(get_class($auth) . ' must implement yii\filters\auth\AuthInterface');
                }
            }

            $identity = $auth->authenticate($user, $request, $response);
            if ($identity !== null) {
                return $identity;
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function challenge($response)
    {
        foreach ($this->authMethods['classes'] as $method) {
            /** @var $method AuthInterface */
            $method->challenge($response);
        }
    }

}
