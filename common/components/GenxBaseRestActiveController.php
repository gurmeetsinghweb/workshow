<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

use yii\filters\Cors;

/**
 * Description of GenxBaseRestController
 *
 * @author Acer
 */
class GenxBaseRestActiveController extends \yii\rest\ActiveController
{

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function afterAction($action, $result)
    {
        if (is_array($result)) {
            $result['__params']['b'] = \Yii::$app->getRequest()->getBodyParams();
            $result['__params']['p'] = \Yii::$app->getRequest()->post();
            $result['__params']['g'] = \Yii::$app->getRequest()->get();
        }
        return parent::afterAction($action, $result);
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['authenticator']);

        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
        ];

        $behaviors['authenticator'] = [
            'class' => GenXParamAuth::className(),
            'except' => ['options'],
//            'params' => [
//                    'except' => ['options', 'search'],
//                ]
        ];

//        $behaviors['authenticator'] = [
//            'class' => GenXCompositeAuth::className(),
//            'authMethods' => [
//                'classes' => [
//                    GenXParamAuth::className(),
//                    GenXBearerAuth::className()
//                ],
//                'params' => [
//                    'except' => ['options', 'search'],
//                ]
//            ],
//        ];
        
        return $behaviors;
    }

    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = False;
    }

    public function getModel()
    {
        return new $this->modelClass();
    }

    public function actionSearch()
    {
        if ($this->isOPTIONS()) {
            return [];
        }

        /* @var $modelClass \yii\db\BaseActiveRecord */
        $modelClass = $this->getModel();

        if (method_exists($modelClass, 'search')) {
            return $modelClass->search(\Yii::$app->request->get());
        }

        return new GenXDataProvider([
            'query' => $modelClass::find(),
            'pagination' => [
                'pageSize' => \Yii::$app->request->get('pageSize', 10),
            ],
        ]);
    }

    public function actionListAll()
    {
        if ($this->isOPTIONS()) {
            return [];
        }

        /* @var $modelClass GenxBaseModel */
        $modelClass = $this->getModel();

        return new \yii\data\ActiveDataProvider([
            'query' => $modelClass::find(),
            'pagination' => [
                'pageSize' => 1000,
            ],
        ]);
    }

    protected function buildExpandAttributes($model, $return = array())
    {
        if (\Yii::$app->request->get('expand')) {
            $expand_attributes = explode(",", \Yii::$app->request->get('expand'));
            if (sizeof($expand_attributes)) {
                foreach ($expand_attributes as $att) {
                    $attribute = trim($att);
                    if ($model->{$attribute}) {
                        $return[$attribute] = $model->{$attribute}->exportAbleAttributes;
                    }
                }
            }
        }

        return $return;
    }

    protected function sendFcm($user, $message)
    {
        if (\Yii::$app->getRequest()->getMethod() === 'OPTIONS') {
            return [];
        }

        $to = $user->fcmToken->fcm_registration_token;
        $ch = curl_init("https://fcm.googleapis.com/fcm/send");
        $header = array(
            "Content-Type: application/json",
            "Authorization: key=AIzaSyC4od38OZkAVvnNXY0rl1YVUBMz_rcPc20"
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);

        $data = [
//            'notification' => [
//                'title' => $params['title'],
//                'body' => $params['body'],
//                'sound' => 'default'
//            ],
            'to' => $to,
            'data' => [
                'title' => $message['title'],
                'body' => $message['body'],
            ]
        ];
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        $response = curl_exec($ch);
        curl_close($ch);
        return json_decode($response);
    }

    protected function isOPTIONS() {
        return (\Yii::$app->getRequest()->getMethod() === 'OPTIONS');
    }
}
