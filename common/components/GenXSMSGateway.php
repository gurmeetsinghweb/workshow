<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

/**
 * Description of GenXSMSGateway
 *
 * @author Genx
 */
class GenXSMSGateway {

    public $authKey = '10181AlD9Jl8qMi56c967e1';
    public $apiUrl = 'http://panel.msgclub.net/api/sendhttp.php';
    public $apiRoute = 4;
    public $senderID = 'LKBIND';
    public $message = Null;
    public $mobileNumber = Null;

    public function __construct($mobileNumber = Null, $message = Null) {
        $this->message = $message;
        $this->mobileNumber = $mobileNumber;
    }

    public function send() {
        $postData = array(
            'authkey' => $this->authKey,
            'mobiles' => $this->mobileNumber,
            'message' => $this->message,
            'sender' => $this->senderID,
            'route' => $this->apiRoute
        );

        $query_String_data = [];
        foreach($postData as $key => $val) {
            $query_String_data[] = $key . "=" . urlencode($val);
        }
        
        $final_url = $this->apiUrl . "?" . implode('&', $query_String_data);
        $output = file_get_contents($final_url);
        return $output;
    }

}
