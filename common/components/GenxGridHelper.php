<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

use yii\helpers\Html;
use \yii\helpers\ArrayHelper;

/**
 * Description of GridHelper
 *
 * @author Genx
 */
class GenxGridHelper
{

    public static function linkedID()
    {
        return [
            'attribute' => 'id',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a("#" . $model->id, $model->getURL());
            },
        ];
    }

    public static function linkedTitle($attrib = 'title')
    {
        return [
            'attribute' => $attrib,
            'format' => 'raw',
            'value' => function ($model) use ($attrib) {
                return Html::a($model->{$attrib}, $model->getURL());
            },
        ];
    }

    public static function ActionColumn($options = [])
    {
        $defauls = [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}',
        ];
        // {delete}
        $options = ArrayHelper::merge($defauls, $options);
        return $options;
    }

    public static function users()
    {
        $user_data = \common\models\User::find()
                        ->andWhere(['!=', 'type', \common\models\User::ROLE_SYSADMIN])
                        ->andWhere(['status' => GenxBaseModel::STATUS_ACTIVE])
                        ->asArray()->all();
        return [
            'attribute' => 'user_id',
            'value' => function($data) {
                return $data->user->name;
            },
            'filter' => ArrayHelper::map($user_data, 'id', 'name'),
        ];
    }

    public static function countries()
    {
        return [
            'attribute' => 'country_id',
            'value' => function($data) {
                if (!$data->country) {
                    return 'NAN';
                }
                return $data->country->name;
            },
            'filter' => ArrayHelper::map(\common\models\Country::find()->andWhere(['status' => GenxBaseModel::STATUS_ACTIVE])->asArray()->all(), 'id', 'name'),
        ];
    }

    public static function states()
    {
        return [
            'attribute' => 'state_id',
            'value' => function($data) {
                if (!$data->state) {
                    return 'NAN';
                }
                return $data->state->name;
            },
            'filter' => ArrayHelper::map(\common\models\State::find()->andWhere(['status' => GenxBaseModel::STATUS_ACTIVE])->asArray()->all(), 'id', 'name')
        ];
    }

    public static function cities()
    {
        return [
            'attribute' => 'city_id',
            'value' => function($data) {
                return $data->city->name;
            },
            'filter' => ArrayHelper::map(\common\models\City::find()->andWhere(['status' => GenxBaseModel::STATUS_ACTIVE])->asArray()->all(), 'id', 'name')
        ];
    }

    public static function status($options = [])
    {
        return [
            'attribute' => 'status',
            'value' => function($data) {
                switch ($data->status) {
                    case GenxBaseModel::STATUS_ACTIVE:
                        return "Active";
                        break;
                    case GenxBaseModel::STATUS_REDEEMED:
                        return "Redeemed";
                        break;
                    case GenxBaseModel::STATUS_CANCLED:
                        return "Canceled";
                        break;

                    case GenxBaseModel::STATUS_DISABLED:
                        return "In-Active";
                        break;
                }
            },
            'filter' => GenxBaseModel::getStatusDropdown($options)
        ];
    }

    public static function featured($options = [])
    {
        return [
            'attribute' => 'featured',
            'value' => function($data) {
                switch ($data->featured) {
                    case GenxBaseModel::FEATURED_ENABLE:
                        return "Active";
                        break;
                    case GenxBaseModel::FEATURED_DISABLE:
                        return "Not-Active";
                        break;
                }
            },
            'filter' => GenxBaseModel::getFeaturedDropdown($options)
        ];
    }

    public static function statusToggle()
    {
        return [
            'attribute' => 'status',
            'label' => 'Update Status',
            'format' => 'raw',
            'value' => function($data) {
                switch ($data->status) {
                    case GenxBaseModel::STATUS_ACTIVE:
                        return Html::a('<i class="fa fa-toggle-on" style="color:green; font-size:30px;" aria-hidden="true"></i>', $data->getDeactivateURL());
                        break;
                    case GenxBaseModel::STATUS_DISABLED:
                        return Html::a('<i class="fa fa-toggle-off" style="color:red; font-size:30px;" aria-hidden="true"></i>', $data->getActivateURL());
                        break;
                    case GenxBaseModel::STATUS_APPROVED:
                        return Html::a( '<i class="fa fa-refresh" aria-hidden="true"></i> Resend Code', \yii\helpers\Url::toRoute([
                                            '/deal/resend-activation-code',
                                            'id' => $data->id
                                        ]), ['class' => 'btn btn-warning btn-sm']);
                        break;
                    case GenxBaseModel::STATUS_INCOMPLETE:
                        return Html::a( '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Incomplete', '#' , ['class' => 'btn btn-danger btn-sm']);
                        break;
                    case GenxBaseModel::STATUS_PENDINGAPPROVAL:
                         $url = \yii\helpers\Url::toRoute(['/deal/complete', 'id' => $data->id]);
                        return Html::a( '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Pending Approval', $url,
                                [
                'class' => 'btn btn-primary btn-sm',
                'data' => [
                    'confirm' => \Yii::t('app', 'Are you sure to Approve this deal'),
                    'method' => 'post',
                ]
            ]
                                
                                );
                        break;
                    
                }
            },
            'filter' => False,
            'visible' => \Yii::$app->user->isManager(),
        ];
    }

    public static function updatedTime()
    {
        return [
            'attribute' => 'updated_at',
            'value' => function($data) {
                return $data->getUpdatedTime();
            },
        ];
    }

    public static function createdTime()
    {
        return [
            'attribute' => 'created_at',
            'value' => function($data) {
                return $data->getCreatedTime();
            },
            'filter' => False,
        ];
    }

}
