<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;
use common\models\City;
use common\models\CitySearch;
use yii\db\Expression;
use Yii;

/**
 * Description of GenXHelper
 *
 * @author Genx Sharna @ GenX Infotech
 */
class GenXHelper
{

    static $GLOBAL_VARS = array();

    public static function c($array)
    {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
    }

    public static function isLocal()
    {
        return (bool) \Yii::$app->params['is_local'];
    }

    public static function DevErrors($errors)
    {
        if (!YII_DEBUG) {
            return False;
        }

        if (sizeof($errors)) {
            foreach ($errors as $key => $error) {
                foreach ($error as $_err)
                    Yii::$app->getSession()->addFlash('error', $_err);
            }
            print_r($errors);
        }

        return True;
    }

    public static function setPageVar($var_name, $var_value)
    {
        return self::$GLOBAL_VARS[$var_name] = $var_value;
    }

    public static function getPageVar($var_name)
    {
        if (!isset(self::$GLOBAL_VARS[$var_name])) {
            return Null;
        }

        return self::$GLOBAL_VARS[$var_name];
    }

    public static function buildTitle($title)
    {
        return \Yii::t('app', $title . ' - ' . \Yii::$app->getHomeUrl());
    }

    public static function displayAmount($amount, $show_unit = True)
    {
        return ((($show_unit) ? \Yii::$app->params['currency_unit'] : "") . "" . $amount);
    }

    public static function contentHeader(\yii\web\View &$view, $title = '')
    {
        $view->beginBlock('content-header');
        echo (($title) ? $title : $view->title);
        $view->endBlock();
    }

    public static function contentHeaderButtons(\yii\web\View &$view, $buttons)
    {
        if (!is_array($buttons) || !sizeof($buttons)) {
            return False;
        }
        $button_html = '';
        foreach ($buttons as $button) {
            $button_html .= '<a href="' . $button['url'] . '"';
            if (isset($button['data']) && is_array($button['data']) && sizeof($button['data'])) {
                foreach ($button['data'] as $k => $v) {
                    $data_string_arr[] = 'data-' . $k . '="' . $v . '"';
                }

                $button_html .= implode(" ", $data_string_arr);
            }
            $button_html .= '><button type="button" class="btn btn-sm btn-icon btn-inverse btn-round" data-toggle="tooltip" data-original-title="' . $button['title'] . '">
                                <i class="icon ' . $button['icon'] . '" aria-hidden="true"></i>
                            </button>
                            </a>';
        }
        $html = '
            <div class="page-header-actions">
                ' . $button_html . '
            </div>
            ';

        $view->beginBlock('content-header-buttons');
        echo $html;
        $view->endBlock();
    }

    public static function accountSum($options = array())
    {
        $cartmodel = new \common\models\CartSearch();

        $totalreceived = "round(sum(cart.net_price),2) AS total_payment";
        $totalmetchant = "round(sum(cart.merchant_total_payment),2) AS total_merchant";
        $totalmetchantpending = "round(sum(if(cart.transaction_log_id is NULL,cart.merchant_total_payment,0)),2) AS total_merchantpending";
        $totalmetchantgiven = "round(sum(if(cart.transaction_log_id is not NULL,cart.merchant_total_payment,0)),2) AS total_merchantgiven";
        $totalcompany = "round(sum(cart.site_total_payment),2) AS total_company";
        $totalcompanypending = "round(sum(if(cart.transaction_log_id is NULL,cart.site_total_payment,0)),2) AS total_companypending";
        $totalcompanygiven = "round(sum(if(cart.transaction_log_id is not NULL,cart.site_total_payment,0)),2) AS total_companygiven";
        $totalcompanytax = "round(sum(cart.site_gst),2) AS total_companytax";

        $search_options['select'] = [
            "cart.*",
            $totalreceived,
            $totalmetchant,
            $totalmetchantpending,
            $totalmetchantgiven,
            $totalcompany,
            $totalcompanypending,
            $totalcompanygiven,
            $totalcompanytax
        ];



        if (isset($options['select'])) {
            $search_options['select'] = $options['select'];
        }

        $metrics_data_provider = $cartmodel->search([]);
        $metrics_data_provider
                ->query
                ->select($search_options['select']);

        if (isset($options['andWhere']) && is_array($options['andWhere']) && sizeof($options['andWhere'])) {
            foreach ($options['andWhere'] as $where) {
                $metrics_data_provider->query->andWhere($where);
            }
        }
        if (isset($options['groupby']) && is_array($options['groupby']) && sizeof($options['groupby'])) {


            $metrics_data_provider->query->groupby($options['groupby']);
        }

        if (isset($options['all']) && $options['all'] > 0) {

            $metrics_data_provider->query->all();
            $model = $metrics_data_provider->getModels();
            if (isset($options['dataprovider'])) {
                return $metrics_data_provider;
            } else {
                return $model;
            }
        } else {

            $metrics_data_provider->query->asArray();
            $model = $metrics_data_provider->getModels();
            return $model;
        }
    }

    public static function validateCart($cart)
    {
        $cartModel = new \common\models\Cart();

        foreach ($cart as $key => $values) {

            if (!$values['deal_option']->isInStock()) {

                $error = [
                    'validate' => false,
                    'errors' => [
                        $values['deal_option']->name . " -> This voucher is out of stock \!"
                    ],
                    'route' => 'cart/'
                ];
                //$route = "cart/";
                return $error;
            }

            $totalbuy = (int) $cartModel->find()
                            ->andWhere(["deal_option_id" => $values['deal_option']->id])
                            ->andWhere(["user_id" => Yii::$app->user->id])->sum("qty");
            if($values['deal_option']->no_per_person)
            {    
            if ($totalbuy <= $values['deal_option']->no_per_person) {
                $newbuy = $values['qty'] + $totalbuy;

                if ($newbuy > $values['deal_option']->no_per_person) {
                    $error = [
                        'validate' => false,
                        'errors' => [
                             "Only ".$values['deal_option']->no_per_person." Allowed per customer for this voucher."
                        ],
                        'route' => 'cart/'
                    ];
                    //$route = "cart/";
                    return $error;
                }
            }
            }
            if($values['qty'] > $values['deal_option']->getPendingStock()){
                $error = [
                        'validate' => false,
                        'errors' => [
                            $values['deal_option']->name . " ->  Total Stock available is ".$values['deal_option']->getPendingStock()
                        ],
                        'route' => 'cart/'
                    ];
                    return $error;
                    
            }   
        }
        $error = [
            'validate' => true,
            'errors' => [],
            'route' => 'cart/proceed-checkout'
        ];
        return $error;
    }

    // cancel request

    public static function checkDealCancelRequest($couponId)
    {

        return \common\models\Request::find()->Where([
                    'from_id' => Yii::$app->user->id,
                    'type' => 'cancel_coupon',
                    'object_id' => $couponId,
                    'status' => \common\models\Request::STATUS_ACTIVE,
                ])->one();
    }

    // if deal request is alerady send .

    public static function checkDealUpdateRequest($dealId)
    {

        return \common\models\Request::find()->Where([
                    'from_id' => Yii::$app->user->id,
                    'type' => 'deal_update',
                    'object_id' => $dealId,
                    'status' => \common\models\Request::STATUS_ACTIVE,
                ])->one();
    }

    public static function slug($string)
    {
        $string = strtolower($string);
        $string = preg_replace('/[^a-z0-9 -]+/', '', $string);
        $string = str_replace(' ', '-', $string);
        return trim($string, '-');
    }

    public static function username($string)
    {
        $string = strtolower($string);
        $string = preg_replace('/[^a-z0-9 -]+/', '', $string);
        $string = str_replace(' ', '_', $string);
        return trim($string, '_');
    }

    public static function getBanner()
    {

        $couponModel = new \common\models\DiscountcouponDetails();
        $todaytime = time();
        $banner = $couponModel->find()->andWhere(['<', 'coupon_start_dt', $todaytime])->andWhere(['>', 'coupon_expire_dt', $todaytime])->orderBy(new Expression('rand()'))->one();
        return $banner;
    }

    public static function couponValidate($model)
    {

        if (empty($model)) {
            $error = [
                'validate' => false,
                'code' => 1,
                'errors' => [
                    "Unfortunately the Promo Code is not valid.",
                ]
            ];
            return $error;
        } else {

            $order = new \common\models\Order();
            $todaytime = time();
// total used
            if ($model->total_qty) {
                $totaluserused = $order->find()->andWhere(['discountcoupon_id' => $model->id])->count();
                if ($totaluserused >= $model->total_qty) {
                    $error = [
                        'validate' => false,
                        'code' => 2,
                        'errors' => [
                            "Inactive Promo Code. Try again later.",
                        ]
                    ];
                    return $error;
                }
            }

            // if check already used
            if ($model->total_person_allow) {
                $myused = $order->find()->andWhere(['user_id' => Yii::$app->user->id])->andWhere(['discountcoupon_id' => $model->id])->count();
                if ($myused >= $model->total_person_allow) {
                    $error = [
                        'validate' => false,
                        'code' => 3,
                        'errors' => [
                            "Promo Code has exceeded maximum use on your account",
                        ]
                    ];
                    return $error;
                }
            }
            // check coupon date 
            if (!($todaytime > $model->coupon_start_dt && $todaytime < $model->coupon_expire_dt)) {
                $error = [
                    'validate' => false,
                    'code' => 4,
                    'errors' => [
                        "Promo Code Expired",
                    ]
                ];
                return $error;
            }
            // check amount and price
            $getcart = yii::$app->session['cart'];
            $totalprice = 0;
            foreach ($getcart as $key => $options) {
                $netprice = $options['deal_option']->selling_price * $options['qty'];
                $totalprice += $netprice;
            }
            // check if minimum balance 
            if ($model->min_bill) {
                if ($totalprice < $model->min_bill) {
                    $error = [
                        'validate' => false,
                        'code' => 5,
                        'errors' => [
                            "Unfortunately the Promo Code requires < Minimum $model->min_bill > purchase. ",
                        ]
                    ];
                    return $error;
                }
            }

            // check out category valid
            if ($model->deal || $model->category) {

                $searchcat = [];
                $dealExist = 0;
                $catExist = 0;
                if ($model->deal) {
                    foreach ($model->discountcouponDeals as $getdeal) {
                        $searchdeal[] = $getdeal->deal_id;
                    }
                    foreach ($getcart as $key => $options) {
                        $cartdeal = $options['deal']->id;
                        if (!(in_array($cartdeal, $searchdeal))) {
                            $dealExist = 1;
                            break;
                        }
                    }
                } else {
                    $dealExist = 1;
                }
                // check category
                if ($model->category) {
                    foreach ($model->discountcouponCategories as $getcategory) {
                        $searchcat[] = $getcategory->category_id;
                    }
                    foreach ($getcart as $key => $options) {
                        // gurmeet 16/7/2017 remove $options['deal']->main_category_id; and apply multipal category table and check each record on single deal
                        
                        //$cartdeal = $options['deal']->main_category_id;
                        
                        
                        $cartdealarray = $options['deal']->dealCategory;
                        $chkcatexist=0;
                        foreach($cartdealarray as $catdealarray)
                        {
                            $dealcat_id=$catdealarray->category_id;
                            if(in_array($dealcat_id, $searchcat))
                            {
                                $chkcatexist=1;
                                break;
                            }        
                        }
                        if(!$chkcatexist){
                            $catExist = 1;
                            break;
                        }
                     
                       //16/7/2017 gurmeet remove old single check 
                       // if (!(in_array($cartdeal, $searchcat))) {
                       //     $catExist = 1;
                       //     break;
                       // }
                    }
                } else {
                    $catExist = 1;
                }

                if ($dealExist && $catExist) {
                    $error = [
                        'validate' => false,
                        'code' => 8,
                        'errors' => [
                            "Invalid Promo Code Category.",
                        ]
                    ];
                    return $error;
                }
            }

            /*
              if($model->category)
              {
              $searchcat= [];
              $catExist=0;
              foreach($model->discountcouponCategories as $getcategory)
              {
              $searchcat[]=$getcategory->category_id;
              }
              foreach ($getcart as $key => $options) {
              $cartdeal=$options['deal']->main_category_id;
              if(!(in_array($cartdeal, $searchcat)))
              {
              $catExist=1;
              break;
              }
              }
              if($catExist){
              $error = [
              'validate' => false,
              'code'=>8,
              'errors' => [
              "Sorry, this promo code is not available for this category. Please try again in the correct category",
              ]
              ];
              return $error;
              }


              }
             */





            // finally return 
            if ($model->amount_type == 'percentage') {

                if ($model->max_credit) {

                    $totaldiscount = round((($totalprice * $model->amount_value) / 100), 2);
                    $creditamount = $totaldiscount;
                    if ($totaldiscount > $model->max_credit) {
                        $creditamount = $model->max_credit;
                    }


                    $totalper = round((($creditamount * 100) / $totalprice), 2);
                    $error = [
                        'validate' => true,
                        'code' => 6,
                        'errors' => [
                            "Successfull updated",
                        ],
                        'model' => $model,
                        'totalper' => $totalper,
                        'totalcredit' => $creditamount
                    ];
                    return $error;
                } else {

                    $totaldiscount = round((($totalprice * $model->amount_value) / 100), 2);
                    $error = [
                        'validate' => true,
                        'code' => 7,
                        'errors' => [
                            "Successfull updated",
                        ],
                        'model' => $model,
                        'totalper' => $model->amount_value,
                        'totalcredit' => $totaldiscount
                    ];
                    return $error;
                }
            }
            // amount type value 
            else {



                $totalper = round((($model->amount_value * 100) / $totalprice), 2);
                $error = [
                    'validate' => true,
                    'errors' => [
                        "Successfull updated",
                    ],
                    'code' => 7,
                    'model' => $model,
                    'totalper' => $totalper,
                    'totalcredit' => $model->amount_value
                ];
                return $error;
            }
        }
    }

    public static function getThemeAssetsURL($theme, $app = 'frontend')
    {
        return Yii::$app->assetManager->getPublishedUrl('@' . $app . '/themes/' . $theme);
    }

    // date formet
    public static function getDateFormat($date)
    {
        return date("d F, Y", $date);
    }

    public static function totalDealSoldAount($options = array(), $dateBetween = array())
    {

        $metrics = new \common\models\CartSearch();
        $metrics_data_provider = $metrics->search([]);
        $metrics_data_provider
                ->query
                ->select("cart.*,sum(cart.net_price) as total_sale, sum(cart.qty) as total_deals,sum(merchant_total_payment) as merchant_sale, sum(site_total_payment) as company_sale")
                ->asArray();

        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $metrics_data_provider->query->andWhere(['cart.merchant_id' => Yii::$app->user->getLoggedinID()]);
        }
        if (!empty($dateBetween)) {
            $metrics_data_provider->query
                    ->andWhere(['between', 'cart.created_at', $dateBetween['start_time'], $dateBetween['current_time']])
                    ->groupBy('DATE(FROM_UNIXTIME(cart.created_at))');
        }
        //GenXHelper::c($options);

        if (isset($options['andWhere']) && is_array($options['andWhere']) && sizeof($options['andWhere'])) {
            foreach ($options['andWhere'] as $where) {

                $metrics_data_provider->query->andWhere($where);
            }
        }
        if (isset($_REQUEST['page'])) {
            $metrics_data_provider->pagination->pageSize = 1;
            $metrics_data_provider->pagination->page = 0;
        }
        // $metrics_data_provider->pagination->currentPage=0;
        // $metrics_data_provider->query->limit(1)->offset(0); 
        $models = $metrics_data_provider->getModels();
        if($models)
        {    
        return $models;
        }
        else{
            return 0;
        }
    }

    public static function getHomePageCategory($categoryTree)
    {
        $gethomecategory = array();
       
        foreach ($categoryTree as $categories) {
            foreach ($categories as $category) {

                //$gethomecategory[]= $category;

                if (($category['showhome']) && ($category['active_deals'])) {
                    $gethomecategory[] = $category;
                }
                if (isset($category['children'])) {
                    foreach ($category['children'] as $categorymain) {

                        if (($categorymain['showhome']) && ($categorymain['active_deals'])) {
                            $categorymain['parent'] = $category['slug'];
                            $gethomecategory[] = $categorymain;
                        }

                        if (isset($categorymain['children'])) {
                            foreach ($categorymain['children'] as $categorysub) {

                                if (($categorysub['showhome']) && ($categorysub['active_deals'])) {

                                    $categorysub['parentsub'] = $categorymain['slug'];
                                    $gethomecategory[] = $categorysub;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $gethomecategory;
    }
    
    public static function getHomePageCategorySerial(){
        $getcategoryList = array();
        $categoryModel=new \common\models\CategorySearch();
        $categorydealProvider = $categoryModel->search([]);
        $categorydealProvider->query->andWhere(['category.showhome'=>1]);
        $categorydealProvider->query->orderBy('serial_order asc');
        $category=$categorydealProvider->getModels();
       
        foreach($category as $categorylist)
        {
            if($categorylist->getDeals(False)->count())
            {    
                $getcategoryList[]=$categorylist;
                
            }
        }    
        return $getcategoryList;
    }
    //code start for homepage select cities menu created by kuldeep 7/21/2017
    public function getHomePageCities($term = Null)
    {
        $selected_city = \Yii::$app->request->cookies->getValue('selected_city', False);
        if ($selected_city) {
            $selected_city_is = unserialize(base64_decode($selected_city));
        } else {
            $city = \common\models\City::getSelectedDefaultCity();
            if ($city) {

                $defaultItem = [
                    'selected_city' => base64_encode(serialize([
                        'id' => $city['id'],
                        'name' => $city['name'],
                        'slug' => $city['slug'],
                        'search_name' => $city['search_name'],
                        'latitude' => $city['latitude'],
                        'longitude' => $city['longitude'],
                                    ]
                            )
                    ),
                ];

                $selected_city_is = $defaultItem['selected_city'];
                $selected_city_is = unserialize(base64_decode($selected_city_is));
            }
        }
        $cityModel = new \common\models\City;
        $citySearchModel = new \common\models\CitySearch;
        $city_search_options = [];
        $term = trim($term);
        if (!empty($term)) {
            $city_data = City::find()->andWhere(['like', 'name', $term])->limit(20)->all();
        } else {
            $city_data = City::getFeaturedCity()->all();
        }
        $return = [];
         if (empty($term) && sizeof($selected_city_is)) {
            $return['results'][] = [
                'text' => $selected_city_is['name'],
                'id' => $selected_city_is['id'],
                'total_deals_in_city' => 0
            ];
        }
        if (sizeof($city_data)) {
            foreach ($city_data as $city) {
                if(($key = array_search($city->name, $selected_city_is)) !== false) {
                    unset($city[$key]);
                }else{
                    $return['results'][] = [
                        'text' => $city->name,
                        'id' => $city->id,
                        'total_deals_in_city' => $city->total_deals_in_city
                    ];
                }
            }

            $return['more'] = False;
        }
        return $return;
      /*  $return['results'][] = [
            'text' => 'All Australia',
            'id' => "-1",
        ];
        */
        // send with json headers
//        header('Content-Type: application/json');
//        echo json_encode($return);
    }
        //code ends for homepage select cities menu created by kuldeep 7/21/2017

    public static function encryptUserLink($user_id, $next_url, $controllerUrl, $validation_time = 1296000)
    {
        $user = \common\models\User::findOne($user_id);
        if (!$user) {
            return False;
        }

       
           // $user->generatePasswordResetToken();
           // $user->update(False);
       
        $ts = time();
        $random_1 = time() + $validation_time;
        $random_2 = rand($ts, $random_1);
        $data = base64_encode($next_url);
//        $hash = base64_encode($user->password_reset_token);

        $params = [
            $controllerUrl,
            't' => $ts,
            'd' => $data,
            'r1' => $random_1,
            'r2' => $random_2,
//            'h' => $hash,
        ];
        return \yii\helpers\Url::toRoute($params, True);
    }
    
    // get payment account
    public  static function accountPaymentAdmin(){
        
        $getdate=date('d');
        if($getdate == 1 || $getdate==15){
        $paymentgiven= \yii::$app->params['payment_given']; 
            if($getdate > $paymentgiven[0] and $getdate <= $paymentgiven[1])
            { 
                $day= date('d',strtotime('last day of previous month'));
             
                $slab=1;
                $dateslab = mktime(23,59,59,date('m')-1,$day,date('Y'));
                //date("Y-n-j G:i:s", mktime(23,59,59,date('m')-1,$day,date('Y')));
                
              
              }
            else{
                
                $slab=2;
                $dateslab= mktime(23,59,59,date('m'),15,date('Y'));
                //date("Y-n-j G:i:s", mktime(23,59,59,date('m')-1,15,date('Y')));
               
              }
              
               $search_options['select']=["cart.deal_id", "cart.id", "cart.merchant_id", "sum(cart.merchant_total_payment) as merchant_total_payment", "sum(cart.merchant_amout_excluding_gst) as merchant_amout_excluding_gst", "sum(cart.merchant_gst) as merchant_gst",
                        "sum(cart.site_total_payment) as site_total_payment", "sum(cart.site_amount_excluding_gst) as site_amount_excluding_gst", "sum(cart.site_gst) as site_gst",
                        "sum(cart.net_price) as net_price", "sum(cart.deal_amout_excluding_gst) as deal_amout_excluding_gst", "sum(cart.deal_gst) as deal_gst"];
          $search_options['andWhere']=[
            ['is','cart.transaction_log_id', null],
            ['coupon_code.status' => \common\components\GenxBaseModel::STATUS_REDEEMED],
            ['<', 'coupon_code.updated_at', $dateslab]
          ];
          $search_options['groupby']=['cart.merchant_id'];
          $search_options['all']=1;
          
         $cart = self::accountSum($search_options);
         
         
          if (is_array($cart) and sizeof($cart)) {

                if ($transactionPaymentModel->save()) {
                    
                    $transaction_payment_id = $transactionPaymentModel->id;
                } else {
                    self::DevErrors($transactionPaymentModel->getErrors());
                    return $this->redirect(['/account/']);
                }
                foreach ($cart as $key => $values) {

                    $transactionModel = new TransactionLog();
                    if ($transaction_payment_id) {

                        $transactionModel->merchant_id = $values->merchant_id;
                        $transactionModel->transaction_payment_id = $transaction_payment_id;
                        $transactionModel->total_merchant_amount = $values->merchant_total_payment;
                        $transactionModel->total_merchant_amout_excluding_gst = $values->merchant_amout_excluding_gst;
                        $transactionModel->total_merchant_gst = $values->merchant_gst;
                        $transactionModel->total_site_amount = $values->site_total_payment;
                        $transactionModel->total_site_amount_excluding_gst = $values->site_amount_excluding_gst;
                        $transactionModel->total_site_gst = $values->site_gst;
                        $transactionModel->total_amount = $values->net_price;
                        $transactionModel->total_amount_excluding_gst = $values->deal_amout_excluding_gst;
                        $transactionModel->total_amount_gst = $values->deal_gst;
                        if ($transactionModel->save()) {
                            
                       $sql="update `cart` LEFT JOIN `deal` ON `cart`.`deal_id` = `deal`.`id` LEFT JOIN `coupon_code` ON `cart`.`id` = `coupon_code`.`cart_id` set cart.transaction_payment_id = $transaction_payment_id, cart.transaction_log_id = $transactionModel->id, cart.transaction_log_time_created = ".time()."   WHERE (`cart`.`transaction_log_id` IS NULL) AND (`cart`.`merchant_id`=".$values->merchant_id.") and   (`coupon_code`.`status`=".\common\components\GenxBaseModel::STATUS_REDEEMED.") AND (`coupon_code`.`updated_at` < ".$dateslab.") AND (`deal`.`status` != 0) AND (`coupon_code`.`status` != 0)";     
                       $command = \Yii::$app->db->createCommand($sql);
                       $command->execute();  
                       
                            /*$condition=['and',
                                ['is','cart.transaction_log_id', null],
                                ['cart.merchant_id' => $values->merchant_id],
                                ['coupon_code.status' => \common\components\GenxBaseModel::STATUS_REDEEMED],
                                ['<', 'coupon_code.updated_at', time()]
                                ];
                                
                        
                            $cartModel->updateAll(['transaction_payment_id' => $transaction_payment_id, "transaction_log_id" => $transactionModel->id, "transaction_log_time_created" => time()], $condition);
                             * 
                             */
                        } else {
                            self::DevErrors($transactionModel->getErrors());
                        }
                    }
                }

                self::DevErrors($cartModel->getErrors());
                self::DevErrors($transactionPaymentModel->getErrors());
                return $this->redirect(\yii\helpers\Url::toRoute("view"));
            } else {
                $this->sendErrors(['carterror' => ['No Redords Available.']]);
                return $this->redirect(\yii\helpers\Url::toRoute("index"));
            }
        }
               
        
    }

}
