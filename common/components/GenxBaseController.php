<?php

namespace common\components;

use Yii;
use yii\web\Controller;

/**
 * Description of TNTBaseController
 *
 */
class GenxBaseController extends Controller
{

    STATIC $_CACHE = [];

    public function beforeAction($action)
    {
        return parent::beforeAction($action);
    }

    public function sendErrors($errors)
    {
        if (is_array($errors) && sizeof($errors)) {
            foreach ($errors as $key => $error) {
                if (is_array($error) && sizeof($error)) {
                    foreach ($error as $_err)
                        Yii::$app->getSession()->addFlash('error', $_err);
                }
            }
        }
    }

    public function sendSuccess($success_messages)
    {
        if (is_array($success_messages) && sizeof($success_messages)) {
            foreach ($success_messages as $key => $success) {
                Yii::$app->getSession()->addFlash('success', $success);
            }
        }
    }

    protected function _save($args = array())
    {
        if ($args['id']) {
            $model = $this->findModel($args['id']);
        } else {
            $model = new $args['modelClass']();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
        } else {
            $this->sendErrors($model->getErrors());
        }

        $default_vars = [
            'model' => $model,
        ];

        $view_vars = array_merge($default_vars, $args['view_vars']);
        return $this->render((($args['id']) ? 'update' : 'create'), $view_vars);
    }

    public function c($array)
    {
        return GenXHelper::c($array);
    }

}
