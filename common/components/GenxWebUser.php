<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

use common\models\User;

/**
 * Description of GenxWebUser
 *
 * @author Genx
 */
class GenxWebUser extends \yii\web\User
{

    /**
     * 
     * @return User
     */
    public function merchant()
    {
        if ($this->isOnlyMerchant()) {
            return $this->identity;
        }

        return Null;
    }

    public function getLoggedinID()
    {
        return $this->identity->id;
    }

    public function getName()
    {
        return $this->identity->name;
    }

    public function isAdmin()
    {
        return \Yii::$app->user->can(User::ROLE_ADMIN);
    }

    public function isMerchant()
    {
        return \Yii::$app->user->can(User::ROLE_MERCHANT);
    }

    public function isSalesMember()
    {
        return (\Yii::$app->user->can(User::ROLE_SM) || \Yii::$app->user->can(User::ROLE_SR));
    }

    public function isCustomerServiceMember()
    {
        return (\Yii::$app->user->can(User::ROLE_CSM) || \Yii::$app->user->can(User::ROLE_CSR));
    }

    public function isOnlyMerchant()
    {
        return ($this->isMerchant() && !$this->isManager());
    }

    public function isManager()
    {
        return \Yii::$app->user->can(User::ROLE_MANAGER);
    }

    public function canAccessAdmin()
    {
        return (
                $this->isMerchant() ||
                $this->isSalesMember() ||
                $this->isCustomerServiceMember()
                );
    }

    public function onlyTeam()
    {
        return (
                $this->isManager() ||
                $this->isSalesMember() ||
                $this->isCustomerServiceMember()
                );
    }

    public function hasPurchased($deal_id, $deal_option_id)
    {
        return ($this->identity->hasPurchased($deal_id, $deal_option_id));
    }

}
