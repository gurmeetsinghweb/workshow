<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

/**
 * Description of GenXParamAuth
 *
 * @author Acer
 */
class GenXBearerAuth extends \yii\filters\auth\HttpBearerAuth
{

    public function authenticate($user, $request, $response)
    {
        // lets clear all the options request
        if (\Yii::$app->getRequest()->getMethod() === 'OPTIONS') {
            return True;
        }

        return parent::authenticate($user, $request, $response);
    }

}
