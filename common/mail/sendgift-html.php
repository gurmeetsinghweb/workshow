<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
?>


<div style="padding:0px;margin:0px;background:whitesmoke">


    <div class="" style="height:100%!important;width:100%!important;margin:0;padding:0">


        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="" style="border-collapse:collapse!important;height:100%!important;width:100%!important;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;min-height:100%;color:#444444;background:#eeeeee;margin:0;padding:0" bgcolor="#eeeeee">
            <tbody><tr>
                    <td align="center" valign="top">
                        <table align="center" border="0" cellspacing="0" width="900" id="" style="width:900px!important;min-width:900px!important;border-collapse:collapse!important">
                            <tbody>
                                <tr>
                                    <td align="center" valign="top">

                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="" style="border-collapse:collapse!important;margin:15px 0 10px">
                                            <tbody><tr>
                                                    <td>


                                                    </td>
                                                </tr>
                                            </tbody></table>

                                    </td>
                                </tr>





                                <tr>
                                    <td align="center" valign="top">
                                        <table class="m_-1311167767762592162sectionWrap" border="0" cellpadding="0" cellspacing="0" width="1000" style="overflow:hidden;text-align:center;" id="m_-1311167767762592162content" >
                                            <tbody>
                                                <tr>
                                                    <td id="m_-1311167767762592162header" valign="top" bgcolor="#01aef0" style="max-width:100%!important;background:#eeeeee; padding:0px 60px 0 60px">

                                            <center><img src="<?=   \yii\helpers\Url::toRoute(["themes/main/images/logo.png"],true) ?>" width="350" style="margin-top:0px;height:auto;line-height:100%;outline:none;text-decoration:none;max-width:100%;border:0; margin-bottom:40px;"  tabindex="0"></center>




                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </td>
                </tr>

                <tr style="background:#fff ;box-shadow: 7px 13px 5px rgba(128, 128, 128, 0.87); position:relative;">
                    <td style="width:100%;padding: 15px 20px 3px 20px;border: double 6px #d29a10;border-bottom: 0px !important;font-size:18px;">
                        
                          
                        <table width="100%" style="width:100%;display:inline-block; line-height:1.4;">
                                
                                <tr>
                                    <td>
                                        <h1 style="font-family:Helvetica;font-size:20px;font-style:normal;font-weight:normal;text-align:center;margin:0px auto 0px;text-align:left">
                                            <font color="#4d4d4d; font-weight:bold;">
                                            <p style="width:100%; text-align:center; font-size:25px; font-weight:bold;">Gift Voucher</p>
                                            <p>Hi <strong><?= $gift->gift_name; ?></strong></p>
                                            <p>You have received a gift voucher from <strong><?= $gift->gift_from; ?></strong> with the following message:</p>
                                            <p>
                                                <strong>Message : </strong> <?= $gift->gift_message ?> 
                                            </p>
                                            <p>The Gift Voucher is for <strong><?= $gift->cart->merchant->company->title; ?></strong> for <strong><?= $gift->cart->deal->title ?></strong> . Please note that <strong><?= $gift->cart->merchant->company->title; ?></strong> has fine prints for this Gift Voucher and to have the best experience, please read through to avail the voucher accordingly.</p>
                                            <p>In case you have any questions, please feel free to contact Classifr <a href="<?=   \yii\helpers\Url::toRoute(["/contact"],true) ?>"> CONTACT US </a>
                                            </p>
                                            <p>Enjoy!</p>
                                            <p>Classifr Team</p>
                                            
                                            <span style="color:#4d4d4d"></span> 
                                            </font></h1>
                                    </td>
                                    <!--  <td >
                       
                                    <img src="gift.png" alt="gift" style="width:50%;">
    
                                     </td> -->
                                </tr>
                          
                      </table>
                    </td>
                        
                </tr>

                <tr style="background: #fff; box-shadow: 7px 13px 5px rgba(128, 128, 128, 0.87); position: relative; border: double 6px #d29a10; border-top: 0px; width: 100%; text-align: center;">
                    <td style="background:#fff; ">

                        <img src="<?=   \yii\helpers\Url::toRoute(["themes/main/images/ribbon.jpg"],true) ?>" alt="gift" class="border-img" style=" margin-bottom: 30px;
                             margin-top: 30px;" >
                    </td>
                </tr>

               <tr>
                <td><table style="color: #fff;" width="100%">
                        <tr>
                            <td><p style=" text-align: center; color: #888; font-size: 12px;margin: 15px 0px 5px 0px;color: green;">Address</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">24 Moon Avenue, Majitha Road
                                </p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">Amritsar</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom:0px; margin-top:0px;">PB 143001, India</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">+91-799910-0015</p>
                                <p style="paddcing-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">contact@classifr.com</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">&nbsp;</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;"><a href="<?= yii\helpers\Url::toRoute(["site/vcf"], true); ?>" style="color: #696969; text-decoration:underline">Add us to your address book</a></p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;">&nbsp;</p>
                                <!-- <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;"><span style="color: #888; text-align: center; margin: 10px auto;font-size: 12px;">Want to change how you recieve these mails ? </span></p> -->
                                <!-- <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;">You can <a href="javasrcipt:;" style="color: #696969; text-decoration:underline">update your preferences</a> or <a href="javasrcipt:;" style="color: #696969; text-decoration:underline">unsubscribe from this list</a></p> -->
                                <p style="color: #888; text-align: center; margin: 10px auto;font-size: 12px;">&nbsp;</p>
                            </td>
                        </tr>
                    </table></td>
            </tr>

            </tbody>
        </table>
        </td>
        </tr>
        </tbody>
        </table><div class="yj6qo"></div><div class="adL">
            &nbsp;


        </div></div><div class="adL">                       
    </div></div>
