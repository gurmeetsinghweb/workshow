<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
?>


<div class="rcmBody" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; height: 100% !important; width: 100% !important;  margin: 0; padding: 0">


    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse !important; height: 100% !important; width: 100% !important; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; min-height: 100%; color: #444444; background: #eeeeee; margin: 0; padding: 0" bgcolor="#eeeeee">
        <tr>
            <td align="center" valign="top" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt">
                
                <table align="center" border="0" cellspacing="0" width="900" id="emailContainer" style="width: 900px !important; min-width: 900px !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse !important">
                    <tr>
                        <td width="52%" align="center" valign="top" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt"><!-- node type 8 -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreheader" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse !important; margin: 15px 0 10px">
                                <tbody>
                                    <tr>
                                        <td style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- node type 8 --> </td>
                    </tr>
                    <!-- node type 8 -->
                    <tr>
                        <td colspan="2" align="center" valign="top" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt"><table class="sectionWrap" border="0" cellpadding="0" cellspacing="0" width="1000" style="overflow: hidden; text-align: center; background: #FFFFFF" id="content" bgcolor="#FFFFFF">
                                <tbody>
                                    <tr>
                                        <td id="header" valign="top" bgcolor="#01aef0" style="max-width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #FFFFFF; padding: 20px 60px 0 60px"><center>
                                    <img src="<?= yii\helpers\Url::toRoute(["themes/main/images/logo.png"], true); ?>" width="350" style="margin-top: 0px; -ms-interpolation-mode: bicubic; height: auto; line-height: 100%; outline: none; text-decoration: none; max-width: 100%; border: 0">
                                </center></td>
                    </tr>
                    </tbody>
                </table></td>
        </tr>
        <tr style="background:#fff;">
            <td style="padding-left:33px;">
                <table width="100%"><tr>
                        <td>

                            <table style="width: 69%;padding-left:30px;display: Inline-block;">
                                <tbody>
                                    <tr>
                                        <td><h1 style="font-family: Helvetica; font-size: 20px; font-style: normal; font-weight:normal; text-align: center; margin: 0px auto 0px; text-align:left"><font color="#4d4d4d">Classifr</font></h1></td>
                                    </tr>
                                    <tr>
                                        <td><h1 style="font-family: Helvetica; font-size: 20px; font-style: normal; font-weight: normal; text-align: center; margin: 7px auto 7px; text-align:left"><font color="#4d4d4d"24 Moon Avenue, Amritsar </font></h1></td>
                                    </tr>
                                    <tr>
                                        <td height="27"><h1 style="font-family: Helvetica; font-size: 20px; font-style: normal; font-weight:normal; text-align: center; margin: 0px auto 0px; text-align:left"><font color="green"><a href="#" style="color:green; text-decoration:none">Classifr.com</a></font></h1></td>
                                    
                                    </tr>
                                    <tr>
                                          <td height="27"><h1 style="font-family: Helvetica; font-size: 20px; font-style: normal; font-weight:normal; text-align: center; margin: 0px auto 0px; text-align:left"><font color="green"><a href="#" style="color:green; text-decoration:none">This Service/Product Deal, you can talk directly to Merchant</a></font></h1></td>
                                    </tr>
                                    
                                </tbody>
                            </table>

                        </td>
                        <td>
                            <table width="78%" height="90" align="center" style="display:inline-block;">
                                <tbody>
                                    <tr>
                                        <td ><h1 style="font-family: Helvetica; font-size: 20px; font-style: normal; font-weight:normal; text-align: center; margin: 0px auto 0px; text-align:left"><font color="#4d4d4d; font-weight:bold;"><span class="" > Enquiry  No: </span> </font></h1></td>
                                        <td ><h1 style="font-family: Helvetica; font-size: 20px; font-style: normal; font-weight:normal; text-align: center; margin: 0px auto 0px; text-align:left"><font color="#4d4d4d; font-weight:bold;"><span class="" > <?= $order->id; ?></span> </font></h1></td>
                                    </tr>
                                    <tr>
                                        <td><h1 style="font-family: Helvetica; font-size: 20px; font-style: normal; font-weight:normal; text-align: center; margin: 0px auto 0px; text-align:left"><font color="rgb(77, 77, 77); font-weight:bold;"><span class="" style="color:#4d4d4d"> Date: </span> </font></h1></td>
                                        <td><h1 style="font-family: Helvetica; font-size: 20px; font-style: normal; font-weight:normal; text-align: center; margin: 0px auto 0px; text-align:left"><font color="rgb(77, 77, 77); font-weight:bold;"><span class="" style="color:#4d4d4d" > <?= date("d F Y", $order->created_at); ?></span> </font></h1></td>
                                    </tr>
                                    <tr>
                                        <td><h1 style="font-family: Helvetica; font-size: 20px; font-style: normal; font-weight:normal; text-align: center; margin: 0px auto 0px; text-align:left"><font color="#rgb(77, 77, 77); font-weight:bold;"><span class="" style="color:#4d4d4d" > To: </span> </font></h1></td>
                                        <td><h1 style="font-family: Helvetica; font-size: 20px; font-style: normal; font-weight:normal; text-align: center; margin: 0px auto 0px; text-align:left"><font color="#4d4d4d; font-weight:bold;"><span class="" style="color:#4d4d4d" > <?= $order->user->name; ?></span> </font></h1></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>

                    </tr></table>

            </td>
            <!--third td-->
            <!--row closed-->
        </tr>
        <tr style="background:#fff;">
            <td style="width:100%; box-sizing:border-box"; background:#fff;><table style="border-collapse:collapse; margin: 30px 30px; text-align:Center;" width="97%">
                    <tbody> 
                        <tr style="line-height: 3.4; background-color: #ecebeb">
                            <th style=" width: 44%;
                                padding-right: 20px; border:1px solid #888;">Product</th>
                          
                            <th style="border:1px solid #888; width:17%;">Service/Product Amount </th>
                        </tr>
                        <?php
                        $excgst_price = 0;
                        $gst_price = 0;
                        $net_price = 0;
                        $shipping = 0;
                        foreach ($order->cart as $key => $value) {

                            if ($value->shipping_amount) {
                                $shipping += $value->shipping_amount;
                            }
                            ?>
                            <tr style="">
                                <td style="padding: 10px 11px; line-height:1.4; border:1px solid #888;"><?= $value->deal->title; ?>
                                    <br>
                                    <?= $value->dealOption->name; ?>
                                    <br> </td>
                                <td style="border:1px solid #888; font-weight:bold;"> Rs
                                    <?= $value->net_price; ?> </td>
                            </tr>
                            <?php
                            $net_price+=$value->net_price;
                        }
                        ?>
                        <?php
                        if ($order->discountcoupon_id) {
                            $disamount = $order->discount_amount;
                            $net_price = $net_price - $disamount;
                            ?>
                            <tr style="">
                                <td></td>
                                <td></td>
                                <td colspan="3" style="padding: 14px 11px;font-weight: bold; padding-right: 100px; text-align:right; font-size:18px;"><?= $order->discountCoupon->discount_title; ?> </td>
                                <td style="border:1px solid #888; font-weight:bold;"> - Rs
    <?= $order->discount_amount; ?> </td>
                            </tr>
                                    <?php
                                }
                                ?>
                        <?php
                        if ($shipping) {
                            $net_price = $net_price + $shipping;
                            ?>
                            <tr style="">
                                <td></td>
                                <td></td>
                                <td colspan="3" style="padding: 14px 11px;font-weight: bold; padding-right: 100px; text-align:right; font-size:18px;">Shipping Charges* </td>
                                <td style="border:1px solid #888; font-weight:bold;">  Rs
    <?= $shipping; ?> </td>
                            </tr>
                                    <?php
                                }
                                ?>
                        <tr style="">
                          
                            <td  style="padding: 13px 11px;font-weight: bold; font-size:20px; padding-right: 20px; text-align:right; font-size:20px;"> TOTAL </td>
                            <td style="border:1px solid #888; font-weight:bold;"> Rs
<?= $net_price; ?> </td>
                        </tr>
                </table>
            </td>
        </tr>
        <!-- node type 8 -->
        <tr>
                <td><table style="color: #fff;" width="100%">
                        <tr>
                            <td><p style=" text-align: center; color: #888; font-size: 12px;margin: 15px 0px 5px 0px;color: green;">Address</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">24 Moon Avenue, Majitha Road
                                </p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">Amritsar</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom:0px; margin-top:0px;">PB 143001, India</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">+91-799910-0015</p>
                                <p style="paddcing-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">contact@classifr.com</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">&nbsp;</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;"><a href="<?= yii\helpers\Url::toRoute(["site/vcf"], true); ?>" style="color: #696969; text-decoration:underline">Add us to your address book</a></p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;">&nbsp;</p>
                                <!-- <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;"><span style="color: #888; text-align: center; margin: 10px auto;font-size: 12px;">Want to change how you recieve these mails ? </span></p> -->
                                <!-- <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;">You can <a href="javasrcipt:;" style="color: #696969; text-decoration:underline">update your preferences</a> or <a href="javasrcipt:;" style="color: #696969; text-decoration:underline">unsubscribe from this list</a></p> -->
                                <p style="color: #888; text-align: center; margin: 10px auto;font-size: 12px;">&nbsp;</p>
                            </td>
                        </tr>
                    </table></td>
            </tr>
    </table></td>
</tr>

</table>



</div> 

