<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
?>


<div class="rcmBody" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; height: 100% !important; width: 100% !important;  margin: 0; padding: 0">


    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse !important; height: 100% !important; width: 100% !important; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; min-height: 100%; color: #444444; background: #eeeeee; margin: 0; padding: 0" bgcolor="#eeeeee">
        <tbody><tr>
                <td align="center" valign="top" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt">
                    <table align="center" border="0" cellspacing="0" width="900" id="emailContainer" style="width: 900px !important; min-width: 900px !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse !important">
                        <tbody>
                            <tr>
                                <td align="center" valign="top" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt">
                                    <!-- node type 8 -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreheader" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse !important; margin: 15px 0 10px">
                                        <tbody><tr>
                                                <td style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- node type 8 -->
                                </td>
                            </tr>


                            <!-- node type 8 -->


                            <tr>
                                <td align="center" valign="top" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt">
                                    <table class="sectionWrap" border="0" cellpadding="0" cellspacing="0" width="1000" style="overflow: hidden;  text-align: center; background: #FFFFFF"  id="content" bgcolor="#FFFFFF">
                                        <tbody>
                                            <tr>
                                                <td id="header" valign="top" bgcolor="#01aef0" style="max-width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #FFFFFF; padding: 20px 60px 0 60px">

                                        <center><img src="<?= yii\helpers\Url::toRoute(["../themes/main/images/logo.png"],true); ?>" width="350"  style="margin-top: 0px; -ms-interpolation-mode: bicubic; height: auto; line-height: 100%; outline: none; text-decoration: none; max-width: 100%; border: 0"></center>

                                     

                                </td>
                            </tr>

                        </tbody>
                    </table>
                </td>
            </tr>

            <tr style="background:#fff;">
                <td style="width:100%; padding: 40px 7px 10px 43px;">
                         

                                              <p style="width:100%; text-align:center; font-size:25px; font-weight:bold;">Redeemed Voucher</p>
                                            <p>Hi <strong><?= $cart->user->name ?></strong></p>
                                            <p>Congratulations! your voucher has redeemed successfully</p>
                                            <p>
                                               <strong><?= $cart->deal->title ?></strong><br>
                                               <strong><?= $cart->dealOption->name ?></strong><br>
                                               
                                            </p>
                                            <p>We hope you had a great experience.</p>
                                            
                                            <p>In case you have any questions, please contact your account manager or contact VoucherStore.</p>
                                            <p>Wating for your valuable Feedback <a href="<?= common\components\GenXHelper::encryptUserLink($cart->user_id,  \yii\helpers\Url::toRoute(["/user/rating","cart_id"=>$cart->id],TRUE),"/user/encrypted-link" ) ?>"> Click Here</a>
                                            </p>
                                            <p>Best Wishes</p>
                                            <p>Classifr Team</p>
                                            <p>&nbsp;</p>
                                            <span style="color:#4d4d4d"></span> 

                                          
                                

                </td>

            </tr>

            <!-- node type 8 -->
           <tr>
                <td><table style="color: #fff;" width="100%">
                        <tr>
                            <td><p style=" text-align: center; color: #888; font-size: 12px;margin: 15px 0px 5px 0px;color: green;">Address</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">24 Moon Avenue, Majitha Road
                                </p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">Amritsar</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom:0px; margin-top:0px;">PB 143001, India</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">+91-799910-0015</p>
                                <p style="paddcing-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">contact@classifr.com</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">&nbsp;</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;"><a href="<?= yii\helpers\Url::toRoute(["site/vcf"], true); ?>" style="color: #696969; text-decoration:underline">Add us to your address book</a></p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;">&nbsp;</p>
                                <!-- <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;"><span style="color: #888; text-align: center; margin: 10px auto;font-size: 12px;">Want to change how you recieve these mails ? </span></p> -->
                                <!-- <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;">You can <a href="javasrcipt:;" style="color: #696969; text-decoration:underline">update your preferences</a> or <a href="javasrcipt:;" style="color: #696969; text-decoration:underline">unsubscribe from this list</a></p> -->
                                <p style="color: #888; text-align: center; margin: 10px auto;font-size: 12px;">&nbsp;</p>
                            </td>
                        </tr>
                    </table></td>
            </tr>

        </tbody>
    </table>
</td>
</tr>
</tbody>
</table>
&nbsp;


</div>              




