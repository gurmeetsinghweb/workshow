<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
?>




<table cellpadding="0" cellspacing="0" width="100%" align="center" style="font-size: 12px; font-style: normal; font-variant-caps: normal;
		letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; 
		word-spacing: 0px; -webkit-text-stroke-width: 0px; border: 0px none; border-spacing: 0px; font-family: Helvetica, Arial, sans-serif; 
		font-weight: 100; width: 800px; " class="">
		<tr class="">
			<td class="" style="width: 100%;
    text-align: Center;">
                            <img src="<?= yii\helpers\Url::toRoute(["themes/main/images/logo.png"],true); ?>">
			</td>
		</tr>
		<tr>
			<td>
				<div class="account-activation" style="background:#6bbf6b; border-bottom: 3px solid #000;">
					<div class="container" style="width:1000px;  margin:0 auto; ">
						<p style="margin: 0; color: #fff;  font-weight: bold; font-size: 20px; padding: 46px 0px; text-align: center; 
							text-transform: uppercase;">
							<?= Html::encode($user->name);?> Activate Successfully Register
						</p>
				
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<table style="width: 100%; background: #fff;">
					<tr>
						<td>
							<div class="account-text">
								<div class="container" style="width:1000px;  margin:0 auto; text-align: Center; padding-top:0px;">
									<p style="font-size: 20px; width: 60%; margin: 25px auto;">
                                                                            
                                                  UserId : <?= $user->email; ?><br>
                                                                            Password: <?= $password; ?><br>
                                                                    
                                                    </p>
									

                                                    <p style=" font-size: 18px;">If you need help or have any questions, please <a href="<?= yii\helpers\Url::toRoute(['contact'],true) ?>" style="color:green;"> Contact Us</a></p>
								</div>
							</div>
								
							
							
						</td>
					</tr>
					
				</table>
			</td>
		</tr>
		<tr style="width:100%;">	
			<td style="width:100%;">
				<table style="background: whitesmoke;
   				 color: #fff; width:100%;">
				 <tr style="text-align:center">
						<td>
								<div class="container" style="margin:0 auto; padding-top:0px;"></div>
								
						 
					
						</td>
					</tr>
					 <tr>
                <td><table style="color: #fff;" width="100%">
                        <tr>
                            <td><p style=" text-align: center; color: #888; font-size: 12px;margin: 15px 0px 5px 0px;color: green;">Address</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">24 Moon Avenue, Majitha Road
                                </p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">Amritsar</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom:0px; margin-top:0px;">PB 143001, India</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">+91-799910-0015</p>
                                <p style="paddcing-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">contact@classifr.com</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">&nbsp;</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;"><a href="<?= yii\helpers\Url::toRoute(["site/vcf"], true); ?>" style="color: #696969; text-decoration:underline">Add us to your address book</a></p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;">&nbsp;</p>
                                <!-- <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;"><span style="color: #888; text-align: center; margin: 10px auto;font-size: 12px;">Want to change how you recieve these mails ? </span></p> -->
                                <!-- <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;">You can <a href="javasrcipt:;" style="color: #696969; text-decoration:underline">update your preferences</a> or <a href="javasrcipt:;" style="color: #696969; text-decoration:underline">unsubscribe from this list</a></p> -->
                                <p style="color: #888; text-align: center; margin: 10px auto;font-size: 12px;">&nbsp;</p>
                            </td>
                        </tr>
                    </table></td>
            </tr>

					
				</table>
			</td>
		</tr>
	</table>
