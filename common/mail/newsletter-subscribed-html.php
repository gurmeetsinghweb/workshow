<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
?>
<style>
    @media only screen and (max-device-width: 601px) {
.content {width: 250px !important;}
.col-1 {width: 100%!important;float: left !important;}
.col-custom {width: 100%!important;    margin: 0px !important;padding: 19px !important;float: left !important;}
.title-text{height:110px !important;}
.see-deal{padding: 10px !important;}
}
</style>
    <div style="padding:0px;margin:0px;background:whitesmoke">

        <div style="height:100%!important;width:100%!important;margin:0;padding:0">

            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse!important;height:100%!important;width:100%!important;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;min-height:100%;color:#444444;background:#eeeeee;margin:0;padding:0" bgcolor="#eeeeee">
                <tbody>
                    <tr>
                        <td align="center" valign="top">
                            <table class="content" align="center" border="0" cellspacing="0" width="600" style="border-collapse:collapse!important">
                                <tbody style="background:white;">
                                   

                                    <tr>
                                        <td align="center" valign="top">
                                            <table  border="0" cellpadding="0" cellspacing="0" style="overflow:hidden;text-align:center;background:#ffffff" bgcolor="#FFFFFF">
                                                <tbody>
                                                    <tr>
                                                        <td valign="top" bgcolor="#01aef0" style="max-width:100%!important;background:#ffffff;padding:20px 60px 16px 60px">

                                                            <center>
                                                                <img src="<?=   \yii\helpers\Url::toRoute(["../themes/main/images/logo.png"],true) ?>" width="350" style="margin-top:0px;height:auto;line-height:100%;outline:none;text-decoration:none;max-width:100%;border:0" class="CToWUd a6T" tabindex="0">
                                                                <div class="a6S" dir="ltr" style="opacity: 0.01; left: 691.5px; top: 139px;">
                                                                    <div id=":gn" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download">
                                                                        <div class="aSK J-J5-Ji aYr">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </center>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                <!--<tr style="background:#fff;">
                    <td style="width:100%;padding:0px 7px 10px 43px">

                        <h1 style="font-family:Helvetica;font-size: 22px;font-style:normal;font-weight:normal;text-align:center;margin: 11px auto 12px;text-align:left;"><font color="#77c84e; font-weight:bold;"><span style="color: #77c84e;"> See All Deals: </span> 
                            </font></h1>

                    </td>
                    
                </tr>-->
                <?php
                $i=1;
                foreach($deals as $deal)
                    {
                
                ?>
                
                    <?php if($i==1){ ?>
                                    <tr style="background:white;border:1px solid #f2f2f2;box-shadow: inset 0 0 10px #f3f3f3;display:inline-block;padding:20px;border-right:0px;border-left:0px;">
                   <?php } ?>

                                        <td  class="col-1" style="width:50%;">
                                            <div style="height:380px;">
                                            <table id="Table_01"  class="col-custom" width="276" height="auto" border="0" cellpadding="0" cellspacing="0" style="border:1px #f1f1f1 solid;margin:10px;padding:12px;height:400px;">
                                                <tbody>

                                                    <tr>

                                                        <td colspan="2" >
                                                            <a href="<?= \yii\helpers\Url::toRoute(["../deal/".$deal->slug],TRUE); ?>"><img src="<?= $deal->dealGalleries[0]->getIconURL(); ?>" width="276" style="width: 276px;height: 200px;" alt="banner"></a></td>

                                                    </tr>
                                                  
                                                  <!--  <tr>
                                                        <td colspan="2" height="25" style="font-size: 20px;
    font-weight: bold;
    color: #77c84e;padding:12px 0px 8px 2px;">&nbsp;</td>
                                                       
                                                    </tr>-->
                                                    
                                                   <tr >
                                                        <td colspan="2" width="259" height="31"  style="    
    font-size: 15px;
    color: #0a0a0a;padding:12px 0px 16px 3px;text-align:justify;width:259px;max-width:259px;">
                   <div class="title-text" style="height:100px;overflow: hidden;">
                       <a style="text-decoration:none;" href="<?= \yii\helpers\Url::toRoute(["../deal/".$deal->slug],TRUE); ?>"><?= $deal->title; ?></a></div>
                                                        </td>
                                                       
                                                    </tr>
                                                   
                                                    <tr>
                                                        <td style="font-size: 14px;
                                                            color: #77c84e;padding-left:8px;font-weight:bold;" >
                                                            <small style="font-size:small;font-weight:normal;">From</small> <?= common\components\GenXHelper::displayAmount($deal->minSellingPrice()) ?>
                                                        </td>

                                                  
                                                        <td>
                                                            <a class="see-deal" style="color: white;
background-color: #77c84e;
text-decoration: none;
float: right;
padding: 10px;
margin: 0px 8px 12px 5px;
border-radius: 6px;
font-weight: bold;
font-size: 15px;
" href="<?= \yii\helpers\Url::toRoute(["../deal/".$deal->slug],TRUE); ?>">See Deal</a>
                                                        </td>

                                                    </tr>

                                                </tbody>
                                            </table>

                                            </div>
                                        </td>
   

                                   <?php if($i==2) { ?> </tr><?php $i=0; } ?>
                                    <?php
                                    
                                    
                                    $i++;
                    }
                    $eml=base64_encode($email);
                    ?>
                                    
                                    <!--deal address-->

                                     <tr>
                <td><table style="color: #fff;" width="100%">
                        <tr>
                            <td><p style=" text-align: center; color: #888; font-size: 12px;margin: 15px 0px 5px 0px;color: green;">Address</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">24 Moon Avenue, Majitha Road
                                </p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">Amritsar</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom:0px; margin-top:0px;">PB 143001, India</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">+91-799910-0015</p>
                                <p style="paddcing-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">contact@classifr.com</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">&nbsp;</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;"><a href="<?= yii\helpers\Url::toRoute(["site/vcf"], true); ?>" style="color: #696969; text-decoration:underline">Add us to your address book</a></p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;">&nbsp;</p>
                                <!-- <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;"><span style="color: #888; text-align: center; margin: 10px auto;font-size: 12px;">Want to change how you recieve these mails ? </span></p> -->
                                <!-- <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;">You can <a href="javasrcipt:;" style="color: #696969; text-decoration:underline">update your preferences</a> or <a href="javasrcipt:;" style="color: #696969; text-decoration:underline">unsubscribe from this list</a></p> -->
                                <p style="color: #888; text-align: center; margin: 10px auto;font-size: 12px;">&nbsp;</p>
                            </td>
                        </tr>
                    </table></td>
            </tr>
                            </table>
            </table>
                            </div>
    </div>