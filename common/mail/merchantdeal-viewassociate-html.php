<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
?>


<div style="padding:0px;margin:0px;background:whitesmoke">


    <div style="height:100%!important;width:100%!important;margin:0;padding:0">


        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"  style="border-collapse:collapse!important;height:100%!important;width:100%!important;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;min-height:100%;color:#444444;background:#eeeeee;margin:0;padding:0" bgcolor="#eeeeee">
            <tbody><tr>
                    <td align="center" valign="top">
                        <table align="center" border="0" cellspacing="0" width="900"  style="width:900px!important;min-width:900px!important;border-collapse:collapse!important">
                            <tbody>
                                <tr>
                                    <td align="center" valign="top">

                                        <table border="0" cellpadding="0" cellspacing="0" width="100%"  style="border-collapse:collapse!important;margin:15px 0 10px">
                                            <tbody><tr>
                                                    <td>


                                                    </td>
                                                </tr>
                                            </tbody></table>

                                    </td>
                                </tr>





                                <tr>
                                    <td align="center" valign="top">
                                        <table  border="0" cellpadding="0" cellspacing="0" width="1000" style="overflow:hidden;text-align:center;background:#ffffff"  bgcolor="#FFFFFF">
                                            <tbody>
                                                <tr>
                                                    <td  valign="top" bgcolor="#01aef0" style="max-width:100%!important;background:#ffffff;padding:20px 60px 0 60px">

                                            <center>
                                                <img src="<?=   \yii\helpers\Url::toRoute(["../themes/main/images/logo.png"],true) ?>" width="350" style="margin-top:0px;height:auto;line-height:100%;outline:none;text-decoration:none;max-width:100%;border:0" class="CToWUd a6T" alt="Classifr Logo" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 691.5px; top: 139px;"><div id=":gn" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V" data-tooltip="Download"><div class="aSK J-J5-Ji aYr"></div></div></div></center>




                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </td>
                </tr>

                <tr style="background:#fff">
                    <td style="width:100%;padding:40px 7px 10px 43px">

                        <table style="width:20%;display:inline-block">
                            <tbody>
                                <tr>
                                    <td>
                                        <h1 style="font-family:Helvetica;font-size:20px;font-style:normal;font-weight:normal;text-align:center;margin:0px auto 0px;text-align:left"><span style="color:green;"> Category: </span> 
                                        </h1>
                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <h1 style="font-family:Helvetica;font-size:20px;font-style:normal;font-weight:normal;text-align:center;margin:0px auto 0px;text-align:left"><font color="rgb(77, 77, 77); font-weight:bold;"><span style="color:#4d4d4d">Deal Start: </span> 
                                            </font></h1>
                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <h1 style="font-family:Helvetica;font-size:20px;font-style:normal;font-weight:normal;text-align:center;margin:0px auto 0px;text-align:left"><font color="#rgb(77, 77, 77); font-weight:bold;"><span style="color:#4d4d4d"> Deal Expire : </span> 
                                            </font></h1>
                                    </td>
                                </tr>


                            </tbody>
                        </table>
                        <table style="width:78%;display:inline-block">
                            <tbody>
                                <tr>
                                    <td>
                                        <h1 style="font-family:Helvetica;font-size:20px;font-style:normal;font-weight:normal;text-align:center;margin:0px auto 0px;text-align:left"><font color=""><span><?php
                                        if(count($deal->dealCategory))
                                        {
                                            foreach($deal->dealCategory as $categorylist){
                                                $catdisplay=common\models\Category::findOne($categorylist->category_id);
                                                echo $catdisplay->name ." : ";
                                            }
                                        }
                                        
                                        ?>
                                        
       <!--
        <?php // $deal->mainCategory->name; ?> : <?php // ($deal->sub_category_id) ? $deal->subCategory->name : ""; ?> : <?php // ($deal->sub_sub_category_id) ? $deal->subSubCategory->name : ""; ?></span> 
       19/7/2017 gurmeet 
       REMOVE category list and implement new cat system;
       --> 
                                            </font></h1>
                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <h1 style="font-family:Helvetica;font-size:20px;font-style:normal;font-weight:normal;text-align:center;margin:0px auto 0px;text-align:left"><font color="rgb(77, 77, 77); font-weight:bold;"><span style="color:#4d4d4d"> <span class="aBn" data-term="goog_828551732" tabindex="0"><span class="aQJ"><?= \common\components\GenXHelper::getDateFormat($deal->start_time); ?></span></span></span> 
                                            </font></h1>
                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <h1 style="font-family:Helvetica;font-size:20px;font-style:normal;font-weight:normal;text-align:center;margin:0px auto 0px;text-align:left"><font color="#4d4d4d; font-weight:bold;"><span style="color:#4d4d4d"> <?= \common\components\GenXHelper::getDateFormat($deal->expire_time); ?></span> 
                                            </font></h1>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </td>
                </tr>


                <tr style="background:#fff">
                    <td style="width:100%;padding:0px 7px 10px 43px">

                        <table style="">
                            <tbody>
                                <tr>
                                    <td>
                                        <h1 style="font-family:Helvetica;font-size:20px;font-style:normal;font-weight:normal;text-align:center;margin:0px auto 0px;text-align:left"><font color=" font-weight:bold;"><span style="color:green;"> Description: </span> 
                                            </font></h1>
                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <h1 style="font-family:Helvetica;font-size:20px;font-style:normal;font-weight:normal;text-align:center;margin:0px auto 0px;text-align:left"><font color="rgb(77, 77, 77); font-weight:bold;"><span style="color:#4d4d4d"><?= $deal->description; ?></span> 
                                            </font></h1>
                                    </td>

                                </tr>


                            </tbody>
                        </table>

                    </td>
                </tr>
                
                
                <tr style="background:#fff">
                    <td style="width:100%;padding:0px 7px 10px 43px">

                        <table style="">
                            <tbody>
                                <tr>
                                    <td>
                                        <h1 style="font-family:Helvetica;font-size:20px;font-style:normal;font-weight:normal;text-align:center;margin:0px auto 0px;text-align:left"><font color=" font-weight:bold;"><span style="color:green;"> How to redeem: </span> 
                                            </font></h1>
                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <h1 style="font-family:Helvetica;font-size:20px;font-style:normal;font-weight:normal;text-align:center;margin:0px auto 0px;text-align:left"><font color="rgb(77, 77, 77); font-weight:bold;"><span style="color:#4d4d4d"><?= $deal->how_redeemed; ?></span> 
                                            </font></h1>
                                    </td>

                                </tr>


                            </tbody>
                        </table>

                    </td>
                </tr>
                
                

                <tr style="background:#fff">
                    <td style="width:100%;padding:0px 7px 10px 43px">

                        <table style="">
                            <tbody>
                                <tr>
                                    <td>
                                        <h1 style="font-family:Helvetica;font-size:20px;font-style:normal;font-weight:normal;text-align:center;margin:0px auto 0px;text-align:left"><font color="#4d4d4d; font-weight:bold;"><span style="color:green;"> Fine Prints : </span> 
                                            </font></h1>
                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <h1 style="font-family:Helvetica;font-size:20px;font-style:normal;font-weight:normal;text-align:center;margin:0px auto 0px;text-align:left"><font color="rgb(77, 77, 77); font-weight:bold;"><span style="color:#4d4d4d">
                                                <?= $deal->terms; ?>
                                            </span> 
                                            </font></h1>
                                    </td>

                                </tr>


                            </tbody>
                        </table>

                    </td>
                </tr>

                <tr style="background:#fff">
                    <td style="width:100%;padding:12px 7px 7px 43px">

                        <h1 style="font-family:Helvetica;font-size: 22px;font-style:normal;font-weight:normal;text-align:center;margin: 11px auto 12px;text-align:left;"><font color="#4d4d4d; font-weight:bold;" style="color: black;"><span style="color: green;"> Deal Price Option </span> 
                            </font></h1>

                    </td>
                </tr>
                <tr style="background:#fff">
                    <td style="width:100%;box-sizing:border-box">
                        <table style="border-collapse:collapse;margin:0px 30px 30px 30px;text-align:Center" width="94%">
                            <tbody><tr style="line-height:3.4;background-color:#ecebeb">
                                    <th style="width:15%;padding-right:20px;border:1px solid #888">Name</th>
                                    <th style="width:15%; border:1px solid #888">  List Price   </th> 
                                    <th style="width:15%; border:1px solid #888">Selling Price</th>
                                    <th style="width:17%; border:1px solid #888">
                                        Total Discount
                                    </th>
                                    <th style="width:25%; border:1px solid #888">
                                        Number of Persons Allow
                                    </th>
                                    <th style="width:15%; border:1px solid #888;width:17%">
                                        Total Items
                                    </th>

                                </tr>

                                <?php
                                foreach ($deal->dealOptions as $option) {
                                    ?>
                                    <tr>
                                        <td style="padding:10px 11px;line-height:1.4;border:1px solid #888"><?= $option->name; ?></td>
                                        <td style="padding:10px 11px;line-height:1.4;border:1px solid #888"><?= $option->list_price; ?></td>
                                        <td style="padding:10px 11px;line-height:1.4;border:1px solid #888"><?= $option->selling_price; ?></td>
                                        <td style="padding:10px 11px;line-height:1.4;border:1px solid #888"><?= $option->discount . " %"; ?></td>

                                        <td style="padding:10px 11px;line-height:1.4;border:1px solid #888"><?= $option->no_per_person; ?></td>
                                       <!--  <td><?= (int) $option->total_sold; ?></td> -->
                                        <td style="border:1px solid #888;font-weight:bold"><?php $total = (int) $option->total_items;
                                echo ($total) ? $total : "Unlimted";
                                    ?></td>

                                    </tr>
                                    <?php
                                }
                                ?>





                            </tbody>

                        </table>


                    </td>

                </tr>

                <tr style="background:#fff">
                    <td style="width:100%;padding:0px 7px 10px 43px">

                        <h1 style="font-family:Helvetica;font-size: 22px;font-style:normal;font-weight:normal;text-align:center;margin: 11px auto 12px;text-align:left;"><font color="#4d4d4d; font-weight:bold;" style="color: black;"><span style="color: green;"> Deal Image </span> 
                            </font></h1>

                    </td>
                </tr>
                <tr style="background: -webkit-linear-gradient(top, #ffffff 0%,#dedede 50%,#dcdcdc 51%,#dcdcdc 100%); /* Chrome10-25,Safari5.1-6 */border: 1px solid #cecece;box-shadow: inset 0 0 10px #403d3d;/* padding: 10px; */">

                    <td style="width:100%;padding:0px 30px 10px 30px;">

                        <table style="width:100%;display:inline-block">
                            <tbody>
                                <tr>
                                    <td>
                                        <h1 style="font-family:Helvetica;font-size: 22px;font-style:normal;font-weight:normal;text-align:center;margin: 11px auto 12px;text-align:left;"><font color="#4d4d4d; font-weight:bold;" style="color: black;"><span style="color: #676060;"> Image </span> 
                                            </font></h1>

                                    </td>

                                    <td>
                                        <h1 style="font-family:Helvetica;font-size:20px;font-style:normal;font-weight:normal;text-align:center;margin:0px auto 0px;text-align:left"><font color="#4d4d4d; font-weight:bold;"><span>Desc </span> 
                                            </font></h1>
                                    </td>

                                </tr>
                                <?php
                                foreach ($deal->dealGalleries as $gallery) {
                                    ?>
                                    <tr>
                                        <td><?= Html::img($gallery->getIconURL('small')); ?></td>
                                        <td><?= $gallery->description; ?></td>

                                    </tr>
                                    <?php
                                }
                                ?>




                            </tbody>
                        </table>

                    </td>
                </tr>
                <!--deal address-->

                <tr style="background:#fff">
                    <td style="width:100%;padding:17px 7px 0px 43px">

                        <h1 style="font-family:Helvetica;font-size: 22px;font-style:normal;font-weight:normal;text-align:center;margin: 11px auto 12px;text-align:left;"><font color="#4d4d4d; font-weight:bold;" style="color: black;"><span style="color: green;"> Deal Address </span> 
                            </font></h1>

                    </td>
                </tr>
                <tr style="background:#fff">
                    <td style="width:100%;box-sizing:border-box">
                        <table style="border-collapse:collapse;margin:0px 30px 0px 30px;text-align:Center" width="94%">
                            <tbody><tr style="line-height:3.4;background-color:#ecebeb">

                                    <th style="width:15%;padding-right:20px;border:1px solid #888">Address</th>
                            <th style="width:15%; border:1px solid #888">Title</th>

                                <th style="width:15%; border:1px solid #888">   longitude  </th> 
                                <th style="width:15%; border:1px solid #888">   Latitude</th>


                                </tr>
                                <?php
                                foreach ($deal->dealAddress as $address) {
                                    ?>
                                    <tr>

                                        <td style="padding:10px 11px;line-height:1.4;border:1px solid #888"><?= $address->address; ?></td>
                                        <td style="padding:10px 11px;line-height:1.4;border:1px solid #888"><?= $address->name ?></td>
                                        <td style="padding:10px 11px;line-height:1.4;border:1px solid #888"><?= $address->geo_long; ?></td>
                                        <td style="padding:10px 11px;line-height:1.4;border:1px solid #888"><?= $address->geo_lat; ?></td>

                                    </tr>
                                    <?php
                                }
                                ?>


                                </tbody>

                        </table>


                    </td>

                </tr>
                <?php
                if($deal->user->send_password==0)
                {    
                ?>
                <tr style="background:#fff">
                    <td style="width:100%;box-sizing:border-box">
                        <table style="border-collapse:collapse;margin:0px 30px 0px 30px;text-align:Center" width="94%">
                            <tbody><tr style="line-height:3.4;background-color:#ecebeb">
                                    <th><h3>Merchant Login Details</h3></th></tr> 
                            <tr><th><h3><a href="https://www.classifr.com/classifrmerchant">https://www.classifr.com/classifrmerchant</a></h3></th></tr> 

                                    <tr>
                                        <td style="padding:10px 11px;line-height:1.4;border:1px solid #888">User Name: <?= $deal->user->email; ?></td><tr>
                                    <tr> <td style="padding:10px 11px;line-height:1.4;border:1px solid #888">Password: <?php $subemail= substr($deal->user->email,0,6); $dt=date('d y',$deal->user->created_at); $dt= str_replace(" ","",$dt); echo $subemail."@".$dt; ?></td></tr>
                                    
                                     
                                </tbody>

                        </table>


                    </td>

                </tr>
                <?php
                }
                ?>



                <tr style="background:#fff">
                    <td style="width:100%;box-sizing:border-box">
                        <table style="width: 100%; background: #fff;">
                            <tr>
                                <td>
                                    <div class="account-text">
                                        <div class="container" style="width:1000px; margin:0 auto; text-align: Center; padding-top:40px;">
                                            <p style="font-size: 20px; width: 60%; margin: 19px auto;"></p>
                                           
                                            <div type="button" style="margin: 47px 0px;box-sizing: border-box;color: #fff;padding: 13px 38px;font-size: 20px;text-align: Center;">
                                                <a href="<?= \yii\helpers\Url::toRoute(["../deal/".$deal->slug,"authhash"=>$deal->user->auth_key],TRUE); ?>" style="display: inline-block;width: 21%;color: #fff;text-decoration: none;background: red;padding: 13px;border: 1px solid #714c4c;pointer: cursor;">

                                                    Click to View deal
                                                </a>  <br><br>
                                                 <!--<a href="<?php // \common\components\GenXHelper::encryptUserLink($deal->user_id,  \yii\helpers\Url::toRoute(["../site/activation","dealid"=>$deal->id],TRUE),"../site/encrypted-link" ); ?>" style="display: inline-block;width: 21%;color: #fff;text-decoration: none;background: green;padding: 13px;border: 1px solid #714c4c;pointer: cursor;">

                                                    Click to Activate // change send-email-to-merchant-for-activation to activation
                                                </a> --> &nbsp; 
                                               
                                              <a href="<?=  \common\components\GenXHelper::encryptUserLink($deal->user_id,  \yii\helpers\Url::toRoute(["../site/activation","dealid"=>$deal->id],TRUE),"../site/encrypted-link" ); ?>" style="display: inline-block;width: 21%;color: #fff;text-decoration: none;background: green;padding: 13px;border: 1px solid #714c4c;pointer: cursor;">
                                                    Send activation link
                                                </a>
                                            </div>

                                        </div>
                                </td>
                            </tr>

                        </table>




                    </td>

                </tr>




                <tr>
                <td><table style="color: #fff;" width="100%">
                        <tr>
                            <td><p style=" text-align: center; color: #888; font-size: 12px;margin: 15px 0px 5px 0px;color: green;">Address</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">24 Moon Avenue, Majitha Road
                                </p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">Amritsar</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom:0px; margin-top:0px;">PB 143001, India</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">+91-799910-0015</p>
                                <p style="paddcing-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">contact@classifr.com</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 0px; margin-top:0px;">&nbsp;</p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;"><a href="<?= yii\helpers\Url::toRoute(["site/vcf"], true); ?>" style="color: #696969; text-decoration:underline">Add us to your address book</a></p>
                                <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;">&nbsp;</p>
                                <!-- <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;"><span style="color: #888; text-align: center; margin: 10px auto;font-size: 12px;">Want to change how you recieve these mails ? </span></p> -->
                                <!-- <p style="padding-top: 0px; text-align: center; color: #888; font-size: 12px; margin-bottom: 6px; margin-top:0px;">You can <a href="javasrcipt:;" style="color: #696969; text-decoration:underline">update your preferences</a> or <a href="javasrcipt:;" style="color: #696969; text-decoration:underline">unsubscribe from this list</a></p> -->
                                <p style="color: #888; text-align: center; margin: 10px auto;font-size: 12px;">&nbsp;</p>
                            </td>
                        </tr>
                    </table></td>
            </tr>
                    </table>  
