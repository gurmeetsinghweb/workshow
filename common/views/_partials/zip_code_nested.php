<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<script>
    var UpdateStateCity = function (zip_code, state_id, city_id) {
        $("#zip_loader").show();
        $("#zip_loader").html("Searching ..");
        $.get("<?= \yii\helpers\Url::toRoute(['/zip-code/get-json-by-zip-code']) ?>?zip_code=" + zip_code, function (data) {
            var response = JSON.parse(data);
            if (!response.error) {
                $('#' + state_id).prop('disabled', 'disabled');
                $('#' + city_id).prop('disabled', 'disabled');
                $("#" + state_id).val(response.state_id);
                $("#" + city_id).val(response.city_id);
                $("#zip_loader").hide();
            } else {
                $("#zip_loader").html("Zip Code not found !!");
                $('#' + state_id).prop('disabled', false);
                $('#' + city_id).prop('disabled', false);
                $("#" + state_id).val(0);
                $("#" + city_id).val(0);
            }
        });
    }
</script>


<?php
echo $form->field($model, 'zip_code')->textInput(['maxlength' => true, 'onChange' => "UpdateStateCity($(this).val(), 'zip_state', 'zip_city')"]);
echo "<span id='zip_loader'></span>";
echo $form->render("@backend/views/_partials/state", ['form' => $form, 'model' => $model, 'id' => 'zip_state', 'disabled' => 'disabled']);
echo $form->render("@backend/views/_partials/city", ['form' => $form, 'model' => $model, 'id' => 'zip_city']);
