<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

?>
<div class="row">
    <div class="col-lg-4">{image}<a id="refresh_captcha" href="javascript:;" style="margin: 0px; padding: 0px; display: table;">Refresh</a></div>
    <div class="col-lg-8">{input}</div>
</div>
<?php
$this->registerJs('
    $(document).ready(function () {
        $("#refresh_captcha").click(function (event) {
            event.preventDefault();
            $(this).parent().children(\'img\').click();
        });
        
        $("#refresh_captcha").click();
    });
    ');
?>