<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

$this->registerJsFile("//cdnjs.cloudflare.com/ajax/libs/tinymce/5.6.1/tinymce.min.js");

$texteditor=$form->field($model, $attribute)->textarea();
if(isset($label))
{
    $texteditor->label($label);
    
}
echo $texteditor;
$this->registerJs(new yii\web\JsExpression("
        tinymce.init({ 
        selector:'textarea' ,
        height: 200,
        plugins: [
          'advlist autolink lists link image charmap print preview anchor',
          'searchreplace visualblocks code fullscreen',
          'insertdatetime media table contextmenu paste code'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        });
    "));

