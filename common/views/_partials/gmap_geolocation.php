<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */


$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=' . Yii::$app->params['gmap_api_key'] . '&callback=initMap&libraries=places&region=IN');
?>
<div id="map" style="height:350px;"></div>
<div id="infoPanel">
    <b>Searched Address:</b> <span id="infoAddress"></span>
    <br />
    <b>Marker Position:</b> <span id="info"></span>
</div>
<script type="text/javascript">
    var map = null;
var marker = null;
var country = 'IN';  
var startDragPosition = null;
    function geocodeAddress(geocoder, resultsMap, marker)      {
        var address = document.getElementById('<?= $options['address_input_id'] ?>').value;
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                resultsMap.setCenter(results[0].geometry.location);
                resultsMap.setZoom(16);
                marker.setPosition(results[0].geometry.location);
                updateMarkerPosition(marker.getPosition());
                updateMarkerAddress(address);
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    function updateMarkerPosition(latLng) {
        document.getElementById('info').innerHTML = [
            latLng.lat(),
            latLng.lng()
        ].join(', ');
        document.getElementById('<?= $options['lat_input_id'] ?>').value = latLng.lat();
        document.getElementById('<?= $options['long_input_id'] ?>').value = latLng.lng();
    }

    function updateMarkerAddress(str) {
        document.getElementById('infoAddress').innerHTML = str;
        document.getElementById('dealaddress-address').value  = str;
        
    }

    function initMap() {
        var geocoder = new google.maps.Geocoder();
      
        var myLatLng = {lat: <?= $model->geo_lat ? $model->geo_lat : 23.86850165282617 ?>, lng: <?= $model->geo_long ? $model->geo_long : 78.61679469472047 ?>};
        var map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            scrollwheel: false,
            zoom: <?= $model->geo_lat ? 16 : 6 ?>,
            componentRestrictions: { country: "IN" }
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Selected Location',
            draggable: true,
        });


        //init autocomplete
        var input = document.getElementById('<?= $options['address_input_id']?>');
        var autocomplete = new google.maps.places.Autocomplete(input, map);
        autocomplete.addListener('place_changed', function () {
            setPlace();
        });

        function setPlace () {
            //infoWindow.close();
            var place = autocomplete.getPlace();
             // options['address_input_id'] = place.formatted_address;
            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }
            
        }
        updateMarkerAddress(document.getElementById('<?= $options['address_input_id'] ?>').value);
        updateMarkerPosition(marker.getPosition());
        google.maps.event.addListener(marker,'dragstart',function() {
            startDragPosition = marker.getPosition();
        });
        google.maps.event.addListener(marker, 'dragend', function () {
            updateMarkerPosition(marker.getPosition());
              geocoder.geocode({'latLng': marker.getPosition()}, function(responses,status) {
                if (status == google.maps.GeocoderStatus.OK && responses[0]) {
                    var countryMarker = addresComponent('country', responses[0], true);
                    if (country != countryMarker) {
                        marker.setPosition(startDragPosition);
                    }
                } else {
                    marker.setPosition(startDragPosition);
                }
                if (responses && responses.length > 0) {
                    updateMarkerAddress(responses[0].formatted_address);
                } else {
                    updateMarkerAddress('Cannot determine address at this location.');
                }
            });
        });

        document.getElementById('<?= $options['address_input_id'] ?>').addEventListener('change', function () {
            geocodeAddress(geocoder, map, marker);
        });

//        if (navigator.geolocation) {
//            navigator.geolocation.getCurrentPosition(function (position) {
//                var pos = {
//                    lat: position.coords.latitude,
//                    lng: position.coords.longitude
//                };
//
//                marker.setPosition(pos);
//                map.setCenter(marker.getPosition());
//                map.setZoom(15);
//                updateMarkerPosition(marker.getPosition());
//            });
//        }
    }
    function addresComponent(type, geocodeResponse, shortName) {
        for(var i=0; i < geocodeResponse.address_components.length; i++) {
          for (var j=0; j < geocodeResponse.address_components[i].types.length; j++) {
            if (geocodeResponse.address_components[i].types[j] == type) {
              if (shortName) {
                return geocodeResponse.address_components[i].short_name;
              }
              else {
                return geocodeResponse.address_components[i].long_name;
              }
            }
          }
        }
        return '';
      }
</script>
