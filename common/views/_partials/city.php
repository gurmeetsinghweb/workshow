<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use \common\models\City;
use yii\helpers\Url;
\backend\assets\Select2Assets::register($this);

$cityId=((isset($id)) ? $id : "city_id");
if($model->city_id){
    $selectarray=[$model->city_id => $model->city->search_name];
    
}else{
    $selectarray=[];
}
    
echo $form
        ->field($model, 'city_id')
        ->label(isset($label) ? $label : Null)
        ->dropDownList($selectarray, ['prompt' => 'Select City', 'id' => $cityId]);


$this->registerJs('
    $(function () {
    $("#'.$cityId.'").select2({
        ajax: {
            url:  "' . Url::toRoute(['/city/autocomplete']) . '",
                dataType: "json",
                delay: 250,
            cache: true,
        },
        delay: 250,
    });
});
');