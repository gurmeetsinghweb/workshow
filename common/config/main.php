<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
         'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '409784472483399',
                    'clientSecret' => '22e504dce132ea7f7bd9e0b346095cd1',
                ],
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => '713340865100-llvstaqg321fded0gaphcmmoutcote30.apps.googleusercontent.com',
                    'clientSecret' => 'BsjnNaHfKKkFMmcLzf6A6PJB',
                ],
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=classifr',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
              'enableSchemaCache' => true,
            // Duration of schema cache.
            'schemaCacheDuration' => 3600,
            // Name of the cache component used to store schema information
            'schemaCache' => 'cache',
        ],
        'response' => [
            'formatters' => [
                'pdf' => [
                    'class' => 'robregonm\pdf\PdfResponseFormatter',
                    'mode' => '', // Optional
                    'format' => 'A4', // Optional but recommended. http://mpdf1.com/manual/index.php?tid=184
                    'defaultFontSize' => 0, // Optional
                    'defaultFont' => '', // Optional
                    'marginLeft' => 5, // Optional
                    'marginRight' => 5, // Optional
                    'marginTop' => 10, // Optional
                    'marginBottom' => 10, // Optional
//                    'marginHeader' => 9, // Optional
//                    'marginFooter' => 9, // Optional
//                    'orientation' => 'Landscape', // optional. This value will be ignored if format is a string value.
                    'options' => [
                    // mPDF Variables
                    // 'fontdata' => [
                    // ... some fonts. http://mpdf1.com/manual/index.php?tid=454
                    // ]
                    ]
                ],
            ]
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
        
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],


     'user' => [
            'class' => 'common\components\GenxWebUser',
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false
        ],
        'authManager' => [
            'class' => 'common\components\GenxRBACManager',
        ]
        
    ],
];
