<?php

return [
    'is_local' => False,
    'supportEmail' => 'contact@classifr.com',//'support@classifr.com', //'contact@classifr.com',
    'supportName' => 'Classifr (VDOT)Support',
    'adminEmail' => 'contact@classifr.com',
    'contactEmail' => 'contact@classifr.com',
    'contactPhone' => '7999100015',
    'contactAddress' => '24 Moon Avenue 2, Majitha Road',
    'FbUrl' => '#',
    'TwUrl' => '#',
    'GPlusUrl' => '#',
    'currency_unit' => '&#x20B9;',
    'user.passwordResetTokenExpire' => 3600,
    'user.rememberMeDuration' => 3600,
    'gmap_api_key' => 'AIzaSyCpPCdHAqfn6D90C_QnKz3-W4ulMuw13E8',
    'searchRadius' => 100,
    'fb_key' => 15,
    'coupon_cancel_interval' => 604800, // 7*24*60*60
    'GST_amount'=>0,
    'payment_given'=>[1,15],
    'deal_commission' =>20,
    'deal_expire_time'=>7776000, // 90 days expire   90 * 24 *60 * 60
    'deal_credit'=> 2,
    'coupon_expire_time'=>86400, // one day calculate 24*60*60
    //deal credit to user afer redeemed the deal by default 2 % 
    'static_email_for_deal_publish'=>true,
    'deal_publish_email'=>'contact@classifr.com'
     ];
