<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string $logo
 * @property string $footer_logo
 * @property string $logo_title
 * @property string $facebook_link
 * @property string $instagram_link
 * @property string $twitter_link
 * @property string $visa_logo
 * @property string $maestro_logo
 * @property string $paypal_logo
 * @property string $address
 * @property string $phone
 * @property string $contact_email
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['logo', 'footer_logo', 'logo_title', 'facebook_link', 'instagram_link', 'twitter_link', 'visa_logo', 'maestro_logo', 'paypal_logo', 'address', 'phone', 'contact_email', 'company_name'], 'string', 'max' => 244],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'logo' => 'Logo',
            'footer_logo' => 'Footer Logo',
            'logo_title' => 'Logo Title',
            'facebook_link' => 'Facebook Link',
            'instagram_link' => 'Instagram Link',
            'twitter_link' => 'Twitter Link',
            'visa_logo' => 'Visa Logo',
            'maestro_logo' => 'Maestro Logo',
            'paypal_logo' => 'Paypal Logo',
            'address' => 'Address',
            'phone' => 'Phone',
            'contact_email' => 'Contact Email',
            'splash_logo' => 'Splash Logo',
            'terms_and_conditions' => 'Terms and Conditions'
        ];
    }

    /**
     * @inheritdoc
     * @return SettingsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SettingsQuery(get_called_class());
    }
}
