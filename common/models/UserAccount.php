<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_account".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $order_id
 * @property integer $cart_id
 * @property integer $transaction_id
 * @property integer $debit
 * @property integer $credit
 * @property string $remarks
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class UserAccount extends \common\components\GenxBaseModel
{

    public $balance;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_account';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return parent::rules([
                        [['user_id', 'order_id', 'cart_id', 'transaction_id', 'debit', 'credit'], 'required'],
                        [['user_id', 'order_id', 'cart_id'], 'integer'],
                        [['credit','debit'], 'double'],
                        [['remarks', 'transaction_id'], 'string'],
                        [['user_id'], 'exist', 'skipOnError' => False, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'cart_id' => Yii::t('app', 'Cart ID'),
            'transaction_id' => Yii::t('app', 'Transaction ID'),
            'debit' => Yii::t('app', 'Debit'),
            'credit' => Yii::t('app', 'Credit'),
            'remarks' => Yii::t('app', 'Remarks'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function debit($amount, $user_id = Null, $order_id = 0, $cart_id = 0, $transaction_id = 0, $remarks = '')
    {
        return self::UpdateAccount([
                    'debit' => $amount,
                    'credit' => 0,
                    'user_id' => (int) (($user_id == null) ? \Yii::$app->user->id : $user_id),
                    'order_id' => $order_id,
                    'cart_id' => $cart_id,
                    'transaction_id' => $transaction_id,
                    'remarks' => $remarks,
        ]);
    }

    public static function credit($amount, $user_id = Null, $order_id = 0, $cart_id = 0, $transaction_id = 0, $remarks = '')
    {
        return self::UpdateAccount([
                    'credit' => $amount,
                    'debit' => 0,
                    'user_id' => (int) (($user_id == null) ? \Yii::$app->user->id : $user_id),
                    'order_id' => $order_id,
                    'cart_id' => $cart_id,
                    'transaction_id' => $transaction_id,
                    'remarks' => $remarks,
        ]);
    }

    public static function UpdateAccount($array)
    {
        $user_account = new self();
        $user_account->user_id = $array['user_id'];
        $user_account->order_id = $array['order_id'];
        $user_account->cart_id = $array['cart_id'];
        $user_account->transaction_id = $array['transaction_id'];
        $user_account->debit = $array['debit'];
        $user_account->credit = $array['credit'];
        $user_account->remarks = $array['remarks'];

        if($user_account->save()) {
            return True;
        }
        
        return $user_account->getErrors();
    }

}
