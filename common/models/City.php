<?php

namespace common\models;

use yii\imagine\Image;
use Yii;

/**
 * This is the model class for table "{{%city}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $iso_2_char
 * @property string $iso_3_char
 * @property string $iso_4_char
 * @property integer $country_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 * @property integer $latitude
 * @property integer $longitude
 * @property integer $zip_code
 * @property integer $search_name
 * @property integer $featured
 *
 * @property Country $country
 * @property State $state
 * @property ServiceRequest[] $serviceRequests
 */
class City extends \common\components\GenxBaseModel
{

    public $imageFile;
    public $total_deals_in_city;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => \yii\behaviors\SluggableBehavior::className(),
            'attribute' => 'name',
            'ensureUnique' => True
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%city}}';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return parent::rules([
                    [['name', 'slug', 'country_id', 'state_id'], 'required'],
                    [['country_id', 'state_id', 'featured', 'show_home', 'status', 'default_city'], 'integer'],
                    [['name', 'slug', 'featured_img'], 'string', 'max' => 255],
                    [['iso_2_char'], 'string', 'max' => 2],
                    [['iso_3_char'], 'string', 'max' => 3],
                    [['iso_4_char'], 'string', 'max' => 4]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'slug' => Yii::t('app', 'Slug'),
            'iso_2_char' => Yii::t('app', 'Iso 2 Char'),
            'iso_3_char' => Yii::t('app', 'Iso 3 Char'),
            'iso_4_char' => Yii::t('app', 'Iso 4 Char'),
            'country_id' => Yii::t('app', 'Country ID'),
            'state_id' => Yii::t('app', 'State ID'),
            'featured' => Yii::t('app', 'Features'),
            'show_home' => Yii::t('app', 'Show Homepage'),
            'featured_img' => Yii::t('app', 'Features Image'),
            'default_city' => Yii::t('app', 'Default City'),
        ];
    }

    public function upload()
    {
        if (!$this->validate()) {
            return False;
        }

        /* if ($this->isNewRecord) {
          $file_name = time() . '.' . $this->imageFile->extension;
          } else {
          $file_name = $this->name;
          } */

        $file_name = "city_" . $this->slug . '.' . $this->imageFile->extension;
        $image_save_path = \Yii::getAlias('@image_uploads/banner/');

        $orignal_file_full_path = $image_save_path . $file_name;
        $saved_orignal = $this->imageFile->saveAs($orignal_file_full_path);
        Image::thumbnail($orignal_file_full_path, 241, 250)->save($image_save_path . $file_name);

        if ($saved_orignal) {
            return $file_name;
        }

        return False;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id']);
    }

    public function getURL($action = 'view', $only_route = False, $full_url = False)
    {
        return parent::getURL("/city/view", $only_route, $full_url);
    }

    public function getCompanies()
    {
        return $this->hasOne(Company::className(), ['city_id' => 'id'])->inverseOf('city');
    }

    /**
     * 
     * @inheritdoc
     * @return ActiveQuery the newly created [[ActiveQuery]] instance.
     */
    public static function getTopCities($options = array())
    {
        $find = self::find()
                ->select('city.*, count(deal.city_id) as total_deals_in_city')
                ->innerJoin('deal', 'deal.city_id = city.id')
                ->groupBy('`deal`.city_id')
                ->orderBy('total_deals_in_city DESC');

        if (isset($options['andFilterWhere']) && sizeof($options['andFilterWhere'])) {
            foreach ($options['andFilterWhere'] as $andFilterWhere) {
                $find->andFilterWhere($andFilterWhere);
            }
        }

        return $find;
    }

    public static function getSelectedDefaultCity()
    {
        if (!isset(static::$_CACHE['getSelectedDefaultCity'])) {
            $find = self::find()->andWhere(['default_city' => 1])->one();
            static::$_CACHE['getSelectedDefaultCity'] = $find->getAttributes();
        }

        return static::$_CACHE['getSelectedDefaultCity'];
    }

    public static function getFeaturedCity($options = array())
    {
        $find = self::find()
                ->select('city.*')
                ->andWhere(['featured' => 1]);

        if (isset($options['andFilterWhere']) && sizeof($options['andFilterWhere'])) {
            foreach ($options['andFilterWhere'] as $andFilterWhere) {
                $find->andFilterWhere($andFilterWhere);
            }
        }

        return $find;
    }

    public static function getNearBy($lat, $long, $radius_in_miles)
    {
        $find = self::find()
                ->select('city.*, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( city.latitude ) ) * cos( radians( city.longitude ) - radians(' . $long . ') ) + sin( radians(' . $lat . ') ) * sin( radians( city.latitude ) ) ) ) AS distance')
                ->having(['<', 'distance', $radius_in_miles])
                ->orderBy('distance');

        return $find;
    }

    public function getBannerURL($full = True)
    {
        $file_name = $this->featured_img;

        return \yii\helpers\Url::toRoute([((\Yii::$app->id == 'app-backend') ? ".." : "") . '/uploads/banner/' . $file_name], $full);
    }

}
