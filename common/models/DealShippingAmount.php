<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "deal_shipping_amount".
 *
 * @property integer $id
 * @property integer $deal_id
 * @property integer $state_id
 * @property double $shipping_amount
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class DealShippingAmount extends \common\components\GenxBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal_shipping_amount';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return parent::rules([
            [['deal_id', 'state_id', 'shipping_amount'], 'required'],
            [['deal_id', 'state_id'], 'integer'],
            [['shipping_amount'], 'number'],
	     [['deal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Deal::className(), 'targetAttribute' => ['deal_id' => 'id']],
          ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            
            'deal_id' => Yii::t('app', 'Deal ID'),
            'state_id' => Yii::t('app', 'State'),
            'shipping_amount' => Yii::t('app', 'Shipping Amount'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    
    public function getDeal()
    {
        return $this->hasOne(Deal::className(), ['id' => 'deal_id'])->inverseOf('deal');
    }
    
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id']);
    }
    
    
}
