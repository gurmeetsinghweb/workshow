<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%enquiry}}".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email_id
 * @property integer $phone
 * @property string $business_name
 * @property string $business_email
 * @property integer $category_id
 * @property string $website
 * @property integer $city
 * @property integer $user_id
 * @property string $description
 * @property integer $newsletter
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $title
 * @property integer $discount_desire
 * @property City $city
 * @property Category $category
 * @property User $user
 */
class Enquiry extends \common\components\GenxBaseModel
{

    public $verificationCode;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%enquiry}}';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return parent::rules([
                    // [['email_id', 'contact_type'], 'required'],
                    [['first_name'], 'required','message' => "Please enter your first name"],
                    [['email_id'],'required','message'=>'Please enter a valid email address'],
                    [['category_id', 'newsletter','discount_desire','user_id'], 'integer'],
                   // ['description', 'required'],
                    [['phone'], 'integer', "message" => "Please enter correct phone no", 'min' => 1000000000, 'max' => 9999999999],
                    [['phone'],'required','message'=>'Please enter phone no'],
            
                    [['description', 'city', 'country', 'state', 'postcode', 'street_address', 'fileupload','title'], 'string'],
                    [['website'], 'url'],
                    [['first_name', 'last_name'], 'match', 'pattern' => '/^[a-z ]*$/i', "message" => "Only alphabets are allowed"],
                    [['first_name', 'last_name', 'email_id', 'business_name', 'business_email', 'website'], 'string', 'max' => 200],
                    [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
                    [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            
                    ['verificationCode', 'captcha', 'when' => function() {
                            if (Yii::$app->id === 'app-console') {
                                return False;
                            }
                        }],
                    ['email_id', 'email'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email_id' => 'Email ID',
            'phone' => 'Phone',
            'business_name' => 'Business Name',
            'business_email' => 'Business Email',
            'category_id' => 'Category ID',
            'title'=>'Title',
            'discount_desire'=>'Discount Desire',
            'website' => 'Website',
            'city' => 'City',
            'country' => 'Country',
            'state' => 'State',
            'postcode' => 'Postcode',
            'description' => 'Description',
            'newsletter' => 'Newsletter',
            'status' => 'Status',
            'fileupload' => 'File Upload',
            'contact_type' => 'Contact Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'street_address' => 'Street Address',
        ];
    }

    public static function getFormTypeForForm()
    {
        return [
            'promote' => 'Promote Your Business',
            'work' => 'Work With Us',
            'contact' => 'Contact',
            'save' => 'Save'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
    
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    

    public function afterSave($insert, $changedAttributes)
    {
        try {
            $newsletter = new \common\models\Newsletter();
            $newsletter->email = $this->email_id;
            $newsletter->save();
        } catch (Exception $e) {
            
        }

        return parent::afterSave($insert, $changedAttributes);
    }

}
