<?php

namespace common\models;

use Yii;
use yii\imagine\Image;

/**
 * This is the model class for table "discountcoupon_details".
 *
 * @property integer $id
 * @property string $discount_coupon
 * @property string $discount_title
 * @property string $slug
 * @property string $amount_type
 * @property integer $amount_value
 * @property integer $total_person_allow
 * @property integer $min_bill
 * @property integer $max_credit
 * @property integer $coupon_start_dt
 * @property integer $coupon_expire_dt
 * @property integer $total_qty
 * @property integer $total_used
 * @property integer $category
 * @property integer $deal
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $coupon_img
 * @property string $coupon_img_url
 * @property DiscountcouponCategory[] $discountcouponCategories
 * @property DiscountcouponDeal[] $discountcouponDeals
 */
class DiscountcouponDetails extends \common\components\GenxBaseModel
{

    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discountcoupon_details';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => \yii\behaviors\SluggableBehavior::className(),
            'attribute' => 'discount_coupon',
            'ensureUnique' => True
        ];

        return $behaviors;
    }

    public function rules($rules = [])
    {
        return parent::rules([
                    [['discount_coupon', 'discount_title', 'slug', 'amount_type', 'amount_value', 'total_person_allow', 'max_credit', 'coupon_start_dt', 'coupon_expire_dt'], 'required'],
                    [['amount_value', 'total_person_allow', 'min_bill', 'max_credit', 'coupon_start_dt', 'coupon_expire_dt', 'total_qty', 'total_used', 'category', 'deal'], 'integer'],
                    [['discount_coupon', 'discount_title', 'amount_type'], 'string', 'max' => 255],
                    [['coupon_img', 'coupon_img_url'], 'string'],
                    [['discount_coupon'], 'unique', 'targetAttribute' => ['discount_coupon']],
        ]);
    }

    public function beforeValidate()
    {


        if (strlen($this->coupon_start_dt) > 10) {
            $this->coupon_start_dt = (int) (substr($this->coupon_start_dt, 0, 10));
        }

        if (strlen($this->coupon_expire_dt) > 10) {
            $this->coupon_expire_dt = (int) (substr($this->coupon_expire_dt, 0, 10));
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'discount_coupon' => 'Discount Coupon',
            'discount_title' => 'Discount Title',
            'amount_type' => 'Amount Type',
            'slug' => Yii::t('app', 'Slug'),
            'amount_value' => 'Amount Value',
            'total_person_allow' => 'Total Person Allow',
            'min_bill' => 'Min Bill',
            'max_credit' => 'Max Credit',
            'coupon_start_dt' => 'Coupon Start Dt',
            'coupon_expire_dt' => 'Coupon Expire Dt',
            'total_qty' => 'Total Qty',
            'total_used' => 'Total Used',
            'category' => 'Category',
            'deal' => 'Deal',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'coupon_img' => 'Coupon Image',
            'coupon_img_url' => 'Coupon Image Url',
        ];
    }

    public function upload()
    {
        if (!$this->validate()) {
            return False;
        }

        /* if ($this->isNewRecord) {
          $file_name = time() . '.' . $this->imageFile->extension;
          } else {
          $file_name = $this->name;
          } */

        $file_name = "coupon_" . $this->slug . '.' . $this->imageFile->extension;
        $image_save_path = \Yii::getAlias('@image_uploads/banner/');

        $orignal_file_full_path = $image_save_path . $file_name;
        $saved_orignal = $this->imageFile->saveAs($orignal_file_full_path);
        Image::thumbnail($orignal_file_full_path, 1100, 190)->save($image_save_path . $file_name);

        if ($saved_orignal) {
            return $file_name;
        }

        return False;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscountcouponCategories()
    {
        return $this->hasMany(DiscountcouponCategory::className(), ['discount_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscountcouponDeals()
    {
        return $this->hasMany(DiscountcouponDeal::className(), ['discount_id' => 'id']);
    }

    public function getCart()
    {
        return $this->hasMany(Cart::className(), ['discountcoupon_id' => 'id']);
    }

    public function getOrder()
    {
        return $this->hasMany(Order::className(), ['discountcoupon_id' => 'id']);
    }

    public function getBannerURL($full = True)
    {
        $file_name = $this->coupon_img;

        return \yii\helpers\Url::toRoute([((\Yii::$app->id == 'app-backend') ? ".." : "") . '/uploads/banner/' . $file_name], $full);
    }

}
