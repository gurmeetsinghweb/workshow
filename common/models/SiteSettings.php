<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%site_settings}}".
 *
 * @property integer $id
 * @property string $about
 * @property string $objective
 */
class SiteSettings extends \common\components\GenxBaseModel
{

    public $all;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%site_settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        $rules = [
            [['key', 'value'], 'string'],
            [['key', 'value'], 'required'],
            [['created_at', 'updated_at'], 'integer']
        ];

        return parent::rules($rules);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Key'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

}
