<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "transaction_log".
 *
 * @property integer $id
 * @property integer $merchant_id
 * @property double $total_merchant_amount
 * @property double $total_merchant_amout_excluding_gst
 * @property double $total_merchant_gst
 * @property double $total_site_amount
 * @property double $total_site_amount_excluding_gst
 * @property double $total_site_gst
 * @property double $total_amount
 * @property double $total_amount_excluding_gst
 * @property double $total_amount_gst
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Cart[] $carts
 * @property User $merchant
 */
class TransactionLog extends \common\components\GenxBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction_log';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
         return parent::rules([
            [['merchant_id','transaction_payment_id'], 'integer'],
            [['total_merchant_amount', 'total_merchant_amout_excluding_gst', 'total_merchant_gst', 'total_site_amount', 'total_site_amount_excluding_gst', 'total_site_gst', 'total_amount', 'total_amount_excluding_gst', 'total_amount_gst'], 'number'],
            [['total_merchant_amout_excluding_gst', 'total_site_amount_excluding_gst', 'total_amount_excluding_gst', 'total_amount_gst'], 'required'],
            [['merchant_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['merchant_id' => 'id']],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'merchant_id' => 'Merchant ID',
            'transaction_payment_id' =>'Transaction Payment Id',
            'total_merchant_amount' => 'Total Merchant Amount',
            'total_merchant_amout_excluding_gst' => 'Total Merchant Amout Excluding Gst',
            'total_merchant_gst' => 'Total Merchant Gst',
            'total_site_amount' => 'Total Site Amount',
            'total_site_amount_excluding_gst' => 'Total Site Amount Excluding Gst',
            'total_site_gst' => 'Total Site Gst',
            'total_amount' => 'Total Amount',
            'total_amount_excluding_gst' => 'Total Amount Excluding Gst',
            'total_amount_gst' => 'Total Amount Gst',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarts()
    {
        return $this->hasMany(Cart::className(), ['transaction_log_id' => 'id'])->inverseOf('transactionLog');
    }
    
    public function getMerchant()
    {
        return $this->hasOne(User::className(), ['id' => 'merchant_id']);
    }
    public function getTransactionPayment()
    {
        return $this->hasOne(TransactionPayment::className(), ['id' => 'transaction_payment_id']);
    }
    
}
