<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%request}}".
 *
 * @property integer $id
 * @property integer $from_id
 * @property string $type
 * @property integer $assigned_id
 * @property string $remarks
 * @property integer $object_id
 * @property string $object_class
 * @property string $data
 * @property string $response
 * @property integer $status
 * @property integer $updated_at
 * @property integer $created_at
 * 
 * @property From $from
 * @property Assigned $assigned
 * @property Object $object
 */
class Request extends \common\components\GenxBaseModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return parent::rules([
            [['from_id', 'type', 'assigned_id', 'remarks', 'object_id', 'object_class', 'data'], 'required'],
            [['from_id', 'assigned_id', 'object_id'], 'integer'],
            [['remarks', 'data', 'response'], 'string'],
            [['type'], 'string', 'max' => 50],
            [['from_id'], 'exist', 'skipOnError' => False, 'targetClass' => User::className(), 'targetAttribute' => ['from_id' => 'id']],
            [['assigned_id'], 'exist', 'skipOnError' => False, 'targetClass' => User::className(), 'targetAttribute' => ['assigned_id' => 'id']],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'from_id' => Yii::t('app', 'From ID'),
            'type' => Yii::t('app', 'Type'),
            'assigned_id' => Yii::t('app', 'Assigned ID'),
            'remarks' => Yii::t('app', 'Remarks'),
            'object_id' => Yii::t('app', 'Object ID'),
            'data' => Yii::t('app', 'Data'),
            'response' => Yii::t('app', 'Response'),
            'status' => Yii::t('app', 'Status'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public function getFrom()
    {
        return $this->hasOne(User::className(), ['id' => 'from_id']);
    }

    public function getAssigned()
    {
        return $this->hasOne(User::className(), ['id' => 'assigned_id']);
    }

    public function getObject()
    {
        $model_class = $this->object_class;
        return $model_class::find($this->object_id);
    }

}
