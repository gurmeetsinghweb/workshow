<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%state}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $iso_2_char
 * @property string $iso_3_char
 * @property string $iso_4_char
 * @property integer $country_id
 *
 * @property City[] $cities
 * @property ServiceRequest[] $serviceRequests
 * @property Country $country
 */
class State extends \common\components\GenxBaseModel
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => \yii\behaviors\SluggableBehavior::className(),
            'attribute' => 'name',
            'ensureUnique' => True
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%state}}';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return [
            [['name', 'slug', 'country_id'], 'required'],
            [['country_id', 'status'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['iso_2_char'], 'string', 'max' => 2],
            [['iso_3_char'], 'string', 'max' => 3],
            [['iso_4_char'], 'string', 'max' => 4]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'slug' => Yii::t('app', 'Slug'),
            'iso_2_char' => Yii::t('app', 'Iso 2 Char'),
            'iso_3_char' => Yii::t('app', 'Iso 3 Char'),
            'iso_4_char' => Yii::t('app', 'Iso 4 Char'),
            'country_id' => Yii::t('app', 'Country ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['state_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceRequests()
    {
        return $this->hasMany(ServiceRequest::className(), ['state_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
    
    public function getURL($action = 'view', $only_route = False, $full_url = False)
    {
        return parent::getURL("/state/view", $only_route, $full_url);
    }

}
