<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TransactionLog;

/**
 * TransactionLogSearch represents the model behind the search form about `common\models\TransactionLog`.
 */
class TransactionLogSearch extends TransactionLog
{
    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return [
            [['id', 'merchant_id', 'created_at', 'updated_at'], 'integer'],
            [['total_merchant_amount', 'total_merchant_gst', 'total_site_amount', 'total_site_gst', 'total_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransactionLog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'merchant_id' => $this->merchant_id,
            'total_merchant_amount' => $this->total_merchant_amount,
            'total_merchant_gst' => $this->total_merchant_gst,
            'total_site_amount' => $this->total_site_amount,
            'total_site_gst' => $this->total_site_gst,
            'total_amount' => $this->total_amount,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
        
        
        return $dataProvider;
    }
}
