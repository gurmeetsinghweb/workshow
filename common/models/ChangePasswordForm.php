<?php

namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class ChangePasswordForm extends Model
{

    public $oldpassword;
    public $newpassword;
    public $newpassword_repeat;
    public $verificationCode;
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['oldpassword', 'newpassword', 'newpassword_repeat', 'verificationCode'], 'required'],
            // password is validated by validatePassword()
            ['oldpassword', 'validatePassword'],
            ['newpassword', 'string', 'min' => 6],
            [['newpassword_repeat'], 'compare', 'compareAttribute' => 'newpassword', 'message' => "Both Passwords must match"],
            ['verificationCode', 'captcha']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'oldpassword' => 'Old Password',
            'newpassword' => 'New Password',
            'newpassword_repeat' => 'New Password (Repeat)',
            'verificationCode' => 'Verification Code',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->oldpassword)) {
                $this->addError($attribute, 'Incorrect password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function changePassword()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            if ($user && $user->validatePassword($this->oldpassword)) {
                $user->setPassword($this->newpassword_repeat);
                if ($user->save()) {
                    return True;
                } else {
                    if (sizeof($user->getErrors()) && is_array($user->getErrors())) {
                        foreach ($user->getErrors() as $errs) {
                            foreach($errs as $err)
                            Yii::$app->getSession()->addFlash('error', $err);
                        }
                    }
                    return False;
                }
            }
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = Yii::$app->user->identity;
        }

        return $this->_user;
    }

}
