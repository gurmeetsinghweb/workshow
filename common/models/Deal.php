<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%deal}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $company_id
 * @property integer $main_category_id
 * @property integer $deal_remain
 * @property integer $sub_category_id
 * @property integer $sub_sub_category_id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property string $summary
 * @property integer $start_time
 * @property integer $expire_time
 * @property string $terms
 * @property string $how_redeemed
 * @property integer $city_id
 * @property integer $state_id
 * @property integer $coupon_expire
 * @property integer $country_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property double $geo_lat
 * @property double $geo_long
 * @property string $address
 * @property string $featured
 * @property integer $shipping_applied
 * @property string $sponsored
 * @property string $slider
 * @property string $deal_commission
 * @property string $deal_credit
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property City $city
 * @property Company $company
 * @property Country $country
 * @property Category $mainCategory
 * @property State $state
 * @property Category $subCategory
 * @property Category $subSubCategory
 * @property User $user
 * @property Baseprice $base_price 
 * @property Basediscount $base_discount 
 * @property DealGallery[] $dealGalleries
 * @property DealOption[] $dealOptions
 * @property DealReview[] $dealReviews
 * @property DealCategory[] $dealCategory
 */
class Deal extends \common\components\GenxBaseModel
{

    public $totaldealoption;

    const STATUS_PROCESSING = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%deal}}';
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => \yii\behaviors\SluggableBehavior::className(),
            'attribute' => 'title',
            'ensureUnique' => True
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return parent::rules([
                    [['status'], 'default', 'value' => self::STATUS_PROCESSING],
                    [['deal_remain'],'default','value'=>rand(20,50)],
                    [['deal_commission'], 'default', 'value' => 20],
                    [['coupon_expire'], 'default', 'value' => 90],
            
                    [['deal_credit'], 'default', 'value' => Yii::$app->params['deal_credit']],
                    [
                        ['user_id'],
                        'default',
                        'value' => ((\Yii::$app->user->merchant()) ? \Yii::$app->user->merchant()->id : Null),
                    ],
                    [
                        ['user_id'],
                        'required',
                        'message' => "Merchant can't be blank"
                    ],
                    [['company_id'], 'default', 'value' => ((\Yii::$app->user->merchant() && \Yii::$app->user->merchant()->company) ? \Yii::$app->user->merchant()->company->id : Null)],
                    [['company_id', 'title', 'city_id', 'deal_commission'], 'required'],
                    [['user_id', 'company_id', 'start_time', 'expire_time', 'city_id', 'state_id', 'country_id', 'base_discount','coupon_expire'], 'integer'],
                    [['description', 'summary', 'terms','how_redeemed','meta_title','meta_description','meta_keyword'], 'string'],
                    [['base_price','deal_credit','deal_commission'], 'double'],
                    [['title', 'slug', 'address'], 'string', 'max' => 255],
                    [['geo_lat', 'geo_long'], 'number'],
                    [['featured', 'sponsored', 'slider','shipping_applied'], 'default', 'value' => 0],
                    [['featured', 'sponsored', 'slider','shipping_applied'], 'integer'],
                    [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
                    [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
                    [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
                //    [['main_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['main_category_id' => 'id']],
                    [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['state_id' => 'id']],
                 //   [['sub_category_id'], 'exist', 'skipOnEmpty' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['sub_category_id' => 'id']],
                  //  [['sub_sub_category_id'], 'exist', 'skipOnEmpty' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['sub_sub_category_id' => 'id']],
                    [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ]);
    }

    public function beforeValidate()
    {
       /* if ($this->category_id) {
            $category_map = (new Category)->getTreeUpperBound($this->category_id);
            $this->main_category_id = (int) $category_map[0];
            if (isset($category_map[1])) {
                $this->sub_category_id = (int) $category_map[1];
            }
            else{
               $this->sub_category_id = NULL; 
            }

            if (isset($category_map[2])) {
                $this->sub_sub_category_id = (int) $category_map[2];
            }
            else{
                $this->sub_sub_category_id = NULL;
            }
        }*/

        if (strlen($this->start_time) > 10) {
            $this->start_time = (int) (substr($this->start_time, 0, 10));
        }

        if (strlen($this->expire_time) > 10) {
            $this->expire_time = (int) (substr($this->expire_time, 0, 10));
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'main_category_id' => Yii::t('app', 'Main Category'),
            'sub_category_id' => Yii::t('app', 'Sub Category'),
            'sub_sub_category_id' => Yii::t('app', 'Sub Sub Category'),
            'title' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Slug'),
            'description' => Yii::t('app', 'Description'),
            'summary' => Yii::t('app', 'Summary'),
            'start_time' => Yii::t('app', 'Start Date'),
            'expire_time' => Yii::t('app', 'Expire Date'),
            'terms' => Yii::t('app', 'Terms'),
            'how_redeemed' => Yii::t('app', 'How to Redeemed'),
            'city_id' => Yii::t('app', 'City'),
            'state_id' => Yii::t('app', 'State'),
            'country_id' => Yii::t('app', 'Country'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'meta_title'=>Yii::t('app', 'Meta Title'),
            'meta_description'=>Yii::t('app', 'Meta Description'),
            'meta_keyword'=>Yii::t('app', 'Meta Keyword'),
	     
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id'])->inverseOf('deals');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'main_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id'])->inverseOf('deals');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'sub_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubSubCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'sub_sub_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id'])->inverseOf('deals');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDealGalleries()
    {
        return $this->hasMany(DealGallery::className(), ['deal_id' => 'id'])->orderBy(['sort_order' => SORT_ASC])->inverseOf('deal');
    }
    
    public function getDealShipping()
    {
        return $this->hasMany(DealShippingAmount::className(), ['deal_id' => 'id'])->inverseOf('deal');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDealCategory()
    {
        return $this->hasMany(DealCategory::className(), ['deal_id' => 'id'])->orderBy(['id' => SORT_ASC])->inverseOf('deal');
    }
    
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDealOptions()
    {
        return $this->hasMany(DealOption::className(), ['deal_id' => 'id'])->inverseOf('deal');
    }

    public function getDealAddress()
    {
        return $this->hasMany(DealAddress::className(), ['deal_id' => 'id'])->inverseOf('deal');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCart()
    {
        return $this->hasMany(Cart::className(), ['deal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDealReviews()
    {
        return $this->hasMany(DealReview::className(), ['deal_id' => 'id'])->inverseOf('deal');
    }

    public function getAvgRating()
    {
        return round($this->getDealReviews()->average('rate'));
    }

    public function getTotalReviews()
    {
        return round($this->getDealReviews()->count());
    }

    public function trimmedTitle($length = 100)
    {
        return substr($this->title, 0, $length) . ((strlen($this->title) > $length ? '...' : ''));
    }

    public function trimmedDescription($length = 100)
    {
        $description = strip_tags($this->description);
        return substr($description, 0, $length) . ((strlen($description) > $length ? '...' : ''));
    }

    public function getInStockDealOptions()
    {
        return $this->getDealOptions()
                        ->select(["*", "(IF(total_items = 0, 1000000, total_items) - total_sold) as pending_stock"])
                        ->having('pending_stock > 0');
    }

    // price offer 
    public function minlistOption()
    {
        return $this->getDealOptions()->orderBy('list_price asc')->one();
    }

    public function maxListOption()
    {
        return $this->getDealOptions()->orderBy('list_price desc')->one();
    }

    // discount offer
    public function minlistDiscountOffer()
    {
        return $this->getDealOptions()->orderBy('discount asc')->one();
    }

    public function maxListDiscountOffer()
    {
        return $this->getDealOptions()->orderBy('discount desc')->one();
    }

    public function minListDiscount()
    {
        return $this->minlistOption()->discount;
    }

    public function maxListDiscount()
    {
        return $this->maxListDiscountOffer()->discount;
    }

    public function minListPrice()
    {
        return $this->minlistOption()->list_price;
    }

    public function maxListPrice()
    {
        return $this->maxListOption()->list_price;
    }

    public function minSellingOption()
    {
        return $this->getDealOptions()->orderBy('selling_price asc')->one();
    }

    public function maxSellingtOption()
    {
        return $this->getDealOptions()->orderBy('selling_price desc')->one();
    }

    public function minSellingPrice()
    {
        return $this->minSellingOption()->selling_price;
    }

    public function maxSellingPrice()
    {
        return $this->maxSellingtOption()->selling_price;
    }

    public function minSaving()
    {
        return (int) $this->minListPrice() - $this->minSellingPrice();
    }

    public function mindiscount($percentage = True)
    {
        if($this->minListPrice())
        {
        return round(100 - ($this->minSellingPrice() / $this->minListPrice() * 100));
        }
        else{
            return 0;
        }
    }

    public function totalSold()
    {
        return (int) $this->getDealOptions()->sum('total_sold');
    }

    public function totalStock()
    {
        if ($this->getDealOptions()->andWhere(['total_items' => 0])->exists()) {
            return 1000000;
        }

        return (int) $this->getDealOptions()->sum('total_items');
    }
    
    public function isExpired(){
        if($this->expire_time >= time()){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    
    public function isShipping(){
        if($this->shipping_applied == 1){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function hasStock()
    {
        return (int) ($this->totalStock() - $this->totalSold());
    }

    public function getURL($action = 'view', $only_route = False, $full_url = False)
    {
        if (\Yii::$app->id !== 'app-frontend') {
            return parent::getURL("/deal/view", $only_route, $full_url);
        }

        return \common\components\VSHelper::setDealBasedURL('deal', ['slug' => $this->slug]);
    }

    public function getIconURL($size = 'medium', $full = True)
    {
        return $this->dealGalleries[0]->getIconURL($size, $full);
    }

    public static function find($active_only = True)
    {
        $find = parent::find($active_only);
        if (Yii::$app->id == 'app-frontend') {
            $time = time();
            $find->andWhere(['or', ['<', 'start_time', $time], ['>', 'expire_time', $time]]);
        }

        return $find;
    }

    public function disable()
    {
        if ($this->status == Deal::STATUS_INCOMPLETE) {
            return TRUE;
        } else {
            $this->status = Deal::STATUS_INCOMPLETE;
            return $this->update();
        }
    }

}
