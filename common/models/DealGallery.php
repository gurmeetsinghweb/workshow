<?php

namespace common\models;

use yii\imagine\Image;
use Yii;

/**
 * This is the model class for table "{{%deal_gallery}}".
 *
 * @property integer $id
 * @property integer $deal_id
 * @property integer $deal_option_id
 * @property integer $sort_order
 * @property string $name
 * @property string $size
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Deal $deal
 */
class DealGallery extends \common\components\GenxBaseModel
{

    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%deal_gallery}}';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return parent::rules([
                    [['deal_id', 'name'], 'required'],
                    [['sort_order'], 'default', 'value' => 20],
                    [['deal_id', 'deal_option_id', 'sort_order'], 'integer'],
                    [['imageFile'], 'file', 'skipOnEmpty' => !($this->isNewRecord), 'extensions' => 'png, jpg'],
                    [['size', 'name', 'description'], 'string', 'max' => 255],
                    [['deal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Deal::className(), 'targetAttribute' => ['deal_id' => 'id']],
                    [['deal_option_id'], 'exist', 'skipOnError' => true, 'targetClass' => DealOption::className(), 'targetAttribute' => ['deal_option_id' => 'id']],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'deal_id' => Yii::t('app', 'Deal ID'),
            'name' => Yii::t('app', 'Image'),
            'description' => Yii::t('app', 'Description'),
            'size' => Yii::t('app', 'Size'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeal()
    {
        return $this->hasOne(Deal::className(), ['id' => 'deal_id'])->inverseOf('dealGalleries');
    }

    public function upload()
    {
        if (!$this->validate()) {
            return False;
        }

        if ($this->isNewRecord) {
            $file_name = time() . '.' . $this->imageFile->extension;
        } else {
            $file_name = $this->name;
        }
        $image_save_path = \Yii::getAlias('@image_uploads/deals/');
        if (!file_exists($image_save_path)) {
            mkdir($image_save_path, 0755);
        }

        if ($this->imageFile) {
            $orignal_file_full_path = $image_save_path . $this->deal_id . '_orignal_' . $file_name;
            $saved_orignal = $this->imageFile->saveAs($orignal_file_full_path);
            $icons = [
                'large' => [
                    'width' => 900,
                    'height' => 600
                ],
                'medium' => [
                    'width' => 425,
                    'height' => 250
                ],
                'small' => [
                    'width' => 200,
                    'height' => 130
                ]
            ];

            if ($saved_orignal) {
                foreach ($icons as $size => $icon) {
                    Image::thumbnail($orignal_file_full_path, $icon['width'], $icon['height'])
                            ->save($image_save_path . $this->deal_id . '_' . $size . '_' . $file_name, array('jpeg_quality' => 100));
                }
                return $file_name;
            }
        }

        return False;
    }

    public function getIconURL($size = 'medium', $full = True)
    {
        $file_name = $this->deal_id . '_' . $size . '_' . $this->name;

        return \yii\helpers\Url::toRoute([((\Yii::$app->id == 'app-backend') ? ".." : "") . '/uploads/deals/' . $file_name], $full);
    }

}
