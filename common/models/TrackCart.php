<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "track_cart".
 *
 * @property integer $id
 * @property integer $cart_id
 * @property integer $track_id
 * @property string $url
 * @property string $company_name
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 */
class TrackCart extends \common\components\GenxBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'track_cart';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return parent::rules([
            [['cart_id', 'track_id', 'url', 'company_name'], 'required'],
            [['cart_id', 'track_id'], 'integer'],
            [['url', 'company_name'], 'string', 'max' => 120],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'cart_id' => Yii::t('app', 'Cart ID'),
            'track_id' => Yii::t('app', 'Track ID'),
            'url' => Yii::t('app', 'Url'),
            'company_name' => Yii::t('app', 'Company Name'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
    
    public function getCart()
    {
        return $this->hasOne(Cart::className(), ['id' => 'cart_id']);
    }
}
