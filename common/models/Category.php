<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%category}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property integer $parent_id
 * @property integer $icon'
 * @property string $fa'
 * @property integer $total_deals
 * @property integer $active_deals
 * @property integer $showhome
 * @property string $meta_keyword 
 * @property integer $serial_order

 */
class Category extends \common\components\GenxBaseModel
{

    private $deal_count_cache_duration = 1800;
    private $user_selected_city_info = Null;

    public function __construct($config = array())
    {
        if (Yii::$app->id == 'app-frontend') {
            // check selected city
            $selected_city = \Yii::$app->request->cookies->getValue('selected_city', False);
            if ($selected_city) {
                $this->user_selected_city_info = unserialize(base64_decode($selected_city));
            } else {
                $this->user_selected_city_info = City::getSelectedDefaultCity();
            }
        }
        return parent::__construct($config);
    }

    public function behaviors()
    {
        $parent_behaviors = parent::behaviors();
        $parent_behaviors[] = [
            'class' => \yii\behaviors\SluggableBehavior::className(),
            'attribute' => 'name',
            'ensureUnique' => True,
        ];

        return $parent_behaviors;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return parent::rules([
                    [['name', 'slug'], 'required'],
                    [['parent_id', 'total_deals', 'active_deals', 'showhome', 'serial_order'], 'integer'],
                    [['name', 'slug', 'icon', 'fa'], 'string', 'max' => 150],
                    [['meta_title', 'meta_description','meta_keyword'], 'string'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'slug' => Yii::t('app', 'Slug'),
            'icon' => Yii::t('app', 'Icon'),
            'showhome' => Yii::t('app', 'Show Home'),
            'serial_order' => Yii::t('app', 'Serial Order'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'fa' => Yii::t('app', 'Font Awesome'),
        ];
    }

    public function getParent()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }

    public function getLevel()
    {
        $upper_bound = $this->getTreeUpperBound();
        switch (count($upper_bound)) {
            case 1:
                return 'main_category_id';
            case 2:
                return 'sub_category_id';
            case 3:
                return 'sub_sub_category_id';
        }
    }

    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getDeals($location = TRUE)
    {

        // gurmeet 13 july 2017 [$this->getLevel() => 'id'] replace with via query ['id' => 'deal_id'])->via('dealCategory') and making relation 
        $relations = $this->hasMany(Deal::className(), ['id' => 'deal_id'])->via('dealCategory');
        if (Yii::$app->id == 'app-frontend') {
            $relations->joinWith('dealAddress');
            if ($location) {

                                $relations
                        ->distinct()
                        ->select('deal.*')
                        ->where(['and', ['<', 'start_time', time()], ['>=', 'expire_time', time()], ['deal.status' => Deal::STATUS_ACTIVE],['deal_address.city_id'=>$this->user_selected_city_info['id']]])
//->andWhere(['>=', 'expire_time', time()])
                        ->orderBy('created_at');


//                $relations
//                        ->distinct()
//                        ->select('deal.*, ( 6371 * acos( cos( radians(' . $this->user_selected_city_info['latitude'] . ') ) * cos( radians( deal_address.geo_lat) ) * cos( radians( deal_address.geo_long ) - radians(' . $this->user_selected_city_info['longitude'] . ') ) + sin( radians(' . $this->user_selected_city_info['latitude'] . ') ) * sin( radians( deal_address.geo_lat ) ) ) ) AS distance')
//                        ->having(['<', 'distance', Yii::$app->params['searchRadius']])
//                        ->where(['and', ['<', 'start_time', time()], ['>=', 'expire_time', time()], ['deal.status' => Deal::STATUS_ACTIVE]])
////->andWhere(['>=', 'expire_time', time()])
//                        ->orderBy('distance');
            } else {
                $relations
                        ->distinct()
                        ->where(['and', ['<', 'start_time', time()], ['>=', 'expire_time', time()], ['deal.status' => Deal::STATUS_ACTIVE]]);
//->andWhere(['>=', 'expire_time', time()])
            }
            // \common\components\GenXHelper::c($relations) ."<br>";
        }

        //\common\components\GenXHelper::c($relations);
        return $relations;
    }

    public function getSubCategory($pid, $location = True)
    {
        $subcategory = self::find()->andWhere(['parent_id' => $pid])->orderBy(["name" => SORT_ASC])->all();
        foreach ($subcategory as $value) {
            $value->active_deals = \Yii::$app->cache->get($this->getCategoryCacheKey($value->id . "-" . (int) $location));
            if ($value->active_deals === False) {
                $value->active_deals = (int) $value->getDeals($location)->count();
                \Yii::$app->cache->set($this->getCategoryCacheKey($value->id . "-" . (int) $location), $value->active_deals, $this->deal_count_cache_duration);
            }
        }

        return $subcategory;
    }

    public function getTree($get_active_count = False, $location = True)
    {

        $categories = self::find()->andWhere(['status' => 10])->orderBy(['id' => SORT_DESC])->all();
        //  \common\components\GenXHelper::c($categories);
        $final_data = [];
        foreach ($categories as $_category) {
            $category = $_category->attributes;
            if ($get_active_count) {
                $category['active_deals'] = \Yii::$app->cache->get($this->getCategoryCacheKey($_category->id . "-" . (int) $location));
                if ($category['active_deals'] === False) {
                    // 12_07_2017 - gurmeet Sharma replaced: $_category->getDeals()->count();
                    $category['active_deals'] = (int) count($_category->getDeals($location)->asArray()->all());
                    \Yii::$app->cache->set($this->getCategoryCacheKey($_category->id . "-" . (int) $location), $category['active_deals'], $this->deal_count_cache_duration);
                }
            }

            if (isset($final_data[$category['id']])) {
                $category['children'] = $final_data[$category['id']];
                if ($get_active_count) {
                    foreach ($final_data[$category['id']] as $_cat_1) {
                        //$category['active_deals'] += $_cat_1['active_deals'];
                    }
                }
                unset($final_data[$category['id']]);
            }
            $final_data[(int) $category['parent_id']][] = $category;
        }

        return $final_data;
    }

    public function getHeaderTree($get_active_count = False, $location = True)
    {
        $categories = self::find()->andWhere(['status' => 10, 'parent_id' => 0])->orderBy(['id' => SORT_DESC])->all();
        //  \common\components\GenXHelper::c($categories);
        $final_data = [];
        foreach ($categories as $_category) {
            $category = $_category->attributes;
            if ($get_active_count) {
                $category['active_deals'] = \Yii::$app->cache->get($this->getCategoryCacheKey($_category->id . "-" . (int) $location));
                if ($category['active_deals'] === False) {
                    // 12_07_2017 - gurmeet Sharma replaced: $_category->getDeals()->count();
                    $category['active_deals'] = (int) count($_category->getDeals($location)->asArray()->all());
                    \Yii::$app->cache->set($this->getCategoryCacheKey($_category->id . "-" . (int) $location), $category['active_deals'], $this->deal_count_cache_duration);
                }
            }

            if (isset($final_data[$category['id']])) {
                $category['children'] = $final_data[$category['id']];
                if ($get_active_count) {
                    foreach ($final_data[$category['id']] as $_cat_1) {
                        //$category['active_deals'] += $_cat_1['active_deals'];
                    }
                }
                unset($final_data[$category['id']]);
            }
            $final_data[(int) $category['parent_id']][] = $category;
        }

        return $final_data;
    }

    public function getTreeForDropdown($options = array())
    {
        $default_options = [
            'blank' => False,
            'prompt_text' => '--None--',
            'level' => 1,
            'indexedBy' => 'id',
        ];
        $options = array_merge($default_options, $options);
        $all_categories = $this->getTree();
        //\common\components\GenXHelper::c($all_categories);
        $final_array = [];
        if ($options['blank']) {
            $final_array[0] = $options['prompt_text'];
        }
        if (sizeof($all_categories)) {
            foreach ($all_categories as $parent => $_services) {
                foreach ($_services as $service) {
                    $final_array[$service[$options['indexedBy']]] = $service['name'];
                    if ($options['level'] >= 1 && isset($service['children'])) {
                        foreach ($service['children'] as $child_1) {
                            $final_array[$child_1[$options['indexedBy']]] = $service['name'] . " : " . $child_1['name'];
                            if ($options['level'] >= 2 && isset($child_1['children'])) {
                                foreach ($child_1['children'] as $child_2) {
                                    $final_array[$child_2[$options['indexedBy']]] = $service['name'] . " : " . $child_1['name'] . " : " . $child_2['name'];
                                }
                            }
                        }
                    }
                }
            }
        }

        return $final_array;
    }

    public function getCategoryTreeForDropdown()
    {
        $categories = self::find()->andWhere(['status' => 10])->orderBy(['parent_id' => SORT_ASC, 'name' => SORT_ASC])->all();


        $return = [];
        if (sizeof($categories)) {
            foreach ($categories as $cat) {
                $getUper = $this->getTreeUpperBound($cat->id);
                $getcats = "";
                foreach ($getUper as $cats) {
                    $getcat = self::findOne($cats);
                    $getcats .= $getcat->name . " : ";
                }
                $return[$cat->id] = $getcats;
            }
        }

        // send with json headers
        // header('Content-Type: application/json');
        return $return;
    }

    public function getTreeUpperBound($id = Null)
    {
        $category_tree = [];
        if (!$id) {
            $category = $this;
        } else {
            if (!isset(static::$_CACHE['CATEGORY_' . $id])) {
                static::$_CACHE['CATEGORY_' . $id] = self::findOne($id);
            }
            $category = static::$_CACHE['CATEGORY_' . $id];
        }
        if ($category) {
            array_unshift($category_tree, $category->id);
            if ($category->parent_id) {
                $parent_1 = $category->parent;
                if ($parent_1) {
                    array_unshift($category_tree, $parent_1->id);
                    if ($parent_1->parent_id) {
                        $parent_2 = $parent_1->parent;
                        if ($parent_2) {
                            array_unshift($category_tree, $parent_2->id);
                        }
                    }
                }
            }
        }

        return $category_tree;
    }

    function getDealCategory()
    {
        return $this->hasMany(DealCategory::className(), ['category_id' => 'id'])->inverseOf('category');
    }

    function getCategoryCacheKey($unique_id)
    {
        return 'CATEGORY_ACTIVE_DEAL_COUNT_' . $unique_id . '_' . md5($this->user_selected_city_info['latitude'] . "_" . $this->user_selected_city_info['longitude']);
    }

}
