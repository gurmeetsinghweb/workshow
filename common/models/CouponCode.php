<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "coupon_code".
 *
 * @property integer $id
 * @property integer $cart_id
 * @property string $coupon_no
 * @property string $coupon_file
 * @property string $coupon_path 
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $expires_at
 *
 * @property Cart $cart
 */
class CouponCode extends \common\components\GenxBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coupon_code';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return parent::rules( [
            [['cart_id', 'coupon_no', 'expires_at'], 'required'],
            [['cart_id', 'expires_at'], 'integer'],
            [['coupon_no'], 'string', 'max' => 100],
            [['coupon_file','coupon_path'],'string'],
            [['cart_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cart::className(), 'targetAttribute' => ['cart_id' => 'id']],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cart_id' => 'Cart ID',
            'coupon_no' => 'Coupon No',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCart()
    {
        return $this->hasOne(Cart::className(), ['id' => 'cart_id']);
    }
    
    public function getExiresDate() {
        return date('F j, Y', $this->expires_at);
    }
    
    public function isRedeemed() {
        return ($this->status === self::STATUS_REDEEMED);
    }
}
