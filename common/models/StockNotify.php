<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "stock_notify".
 *
 * @property integer $id
 * @property string $email
 * @property integer $deal_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 */
class StockNotify extends \common\components\GenxBaseModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stock_notify';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = [])
    {
        return parent::rules([
                        [['email', 'deal_id'], 'required'],
                        [['deal_id'], 'integer'],
                        [['email'], 'email'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'deal_id' => Yii::t('app', 'Deal ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
    
     public function afterSave($insert, $changedAttributes)
    {
        try {
            $newsletter = new \common\models\Newsletter();
            $newsletter->email = $this->email;
            $newsletter->save();
        } catch (Exception $e) {
            
        }

        return parent::afterSave($insert, $changedAttributes);
    }

}
