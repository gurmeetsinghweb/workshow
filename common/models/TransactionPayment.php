<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "transaction_payment".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property TransactionLog[] $transactionLogs
 */
class TransactionPayment extends \common\components\GenxBaseModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return parent::rules($rules);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactionLogs()
    {
        return $this->hasMany(TransactionLog::className(), ['transaction_payment_id' => 'id']);
    }

    public function getCart()
    {
        return $this->hasMany(Cart::className(), ['transaction_payment_id' => 'id']);
    }

}
