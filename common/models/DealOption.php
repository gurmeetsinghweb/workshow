<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%deal_option}}".
 *
 * @property integer $id
 * @property integer $deal_id
 * @property string $name
 * @property string $slug
 * @property integer $list_price
 * @property integer $selling_price
 * @property integer $no_per_person
 * @property integer $total_sold
 * @property integer $total_redeem
 * @property integer $total_items
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $discount
 * 
 *
 * @property Deal $deal
 */
class DealOption extends \common\components\GenxBaseModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%deal_option}}';
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => \yii\behaviors\SluggableBehavior::className(),
            'attribute' => 'name',
            'ensureUnique' => True
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return parent::rules(
                        [
                            [['total_sold', 'total_items'], 'default', "value" => 0],
                            [['deal_id', 'name', 'list_price', 'selling_price'], 'required'],
                            [['deal_id', 'no_per_person', 'total_sold','total_redeem', 'total_items','discount'], 'integer'],
                            [['list_price', 'selling_price'], 'double'],
                            [['name', 'slug'], 'string', 'max' => 255],
                            [['deal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Deal::className(), 'targetAttribute' => ['deal_id' => 'id']],
                        ]
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'deal_id' => Yii::t('app', 'Deal ID'),
            'name' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Slug'),
            'list_price' => Yii::t('app', 'List Price'),
            'selling_price' => Yii::t('app', 'Selling Price'),
            'no_per_person' => Yii::t('app', 'No Per Person'),
            'total_sold' => Yii::t('app', 'Total Sold'),
            'total_redeem' => Yii::t('app', 'Total Redeem'),
             'status' => Yii::t('app', 'Status'),
             'discount' => Yii::t('app', 'Discount'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeal()
    {
        return $this->hasOne(Deal::className(), ['id' => 'deal_id'])->inverseOf('dealOptions');
    }

    public function getCart()
    {
        return $this->hasMany(Cart::className(), ['deal_option_id' => 'id']);
    }

    public function getPendingStock() {
        if(!$this->total_items) {
            return 1000000;
        }
        return ($this->total_items - $this->total_sold);
    }
    
    public function isInStock() {
        if($this->getPendingStock()<=0){
            return False;
        }else{
            return True;
        }
    }
    
    public function totalSaving()
    {
        return round($this->list_price - $this->selling_price);
    }

    public function discount()
    {
        if($this->list_price)
        {   
        return (100 - round(($this->selling_price / $this->list_price) * 100));
        }
        else{
            return 0;
        }
    }

    public function totalSold()
    {
        return $this->getCart()->sum('qty');
    }

}
