<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CouponCode;

/**
 * CouponCodeSearch represents the model behind the search form about `common\models\CouponCode`.
 */
class CouponCodeSearch extends CouponCode
{
    /**
     * @inheritdoc
     */
    public function rules($array = [])
    {
        return [
            [['id', 'cart_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['coupon_no'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CouponCode::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'coupon_code.id' => $this->id,
            'coupon_code.cart_id' => $this->cart_id,
            'coupon_code.status' => $this->status,
            'coupon_code.created_at' => $this->created_at,
            'coupon_code.updated_at' => $this->updated_at,
        ]);
// 13/7/2017 gurmeet apply  command leftjoin  remove ->join('LEFT JOIN','deal_category','deal.id=deal_category.deal_id')
        $query->joinWith('cart')->joinWith('cart.deal');
        $query->andFilterWhere(['like', 'coupon_no', $this->coupon_no]);

        return $dataProvider;
    }
}
