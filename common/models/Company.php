<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%company}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $title
 * @property string $slug
 * @property double $geo_lat
 * @property double $geo_long
 * @property string $logo
 * @property string $address
 * @property integer $city_id
 * @property integer $state_id
 * @property integer $country_id
 * @property string $contact
 * @property string $website
 * @property string $facebook
 * @property string $twitter
 * @property string $youtube
 * @property string $gplus
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 * @property Country $country
 * @property State $state
 * @property User $user
 */
class Company extends \common\components\GenxBaseModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company}}';
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => \yii\behaviors\SluggableBehavior::className(),
            'attribute' => 'title',
            'ensureUnique' => True
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        $rules = [
            [['user_id', 'city_id', 'state_id', 'country_id'], 'integer'],
            [['geo_lat', 'geo_long','contact'], 'number'],
            [['title', 'slug', 'address', 'trading_name'], 'string', 'max' => 255],
            [['website', 'facebook', 'twitter', 'youtube', 'gplus'], 'url'],
            [['user_id'], 'default', 'value' => ((\Yii::$app->user->merchant()) ? \Yii::$app->user->merchant()->id : Null)],
            [['title', 'user_id'], 'required'],
            [['description', 'abn_number'], 'string'],
            [['logo'], 'file', 'extensions' => 'png, jpg'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['state_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];

        return parent::rules($rules);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'title' => Yii::t('app', 'Title'),
            'trading_name' => Yii::t('app', 'Trading Name'),
            'description' => Yii::t('app', 'Description'),
            'slug' => Yii::t('app', 'Slug'),
            'geo_lat' => Yii::t('app', 'Geo Lat'),
            'geo_long' => Yii::t('app', 'Geo Long'),
            'logo' => Yii::t('app', 'Logo'),
            'address' => Yii::t('app', 'Address'),
            'city_id' => Yii::t('app', 'City ID'),
            'state_id' => Yii::t('app', 'State ID'),
            'country_id' => Yii::t('app', 'Country ID'),
            'contact' => Yii::t('app', 'Contact No'),
            'website' => Yii::t('app', 'Website'),
            'abn_number' => Yii::t('app', 'A.B.N. Number'),
            'facebook' => Yii::t('app', 'Facebook'),
            'twitter' => Yii::t('app', 'Twitter'),
            'youtube' => Yii::t('app', 'Youtube'),
            'gplus' => Yii::t('app', 'Gplus'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id'])->inverseOf('companies');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id'])->inverseOf('companies');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id'])->inverseOf('companies');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id'])->inverseOf('company');
    }

    public function getURL($action = 'view', $only_route = False, $full_url = False)
    {
        $route = ['/user/view', "id" => $this->user_id, 'selected_tab' => 'company-profile'];
        if ($only_route) {
            return $route;
        }
        return \yii\helpers\Url::toRoute($route, $full_url);
    }

}
