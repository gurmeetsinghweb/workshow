<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "deal_category".
 *
 * @property integer $id
 * @property integer $deal_id
 * @property integer $category_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property Category $category
 */
class DealCategory extends \common\components\GenxBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal_category';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return parent::rules([
            [['deal_id', 'category_id'], 'required'],
            [['deal_id', 'category_id'], 'integer'],
            [['deal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Deal::className(), 'targetAttribute' => ['deal_id' => 'id']],
         ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'deal_id' => 'Deal ID',
            'category_id' => 'Category ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeal()
    {
        return $this->hasMany(Deal::className(), ['id' => 'deal_id'])->inverseOf('dealCategory');
    }
    // relation with category
     public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id'])->inverseOf('dealCategory');
    }
}
