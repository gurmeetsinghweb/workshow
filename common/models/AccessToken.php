<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%access_token}}".
 *
 * @property integer $id
 * @property string $token
 * @property integer $created_at
 * @property integer $lifetime
 * @property integer $user_id
 *
 * @property User $user
 */
class AccessToken extends \common\components\GenxBaseModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%access_token}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return parent::rules([
                    [['created_at', 'lifetime', 'user_id'], 'integer'],
                    [['token'], 'string', 'max' => 100],
                    [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
                        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'token' => Yii::t('app', 'Token'),
            'created_at' => Yii::t('app', 'Created At'),
            'lifetime' => Yii::t('app', 'Lifetime'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }

    public function beforeSave($insert)
    {
//        self::updateAll(['lifetime' => 0], ['user_id' => $this->user_id]);
        return parent::beforeSave($insert);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function generatePublicToken($lifetime)
    {

        if (!$this->token) {
            $_token = Yii::$app->security->generateRandomString(40);
        } else {
            $_token = explode(".", $this->token)[0];
        }

        $time = $this->created_at;
        if (!$time) {
            $time = time();
        }

        return $_token
                . "."
                . ($time + $lifetime)
                . "."
                . md5($lifetime . $_token);
    }

    public function verifyPublicToken($token)
    {
        $_token = explode('.', $token);

        // token has expired
        if ($this->isExpired()) {
            return False;
        }

        // second arg should be same with the sum of created_at and lifetime
        if ((int) ($this->created_at + $this->lifetime) != (int) $_token[1]) {
            return False;
        }

        // recreate token to check verify
        if ($token != $this->generatePublicToken((int) $this->lifetime)) {
            return False;
        }

        return True;
    }

    public function isExpired()
    {
        return (((int) ($this->created_at + $this->lifetime)) < time());
    }

    public static function find($active_only = False)
    {
        return parent::find($active_only);
    }

}
