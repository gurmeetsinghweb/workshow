<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property integer $sms_otp
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $mobile
 * @property integer $status
 * @property string $type
 * @property string $name
 * @property string $nationality
 * @property integer $country_id
 * @property integer $dob
 * @property integer $send_password
 * @property integer $yob
 * @property string $hint_question
 * @property string $hint_answer
 * @property integer $agreed
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $address
 * 
 * @property AccessToken $accessToken 
 */
class User extends \common\components\GenxBaseModel implements IdentityInterface
{

    const ROLE_SYSADMIN = 'sysadmin';
    const ROLE_ADMIN = 'admin';
    const ROLE_MANAGER = 'manager';
    const ROLE_MERCHANT = 'merchant';
    const ROLE_USER = 'user';
    const ROLE_CSR = 'customer_service_rep';
    const ROLE_CSM = 'customer_service_manager';
    const ROLE_SR = 'sales_rep';
    const ROLE_SM = 'sales_manager';
    //
    const STATUS_DELETED = 0;
    const STATUS_PENDING_VALIDATION = 1;
    const STATUS_PENDING_ACTIVATION = 2;
    const STATUS_ACTIVE = 10;

    // dummy fields
    public $dob_d;
    public $dob_m;
    public $dob_y;
    public $old_password;
    public $password;
    public $password_repeat;
    public $verificationCode;
    public $email_repeat;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => \yii\behaviors\SluggableBehavior::className(),
            'attribute' => 'name',
            'ensureUnique' => True
        ];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return [
            [['auth_key', 'password_hash', 'email', 'type', 'name'], 'required'],
            [['old_password', 'password', 'password_repeat'], 'required', 'on' => 'password_change'],
            [['password', 'password_repeat'], 'required', 'on' => 'signup'],
            [['email_repeat', 'password'], 'required', 'on' => 'guest-checkout'],
            [['agreed'], 'required', 'on' => 'guest-checkout', 'requiredValue' => 1, "message" => "Terms must be agreed",],
            [['sms_otp', 'status', 'dob', 'yob', 'agreed', 'created_at', 'updated_at', 'country_id', 'state_id', 'city_id','send_password'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'name', 'hint_question', 'hint_answer', 'old_password'], 'string', 'max' => 255],
            [['agreed'], 'required', 'on' => 'signup', 'requiredValue' => 1, "message" => "Terms must be agreed",],
            [['image_url'], 'image', 'extensions' => 'jpg, jpeg'],
            [['username'], 'string', 'min' => 3],
            [['name'], 'string', 'min' => 3],
            [['name'], 'match', 'pattern' => '/^[a-z ]*$/i', "message" => "Only alphabets are allowed"],
            [['username'], 'match', 'pattern' => '/^[a-z0-9_]*$/i', "message" => "Only alphabets, Numbers and '_' allowed"],
            [['password'], 'string', 'min' => 6],
            [['hint_answer'], 'string', 'min' => 3],
            [['mobile'], 'number', 'min' => 100000000, 'max' => 999999999],
            [['mobile'], 'default', 'value' => Null],
            [['auth_key'], 'string', 'max' => 32],
            [['type', 'nationality'], 'string', 'max' => 50],
            [['username'], 'unique', 'targetAttribute' => ['username']],
            [['email'], 'unique', 'targetAttribute' => ['email']],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            [['sms_otp'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['password_repeat'], 'compare', 'compareAttribute' => 'password', 'message' => "Both Passwords must match"],
            [['email_repeat'], 'compare', 'compareAttribute' => 'email', 'message' => "Both email must match"],
            ['status', 'in', 'range' => range(self::STATUS_DELETED, self::STATUS_ACTIVE)],
            ['verificationCode', 'captcha', 'when' => function() {
                    if (Yii::$app->id === 'app-console') {
                        return False;
                    }
                }],
            ['address', 'string', 'max' => 255],
            
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'sms_otp' => Yii::t('app', 'Sms Otp'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Email'),
            'mobile' => Yii::t('app', 'Mobile'),
            'status' => Yii::t('app', 'Status'),
            'type' => Yii::t('app', 'Type'),
            'name' => Yii::t('app', 'Name'),
            'nationality' => Yii::t('app', 'Nationality'),
            'country_id' => Yii::t('app', 'Country'),
            'state_id' => Yii::t('app', 'State'),
            'city_id' => Yii::t('app', 'City'),
            'dob' => Yii::t('app', 'Date of Birth'),
            'yob' => Yii::t('app', 'Year of Birth'),
            'hint_question' => Yii::t('app', 'Hint Question'),
            'hint_answer' => Yii::t('app', 'Hint Answer'),
            'agreed' => Yii::t('app', 'I Agree all terms and conditions'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'image_url' => Yii::t('app', 'Avatar'),
            'email_repeat' => Yii::t('app', 'Confirm Email'),
            'address' => Yii::t('app', 'Address'),
	     'shipping_name' => Yii::t('app', 'Name'),
            'shipping_address' => Yii::t('app', 'Address'),
            'shipping_state' => Yii::t('app', 'State'),
            'shipping_city' => Yii::t('app', 'City'),
            'shipping_postcode' => Yii::t('app', 'Postcode'),
            'shipping_mobile' => Yii::t('app', 'Mobile'),
        ];
    }

    /**
     * 
     * @return type
     */
    public function beforeValidate()
    {
        if (Yii::$app->id !== 'app-console') {
            if (!empty($this->password_repeat)) {
                $this->setPassword($this->password_repeat);
                if ($this->isNewRecord) {
                    if (empty($this->username)) {
                        $this->username = uniqid();
                    }
                    $this->generateAuthKey();
                    $this->generateSmsOTP();
                    $this->generateAccessToken();
                }
            }
        }

        return parent::beforeValidate();
    }

    /**
     * 
     * @param type $insert
     * @param type $changedAttributes
     * @return boolean
     */
    public function afterSave($insert, $changedAttributes)
    {
        // lets assign role here
        $role = \Yii::$app->getAuthManager()->getRole($this->type);
        \Yii::$app->getAuthManager()->revokeAll($this->id);
        \Yii::$app->getAuthManager()->assign($role, $this->id);
        $saved = True;

        try {
            $newsletter = new \common\models\Newsletter();
            $newsletter->email = $this->email;
            $newsletter->save();
        } catch (Exception $e) {
            
        }

        return $saved;
    }

    public function delete()
    {
        $time = time();
        $this->status = User::STATUS_DELETED;
        $this->username = "DELETED_{$time}_" . $this->username;
        $this->email = "DELETED_{$time}_" . $this->email;
        $this->mobile = "DELETED_{$time}_" . $this->mobile;
        return $this->save();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    
    public function getShippingstate()
    {
        return $this->hasOne(State::className(), ['id' => 'shipping_state']);
    }
    
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['user_id' => 'id'])->inverseOf('user');
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username, $status = 10)
    {
        return static::findOne(['username' => $username, 'status' => $status]);
    }

    /**
     * Finds user by username
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email, $status = 10)
    {
        return static::findOne(['email' => $email, 'status' => $status]);
    }

    /**
     * Finds user by auth_key
     *
     * @param string $auth_key
     * @return static|null
     */
    public static function findByAuthKey($auth_key, $status = 10)
    {
        return static::findOne(['auth_key' => $auth_key, 'status' => $status]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {

            return null;
        }

        return static::findOne([
                    'password_reset_token' => $token,
                    'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function getAuth_token()
    {
        return $this->accessToken->token;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function generateAccessToken($lifetime = 60)
    {
        $accessToken = new AccessToken();
        $accessToken->lifetime = $lifetime;
        $accessToken->token = $accessToken->generatePublicToken($lifetime);
        $accessToken->user_id = $this->id;
        if ($accessToken->save()) {
            return $accessToken->token;
        } else {
            $this->addError('AccessToken', $accessToken->getErrors());
        }
        return False;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $_token = explode('.', $token);
        // Token must have 3 values
        if (sizeof($_token) !== 3) {
            return Null;
        }

        $accessToken = AccessToken::findOne(['token' => $token]);
        // if no access token is found, then just return back
        if (!$accessToken) {
            return Null;
        }

        // check if access token is still valid
        if (!$accessToken->verifyPublicToken($token)) {
            return Null;
        }

        // if all good, then lets return the user
        return $accessToken->user;
    }

    public function getAccessToken()
    {
        return $this->hasOne(AccessToken::className(), ['user_id' => 'id'])->orderBy('created_at DESC');
    }

    public function generateSmsOTP()
    {
        $this->sms_otp = $this->id . substr(time(), -6, 6);
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getHintQuestions()
    {
        return [
            1001 => "What is your vehicle number ?",
            1002 => "What is the name of your pet ?",
            1003 => "Which is your favourite place ?",
            1004 => "Which is your favourite food ?",
        ];
    }

    public function afterFind()
    {
        $this->dob_y = date('Y', $this->dob);
        $this->dob_d = date('j', $this->dob);
        $this->dob_m = date('n', $this->dob);
        return parent::afterFind();
    }

    public function getIconURL($size = "medium", $route = '')
    {
        if (strlen($this->image_url)) {
            $image_name = explode(".", $this->image_url);
            return \yii\helpers\Url::toRoute("{$route}/uploads/users/" . $image_name[0] . "_" . $size . "." . $image_name[1]);
        }
        return "https://www.gravatar.com/avatar/" . md5(strtolower(trim($this->email))) . "?s=50";
    }

    public function getUserTypeDropdownForForm()
    {
        return [
            self::ROLE_ADMIN => self::ROLE_ADMIN,
            self::ROLE_MANAGER => self::ROLE_MANAGER,
            self::ROLE_SM => self::ROLE_SM,
            self::ROLE_SR => self::ROLE_SR,
            self::ROLE_CSM => self::ROLE_CSM,
            self::ROLE_CSR => self::ROLE_CSR,
            //self::ROLE_MERCHANT => self::ROLE_MERCHANT,
            self::ROLE_USER => self::ROLE_USER,
        ];
    }

    public function getUserStatusDropdownForForm()
    {
        return [
            self::STATUS_ACTIVE => "Active",
            self::STATUS_PENDING_VALIDATION => "Validation Pending",
        ];
    }

    public function getAdminViewURL()
    {
        return \yii\helpers\Url::toRoute(['/user/view/', 'id' => $this->id]);
    }

    public function getEmailValidationLink()
    {
        return \yii\helpers\Url::toRoute(['/site/validate-email', 'hash' => $this->getAuthKey()], True);
    }

    public function getDeals()
    {
        return $this->hasMany(Deal::className(), ['user_id' => 'id']);
    }

    public function hasPurchased($deal_id, $deal_option_id = Null)
    {
        return Cart::find()
                        ->andWhere(['user_id' => $this->id])
                        ->andWhere(['deal_id' => $deal_id])
                        ->andWhere(['>', 'order_id', 0])
                        ->andFilterWhere(['deal_option_id' => $deal_option_id])
                        ->exists();
    }

    public function getAccount()
    {
        return $this->hasMany(UserAccount::className(), ['user_id' => 'id']);
    }

    public function getAccountBalance()
    {
        $query = $this->getAccount();
        $query->select('*, (SUM(credit) - SUM(debit)) AS balance');
        $data = $query->one();

        if ($data) {
            return (int) $data->balance;
        }

        return 0;
    }

}
