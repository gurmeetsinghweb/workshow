<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\City;

/**
 * CitySearch represents the model behind the search form about `common\models\City`.
 */
class CitySearch extends City
{

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return [
            [['id', 'country_id', 'state_id','featured','show_home'], 'integer'],
            [['name', 'slug', 'iso_2_char', 'iso_3_char', 'zip_code', 'search_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = City::find();

        $dataProvider = new \common\components\GenXDataProvider([
            'query' => $query,
        ]);
        if (is_null($this->status) || empty($this->status)) {
            $query->andWhere(["status" => User::STATUS_ACTIVE]);
        }

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'country_id' => $this->country_id,
            'state_id' => $this->state_id,
            'zip_code' => $this->zip_code,
            'featured'=>  $this->featured,
            'show_home'=>$this->show_home
        ]);
        
        $query->andFilterWhere(['like', 'name', $this->name])
        ->andFilterWhere(['like', 'search_name', $this->search_name])
                ->andFilterWhere(['like', 'iso_2_char', $this->iso_2_char])
                ->andFilterWhere(['like', 'iso_3_char', $this->iso_3_char])
                ->andFilterWhere(['like', 'iso_4_char', $this->iso_4_char]);

        return $dataProvider;
    }

}
