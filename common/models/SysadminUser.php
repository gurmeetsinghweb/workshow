<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%sysadmin_user}}".
 *
 * @property integer $id
 *
 * @property User $id0
 */
class SysadminUser extends \common\components\GenxBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sysadmin_user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return [
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }
}
