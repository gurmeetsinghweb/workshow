<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "gift_send".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $cart_id
 * @property integer $gift_name
 * @property string $gift_email
 * @property string $gift_message
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Cart $cart
 * @property User $user
 */
class GiftSend extends \common\components\GenxBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gift_send';
    }

    /**
     * @inheritdoc
     */
     public function rules($rules = array())
   {
         return parent::rules([
           [['user_id', 'cart_id', 'gift_name', 'gift_email', 'gift_message','gift_from'], 'required'],
            [['user_id', 'cart_id'], 'integer'],
            [['gift_message','gift_name','gift_from'], 'string'],
            [['gift_email'], 'string', 'max' => 255],
            ['gift_email', 'email'],
            [['cart_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cart::className(), 'targetAttribute' => ['cart_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'cart_id' => 'Cart ID',
            'gift_name' => 'Gift Name',
            'gift_email' => 'Gift Email',
            'gift_message' => 'Gift Message',
            'gift_from' => 'Gift From',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCart()
    {
        return $this->hasOne(Cart::className(), ['id' => 'cart_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
