<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%newsletter}}".
 *
 * @property integer $id
 * @property string $email
 * @property integer $local_deals
 * @property integer $created_at
 * @property integer $updated_at
 */
class Newsletter extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%newsletter}}';
    }

    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['local_deals', 'created_at', 'updated_at'], 'integer'],
            [['email'], 'string', 'max' => 255],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'local_deals' => 'Local Deals',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

}
