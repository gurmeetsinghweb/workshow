<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%deal_review}}".
 *
 * @property integer $id
 * @property integer $deal_id
 * @property integer $cart_id
 * @property string $review
 * @property integer $rate
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Deal $deal
 */
class DealReview extends \common\components\GenxBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%deal_review}}';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return parent::rules([
            [['deal_id', 'deal_option_id', 'rate', 'user_id','cart_id'], 'required'],
            [['deal_id', 'deal_option_id', 'user_id','cart_id'], 'integer'],
            [['rate'], 'double'],
            [['review', 'title'], 'string'],
            [['deal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Deal::className(), 'targetAttribute' => ['deal_id' => 'id']],
            [['cart_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cart::className(), 'targetAttribute' => ['cart_id' => 'id']],
            [['deal_option_id'], 'exist', 'skipOnError' => true, 'targetClass' => DealOption::className(), 'targetAttribute' => ['deal_option_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => DealOption::className(), 'targetAttribute' => ['deal_option_id' => 'id']],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'deal_id' => Yii::t('app', 'Deal ID'),
            'review' => Yii::t('app', 'Review'),
            'rate' => Yii::t('app', 'Rate'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    
    public function getCart()
    {
        return $this->hasOne(Cart::className(), ['id' => 'cart_id']);
    }
    
    public function getDeal()
    {
        return $this->hasOne(Deal::className(), ['id' => 'deal_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDealOption()
    {
        return $this->hasOne(DealOption::className(), ['id' => 'deal_option_id']);
    }
}
