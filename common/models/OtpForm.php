<?php

namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class OtpForm extends Model {

    public $otp;
    public $verificationCode;
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            // username and password are both required
            [['otp'], 'required'],
            [['otp'], 'exist', 'targetClass' => '\common\models\User', 'targetAttribute' => 'sms_otp', 'message' => 'Invalid Code'],
            ['verificationCode', 'captcha']
        ];
    }

    public function attributeLabels() {
        return [
            'otp' => 'OTP Code'
        ];
    }

    public function validateUser($auth_key) {
        if ($this->validate() && $this->getUser($auth_key)) {
            $this->getUser($auth_key)->status = User::STATUS_ACTIVE;
            if($this->getUser($auth_key)->save()) {
                return True;
            } else {
                $this->addErrors($this->getUser()->getErrors());
            }
        }

        return False;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser($auth_key) {
        if ($this->_user === null) {
            $this->_user = User::findByAuthKey($auth_key, User::STATUS_PENDING_VALIDATION);
        }

        return $this->_user;
    }

}
