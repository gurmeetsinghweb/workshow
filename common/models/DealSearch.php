<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Deal;

/**
 * DealSearch represents the model behind the search form about `common\models\Deal`.
 */
class DealSearch extends Deal
{

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return [
            [['id', 'company_id', 'start_time', 'expire_time', 'city_id', 'state_id', 'country_id', 'status', 'created_at', 'updated_at', 'user_id'], 'integer'],
            [['title', 'slug', 'expire_time', 'description', 'summary', 'terms'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$location = True)
    {
        $query = self::find()->distinct();

        // add conditions that should always apply here

        $dataProvider = new \common\components\GenXDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        // check selected city
        if (Yii::$app->id == 'app-frontend') {
            
            if($location)
            {
            $selected_city = \Yii::$app->request->cookies->getValue('selected_city', False);
            $query->joinWith('dealAddress');
            if ($selected_city) {
                $user_selected_city_info = unserialize(base64_decode($selected_city));
            } else {
                $user_selected_city_info = City::getSelectedDefaultCity();
            }
//            \common\components\GenXHelper::c($user_selected_city_info);

            // comment lat long
//                                ->select('deal.*, ( 6371 * acos( cos( radians(' . $user_selected_city_info['latitude'] . ') ) * cos( radians( deal_address.geo_lat) ) * cos( radians( deal_address.geo_long ) - radians(' . $user_selected_city_info['longitude'] . ') ) + sin( radians(' . $user_selected_city_info['latitude'] . ') ) * sin( radians( deal_address.geo_lat ) ) ) ) AS distance')
//                    ->having(['<', 'distance', Yii::$app->params['searchRadius']])
//                    ->where(['and',['<', 'start_time', time()],['>=', 'expire_time', time()],['deal.status'=>  Deal::STATUS_ACTIVE],['deal_address.city_id'=> $user_selected_city_info['id']]])

            $query
                    ->select('deal.*')
                    ->where(['and',['<', 'start_time', time()],['>=', 'expire_time', time()],['deal.status'=>  Deal::STATUS_ACTIVE],['deal_address.city_id'=> $user_selected_city_info['id']]])
                    //->orWhere(['deal.city_id'=>1])
                   
                    ->orderBy('distance');
            }
            else{
                $query
                    ->where(['and',['<', 'start_time', time()],['>=', 'expire_time', time()],['deal.status'=>  Deal::STATUS_ACTIVE]]);
                   
                  
            }
        }



        // grid filtering conditions

        $query->andFilterWhere([
            'deal.id' => $this->id,
            'deal.user_id' => $this->user_id,
            'deal.company_id' => $this->company_id,
            'deal.start_time' => $this->start_time,
            'deal.expire_time' => $this->expire_time,
            'deal.state_id' => $this->state_id,
            'deal.country_id' => $this->country_id,
            'deal.status' => $this->status,
            'deal.created_at' => $this->created_at,
            'deal.updated_at' => $this->updated_at,
        ]);

        $query->joinWith('dealOptions');
        //\common\components\GenXHelper::c($query);
        // only front end join command with category
        if (Yii::$app->id == 'app-frontend') {
            $query->joinWith('dealCategory');
        }


//        $query->orFilterWhere(['like', 'deal_option.name', $this->title]);

        $query->andFilterWhere(['or',['like', 'deal.title', $this->title],['like', 'deal.description', $this->title]])
               // ->andFilterWhere(['like', 'deal.slug', $this->slug])
                //->orFilterWhere(['like', 'deal.description', $this->title])
                ->andFilterWhere(['like', 'deal.summary', $this->summary])
                ->andFilterWhere(['like', 'deal.terms', $this->terms]);

        return $dataProvider;
    }

    public static function find($active_only = True)
    {
        $find = parent::find($active_only);
        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $find->andWhere(['user_id' => Yii::$app->user->getLoggedinID()]);
        }

        return $find;
    }

}
