<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\BaseActiveRecord;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $discountcoupon_id
 * @property integer $total_qty
 * @property integer $total_deals
 * @property double $unit_price
 * @property double $tax_rule
 * @property double $final_price
 * @property string $order_file
 * @property string $order_path
 * @property double $payment_transaction_id
 * @property double $payment_transaction_data
 * @property integer $created_at
 * @property integer $discountcoupon_id
 * @property double $discount_amount
 * @property double $discount_percentage
 *
 * @property Cart[] $carts
 * @property User $user
 */
class Order extends \common\components\GenxBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return parent::rules([
            [['user_id', 'total_qty', 'total_deals', 'unit_price', 'final_price'], 'required'],
            [['user_id','discountcoupon_id', 'total_qty', 'total_deals'], 'integer'],
            [['unit_price', 'tax_rule', 'final_price'], 'number'],
            [['payment_transaction_id', 'payment_transaction_data','order_file','order_path'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            
        ]);
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'discountcoupon_id' => 'Discount Code',
            'total_qty' => 'Total Qty',
            'total_deals' => 'Total Deals',
            'unit_price' => 'Unit Price',
            'tax_rule' => 'Tax Rule',
            'final_price' => 'Final Price',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarts()
    {
        return $this->hasMany(Cart::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
     public function getCart()
    {
        return $this->hasMany(Cart::className(), ['order_id' => 'id']);
    }
     public function getDiscountCoupon()
    {
              return $this->hasOne(DiscountcouponDetails::className(), ['id' => 'discountcoupon_id']);
  
    }
    
}
