<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "discountcoupon_deal".
 *
 * @property integer $id
 * @property integer $discount_id
 * @property integer $deal_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property DiscountcouponDetails $discount
 */
class DiscountcouponDeal extends \common\components\GenxBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discountcoupon_deal';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = [])
    {
        return parent::rules([
            [['discount_id', 'deal_id'], 'required'],
            [['discount_id', 'deal_id'], 'integer'],
            [['discount_id'], 'exist', 'skipOnError' => true, 'targetClass' => DiscountcouponDetails::className(), 'targetAttribute' => ['discount_id' => 'id']],
            [['deal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Deal::className(), 'targetAttribute' => ['deal_id' => 'id']],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'discount_id' => 'Discount ID',
            'deal_id' => 'Deal ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscount()
    {
        return $this->hasOne(DiscountcouponDetails::className(), ['id' => 'discount_id']);
    }
    public function getDeal()
    {
        return $this->hasOne(Deal::className(), ['id' => 'deal_id']);
    }
    
}
