<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Country;

/**
 * CountrySearch represents the model behind the search form about `common\models\Country`.
 */
class CountrySearch extends Country
{

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return [
            [['id'], 'integer'],
            [['name', 'slug', 'iso_2_char', 'iso_3_char', 'iso_4_char', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Country::find();

        $dataProvider = new \common\components\GenXDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'slug', $this->slug])
                ->andFilterWhere(['like', 'iso_2_char', $this->iso_2_char])
                ->andFilterWhere(['like', 'iso_3_char', $this->iso_3_char])
                ->andFilterWhere(['like', 'iso_4_char', $this->iso_4_char]);

        return $dataProvider;
    }

}
