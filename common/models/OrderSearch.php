<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Order;

/**
 * OrderSearch represents the model behind the search form about `common\models\Order`.
 */
class OrderSearch extends Order
{

    /**
     * @inheritdoc
     */
    public function rules($rules = Array())
    {
        return [
            [['id', 'user_id', 'total_qty', 'created_at', 'updated_at', 'status'], 'integer'],
            [['discountcoupon_id', 'total_deals'], 'safe'],
            [['unit_price', 'tax_rule', 'final_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'total_qty' => $this->total_qty,
            'unit_price' => $this->unit_price,
            'tax_rule' => $this->tax_rule,
            'final_price' => $this->final_price,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'discountcoupon_id', $this->discountcoupon_id])
                ->andFilterWhere(['like', 'total_deals', $this->total_deals]);

        return $dataProvider;
    }

    public static function find($active_only = True)
    {
        $find = parent::find($active_only);
        $find->joinWith('cart');
        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $find->andWhere(['cart.merchant_id' => Yii::$app->user->getLoggedinID()]);
        }

        return $find;
    }

}
