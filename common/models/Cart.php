<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%cart}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $deal_id
 * @property integer $deal_option_id
 * @property integer $order_id
 * @property double $unit_price
 * @property string $qty
 * @property double $net_price
 * @property integer $dd
 * @property integer $mm
 * @property integer $yy
 * @property integer $city_id
 * @property integer $gift_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $merchant_id
 * @property integer $discountcoupon_id
 * @property double $discount_amount
 * @property double $discount_percentage
 * @property integer $transaction_log_id 
 * @property integer $transaction_log_time_created 
 * @property double $deal_gst 
 * @property double $deal_amout_excluding_gst 
 * @property double $merchant_amout_excluding_gst 
 * @property double $merchant_gst 
 * @property double $merchant_total_payment 
 * @property double $site_amount_excluding_gst 
 * @property double $site_gst 
 * @property double $site_total_payment 
 *
 * @property City $city
 * @property Deal $deal
 * @property DealOption $dealOption
 * @property User $user
 * @property Order $order
 * @property CouponCode $couponCode
 * @property User $merchant
 * @property TransactionLog $transactionLog
 * @property SendGift $sendGift
 * @property DiscountCoupon $discountCoupon
 */
class Cart extends \common\components\GenxBaseModel
{

    public $total_sale;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
       // Note:  if deal is expired or delete  deal on that case cart deal_id update not possible. manual fixing  
        return parent::rules([
                    [['user_id', 'deal_id', 'deal_option_id', 'unit_price', 'qty', 'net_price', 'dd', 'mm', 'yy'], 'required'],
                    [['user_id', 'deal_id', 'deal_option_id', 'order_id', 'dd', 'mm', 'yy', 'city_id', 'merchant_id', 'transaction_log_id', 'transaction_payment_id', 'transaction_log_time_created'], 'integer'],
                    [['unit_price', 'net_price', 'deal_gst', 'deal_amout_excluding_gst', 'merchant_amout_excluding_gst', 'merchant_gst', 'merchant_total_payment', 'site_amount_excluding_gst', 'site_gst', 'site_total_payment'], 'number'],
                    [['qty'], 'string', 'max' => 255],
                    [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
                    [['deal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Deal::className(), 'targetAttribute' => ['deal_id' => 'id']],
                    [['deal_option_id'], 'exist', 'skipOnError' => true, 'targetClass' => DealOption::className(), 'targetAttribute' => ['deal_option_id' => 'id']],
                    [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
                    [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
                    [['merchant_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['merchant_id' => 'id']],
                    [['transaction_log_id'], 'exist', 'skipOnError' => true, 'targetClass' => TransactionLog::className(), 'targetAttribute' => ['transaction_log_id' => 'id']],
                    [['gift_id'], 'exist', 'skipOnError' => true, 'targetClass' => GiftSend::className(), 'targetAttribute' => ['gift_id' => 'id']],
                    [['discountcoupon_id'], 'exist', 'skipOnError' => true, 'targetClass' => DiscountcouponDetails::className(), 'targetAttribute' => ['discountcoupon_id' => 'id']],   
                    //[['cart_id'], 'secure'],
            
            
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'deal_id' => 'Deal ID',
            'merchant_id' => 'Merchant ID',
            'deal_option_id' => 'Deal Option ID',
            'order_id' => 'Order ID',
            'unit_price' => 'Unit Price',
            'qty' => 'Qty',
            'net_price' => 'Net Price',
            'dd' => 'Dd',
            'mm' => 'Mm',
            'yy' => 'Yy',
            'city_id' => 'City ID',
            'gift_id'=>'Gift ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'cart_id' => 'cart id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getTransactionLog()
    {
        return $this->hasOne(TransactionLog::className(), ['id' => 'transaction_log_id'])->inverseOf('carts');
    }

    public function getTransactionPayment()
    {
        return $this->hasOne(TransactionPayment::className(), ['id' => 'transaction_payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeal()
    {
        //return Deal::find(false)->andWhere(['id'=>  $this->deal_id]);
        return $this->hasOne(Deal::className(), ['id' => 'deal_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDealOption()
    {
        return $this->hasOne(DealOption::className(), ['id' => 'deal_option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
     public function getCartShipping()
    {
        return $this->hasOne(CartShipping::className(), ['cart_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
    
    public function getCarttrack()
    {
        return $this->hasOne(TrackCart::className(), ['cart_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCouponCode()
    {
        return $this->hasOne(CouponCode::className(), ['cart_id' => 'id']);
    }

    public function getMerchant()
    {
        return $this->hasOne(User::className(), ['id' => 'merchant_id']);
    }
    public function getSendGift()
    {
              return $this->hasOne(GiftSend::className(), ['id' => 'gift_id']);
  
    }       
    public function getDiscountCoupon()
    {
              return $this->hasOne(DiscountcouponDetails::className(), ['id' => 'discountcoupon_id']);
  
    }
    

    public function hasReviewed($user_id = 0)
    {
        if (!$user_id) {
            $user_id = \Yii::$app->user->getId();
        }

        return DealReview::find()
                        ->andWhere(['user_id' => $user_id])
                        ->andWhere(['deal_id' => $this->deal_id])
                        ->andWhere(['deal_option_id' => $this->deal_option_id])
                        ->exists();
    }
    

}
