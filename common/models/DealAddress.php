<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "deal_address".
 *
 * @property integer $id
 * @property integer $deal_id
 * @property integer $address
 * @property double $geo_long
 * @property double $geo_lat
 * @property integer $state_id
 * @property integer $city_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 * @property integer $show_address
 * @property integer $show_name
 * @property Deal $deal
 */
class DealAddress extends \common\components\GenxBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal_address';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return parent::rules([
            [['deal_id', 'address','name', 'geo_long', 'geo_lat'], 'required'],
            [['deal_id','state_id','city_id','created_at', 'updated_at', 'status','show_name','show_address'], 'integer'],
            [['address'], 'string', 'max' => 255],
            [['geo_long', 'geo_lat'], 'number'],
            [['deal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Deal::className(), 'targetAttribute' => ['deal_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'deal_id' => 'Deal ID',
            'address' => 'Address',
            'show_name'=>'Show Name',
            'show_address'=>'Show Address',
            'geo_long' => 'Geo Long',
            'geo_lat' => 'Geo Lat',
            'state_id' =>'State Id',
            'city_id' =>'City Id',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeal()
    {
        return $this->hasOne(Deal::className(), ['id' => 'deal_id'])->inverseOf('dealAddress');
    }
    
     public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }
}
