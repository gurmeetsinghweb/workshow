<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%activity_river}}".
 *
 * @property integer $id
 * @property integer $object_id
 * @property string $object_class
 * @property string $action
 * @property integer $subject_id
 * @property string $subject_class
 * @property string $remarks
 * @property integer $owner_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class ActivityRiver extends \common\components\GenxBaseModel
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%activity_river}}';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return parent::rules([
                    [['object_id', 'object_class', 'action', 'subject_id', 'subject_class', 'owner_id'], 'required'],
                    [['object_id', 'subject_id', 'owner_id'], 'integer'],
                    [['remarks'], 'string'],
                    [['object_class', 'subject_class'], 'string', 'max' => 50],
                    [['action'], 'string', 'max' => 100],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'object_id' => 'Object ID',
            'object_class' => 'Object Class',
            'action' => 'Action',
            'subject_id' => 'Subject ID',
            'subject_class' => 'Subject Class',
            'remarks' => 'Remarks',
            'owner_id' => 'Owner ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function setActivity($object, $action, $subject, $remarks = '')
    {
        $this->object_id = $object->id;
        $this->object_class = $object::className();
        $this->action = $action;
        $this->subject_id = $subject->id;
        $this->subject_class = $subject::className();
        $this->remarks = $remarks;
        $this->owner_id = Yii::$app->user->getId();
        $this->status = self::STATUS_ACTIVE;
    }

    /**
     * 
     * @param string $action
     * @param \common\components\GenxBaseModel $subject
     * @param string $remarks
     * @return type
     */
    public static function logActivity($action, &$subject, $remarks = '')
    {
        $final_remarks = [
            'user_remkars' => $remarks,
            'attributes' => $subject->getAttributes()
        ];

        // lets save activity river
        $activity_river = new self();
        $activity_river->setActivity(Yii::$app->user->getIdentity(), $action, $subject, json_encode($final_remarks));
        return $activity_river->save();
    }

}
