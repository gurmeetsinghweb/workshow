<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Cart;

/**
 * CartSearch represents the model behind the search form about `common\models\Cart`.
 */
class CartSearch extends Cart
{

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return [
            [['id', 'user_id', 'deal_id', 'deal_option_id', 'merchant_id', 'order_id', 'dd', 'mm', 'yy', 'city_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['unit_price', 'net_price'], 'number'],
            [['qty'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cart::find(False);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cart.id' => $this->id,
            'cart.user_id' => $this->user_id,
            'cart.deal_id' => $this->deal_id,
            'cart.deal_option_id' => $this->deal_option_id,
            'cart.merchant_id' => $this->merchant_id,
            'cart.order_id' => $this->order_id,
            'cart.unit_price' => $this->unit_price,
            'cart.net_price' => $this->net_price,
            'cart.qty' => $this->qty,
            'cart.dd' => $this->dd,
            'cart.mm' => $this->mm,
            'cart.yy' => $this->yy,
            'cart.city_id' => $this->city_id,
            'cart.status' => $this->status,
            'cart.created_at' => $this->created_at,
            'cart.updated_at' => $this->updated_at,
        ]);
        //->join('LEFT JOIN','deal_category','deal.id=deal_category.deal_id')
        $query->joinWith('deal');
        $query->joinWith('couponCode');
//        $query->joinWith('transactionLog');
        return $dataProvider;
    }

    public static function find($active_only = True)
    {
        $find = parent::find($active_only);
        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $find->andWhere(['cart.merchant_id' => Yii::$app->user->getLoggedinID()]);
        }

        return $find;
    }

}
