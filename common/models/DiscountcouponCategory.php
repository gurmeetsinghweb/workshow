<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "discountcoupon_category".
 *
 * @property integer $id
 * @property integer $discount_id
 * @property integer $category_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property DiscountcouponDetails $discount
 * @property Category $category
 */
class DiscountcouponCategory extends \common\components\GenxBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discountcoupon_category';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = [])
    {
        return parent::rules([
            [['discount_id', 'category_id'], 'required'],
            [['discount_id', 'category_id'], 'integer'],
            [['discount_id'], 'exist', 'skipOnError' => true, 'targetClass' => DiscountcouponDetails::className(), 'targetAttribute' => ['discount_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'discount_id' => 'Discount ID',
            'category_id' => 'Category ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscount()
    {
        return $this->hasOne(DiscountcouponDetails::className(), ['id' => 'discount_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
