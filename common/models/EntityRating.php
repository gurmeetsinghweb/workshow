<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%entity_rating}}".
 *
 * @property integer $id
 * @property integer $rate
 * @property integer $user_id
 * @property integer $entity_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class EntityRating extends \common\components\GenxBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%entity_rating}}';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return [
            [['rate', 'user_id', 'entity_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['user_id', 'entity_id', 'created_at', 'updated_at'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'rate' => Yii::t('app', 'Rate'),
            'user_id' => Yii::t('app', 'User ID'),
            'entity_id' => Yii::t('app', 'Entity ID'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
