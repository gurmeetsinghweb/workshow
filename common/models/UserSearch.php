<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserSearch extends User
{

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
        return [
            [['id','sms_otp', 'status', 'dob', 'yob', 'agreed', 'created_at', 'updated_at'], 'integer'],
            [['username', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'mobile', 'type', 'name', 'slug', 'image_url', 'nationality', 'country_id', 'hint_question', 'hint_answer'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $query = User::find();
        if (is_null($this->status) || empty($this->status)) {
            $query->andWhere(["status" => User::STATUS_ACTIVE]);
        }

        $dataProvider = new \common\components\GenXDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'sms_otp' => $this->sms_otp,
            'status' => $this->status,
            'dob' => $this->dob,
            'yob' => $this->yob,
            'agreed' => $this->agreed,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'type' => $this->type,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'nationality', $this->nationality])
            ->andFilterWhere(['like', 'country_id', $this->country_id])
            ->andFilterWhere(['like', 'hint_question', $this->hint_question])
            ->andFilterWhere(['like', 'hint_answer', $this->hint_answer]);

        return $dataProvider;
    }

}
