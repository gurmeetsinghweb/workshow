<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cart_shipping".
 *
 * @property integer $id
 * @property integer $cart_id
 * @property double $shipping_amount
 * @property string $shipping_name
 * @property string $shipping_address
 * @property string $shipping_state
 * @property string $shipping_city
 * @property integer $shipping_postcode
 * @property integer $shipping_mobile
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class CartShipping extends \common\components\GenxBaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart_shipping';
    }

    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
         return parent::rules([
            [['cart_id', 'shipping_amount', 'shipping_name', 'shipping_address', 'shipping_state', 'shipping_city', 'shipping_postcode', 'shipping_mobile'], 'required'],
            [['cart_id', 'shipping_postcode', 'shipping_mobile'], 'integer'],
            [['shipping_amount'], 'number'],
            [['shipping_name', 'shipping_address', 'shipping_state', 'shipping_city'], 'string', 'max' => 120],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'cart_id' => Yii::t('app', 'Cart ID'),
            'shipping_amount' => Yii::t('app', 'Shipping Amount'),
            'shipping_name' => Yii::t('app', 'Shipping Name'),
            'shipping_address' => Yii::t('app', 'Shipping Address'),
            'shipping_state' => Yii::t('app', 'Shipping State'),
            'shipping_city' => Yii::t('app', 'Shipping City'),
            'shipping_postcode' => Yii::t('app', 'Shipping Postcode'),
            'shipping_mobile' => Yii::t('app', 'Shipping Mobile'),
	     'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
     public function getCart()
    {
        return $this->hasOne(Cart::className(), ['id' => 'cart_id']);
    }
    
}
