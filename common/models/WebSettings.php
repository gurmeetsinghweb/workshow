<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "web_settings".
 *
 * @property int $id
 * @property string $name
 * @property string|null $website_title
 * @property string|null $website_email
 * @property string|null $website_mobile
 * @property string|null $website_fax
 * @property string|null $website_address
 * @property string|null $website_googlemap
 * @property string $slug
 * @property int $created_at
 * @property int $updated_at
 * @property int|null $status
 */
class WebSettings extends \common\components\GenxBaseModel
{
    /**
     * {@inheritdoc}
     */
   
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => \yii\behaviors\SluggableBehavior::className(),
            'attribute' => 'name',
            'ensureUnique' => True
        ];

        return $behaviors;
    }
    
    
    public static function tableName()
    {
        return 'web_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules($rules = array())
    {
        return parent::rules([
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['website_title', 'website_email', 'website_mobile', 'website_fax', 'website_address', 'website_googlemap'], 'string', 'max' => 200],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'website_title' => 'Website Title',
            'website_email' => 'Website Email',
            'website_mobile' => 'Website Mobile',
            'website_fax' => 'Website Fax',
            'website_address' => 'Website Address',
            'website_googlemap' => 'Website Googlemap',
            'slug' => 'Slug',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }
}
