<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DiscountcouponDetails;

/**
 * DiscountcouponDetailsSearch represents the model behind the search form about `common\models\DiscountcouponDetails`.
 */
class DiscountcouponDetailsSearch extends DiscountcouponDetails
{
    /**
     * @inheritdoc
     */
    public function rules($rules = array())
    {
       return [
            [['id', 'amount_value', 'total_person_allow', 'min_bill', 'max_credit', 'coupon_start_dt', 'coupon_expire_dt', 'total_qty', 'total_used', 'category', 'deal'], 'integer'],
            [['discount_coupon', 'discount_title', 'amount_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DiscountcouponDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'amount_value' => $this->amount_value,
            'total_person_allow' => $this->total_person_allow,
            'min_bill' => $this->min_bill,
            'max_credit' => $this->max_credit,
            'coupon_start_dt' => $this->coupon_start_dt,
            'coupon_expire_dt' => $this->coupon_expire_dt,
            'total_qty' => $this->total_qty,
            'total_used' => $this->total_used,
            'category' => $this->category,
            'deal' => $this->deal,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'discount_coupon', $this->discount_coupon])
            ->andFilterWhere(['like', 'discount_title', $this->discount_title])
            ->andFilterWhere(['like', 'amount_type', $this->amount_type]);

        return $dataProvider;
    }
}
