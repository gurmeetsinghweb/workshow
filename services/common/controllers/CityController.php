<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace services\common\controllers;
use common\models\City;
/**
 * Description of CityController
 *
 * @author Acer
 */
class CityController extends \common\components\GenxBaseRestActiveController
{
    public $modelClass = "services\common\models\CityService";
    public $selected_city = [];
    public function behaviors()
    {
        $behaviours = parent::behaviors();
        array_push($behaviours['authenticator']['except'], 'search');
        array_push($behaviours['authenticator']['except'], 'get-home-page-cities');
        return $behaviours;
    }
    
    public function actionGetHomePageCities($term=NULL){
        
        //return \common\components\GenXHelper::getHomePageCities();
        $city_search_options = [];
        $term = trim($term);
        if (!empty($term)) {
            $city_data = City::find()->andWhere(['like', 'name', $term])->limit(20)->all();
        } else {
            $city_data = City::getFeaturedCity()->all();
        }

          $return = [];

        if (sizeof($city_data)) {
            foreach ($city_data as $city) {
                if(($key = array_search($city->name, $this->selected_city)) !== false) {
                    unset($city[$key]);
                }else{
                    $return['results'][] = [
                        'id' => $city->id,
                        'text' => $city->name,
                        'latitude' => $city->latitude,
                        'longitude' => $city->longitude,
                        `name` => $city->name,
                        'slug' => $city->slug,
                        'country_id' => $city->country_id,
                        'state_id' => $city->state_id,
                        'created_at' => $city->created_at,
                        'updated_at' => $city->updated_at,
                        'status' => $city->status,
                        'latitude' => $city->latitude,
                        'longitude' => $city->longitude,
                        'zip_code' => $city->zip_code,
                        'search_name' => $city->search_name,
                        'featured' => $city->featured,
                        'show_home' => $city->show_home,
                        'featured_img' => $city->featured_img,
                        'default_city' => $city->default_city,
                        'done' => $city->done
                    ];
                }
            }

            $return['more'] = False;
        }
      /*  $return['results'][] = [
            'text' => 'All Australia',
            'id' => "-1",
        ];
        */
        // send with json headers
       // header('Content-Type: application/json');
        return $return;
    }
    
}
