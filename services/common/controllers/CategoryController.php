<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace services\common\controllers;


/**
 * Description of DealController
 *
 * @author Acer
 */
class CategoryController extends \common\components\GenxBaseRestActiveController
{
    public $modelClass = "services\common\models\CategoryService";
    
    public function behaviors()
    {
//        var_dump($bah)
        
        $behaviours = parent::behaviors();
      //  var_dump($behaviours);
       array_push($behaviours['authenticator']['except'], 'home-page-categories');
       array_push($behaviours['authenticator']['except'], 'get-filter-page-categories');
       array_push($behaviours['authenticator']['except'], 'search');
       array_push($behaviours['authenticator']['except'], 'get');
       return $behaviours;
    }
   
    // public function actionHomePageCategories() {
    //     $return['categories'] =  \common\components\GenXHelper::getHomePageCategorySerial();
    //     $return['deals'] = [];
    //     foreach($return['categories'] as $key=> $fetch_cate){
    //          $return['deals'][$key] = $fetch_cate->getDeals()->limit(8)->all();
    //     }
    //     return $return;
    // }
    public function actionGetFilterPageCategories(){
//        return \common\models\Category::getTree();
        $categories = \common\models\Category::find()
                ->andWhere(['status' => 10])
                ->andWhere(['!=','active_deals', '10'])
                ->orderBy(['id' => SORT_DESC])
                ->all();
        $final_data = [];
        foreach ($categories as $_category) {
            $category = $_category->attributes;
            if ($category) {
          
                    $category['active_deals'] = (int) count($_category->getDeals()->asArray()->all());
                  
                }
            }

//            if (isset($final_data[$category['id']])) {
//                $category['children'] = $final_data[$category['id']];
//                if ($get_active_count) {
//                    foreach ($final_data[$category['id']] as $_cat_1) {
//                    }
//                }
//                unset($final_data[$category['id']]);
//            }
//            $final_data[(int) $category['parent_id']][] = $category;


        return $categories;
    }
    
    
}
