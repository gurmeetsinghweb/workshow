<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace services\common\controllers;


/**
 * Description of DealController
 *
 * @author Acer
 */
class DealController extends \common\components\GenxBaseRestActiveController
{
    public $modelClass = "services\common\models\DealService";
    
    public function behaviors()
    {
        $behaviours = parent::behaviors();
        array_push($behaviours['authenticator']['except'], 'get-banner');
        array_push($behaviours['authenticator']['except'], 'city-list');
        array_push($behaviours['authenticator']['except'], 'top-deals');
        array_push($behaviours['authenticator']['except'], 'search');
        array_push($behaviours['authenticator']['except'], 'deals');
        return $behaviours;
    }
    
    public function actionGetBanner(){
        return \common\components\GenXHelper::getBanner();

    }
    public function actionCityList(){
         $cityModels = new \common\models\CitySearch();
        $cityProvider = $cityModels->search([]);
        $cityProvider->query->andWhere(['show_home' => 1]);
        $cityModel = $cityProvider->getModels();
        return $cityModel;
    }
    public function actionTopDeals(){
         $TopModel = new \common\models\DealSearch();
         $TopDealDataProvider = $TopModel->search([]);
         return $TopDealDataProvider;
    }
    public function actionHomePageCategories(){
        $homecategory=  \common\components\GenXHelper::getHomePageCategorySerial();
        return $homecategory;
    }
//    public function actionSearch($params)
//    {
//        $search_params = $this->initSearch($params, [
//            'status' => Null,
//            'name' => Null,
//            'id' => Null,
//            'latitude' => Null,
//            'longitude' => Null
//           
//        ]);
//
//        $search_options['andWhereFilter'] = [
//            ['like', 'id', $search_params['search']['id']],
//            ['like', 'status', $search_params['search']['status']],
//        ];
//        $search_options['SELECT'] = ['deal.*, ( 6371 * acos( cos( radians(' . $search_params['search']['latitude'] . ') ) * cos( radians( deal_address.geo_lat) ) * cos( radians( deal_address.geo_long ) - radians(' . $search_params['search']['longitude'] . ') ) + sin( radians(' . $search_params['search']['latitude'] . ') ) * sin( radians( deal_address.geo_lat ) ) ) ) AS distance'];
//        $search_options['HAVING'] = ['<', 'distance', Yii::$app->params['searchRadius']];
//        $search_options['andWhereFilter'] = [['and',['<', 'start_time', time()],['>=', 'expire_time', time()],['deal.status'=> \common\models\Deal::STATUS_ACTIVE]]];
//        $search_options['orderBy'] = ['distance'];
//        return $this->baseSearch($search_params, $search_options);
//    }
}
