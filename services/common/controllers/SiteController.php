<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace services\common\controllers;

use services\common\models\UserService;
use services\common\models\UserVehicleService;

/**
 * Description of CityController
 *
 * @author Acer
 */
class SiteController extends \common\components\GenxBaseRestController
{

    public function behaviors()
    {
        $behaviours = parent::behaviors();
        array_push($behaviours['authenticator']['except'], 'info');
        array_push($behaviours['authenticator']['except'], 'dashboard');

        return $behaviours;
    }

    public function actionInfo()
    {
        return [
            'api_version' => 'v1',
            'base_url' => \yii\helpers\Url::toRoute(['/'], True),
        ];
    }

}
