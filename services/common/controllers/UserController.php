<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace services\common\controllers;
use Yii;
use common\models\User;

/**
 * Description of SiteController
 *
 * @author Acer
 */
class UserController extends \common\components\GenxBaseRestActiveController
{

    public $modelClass = "services\common\models\UserService";

    public function behaviors()
    {
        $behaviours = parent::behaviors();
        array_push($behaviours['authenticator']['except'], 'create');
        array_push($behaviours['authenticator']['except'], 'validate-username');
        array_push($behaviours['authenticator']['except'], 'validate-email');
        array_push($behaviours['authenticator']['except'], 'validate-mobile');
        array_push($behaviours['authenticator']['except'], 'login');
        array_push($behaviours['authenticator']['except'], 'user-signup');

        return $behaviours;
    }

    public function actionValidateUsername($username)
    {
        $model = $this->getModel();
        $model->username = $username;
        $validated = $model->validate(['username']);
        $return = [
            'validated' => $validated
        ];
        if (!$validated) {
            $return ['errors'] = $model->getErrors();
        }

        return $return;
    }

    public function actionValidateEmail($email)
    {
        $model = $this->getModel();
        $model->email = $email;
        $validated = $model->validate(['email']);
        $return = [
            'validated' => $validated
        ];
        if (!$validated) {
            $return ['errors'] = $model->getErrors();
        }

        return $return;
    }

    public function actionValidateMobile($mobile)
    {
        $model = $this->getModel();
        $model->mobile = $mobile;
        $validated = $model->validate(['mobile']);
        $return = [
            'validated' => $validated
        ];

        if (!$validated) {
            $return ['errors'] = $model->getErrors();
        }

        return $return;
    }

    public function actionLogin()
    {
       
        $user_login = new \common\models\LoginForm();
        $user_login->load(\Yii::$app->getRequest()->getBodyParams(), '');
        $user_login->getUser($this->modelClass);
        $user_login->getUser($this->modelClass);
        $return = [
            'login' => False
        ];

        if ($user_login->login()) {
            var_dump($user_login->login());
            $return['login'] = True;
            $return['auth_token'] = $user_login->getUser()->accessToken->token;
            $return['auth_token_expires'] = ($user_login->getUser()->accessToken->created_at) + ($user_login->getUser()->accessToken->lifetime);
            $return['user'] = $user_login->getUser()->getAttributes(null, ['sms_otp', 'password_hash', 'password_reset_token']);
        }

        return $return;
    }
    public function actionUserSignup(){
          
            $POST                   =   \Yii::$app->getRequest()->getBodyParams();
            $user_model             =   new User();
            $user_model->name       =   $POST['name'];
            $user_model->email      =   $POST['email'];
            $user_model->username   =   $POST['name'];
            $user_model->type       =   User::ROLE_USER;
            $user_model->generateAuthKey();
            $user_model->generateAccessToken();
            $user_model->password_hash = Yii::$app->getSecurity()->generatePasswordHash($POST['password']);
            if(isset($POST['social_type']) && $POST['social_id']){
                $user_model->social_type    =   $POST['social_type'];
                $user_model->social_id      =   $POST['social_id'];
            }
            if($user_model->save()){
                return User::findOne($user_model->id);
            }else{
                return $user_model->getErrors();
            }
            
    }
    public function actionLogout()
    {
        $return['logout'] = False;
        if (\Yii::$app->user->isGuest || \Yii::$app->user->getIdentity()->generateAccessToken(0)) {
            $return['logout'] = True;
        }

        return $return;
    }

    /**
     * 
     * @return \common\models\UserService
     */
    public function getModel()
    {
        return parent::getModel();
    }
    
   
}
