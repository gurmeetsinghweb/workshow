<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace services\common\controllers;

/**
 * Description of CityController
 *
 * @author Acer
 */
class CompanyController extends \common\components\GenxBaseRestActiveController
{

    public $modelClass = "services\common\models\CompanyService";

    public function actionMy()
    {
        
        $return = [];
        $company = \Yii::$app->user->getIdentity()->company;
        if ($company) {
            $return = $this->buildExpandAttributes($company, $company->exportAbleAttributes);
        }
        return $return;
    }

}
