<?php

namespace services\common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Deal;

/**
 * CountrySearch represents the model behind the search form about `common\models\Country`.
 */
class DealService extends Deal
{

    use \common\components\GenxServicesBaseModel;

    public function fields()
    {
        return [
            'id',
            'user_id',
            'company_id',
            'main_category_id',
            'sub_category_id',
            'sub_sub_category_id',
            'title',
            'slug',
            'description',
            'summary',
            'start_time',
            'expire_time',
            'terms',
            'how_redeemed',
            'city_id',
            'state_id',
            'country_id',
            'status',
            'geo_long',
            'geo_lat',
            'deal_remain',
            'created_at',
            'updated_at',
        ];
    }
    public function extraFields() {
            return ['city', 'dealGalleries', 'dealCategory', 'dealOptions','dealAddress','dealReviews'];
    }
    public function search($params)
    {
        $search_params = $this->initSearch($params, [
            'status' => Null,
            'name' => Null,
            'id' =>  Null,
            'category_id' =>  Null,
            'iso_2_char' => Null,
            'latitude' => Null,
            'longitude' => Null,
            'join' => Null
        ]);
//        echo $params['status'];
//        exit();
//        echo var_dump($search_params['search']['status']);
//        exit();
        $search_options['andWhereFilter'] = [
            ['like', 'id', $search_params['search']['id']],
            ['like', 'main_category_id', $search_params['search']['category_id']],
            ['like', 'sub_category_id', $search_params['search']['category_id']],

            ['like', 'iso_2_char', $search_params['search']['iso_2_char']],
        ];
        if($search_params['search']['latitude']){
            $search_options['join'] = ["tbl_type"=>"INNER JOIN", "table_name"=>"deal_address","params"=>"deal.id=deal_address.deal_id" ];
            $search_options['SELECT'] = ['deal.*, ( 6371 * acos( cos( radians(' . $search_params['search']['latitude'] . ') ) * cos( radians( deal_address.geo_lat) ) * cos( radians( deal_address.geo_long ) - radians(' . $search_params['search']['longitude'] . ') ) + sin( radians(' . $search_params['search']['latitude'] . ') ) * sin( radians( deal_address.geo_lat ) ) ) ) AS distance'];
            $search_options['HAVING'] = ['<', 'distance', Yii::$app->params['searchRadius']];
            $search_options['andWhere'] = [['and',['<', 'start_time', time()],['>=', 'expire_time', time()],['deal.status' => \common\models\Deal::STATUS_ACTIVE]]];
            $search_options['orderBy'] = 'distance';
            }
        return $this->baseSearch($search_params, $search_options);
    }

}
