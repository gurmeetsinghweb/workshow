<?php

namespace services\common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\State;

/**
 * StateSearch represents the model behind the search form about `common\models\State`.
 */
class StateService extends State
{

    use \common\components\GenxServicesBaseModel;

    public function fields()
    {
        return [
            'id' => 'id',
            'name' => 'name',
            'country_id' => 'country_id',
            'iso_2_char' => 'iso_2_char',
            'status' => 'status',
            'updated_at' => 'updated_at'
        ];
    }

    public function extraFields()
    {
        return ['country'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(CountryService::className(), ['id' => 'country_id']);
    }

    public function search($params)
    {
        $search_params = $this->initSearch($params, [
            'status' => Null,
            'country_id' => Null,
            'name' => Null,
            'iso_2_char' => Null
        ]);

        $search_options['andWhereFilter'] = [
            ['country_id' => $search_params['search']['country_id']],
            ['like', 'name', $search_params['search']['name']],
            ['like', 'iso_2_char', $search_params['search']['iso_2_char']],
        ];

        return $this->baseSearch($search_params, $search_options);
    }

}
