<?php

namespace services\common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Category;

/**
 * CitySearch represents the model behind the search form about `common\models\City`.
 */
class CategoryService extends Category
{

    use \common\components\GenxServicesBaseModel;

    public function fields()
    {
        return [
            'id',
            'name',
            'slug',
            'icon',
            'showhome',
            'serial_order',
            'parent_id',
            'total_deals',
            'active_deals',
            'fa',
        ];
    }
    public function extraFields() {
        return ['deals','dealGalleries', 'dealCategory', 'dealOptions','dealAddress','dealReviews'];
    }

    public function search($params)
    {
        $search_params = $this->initSearch($params, [
            'status' => Null,
            'name' => Null,
            'id' => Null,
            'iso_2_char' => Null,
            'showhome' => Null,
            'parent_id' => Null,
        ]);

        $search_options['andWhereFilter'] = [
            ['like', 'id', $search_params['search']['id']],
            ['like', 'iso_2_char', $search_params['search']['iso_2_char']],
            ['=', 'showhome', $search_params['search']['showhome']],
           
        ];
        if($search_params['search']['parent_id']){
            $search_options['andWhereFilter'] =  [['=', 'parent_id', $search_params['search']['parent_id']],
            ['>', 'total_deals', 0],
            ['>', 'active_deals', 0],
        ];
        }
        $search_options['orderBy'] = ['serial_order'=> 'asc'];
        
        return $this->baseSearch($search_params, $search_options);
    }

}
