<?php

namespace services\common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Settings;

/**
 * CitySearch represents the model behind the search form about `common\models\City`.
 */
class SettingsService extends Settings
{

    use \common\components\GenxServicesBaseModel;

    public function fields()
    {
        return [
            'id',
            'company_name',
            'logo',
            'footer_logo',
            'logo_title',
            'facebook_link' ,
            'instagram_link' ,
            'twitter_link',
            'visa_logo',
            'maestro_logo',
            'paypal_logo',
            'address',
            'phone' ,
            'contact_email' ,
            'splash_logo',
            'terms_and_conditions'
        ];
    }
//    public function extraFields() {
//        return ['deals'];
//    }

    public function search($params)
    {
        $search_params = $this->initSearch($params, [
            'id' => Null,
            ]);

        $search_options['andWhereFilter'] = [
            ['like', 'id', $search_params['search']['id']],
 
        ];
        
        
        return $this->baseSearch($search_params, $search_options);
    }

}
