<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace services\common\models;

/**
 * Description of CompanyService
 *
 * @author Acer
 */
class CompanyService extends \common\models\Company
{

    use \common\components\GenxServicesBaseModel;

    public function fields()
    {
        return [
            'id' => 'id',
            'title' => 'title',
            'description' => 'description',
            'website' => 'website',
            'facebook' => 'facebook',
            'twitter' => 'twitter',
            'youtube' => 'youtube',
            'gplus' => 'gplus',
            'company_number' => 'company_number',
            'vat_number' => 'vat_number',
            'state_id' => 'state_id',
            'city_id' => 'city_id',
            'user_id' => 'user_id',
            'address' => 'address',
            'pin_code' => 'pin_code',
            'geo_lat' => 'geo_lat',
            'geo_long' => 'geo_long',
            'distance' => 'distance',
            'contact' => 'contact',
            'main_contact_email' => 'main_contact_email',
            'main_contact_name' => 'main_contact_name',
            'main_contact_designation' => 'main_contact_designation',
            'main_contact_number' => 'main_contact_number',
            'company_type_id' => 'company_type_id',
            'service_provider_type_id' => 'service_provider_type_id',
            'status' => 'status',
            'updated_at' => 'updated_at',
            'logo_small' => function($model) {
                return $model->getIconURL('small', '../../', True);
            },
            'logo_medium' => function($model) {
                return $model->getIconURL('medium', '../../', True);
            },
            'logo_large' => function($model) {
                return $model->getIconURL('large', '../../', True);
            },
        ];
    }

    public function extraFields()
    {
        return ['city', 'state', 'owner'];
    }

    public function search($params)
    {
        $search_params = $this->initSearch($params, [
            'title' => Null,
            'state_id' => Null,
            'address' => Null,
            'pin_code' => Null,
            'main_contact_email' => Null,
            'main_contact_name' => Null,
            'status' => Null,
            'user_id' => Null,
            'city_id' => Null,
        ]);

        $search_options['andWhereFilter'] = [
                ['state_id' => $search_params['search']['state_id']],
                ['like', 'title', $search_params['search']['title']],
                ['like', 'address', $search_params['search']['address']],
                ['like', 'main_contact_email', $search_params['search']['main_contact_email']],
                ['like', 'main_contact_name', $search_params['search']['main_contact_name']],
                ['user_id' => $search_params['search']['user_id']],
                ['pin_code' => $search_params['search']['pin_code']],
                ['city_id' => $search_params['search']['city_id']]
        ];

        if (isset($search_params['search']['miles']) && strlen($search_params['search']['geo_lat']) && strlen($search_params['search']['geo_long'])) {
            $search_options['SELECT'] = 'company.*, ( 3959 * acos( cos( radians(' . $search_params['search']['geo_lat'] . ') ) * cos( radians( company.geo_lat) ) * cos( radians( company.geo_long ) - radians(' . $search_params['search']['geo_long'] . ') ) + sin( radians(' . $search_params['search']['geo_lat'] . ') ) * sin( radians( company.geo_lat ) ) ) ) AS distance';
            $search_options['HAVING'] = ['<', 'distance', $search_params['search']['miles']];
        }

        if (isset($search_params['sort'])) {
            $sort_array = $search_params['sort'];
            unset($search_params['sort']);
            if (isset($sort_array['cost'])) {
                
            }

            if (isset($sort_array['distance'])) {
                $search_params['sort']['distance'] = $sort_array['distance'];
            }

            if (isset($sort_array['rating'])) {
                
            }

            if (isset($sort_array['slots'])) {
                
            }
        } else {
            if (isset($search_params['search']['miles'])) {
                $search_params['sort'] = [];
                $search_params['sort']['distance'] = 0;
            }
        }

        return $this->baseSearch($search_params, $search_options);
    }

}
