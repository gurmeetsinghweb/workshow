<?php

namespace services\common\models;

/**
 * CitySearch represents the model behind the search form about `common\models\City`.
 */
class SiteSettingService extends \common\models\SiteSettings
{

    use \common\components\GenxServicesBaseModel;

    public function fields()
    {
        return [
            'id' => 'id',
            'key' => 'key',
            'value' => 'value',
            'status' => 'status',
            'updated_at' => 'updated_at'
        ];
    }

    public function search($params)
    {
        $search_params = $this->initSearch($params, [
            'status' => Null,
            'key' => Null,
            'value' => Null,
        ]);
        
        $search_options['andWhereFilter'] = [
            ['like', 'key', $search_params['search']['key']],
            ['like', 'value', $search_params['search']['value']]
        ];

        return $this->baseSearch($search_params, $search_options);
    }

}
