<?php

namespace services\common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Country;

/**
 * CountrySearch represents the model behind the search form about `common\models\Country`.
 */
class CountryService extends Country
{

    use \common\components\GenxServicesBaseModel;

    public function fields()
    {
        return [
            'id' => 'id',
            'name' => 'name',
            'iso_2_char' => 'iso_2_char',
            'status' => 'status',
            'updated_at' => 'updated_at'
        ];
    }

    public function search($params)
    {
        $search_params = $this->initSearch($params, [
            'status' => Null,
            'name' => Null,
            'iso_2_char' => Null
        ]);

        $search_options['andWhereFilter'] = [
            ['like', 'name', $search_params['search']['name']],
            ['like', 'iso_2_char', $search_params['search']['iso_2_char']],
        ];

        return $this->baseSearch($search_params, $search_options);
    }

}
