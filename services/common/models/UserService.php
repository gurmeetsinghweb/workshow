<?php

namespace services\common\models;

use Yii;
use common\models\User;

class UserService extends User
{

    use \common\components\GenxServicesBaseModel;

    public function fields()
    {
        $fields = parent::fields();
        // remove fields that contain sensitive information
        unset($fields['password_hash'], $fields['password_reset_token']);

        if(!\Yii::$app->user->can(self::ROLE_ADMIN)) {
            unset ($fields['sms_otp']);
        }
        
        // if its just created
        if (time() < $this->created_at + 30) {
            $fields['auth_token'] = 'auth_token';
            unset($fields['auth_key']);
        }

        return $fields;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $search_params = $this->initSearch($params, [
            'status' => Null,
            'type' => Null,
            'username' => Null,
            'email' => Null,
            'name' => Null,
            'mobile' => Null,
            'country_id' => Null,
            'sms_otp' => Null
        ]);
        
        $search_options['andWhereFilter'] = [
            ['<>', 'type', self::ROLE_SYSADMIN],
            [
                User::tableName() . '.type' => ((\Yii::$app->user->isSysAdmin()) ? $search_params['search']['type'] : self::ROLE_USER),
                User::tableName() . '.status' => $search_params['search']['status'],
            ],
            ['like', User::tableName() . '.username', $search_params['search']['username']],
            ['like', User::tableName() . '.email', $search_params['search']['email']],
            ['like', User::tableName() . '.mobile', $search_params['search']['mobile']],
            ['like', User::tableName() . '.name', $search_params['search']['name']],
            ['like', User::tableName() . '.country_id', $search_params['search']['country_id']],
            ['sms_otp' => $search_params['search']['sms_otp']]
        ];

        return $this->baseSearch($search_params, $search_options);
    }
}
