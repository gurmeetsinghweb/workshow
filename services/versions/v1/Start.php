<?php

namespace services\versions\v1;

/**
 * v1 module definition class
 */
class Start extends \yii\base\Module
{

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'services\versions\v1\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

}
