<?php

namespace services\versions\v1\controllers;

/**
 * Default controller for the `v1` module
 */
class DefaultController extends \common\components\GenxBaseRestController
{

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return "Please specify valid action";
    }

}
