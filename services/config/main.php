<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(Yii::getAlias('@services'). '/config/params.php'), require(Yii::getAlias('@services'). '/config/params-local.php')
);

return [
    'id' => 'app-services',
    'name' => 'Classifr Services Module',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'services\controllers',
    'modules' => [
        'v1' => [
            'class' => 'services\versions\v1\Start'
        ],
    ],
    'components' => [
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'response' => [
            'format' => \yii\web\Response::FORMAT_JSON
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                    [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/site',
                    'extraPatterns' => [
                        'OPTIONS,GET info' => 'info',
                    ]
                ],
                    [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/user',
                    'extraPatterns' => [
                        'OPTIONS,GET search' => 'search',
                        'OPTIONS,GET validate/username' => 'validate-username',
                        'OPTIONS,GET validate/email' => 'validate-email',
                        'OPTIONS,GET validate/fb/email' => 'validate-fb-email',
                        'OPTIONS,GET validate/mobile' => 'validate-mobile',
                        'OPTIONS,POST login' => 'login',
                        'OPTIONS,POST logout' => 'logout',
                        'OPTIONS,POST login/fb' => 'fb-login',
                        'OPTIONS,POST gat' => 'get-access-token',
                        'OPTIONS,POST confirm/code' => 'confirm-code',
                        'OPTIONS,POST upload-image' => 'upload-image',
                        'OPTIONS,POST user-signup' => 'user-signup',
                    ]
                ],
                    [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [
                        'v1/country',
                        'v1/state',
                        'v1/city'
                    ],
                    'extraPatterns' => [
                        'OPTIONS,GET search' => 'search',
                        'OPTIONS,GET get-home-page-cities' => 'get-home-page-cities',
                        'OPTIONS,GET autocomplete' => 'autocomplete',
                        'OPTIONS,GET stats' => 'stats',
                        'OPTIONS,GET list-all' => 'list-all',
                    ]
                ],
                 [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [
                        'v1/deal',
                    ],
                    'extraPatterns' => [
                        'OPTIONS,GET search' => 'search',
                        'OPTIONS,GET get-banner' => 'get-banner',
                        'OPTIONS,GET city-list' => 'city-list',
                        'OPTIONS,GET top-deals' => 'top-deals',
                        

                    ]
                ],
                 [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [
                        'v1/category',
                    ],
                    'extraPatterns' => [
                        'OPTIONS,GET search' => 'search',
                        'OPTIONS,GET home-page-categories' => 'home-page-categories',
                        'OPTIONS,GET get-filter-page-categories' => 'get-filter-page-categories',
                        
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [
                        'v1/settings',
                    ],
                    'extraPatterns' => [
                        'OPTIONS,GET search' => 'search',
                        
                      
                    ]
                ],
            ],
        ]
    ],
    'params' => $params,
];
