<?php
return [
    'adminEmail' => 'admin@example.com',
    'tokenExpireTime' => 86400,
];
