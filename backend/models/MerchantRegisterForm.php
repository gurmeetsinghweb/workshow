<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\User;

class MerchantRegisterForm extends Model
{

    public $name;
    public $username;
    public $email;
    public $city_id;
    public $password;
    public $password_repeat;
    public $verificationCode;
    public $agreed;
    private $_user;

    public function rules()
    {
        return $this->getUser()->rules();
    }

    public function register()
    {
        $this->getUser()->status = User::STATUS_PENDING_VALIDATION;
        $this->getUser()->name = $this->name;
        $this->getUser()->email = $this->email;
        $this->getUser()->city_id = $this->city_id;
        $this->getUser()->username = $this->username;
        $this->getUser()->password = $this->password;
        $this->getUser()->password_repeat = $this->password_repeat;
        $this->getUser()->agreed = $this->agreed;
        $this->getUser()->type = User::ROLE_MERCHANT;
        if ($this->getUser()->validate() && $this->getUser()->save()) {
            return True;
        } else {
            foreach ($this->getUser()->getErrors() as $attr => $err) {
                $this->addError($attr, $err[0]);
            }
            return False;
        }
    }

    /**
     * 
     * @return User
     */
    public function getUser()
    {
        if (!($this->_user instanceof User)) {
            $this->_user = new User();
            $this->_user->setScenario('signup');
        }

        return $this->_user;
    }

}
