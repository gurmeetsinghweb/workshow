<?php
/* @var $this yii\web\View */

use yii\grid\GridView;
use common\components\GenxGridHelper;
use yii\helpers\Html;
use common\components\GenxWebUser;

$this->title = 'Admin Home';
backend\assets\DashboardAsset::register($this);
?>
<!-- top tiles -->
 <div class="row top_tiles">
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats  btn-danger" style="background:#004a02">
                  <div class="icon"></div>
                    <div class="count"><i class="fa fa-inr"></i><?= $metrics['total_sale_30_days']?></div>
                    <h3>Total Sale</h3>
                    <p>Last <?= $number_of_days; ?> Days.</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-primary" style="background:#337ab7">
                    <div class="icon"></div>
                    <div class="count"><?= $metrics['total_deals_sold_30_days'] ?></div>
                    <h3>Total Sold Deals</h3>
                    <p>Last <?= $number_of_days; ?> Days.</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-dark" style="background:#7954e3" >
                  <div class="icon"><!--<i class="fa fa-sort-amount-desc"></i>--></div>
                    <div class="count"><i class="fa fa-inr"></i><?=  $metrics['total_sale']?></div>
                    <h3> Total Sale</h3>
                    <p>Overall till date.</p>
                </div>
            </div>
       <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-dark" style="background:#585858" >
                  <div class="icon"><!--<i class="fa fa-sort-amount-desc"></i>--></div>
                    <div class="count"><?= $metrics['total_deals_sold'] ?></div>
                    <h3> Total Sold Deals</h3>
                    <p>Overall till date .</p>
                </div>
            </div>
     <!--
     <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-dark" style="background:#008803" >
                  <div class="icon"></div>
                    <div class="count"><i class="fa fa-inr"></i><?= $metrics['total_my_earning'] ?></div>
                    <h3>Merchant Revenue</h3>
                    <p>Overall till date .</p>
                </div>
            </div>
     
     <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-dark" style="background:#ab0b0b" >
                  <div class="icon"></div>
                    <div class="count"><i class="fa fa-usd"></i><?= $metrics['total_deals_canceled'] ?></div>
                    <h3>Cancelled Deal</h3>
                    <p>Overall till date .</p>
                </div>
            </div>
     <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-dark" style="background:#c79735" >
                  <div class="icon"></div>
                    <div class="count"><i class="fa fa-usd"></i><?= $metrics['total_deals_redemed'] ?></div>
                    <h3>Payment given</h3>
                    <p>Overall till date .</p>
                </div>
            </div>
     
     <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-dark" style="background:#2a3f54" >
                  <div class="icon"></div>
                    <div class="count"><i class="fa fa-usd"></i><?= $metrics['total_unclear'] ?></div>
                    <h3>Redeemed</h3>
                    <p>Redeemed Deals</p>
                </div>
            </div>
     -->
            
 </div>


<!-- /top tiles -->

<div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-12">
                    <h3>Last <?= $number_of_days; ?> Days' Enquiry<small> Amount wise</small></h3>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
                <div style="width: 100%;">
                    <canvas id="sale-wise-chart"></canvas>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>


    <div class="col-md-6 col-sm-6 ">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-12">
                    <h3>Last <?= $number_of_days; ?> Day's Enquiry<small> Customers wise</small></h3>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
                <div style="width: 100%;">
                    <canvas id="deal-wise-chart"></canvas>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

</div>
<br />

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-6">
                    <h3>Last <?= $number_of_days; ?> Days' Enquiry<small> Customers</small></h3>
                </div>
            </div>
            <div class="table-responsive">
            <?php
            $dataProvider->sort = [
                'attributes' => ['id', 'name']
            ];
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'label' => 'Date',
                        'value' => function($model) {
                            return $model->getCreatedTime();
                        }
                    ],
                    [
                        'label' => 'Customer',
                        'attribute' => 'user',
                        'format' => 'raw',
                        'value' => function($model) {
                            return $model->user->name."<br>".$model->user->email."<br>".$model->user->mobile;
                        }
                    ],
                    [
                       
                        'label' => 'Merchant Name',
                        'format'=>'raw',
                        'value'=>function($model){
                        return Html::a($model->merchant->name,\yii\helpers\Url::toRoute(["merchant/index",'UserSearch[email]'=>$model->merchant->email]));                           
// return Html::a(\yii\helpers\Url::toRoute("merchant/index"),$model->merchant->name,["deal-option/create", 'deal_id' => $model->id]);
                        
                        
                        },
                         'visible'=>Yii::$app->user->isAdmin(),       
                    ],
                     [
                        
                        'label' => 'Purchased deal',
                        'format'=>'raw',
                        'value'=>function($model){
                            return "<b>".$model->deal->title."</b><br>".$model->dealOption->name;
                        
                        
                        }
                    ],
                    [
                        'attribute' => 'net_price',
                        'label' => 'Service Amount',
                        'format'=>'raw',
                        'value' => function($model){
                        return "&#x20B9;". $model->net_price;
                        }
                        
                    ],
//                    [
//                        'attribute' => 'qty',
//                        'label' => 'Total Purchased quantity'
//                    ],
//                    
                      [
                            'attribute' => 'couponCode.status',
                            'label' => 'Status',
                            'format' => 'raw',
                            'value' => function($data) {
                                switch ($data->couponCode->status) {
                                    case common\models\CouponCode::STATUS_ACTIVE:
                                    case common\models\CouponCode::STATUS_REDEEMED:
                                        if ($data->couponCode->status == common\models\CouponCode::STATUS_REDEEMED) {
                                            return Html::a('<i class="fa fa-check-circle" style="color:green; font-size:30px;" aria-hidden="true"></i>');
                                        } else {
                                         //   return Html::a('<i class="fa fa-clock-o" style="color:gray; font-size:30px;" aria-hidden="true"></i>');
                                               return Html::a('<i class="fa fa-check-circle" style="color:green; font-size:30px;" aria-hidden="true"></i>');
                                        }
                                        break;
                                     case common\models\CouponCode::STATUS_CANCLED:
                                        return Html::a('<i class="fa fa-ban" style="color:red; font-size:30px;" aria-hidden="true" title="deal cancel"></i>');
                                        break;
                                   
                                }
                            }
                                ]
                          /*  [
                        'attribute' => 'couponCode.status',
                        'label' => 'Redeemed Status',
                        'format' => 'raw',
                        'value' => function($data) {
                            if ($data->couponCode->status == common\models\CouponCode::STATUS_REDEEMED) {
                                return '<i class="fa fa-check-circle" style="color:green; font-size:30px;" aria-hidden="true"></i>';
                            } else {
                                return '<i class="fa fa-clock-o" style="color:gray; font-size:15px;" aria-hidden="true" title="Pending"></i>';
                            }
                        }
                    ]*/
                ],
            ]);
            ?>
            </div>       
        </div>
    </div>
</div>

<?php
$cart_js = "
      
        var sale_wise_chart = new Chart(document.getElementById(\"sale-wise-chart\"), {
        type: 'bar',
        data: " . json_encode($sales_chart_data) . ",
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
      
      var deal_wise_chart = new Chart(document.getElementById(\"deal-wise-chart\"), {
        type: 'bar',
        data: " . json_encode($deals_chart_data) . ",
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
      
    ";

$this->registerJs($cart_js);
?>
