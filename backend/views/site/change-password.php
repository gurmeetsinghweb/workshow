<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = "Change Password";
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to login:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>


            <?= $form->field($model, 'oldpassword')->passwordInput() ?>
            <?= $form->field($model, 'newpassword')->passwordInput() ?>
            <?= $form->field($model, 'newpassword_repeat')->passwordInput() ?>
            <?= $form->field($model, 'verificationCode')->widget(yii\captcha\Captcha::className()); ?>
            <div class="form-group">
                <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>