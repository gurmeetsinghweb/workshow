<?php

use yii\helpers\Html;
use common\components\GenXHelper;
use yii\helpers\Url;
use yii\grid\GridView;
use common\components\GenxGridHelper;

/* @var $this yii\web\View */
/* @var $model common\models\DealGallery */

$this->title = Yii::t('app', 'Create Deal Gallery');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Deal Galleries'), 'url' => ['deal/index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
//GenXHelper::contentHeaderButtons($this, [
//    [
//        "title" => "List",
//        "url" => Url::toRoute(['/deal-gallery/index']),
//        "icon" => "fa fa-list",
//    ]
//]);
?>
<div class="deal-gallery-create panel">
    <div class="panel-body">
        <?=
        $this->render('_form', [
            'model' => $model,
            'deal_model' => $deal_model,
        ])
        ?>
    </div>
</div>

<div class="deal-option-index panel">
    <div class="panel-body">
        <?php 
if(!isset($_REQUEST['update_id']))
{
    
   $button = [

                                    'buttons' => [
                                        'edit' => function ($url, $model, $key) {
                                            $url = Url::toRoute(['update', 'id' => $model->id]);
                                            return Html::a('<span class="glyphicon glyphicon-pencil" style="font-size:20px;"></span>', $url, [
                                                        'title' => \Yii::t('yii', 'Update'),
                                                        'data-pjax' => '0',
                                            ]);
                                        },
                                             
                                                
                                                
                                                
                                               
                                                'delete' => function ($url, $model, $key) {

                                            $url = Url::toRoute(['delete', 'id' => $model->id]);
                                            return Html::a('<span class="glyphicon glyphicon-trash" style="font-size:20px;"></span>', $url, [
                                                        'title' => \Yii::t('yii', 'Delete'),
                                                        'data-confirm' => \Yii::t('yii', 'Are you sure to delete this deal?'),
                                                        'data-method' => 'post',
                                                        'data-pjax' => '0',
                                            ]);
                                        }
                                            ],
                                            'template' => '{edit} {delete} '
     ];
}
else{
    
    $button = [

                                    'buttons' => [
                                        'edit' => function ($url, $model, $key) {
                                            $url = Url::toRoute(['update', 'id' => $model->id,'update_id'=>1]);
                                            return Html::a('<span class="glyphicon glyphicon-pencil" style="font-size:20px;"></span>', $url, [
                                                        'title' => \Yii::t('yii', 'Update'),
                                                        'data-pjax' => '0',
                                            ]);
                                        },
                                             
                                                
                                                
                                                
                                               
                                                'delete' => function ($url, $model, $key) {

                                            $url = Url::toRoute(['delete', 'id' => $model->id,'update_id'=>1]);
                                            return Html::a('<span class="glyphicon glyphicon-trash" style="font-size:20px;"></span>', $url, [
                                                        'title' => \Yii::t('yii', 'Delete'),
                                                        'data-confirm' => \Yii::t('yii', 'Are you sure to delete this deal?'),
                                                        'data-method' => 'post',
                                                        'data-pjax' => '0',
                                            ]);
                                        }
                                            ],
                                            'template' => '{edit} {delete} '
     ];
    
}

// echo $this->render('_search', ['model' => $searchModel]); ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => function($model) {
                        return Html::img($model->getIconURL('small'));
                    }
                ],
                'description',
                // 'updated_at',
                GenxGridHelper::ActionColumn($button),
            ],
        ]);
        ?>
        
        <?php
        
        if($deal_model->status ==  \common\models\Deal::STATUS_INCOMPLETE)
          {
        ?>    
        
            <?= Html::a('Back - Deal Address', ['/deal-address/create', 'deal_id' => $model->deal_id], ['class' => 'pull-left btn btn-primary']) ?>
       <?php 
        if ($dataProvider->getCount()) {
            echo Html::a('Finish', ['/deal/complete', 'id' => $model->deal_id], [
                'class' => 'pull-right btn btn-success',
                'data' => [
                    'confirm' => Yii::t('app', 'Deal will be submitted for Admin verification.'),
                    'method' => 'post',
                ]
            ]);
        }
        }
 else {
     echo Html::a('Submit', ['/deal/index'], ['class' => 'pull-right btn btn-success']);  
 }
        ?>
    </div>
</div>