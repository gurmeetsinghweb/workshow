<?php

use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DealGallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Deal Galleries');
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "New",
        "url" => Url::toRoute(['/deal-gallery/create']),
        "icon" => "fa fa-plus",
    ],
    [
        "title" => "Refresh",
        "url" => Url::toRoute(['/deal-gallery/index']),
        "icon" => "fa fa-refresh",
    ],
]);
?>
<div class="deal-gallery-index panel">
    <div class="panel-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

              GenxGridHelper::linkedID(),
            'deal_id',
              GenxGridHelper::linkedTitle('name'),
            'size',
            // 'status',
            // 'created_at',
            // 'updated_at',

            GenxGridHelper::status(),
            GenxGridHelper::ActionColumn(),
        ],
    ]); ?>
    </div>
</div>
