<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DealGallery */
/* @var $form yii\widgets\ActiveForm */
?>
<h4>
    <?= $deal_model->title ?>
</h4>
<div class="panel">
    <div class="panel-body">
        <!-- Steps -->
        <div id="wizard" class="form_wizard wizard_horizontal">
            <ul class="wizard_steps">
                <li>
                    <a href="#step-1" class="done">
                        <span class="step_no">1</span>
                        <span class="step_descr">
                            Step 1<br />
                            <small>Update Deal</small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-2"  class="done">
                        <span class="step_no">2</span>
                        <span class="step_descr">
                            Step 2<br />
                            <small>Deal options</small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-3" class="done">
                        <span class="step_no">3</span>
                        <span class="step_descr">
                            Step 3<br />
                            <small>Deal Addresses</small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-4" class="selected">
                        <span class="step_no">4</span>
                        <span class="step_descr">
                            Step 4<br />
                            <small>Gallery for deal</small>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- End Steps -->

        <!-- Wizard Content -->
        <div class="wizard-content">
            <div class="wizard-pane active">
                <div class="deal-gallery-form">

                    <?php
                    $form = ActiveForm::begin([
                                'options' => [
                                    'enctype' => 'multipart/form-data'
                                ]
                    ]);
                    ?>
                    <?= $form->field($model, 'imageFile')->fileInput(); ?>
                    <?= $form->field($model, 'description')->textInput(); ?>
                     <?php
                            if(isset($_REQUEST['update_id'])){
                                ?>
                            <input type="hidden" name="update_id" value="1">
                            <?php
                          }
                            ?>
                    <div class="row">
                        <div class="col-sm-8 col-xs-12">
                            <?= $form->field($model, 'deal_option_id')->dropDownList(yii\helpers\ArrayHelper::map(\common\models\DealOption::findAll(['deal_id' => $model->deal_id]), 'id', 'name'), ['prompt' => 'Select Option'])->label('Select if related to deal option'); ?>
                        </div>

                        <div class="col-sm-4 col-xs-12">
                             <?= $form->field($model, 'sort_order')->textInput(); ?>
                        </div>

                    </div>        
                    
                    <div class="form-group pull-right">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
        <!-- End Wizard Content -->

    </div>
</div>