<?php

use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DealOptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Deal Address');
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "New",
        "url" => Url::toRoute(['/deal-address/create']),
        "icon" => "fa fa-plus",
    ],
    [
        "title" => "Refresh",
        "url" => Url::toRoute(['/deal-address/index']),
        "icon" => "fa fa-refresh",
    ],
]);
?>
<div class="deal-option-index panel">
    <div class="panel-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

              GenxGridHelper::linkedID(),
            'deal_id',
              GenxGridHelper::linkedTitle('name'),
            'slug',
            // 'list_price',
            // 'selling_price',
            // 'no_per_person',
            // 'total_sold',
            // 'status',
            // 'created_at',
            // 'updated_at',

            GenxGridHelper::status(),
            GenxGridHelper::ActionColumn(),
        ],
    ]); ?>
    </div>
</div>
