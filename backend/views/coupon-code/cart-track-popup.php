<?php
use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use common\components\GenXHelper;

?>

  <div class="modal-dialog modal-md">
<?php
//\common\components\GenXHelper::c($cart);
//echo $id1 = $_POST["id1"];
//echo $id2 = $_POST["id2"];
?>
    
	   <?php
	     $form = ActiveForm::begin([
                'id' => 'form-send-gift',
		  'action' => 'cart-tracking-action'
                ]);
   ?>
        <?php /*
        $request = Yii::$app->getRequest();
        echo Html::hiddenInput($request->csrfParam, $request->getCsrfToken());*/
        ?>
    <!-- Start: Modal content-->
     <div class="modal-content cancel-request mfp-with-anim mfp-dialog clearfix"> 
    <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
              &times;
            </span>
          </button>
          <h4 class="modal-title" id="myModalLabel">
            Add Tracking Details
          </h4>
        </div>
         
    <div class="modal-body">
        <h4><?= $dealModel->title ?></h4>
          <div class="credit-card-div">
            <div class="panel ">
              <div class="panel-heading">
                
                <!--/ row -->
                
                <!-- / row -->
                <div class="row">
                  <div class="col-xs-12">
                    <div class="form-group">
                       <?= $form->field($trackdata, 'company_name')->textInput(['maxlength' => true])->label("Company Name*"); ?>
                    </div>
                    <div class="form-group">
                       <?= $form->field($trackdata, 'track_id')->textInput(['maxlength' => true])->label("Track ID*"); ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($trackdata, 'url')->textInput(['maxlength' => true])->label("URL*"); ?>
                    </div>
                      
                          <?= $form->field($trackdata, 'cart_id')->hiddenInput(['value'=> $cart_id->id])->label(false); ?>
                          <?= $form->field($trackdata, 'id')->hiddenInput(['value'=> $trackdata->id])->label(false); ?>
                          <input type="hidden" value="1" name="newdata">
                  </div>
                </div>
              


                <div class="gap_cancel"></div>
                <div class="row ">
                  <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust green-btn-2">
                    <input type="submit" class="btn btn-danger btn-raised ripple-effect" value="Cancel"  data-dismiss="modal" aria-label="Close">
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust  green-btn">
                    <input type="submit" class="btn btn-success btn-raised ripple-effect btn-block" value="<?= ($trackdata->id) ? "Update" : "Submit" ?>">
                    
                  </div>
                </div>
                
              </div>
            </div>
          </div>
          
        </div>
     </div>
    <?php ActiveForm::end(); ?>
    
  </div>

