<?php
use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\jui\DatePicker;
use common\components\GenXHelper;

?>

  <div class="modal-dialog modal-md">
<?php
//\common\components\GenXHelper::c($cart);
//echo $id1 = $_POST["id1"];
//echo $id2 = $_POST["id2"];
?>
      <form  action="<?php echo Url::toRoute('coupon-code/extend-coupon-expiry-action') ?>" method="post">

        <?php
        $request = Yii::$app->getRequest();
        echo Html::hiddenInput($request->csrfParam, $request->getCsrfToken());
        ?>
    <!-- Start: Modal content-->
     <div class="modal-content cancel-request mfp-with-anim mfp-dialog clearfix"> 
    <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
              &times;
            </span>
          </button>
          <h4 class="modal-title" id="myModalLabel">
            Update Expiry Date 
          </h4>
        </div>
         
    <div class="modal-body">
        <h4><?= $dealModel->title ?></h4>
          <div class="credit-card-div">
            <div class="panel ">
              <div class="panel-heading">
                
                <!--/ row -->
                
                <!-- / row -->
                <div class="row">
                  <div class="col-xs-12">
                      <div class="form-group">
                          <label for="Name">Name<?= $model->id ?></label>
                          <input type="text" class="form-control" value="<?= $cart_id->merchant->name ?>" readonly="true">
                        </div>
                    
                      <div class="col-sm-4 col-xs-12">
                          <div class="form-group">
                          <label for="Date">Update Expiry Date</label>
                            <?php
                            $enddate = $model->expires_at;
                             echo yii\jui\DatePicker::widget([
                                'clientOptions' => [
                                    'altField' => "#deal-start_time",
                                    'altFormat' => "@",
                                    'minDate'=> 0,
                                   
                                          
                                ],
                                'options' => ['class' => 'form-control'],
                                'value' =>  date('M d Y', ($enddate)),
                                 'name'=>"expire_time",
                            ]);
                                                    
                            
                            ?>
                          </div>
                        </div>
                          <input type="hidden" value="<?= $model->id ?>" name="id">
<!--                          <input type="hidden" value="<?= $enddate ?>" name="expire_time">-->
                  </div>
                </div>
                <div class="gap_cancel"></div>
                <div class="row ">
                  <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust green-btn-2">
                    <input type="submit" class="btn btn-danger btn-raised ripple-effect" value="Cancel"  data-dismiss="modal" aria-label="Close">
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust  green-btn">
                    <input type="submit" class="btn btn-success btn-raised ripple-effect btn-block" value="Submit">
                    <input type="hidden" value="" name="cart">
                  </div>
                </div>
                
              </div>
            </div>
          </div>
          
        </div>
     </div>
    </form>
    
  </div>
  </div>



