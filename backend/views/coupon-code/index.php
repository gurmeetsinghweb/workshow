<?php

use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use common\models\CouponCode;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CouponCodeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Customer Sale Report');
$this->params['breadcrumbs'][] = $this->title;


backend\assets\ChartAsset::register($this);
backend\assets\FromAssets::register($this);

$category_array = \yii\helpers\ArrayHelper::map($category, 'id', 'name');
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "Refresh",
        "url" => Url::toRoute(['/coupon-code/index']),
        "icon" => "fa fa-refresh",
    ],
]);
?>
<style>
    .background-red{
        background-color:red;
    }

    .modal {
    display: none;
    overflow: auto;
    overflow-y: scroll;
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1050;
    -webkit-overflow-scrolling: touch;
    outline: 0;
}

.fade {
    opacity: 0;
    -webkit-transition: opacity .15s linear;
    transition: opacity .15s linear;
}
    </style>
<!-- top tiles -->
<div class=" extend_expiry modal fade" tabindex="-1"></div>
<div class="row top_tiles">
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats  btn-danger" style="background:#004a02">
                  <div class="icon"></div>
                    <div class="count"><i class="fa fa-inr"></i><?= $metrics['total_sale']?></div>
                    <h3>Total Sale</h3>
                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-primary" style="background:#337ab7">
                    <div class="icon"></div>
                    <div class="count"><?= $metrics['total_deals_sold'] ?></div>
                    <h3>Total Qty Sold</h3>
                    <p>&nbsp;</p>
                </div>
            </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-dark" style="background:#7954e3" >
                  <div class="icon"><!--<i class="fa fa-sort-amount-desc"></i>--></div>
                    <div class="count"><i class="fa fa-inr"></i><?= $metrics['total_my_earning'] ?></div>
                    <h3>Merchant Revenue</h3>
                    <p>&nbsp;</p>
                </div>
            </div>
       
     <?php
     if(Yii::$app->user->isAdmin()){
         ?>
       <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-dark" style="background:#585858" >
                  <div class="icon"><!--<i class="fa fa-sort-amount-desc"></i>--></div>
                    <div class="count"><i class="fa fa-inr"></i><?= $metrics['company_sale'] ?></div>
                    <h3>Company Revenue</h3>
                    <p>&nbsp;</p>
                </div>
            </div>
             <?php
    
     }
     ?>
<!--
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-dark" style="background:#ab0b0b" >
                  <div class="icon"></div>
                    <div class="count"><i class="fa fa-inr"></i><?php // $metrics['canceled_deal'] ?></div>
                    <h3>Cancelled Deal</h3>
                    <p>&nbsp;</p>
                </div>
            </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-dark" style="background:#c79735" >
                  <div class="icon"></div>
                    <div class="count"><i class="fa fa-inr"></i><?php // $metrics['payment_given'] ?></div>
                    <h3>Payment given</h3>
                    <p>&nbsp;</p>
                </div>
            </div>
     
     <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-dark" style="background:#2a3f54" >
                  <div class="icon"></div>
                    <div class="count"><i class="fa fa-inr"></i><?php // $metrics['payment_not_clear'] ?></div>
                    <h3>Redeemed</h3>
                    <p>&nbsp;</p>
                </div>
            </div>
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-dark" style="background:#2d787f" >
                  <div class="icon"></div>
                    <div class="count"><i class="fa fa-usd"></i><?php // $metrics['non_redeemed'] ?></div>
                    <h3>Not Redeemed</h3>
                    <p>&nbsp;</p>
                </div>
            </div>
            -->
 </div>


<!-- /top tiles -->

<div class="coupon-code-index panel">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
                <?php
                $form = \yii\widgets\ActiveForm::begin(['method' => 'GET','action'=>  Url::toRoute(["index"])]);
                ?>
                <div class="col-lg-3">
                    Choose Date to filter records
                    <input type="text"  name="date_range" id="date_range" class="form-control" value="<?= $POST['date_range'] ?>" />
                </div>
                <?php
                if(Yii::$app->user->isAdmin()){
                ?>
                 <div class="col-lg-3">
                      Select Merchant
                  <?php 
                
                     echo yii\jui\AutoComplete::widget([
                            'clientOptions' => [
                                'source' => Url::toRoute(['/merchant/merchant-autocomplete']),
                                'select' => new yii\web\JsExpression('function( event, ui ) {
                            $("#merchant").val(ui.item.id);
                            
                            
                          }'),
                            ],
                            'options' => ['class' => 'form-control']
                        ]);
                     ?>
                      <input type="hidden" name="merchant_id" id="merchant">
                 </div>
               <!-- <div class="col-lg-3">
                    Choose Deal to filter records<br />
                    <select id="deals_list" name="deal_category_id" class="form-control">
                        <option></option>
                        <?php
                      /*  if (sizeof($category_array)) {
                            foreach ($category_array as $id => $title) {
                                echo "<option value='{$id}' " . ((isset($POST['deal_id']) && $POST['deal_id'] == $id) ? "SELECTED" : "") . ">{$title}</option>";
                            }
                        } */
                        ?>
                    </select>
                </div>-->
                <?php
                }
                ?>
                <div class="col-lg-3">
                    <br />
                    <input type="submit" value="Filter Records" class="btn btn-success btn-sm"/>
                </div>
                <div class="col-lg-3">
                    <br />
                    <?=
                    yii\helpers\Html::a('Reset', ['coupon-code/index'], ['class' => 'btn btn-danger btn-sm']);
                    ?>
                </div>
                <?php
                yii\widgets\ActiveForm::end();
                ?>
            </div>
        </div>
    </div>
    <hr />
    <div class="panel-body">
        <div class="table-responsive">
        <?=
                        
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'Voucher Id',
                    'attribute' => 'id',
                    
                ],
                [
                    'label' => 'Customer',
                    'attribute' => 'user',
                    'format' => 'raw',
                    'value' => function ($model) {
            // 13/7/17 gurmeet apply check user if available or not
                    if(isset($model->cart->user->name)){
                        return $model->cart->user->name;
                    }
                    else{
                        return "Not Available";
                    }
                    },
                ],
              [
                    'label' => 'Merchant',
                    'attribute' => 'Merchant Name',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->cart->merchant->name;
                    },
                ],
                [
                    'label' => 'Deal',
                    'attribute' => 'deal',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->cart->deal->title . "<br>" . $model->cart->dealOption->name;
                    },
                ],
                
                [
                    'label' => 'Category',
                    'attribute' => 'deal',
                    'format' => 'raw',
                    'value' => function ($model) {
                        if(count($model->cart->deal->dealCategory)){
                            $delcat="";
                            foreach($model->cart->deal->dealCategory as $dealcat){
                                $delcat.=$dealcat->category->name .", ";
                            }
                          $delcat= substr($delcat,0, -1);
                        return $delcat;
                            
                        }
                        else{
                            return "";
                        }
                    },
                ],
                [
                    'label' => 'Amount',
                    'attribute' => 'amount',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return '&#x20B9;' . $model->cart->net_price;
                    },
                ],
//                 [
//                    'label' => 'Qty',
//                    'attribute' => 'qty',
//                    'format' => 'raw',
//                    'value' => function ($model) {
//                        return  $model->cart->qty;
//                    },
//                ],
//                    [
//                    'label' => 'MerchantAmt',
//                    'attribute' => 'merchant_total_amt',
//                    'format' => 'raw',
//                    'value' => function ($model) {
//                        return  $model->cart->merchant_total_payment;
//                    },
//                ],
                
                [
                    'label' => 'Enquiry Date',
                    'format' => 'date',
                    'attribute' => 'created_at',
                    'filter' => False,
                ],
//                            [
//                    'label' => 'Expire at',
//                    'format' => 'raw',
//                    'attribute' => 'expires_at',
//                    'filter' => False,
//                    'value' => function ($model) {
//                            if($model->expires_at <= time()){
//                                return  "<div class='background-red'>".date('M d,Y', $model->expires_at)."</div>";
//                            }else{
//                                return  date('M d,Y', $model->expires_at);
//                            }
//                        
//                    },            
//                ],
                [
                    'label' => 'Updated at',
                    'format' => 'date',
                    'attribute' => 'updated_at',
                    'filter' => False,
                ],
                            
              //  GenxGridHelper::status(['filter_options' => [CouponCode::STATUS_REDEEMED => "Redeemed", CouponCode::STATUS_CANCLED => "Canceled"]]),
               GenxGridHelper::status(),
                   
//                [
//                    'label' => 'Paid',
//                    'format' => 'raw',
//                   'attribute' => 'Status',
//                    'value' => function ($model) {
//                        if($model->cart->transaction_log_id!=''){
//                            return  "Paid<br> ".date('d M Y', ($model->cart->transactionLog->created_at));
//                        }
//                        else{
//                            return "Not Paid";
//                        }
//                        
//                          
//                    },
//                ],
                            
                   [
                    'attribute' => 'actions',
                    'format' => 'raw',
                    'value' => function($model) {
                        $return = \yii\helpers\Html::a(
                                        '<i class="fa fa-list" aria-hidden="true"></i> View Details', \yii\helpers\Url::toRoute([
                                            '/reports/invoice',
                                            'id' => $model->cart->order_id
                                        ]), ['class' => 'btn btn-success btn-sm']);
                           if($model->expires_at <= time() && \Yii::$app->user->isAdmin() && $model->status== CouponCode::STATUS_ACTIVE){
                        $return .="<br><button type='button' value=".$model->id." class='btn btn-raised btn-info ripple-effect btn-md extend_expiry_popup'>Extend Expire Date&nbsp;&nbsp; &nbsp;</button>";
                            }
				if($model->cart->cartShipping && \Yii::$app->user->isMerchant()){
				    
				    $return .="<br><button type='button' value=".$model->id." class='btn btn-raised btn-info ripple-effect btn-md sendgift'>Cart Tracking&nbsp;&nbsp; &nbsp;</button>";
                            }
                            return $return;
                            }
                        ],
				
                        [
                            'attribute' => 'status',
                            'label' => 'Update Status',
                            'format' => 'raw',
                            'value' => function($data) {
                                switch ($data->status) {
                                    case CouponCode::STATUS_ACTIVE:
                                        return Html::a('<i class="fa fa-check-circle" style="color:green; font-size:30px;" aria-hidden="true"></i>');
                                    case CouponCode::STATUS_REDEEMED:
                                        if ($data->status == CouponCode::STATUS_REDEEMED) {
                                            return Html::a('<i class="fa fa-check-circle" style="color:green; font-size:30px;" aria-hidden="true"></i>');
                                        } else {
                                         //   return Html::a('<i class="fa fa-clock-o" style="color:gray; font-size:30px;" aria-hidden="true"></i>');
                                            return Html::a('<i class="fa fa-check-circle" style="color:green; font-size:30px;" aria-hidden="true"></i>');
                                        }
                                        break;
                                     case CouponCode::STATUS_CANCLED:
                                        return Html::a('<i class="fa fa-ban" style="color:red; font-size:30px;" aria-hidden="true" title="deal cancel"></i>');
                                        break;
                                    case CouponCode::STATUS_DISABLED:
                                        return Html::a('<i class="fa fa-toggle-off" style="color:red; font-size:30px;" aria-hidden="true"></i>', $data->getActivateURL());
                                        break;
                                }
                            },
                                    'filter' => False,
                                    'visible' => \Yii::$app->user->isMerchant(),
                                ]
                            ],
                        ]);
                        ?>
        </div>
                    </div>
                </div>

            <div id="load_popup_modal_giftshow_id" class="modal fade" tabindex="-1"></div>

                <?php
                $cart_js = "
                $('#date_range').daterangepicker(null, function(start, end, label) {});
       ";

                $this->registerJs($cart_js);
                ?>

<?=
    $this->registerJs("
    var requestId = $('.extend_expiry_popup').val();
    var extend_expiry = $('.extend_expiry');
    $('.extend_expiry_popup').on('click', function(){
    requestId=$(this).val();
    extend_expiry.load('" . \yii\helpers\Url::toRoute('coupon-code/extend-coupon-exp-date') . "',{'id': requestId},
    function(){
    extend_expiry.modal('show');
    });
    });
    
    var sendGift = $('#load_popup_modal_giftshow_id');
    $('.sendgift').on('click', function(){
	 requestId=$(this).val(); 
	 sendGift.load('" . \yii\helpers\Url::toRoute('coupon-code/cart-tracking') . "',{'cart_id': requestId},
	 function(){
	     sendGift.modal('show');
	     });
	 });
    ");	  
?>

