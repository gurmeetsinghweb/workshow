<?php

use yii\helpers\Html;
use common\components\GenXHelper;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\CouponCode */

$this->title = Yii::t('app', 'Voucher Redemption Centre');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Customers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
use common\components\GenxWebUser;
?>
<style>
    .alert-success{
        display: none;
    }
    </style>
<?php
if(Yii::$app->user->isMerchant()){
    ?>
<div class="coupon-code-redeemed panel">
    <div class="panel-body">
    
        
      
 
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
                <?php
                $form = \yii\widgets\ActiveForm::begin(['method' => 'POST','id'=>"form-submitcoupon",'action'=>  Url::toRoute(["multipal-coupon-redeemed"])]);
                ?>
                <div class="row">
                <div class="col-lg-6">
                    <b>Add Multiple Vouchers for Redeem seperated by "," </b>
                 
                    <textarea rows="4"  name="couponredeemed" id="couponredeemed" class="form-control"  placeholder="(eg: 58d183e5acd63, 58c261234325e)"></textarea>
                </div>
                     <div id="verifycoupon"  class="help-block text_coupon"></div>
                     <div id="verifycoupon"  class="alert alert-success text_coupon"></div>
                     <div id="verifycouponsuccess"  class="help-block text_coupon"></div>
                     
                </div>
                    <div class="row">
                        <br>
                <div class="col-lg-6">                   
                    <button type="submit" class="btn btn-success btn-sm submit">Redeemed Coupon</button>
                    <!--<input type="submit" value="Redeemed Coupon" class="btn btn-success btn-sm"/>-->
                    <?= yii\helpers\Html::a('Reset', ['coupon-code/multipal-coupon-redeemed'], ['class' => 'btn btn-danger btn-sm']); ?>
                </div>
                    </div>     
                <?php
                yii\widgets\ActiveForm::end();
                ?>
            </div>
        </div>
    </div>
        
        
        
        
        
        <?php
$this->registerJs("

    $('.submit').click(function(e){
        e.preventDefault();
        $('.submit').prop('disabled', true);
        if($('#couponredeemed').val() === '') {
            $('#verifycoupon').html('Coupon can\'t be blank');
            $('#couponredeemed').addClass('promo_error');
            return false;
        }
        var serial_data = $('#form-submitcoupon').serialize();
        
        $.post(this.action, serial_data)
//        .done(function(data){
//            var msg=data.responseJSON;
//           $('.alert-success').css('display','block');  
//           $('.alert-success').append(msg);
//            console.log(data);
//        window.location.reload();        
//      })
        .fail(function(data){
            console.log(data);
            var error=data.responseJSON;
            console.log(error);
            $('#verifycoupon').html(error);
            $('#couponredeemed').addClass('promo_error');
            if(error!=''){                
                $('.submit').prop('disabled', false);
            }
            return false;
        });
    });
    ");
?>
        
        
        
        
        
        
    </div>
</div>
<?php
    }
?>