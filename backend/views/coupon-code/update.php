<?php

use yii\helpers\Html;
use common\components\GenXHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\CouponCode */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Coupon Code',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coupon Codes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "List",
        "url" => Url::toRoute(['/coupon-code/index']),
        "icon" => "wb-list",
    ],
    [
        "title" => "Delete",
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
        "url" => Url::toRoute(['/coupon-code/delete', 'id' => $model->id]),
        "icon" => "wb-trash",
    ],
]);
?>
<div class="coupon-code-update panel">
    <div class="panel-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    </div>
</div>
