<?php

use yii\helpers\Html;
use common\components\GenXHelper;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\CouponCode */

$this->title = Yii::t('app', 'Create Coupon Code');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coupon Codes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "List",
        "url" => Url::toRoute(['/coupon-code/index']),
        "icon" => "wb-list",
    ]
]);
?>
<div class="coupon-code-create panel">
    <div class="panel-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    </div>
</div>
