<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\DiscountcouponDetails */
/* @var $form yii\widgets\ActiveForm */
?>

  <div class="wizard-content">
            <div class="wizard-pane active">
<div class="discountcoupon-details-form">

    <?php $form = ActiveForm::begin([
                                'options' => [
                                    'enctype' => 'multipart/form-data'
                                ]
                    ]); ?>
<?php
  //  \common\components\GenXHelper::c($model);
?>
     <div class="row">
         <div class="col-xs-12 col-sm-6  col-lg-4">
                            <?= $form->field($model, 'discount_coupon')->textInput(['maxlength' => true])->label("Discount Coupon Code") ?>
                        </div>
                        <div class="col-sm-6 col-lg-8">
                            <?= $form->field($model, 'discount_title')->textInput(['maxlength' => true]) ?>
                        </div>

                    </div>
   
     
    <div class="col-xs-12 col-sm-6 col-lg-4">
                            <?= $form->field($model, 'amount_type')->dropDownList(['percentage' => '% age', 'amount' => '$ amount'], ['prompt' => 'Select Category'])->label("Select Discount Type"); ?>
                        </div>
        
        <div class="col-xs-12 col-sm-6 col-lg-4">
            
            <?= $form->field($model, 'amount_value')->textInput() ?>
            
        </div>
        
        <div class="col-xs-12 col-sm-6 col-lg-4">
            
            <?= $form->field($model, 'total_person_allow')->textInput() ?>
            
        </div>
        
  
    
    

     
    <div class="col-xs-12 col-sm-6 col-lg-4">
               <?= $form->field($model, 'min_bill')->textInput() ?>
     </div>
        
        <div class="col-xs-12 col-sm-6 col-lg-4">
            
           <?= $form->field($model, 'max_credit')->textInput() ?>
            
        </div>
        
        <div class="col-xs-12 col-sm-6 col-lg-4">
            
            <?= $form->field($model, 'total_qty')->textInput() ?>
            
        </div>
        


      <div class="row">
     
    <div class="col-xs-12 col-sm-6 col-lg-6">
        
        <?php
                            echo '<label class="control-label">Start time</label>';
                            echo yii\jui\DatePicker::widget([
                               
                                'clientOptions' => [
                                    'altField' => "#discountcoupondetails-coupon_start_dt",
                                    'altFormat' => "@",
                                    'minDate'=>'today',
                                    'onSelect' => new \yii\web\JsExpression("function(selectedDate) {
            $( '#w2' ).datepicker( 'option', 'minDate', selectedDate );}"),
                                ],
                                'options' => ['class' => 'form-control',
                                    ],
                                'value' => ($model->coupon_start_dt) ? date('M d, Y', $model->coupon_start_dt) :  date('M d, Y', time())
                            ]);
                            echo $form->field($model, 'coupon_start_dt')->hiddenInput(['value' => (time() * 1000)])->label(False);
                            ?>
        
        
              
     </div>
        
        <div class="col-xs-12 col-sm-6 col-lg-6">
            <?php
                            echo '<label class="control-label">Expire time</label>';
                            echo yii\jui\DatePicker::widget([
                                'clientOptions' => [
                                    'altField' => "#discountcoupondetails-coupon_expire_dt",
                                    'altFormat' => "@",
                                    'minDate'=>date('M d, Y', $model->coupon_start_dt),
                                   'onSelect' => new \yii\web\JsExpression("function(selectedDate) {
            $( '#w1' ).datepicker( 'option', 'maxDate', selectedDate );}"),
                                ],
                              
                                'options' => ['class' => 'form-control',
                                
                                    ],
                                'value' => ($model->coupon_expire_dt) ?  date('M d, Y', $model->coupon_expire_dt) : date('M d, Y', (time() + (60 * 60 * 24 * 7)))
                               
                            ]);
                            echo $form->field($model, 'coupon_expire_dt')->hiddenInput(['value' => (time() + (60 * 60 * 24 * 7)) * 1000])->label(False);
                            ?>
            
          
            
        </div>
        
        
        
    </div>
    
      <div class="row">
     
    <div class="col-xs-12 col-sm-6 col-lg-6">
               <?= $form->field($model, 'coupon_img')->fileInput() ?>
     </div>
        
        <div class="col-xs-12 col-sm-6 col-lg-6">
            
           <?= $form->field($model, 'coupon_img_url')->textInput() ?>
            
        </div>
        
        
    </div>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create Coupon' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>