<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\GenXHelper;
use yii\helpers\Url;
use common\components\GenxGridHelper;



/* @var $this yii\web\View */
/* @var $searchModel common\models\DiscountcouponDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Discountcoupon Details';
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "New",
        "url" => Url::toRoute(['/discountcoupon-details/create']),
        "icon" => "fa fa-plus",
    ],
    [
        "title" => "Refresh",
        "url" => Url::toRoute(['/discountcoupon-details']),
        "icon" => "fa fa-refresh",
    ],
]);
?>
<div class="discountcoupon-details-index">
    <div class="table-responsive">
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                     
                    'attribute' => 'Id',
                    'format'=>'raw',
                    'value' => function($model) {
                        return $model->id;
                          
                    },
                    
                ],  
            'discount_coupon',
            'discount_title',
            'amount_type',
            [
                     
                    'attribute' => 'Amount Value',
                    'format'=>'raw',
                    'value' => function($model) {
                        return $model->amount_value;
                          
                    },
                    
                ],  
            [
                     
                    'attribute' => 'Allow Category',
                    'format'=>'raw',
                    'value' => function($model) {
                        $total_cat = $model->getDiscountcouponCategories()->count();
                        return $total_cat;
                          
                    },
                    
                ],
                [
                     
                    'attribute' => 'Allow Deals',
                    'format'=>'raw',
                    'value' => function($model) {
                        $total_deal = $model->getDiscountcouponDeals()->count();
                        return $total_deal;
                          
                    },
                    
                ],
            // 'total_person_allow',
            // 'min_bill',
            // 'max_credit',
            // 'coupon_start_dt',
            // 'coupon_expire_dt',
            // 'total_qty',
            // 'total_used',
            // 'category',
            // 'deal',
            // 'status',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>
