<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DiscountcouponDetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="discountcoupon-details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'discount_coupon') ?>

    <?= $form->field($model, 'discount_title') ?>

    <?= $form->field($model, 'amount_type') ?>

    <?= $form->field($model, 'amount_value') ?>

    <?php // echo $form->field($model, 'total_person_allow') ?>

    <?php // echo $form->field($model, 'min_bill') ?>

    <?php // echo $form->field($model, 'max_credit') ?>

    <?php // echo $form->field($model, 'coupon_start_dt') ?>

    <?php // echo $form->field($model, 'coupon_expire_dt') ?>

    <?php // echo $form->field($model, 'total_qty') ?>

    <?php // echo $form->field($model, 'total_used') ?>

    <?php // echo $form->field($model, 'category') ?>

    <?php // echo $form->field($model, 'deal') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
