<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DiscountcouponDetails */

$this->title = 'Update Discountcoupon Details: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Discountcoupon Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
$selected_tab = \Yii::$app->request->get('selected_tab', 'coupon-details');
$discount_id=\Yii::$app->request->get('id');
?>



<div class="discountcoupon-details-update">

    
    <div class="col-md-12">
    <!-- Panel -->
    <div class="panel">
        <div class="panel-body">
            <ul class="nav nav-tabs nav-tabs-line" data-plugin="nav-tabs" role="tablist">
                <li <?= (($selected_tab == 'coupon-details') ? 'class="active"' : ''); ?> role="presentation">
                    <a href='<?= \yii\helpers\Url::toRoute(['update','id'=>$discount_id])  ?>'  aria-controls="coupon-details" role="tab">
                        Discount Coupon Details
                    </a>
                </li>
                <li <?= (($selected_tab == 'coupon-category') ? 'class="active"' : ''); ?> role="presentation">
                    <a href='<?= \yii\helpers\Url::toRoute(['category-create','discount_id'=>$discount_id,'selected_tab'=>'coupon-category'])  ?>'   aria-controls="coupon-category" role="tab">
                        Coupon Category Allow
                    </a>
                </li>
                <li <?= (($selected_tab == 'coupon-deal') ? 'class="active"' : ''); ?> role="presentation">
                  <a href='<?= \yii\helpers\Url::toRoute(['deal-create','discount_id'=>$discount_id,'selected_tab'=>'coupon-deal'])  ?>'   aria-controls="coupon-deal" role="tab">
                        Coupon Deal Allow
                    </a>
                 </li>
            </ul>
    
            
            
            
               <div class="tab-content">
                <div class="tab-pane <?= (($selected_tab == 'coupon-details') ? 'active' : ''); ?>" id="coupon-details" role="tabpanel">
                       <?= $this->render('_form', [
                            'model' => $model,
                        ]) ?>
                    
                </div>

            </div>
        </div>
    </div>
    <!-- End Panel -->
</div>
            
            
            
            


</div>

