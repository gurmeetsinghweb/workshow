<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DealOption */
/* @var $form yii\widgets\ActiveForm */
?>
<!-- Panel Wizard Form -->
<h4>
   
</h4>
<div class="panel">
    <div class="panel-body">
        <!-- Steps -->
       
        <!-- End Steps -->

        <!-- Wizard Content -->
        <div class="wizard-content">
            <div class="wizard-pane active">
                <div class="deal-option-form">
                    <div class="row">
                        <?php $form = ActiveForm::begin(); ?>
                        <div class="col-lg-12">
                            
                 Search Category
                  <?php 
                
                     echo yii\jui\AutoComplete::widget([
                         'value'=>($model->category_id) ? $model->category->name : "",   
                         'clientOptions' => [
                                'source' => Url::toRoute(['/category/autocomplete']),
                               
                                'select' => new yii\web\JsExpression('function( event, ui ) {
                            
                            $("#user-id").val(ui.item.id);
                            
                            
                            
                          }'),
                            ],
                            'options' => ['class' => 'form-control']
                        ]);
                     ?>
                 <input type="hidden" name="category[id]" id="user-id" value="<?= ($model->category_id)? $model->category_id : "" ?>">
              
                            
                       
                        </div>
                         
                        <div class="col-lg-12"><br>
                            <?php
                            if(isset($_REQUEST['update_id'])){?>
                            <input type="hidden" name="update_id" value="1">
                            <?php }?>
                        
                        <div class="form-group pull-right">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                            <input type="hidden" name="discount-id" value="<?= $discount_id; ?>">   
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
        <!-- End Wizard Content -->

    </div>
</div>
