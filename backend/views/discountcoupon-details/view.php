<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\DiscountcouponDetails */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Discountcoupon Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discountcoupon-details-view">

   <!--  <h1><?= Html::encode($this->title) ?></h1> -->

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'discount_coupon',
            'discount_title',
            'amount_type',
            'amount_value',
            'total_person_allow',
            'min_bill',
            'max_credit',
            'coupon_start_dt',
            'coupon_expire_dt',
            'total_qty',
            'total_used',
            'category',
            'deal',
            'status',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
