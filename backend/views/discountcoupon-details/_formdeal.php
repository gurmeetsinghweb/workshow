<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DealOption */
/* @var $form yii\widgets\ActiveForm */
?>
<!-- Panel Wizard Form -->
<h4>
   
</h4>
<div class="panel">
    <div class="panel-body">
        <!-- Steps -->
       
        <!-- End Steps -->

        <!-- Wizard Content -->
        <div class="wizard-content">
            <div class="wizard-pane active">
                <div class="deal-option-form">
                     <form action="<?php echo yii\helpers\Url::toRoute(['deal-create', 'id' => $model->id, 'discount_id' => $discount_id,'selected_tab'=>$_REQUEST['selected_tab']]) ?>" method="POST">
                       
                    <div class="row">
                            <?php
                                            $request = Yii::$app->getRequest();
                                            echo yii\helpers\Html::hiddenInput($request->csrfParam, $request->getCsrfToken());
                   ?>  
                        <div class="col-lg-12">
                            
                 Search Merchant
                  <?php 
                
                     echo yii\jui\AutoComplete::widget([
                            'value'=>($merchant_detail) ? $merchant_detail->name : "",
                            'clientOptions' => [
                                'source' => Url::toRoute(['/merchant/merchant-autocomplete']),
                                'select' => new yii\web\JsExpression('function( event, ui ) {
                            $("#merchant").val(ui.item.id);
                            
                            
                          }'),
                            ],
                            'options' => ['class' => 'form-control']
                        ]);
                     ?>
                 <input type="hidden" name="merchant-id" id="merchant" value="<?= ($merchant_detail) ? $merchant_detail->id : "" ?>">
                       
                        </div>
                        
                         
                        <div class="col-lg-12">
                            <?php
                            if($merchant_deal){
                                foreach ($merchant_deal as $deal){
                                ?>
                            <div class="col-lg-12">
                                <br>
                                <input type="checkbox" name="dealid[]" value="<?= $deal->id; ?>"> <?= $deal->title; ?>
                                <br>
                            </div>
                            <?php
                             }
                            }
                            ?>
                            <br>
                            <?php
                            if(isset($_REQUEST['update_id'])){?>
                            <input type="hidden" name="update_id" value="1">
                            <?php }?>
                        
                        <div class="form-group pull-right">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                            <input type="hidden" name="discount-id" value="<?= $discount_id; ?>">   
                        </div>
                    </div>
            </form>
                </div>
            </div>
        </div>
        <!-- End Wizard Content -->

    </div>
</div>
