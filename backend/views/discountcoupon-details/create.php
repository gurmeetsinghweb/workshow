<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DiscountcouponDetails */

$this->title = 'Create Discountcoupon Details';
$this->params['breadcrumbs'][] = ['label' => 'Discountcoupon Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$selected_tab = \Yii::$app->request->get('selected_tab', 'coupon-details');

?>
<div class="discountcoupon-details-create">

    
    <div class="col-md-12">
    <!-- Panel -->
    <div class="panel">
        <div class="panel-body">
            <ul class="nav nav-tabs nav-tabs-line" data-plugin="nav-tabs" role="tablist">
                <li <?= (($selected_tab == 'coupon-details') ? 'class="active"' : ''); ?> role="presentation">
                    <a data-toggle="tab" href="#coupon-details" aria-controls="coupon-details" role="tab">
                        Discount Coupon Details
                    </a>
                </li>
                <li <?= (($selected_tab == 'coupon-category') ? 'class="active"' : ''); ?> role="presentation">
                    <a data-toggle="tab" href="#coupon-category" aria-controls="coupon-category" role="tab">
                        Coupon Category Allow
                    </a>
                </li>
                <li <?= (($selected_tab == 'coupon-deal') ? 'class="active"' : ''); ?> role="presentation">
                    <a data-toggle="tab" href="#coupon-deal" aria-controls="coupon-deal" role="tab">
                        Coupon Deals Allow
                    </a>
                </li>
            </ul>
    
            
            
            
               <div class="tab-content">
                <div class="tab-pane <?= (($selected_tab == 'coupon-details') ? 'active' : ''); ?>" id="coupon-details" role="tabpanel">
                       <?= $this->render('_form', [
                            'model' => $model,
                        ]) ?>
                    
                </div>

                <div class="tab-pane <?= (($selected_tab == 'coupon-category') ? 'active' : ''); ?>" id="coupon-category" role="tabpanel">
                         Discount Coupon create first
                </div>

                <div class="tab-pane <?= (($selected_tab == 'coupon-deal') ? 'active' : ''); ?>" id="coupon-deal" role="tabpanel">
                      Discount Coupon create first
                </div>
            </div>
        </div>
    </div>
    <!-- End Panel -->
</div>
            
            
            
            


</div>
