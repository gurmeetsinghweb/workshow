<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\GenxGridHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\DiscountcouponDetails */

$this->title = 'Update Discount coupon category Details: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Discountcoupon Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
$selected_tab = \Yii::$app->request->get('selected_tab', 'coupon-details');
$discount_id = \Yii::$app->request->get('discount_id');
?>



<div class="discountcoupon-details-update">


    <div class="col-md-12">
        <!-- Panel -->
        <div class="panel">
            <div class="panel-body">
                <ul class="nav nav-tabs nav-tabs-line" data-plugin="nav-tabs" role="tablist">
                    <li <?= (($selected_tab == 'coupon-details') ? 'class="active"' : ''); ?> role="presentation">
                        <a href='<?= \yii\helpers\Url::toRoute(['update', 'id' => $discount_id]) ?>'  aria-controls="coupon-details" role="tab">
                            Discount Coupon Details
                        </a>
                    </li>
                    <li <?= (($selected_tab == 'coupon-category') ? 'class="active"' : ''); ?> role="presentation">
                        <a href='<?= \yii\helpers\Url::toRoute(['category-create', 'discount_id' => $discount_id, 'selected_tab' => 'coupon-category']) ?>'   aria-controls="coupon-category" role="tab">
                            Coupon Category Allow
                        </a>
                    </li>
                   <li <?= (($selected_tab == 'coupon-deal') ? 'class="active"' : ''); ?> role="presentation">
                  <a href='<?= \yii\helpers\Url::toRoute(['deal-create','discount_id'=>$discount_id,'selected_tab'=>'coupon-deal'])  ?>'   aria-controls="coupon-deal" role="tab">
                        Coupon Deal Allow
                    </a>
                 </li>
            </ul>




                <div class="tab-content">


                    <div class="tab-pane <?= (($selected_tab == 'coupon-category') ? 'active' : ''); ?>" id="coupon-category" role="tabpanel">

                        <?=
                        $this->render('_formcategory', [
                            'model' => $model,
                            'discount_id' => $discount_id
                        ])
                        ?>

                    </div>


                </div>
            </div>
        </div>
        <!-- End Panel -->
    </div>






    <div class="deal-option-index panel">
        <div class="panel-body">
            <?php
           

                $button = [

                    'buttons' => [
                        'edit' => function ($url, $model, $key) {
                            $url = Url::toRoute(['category-update', 'id' => $model->id,'discount_id'=>$_REQUEST['discount_id'],'selected_tab'=>$_REQUEST['selected_tab']]);
                            return Html::a('<span class="glyphicon glyphicon-pencil" style="font-size:20px;"></span>', $url, [
                                        'title' => \Yii::t('yii', 'Update'),
                                        'data-pjax' => '0',
                            ]);
                        },
                                'delete' => function ($url, $model, $key) {

                            $url = Url::toRoute(['category-delete', 'id' => $model->id,'discount_id'=>$_REQUEST['discount_id'],'selected_tab'=>$_REQUEST['selected_tab']]);
                            return Html::a('<span class="glyphicon glyphicon-trash" style="font-size:20px;"></span>', $url, [
                                        'title' => \Yii::t('yii', 'Delete'),
                                        'data-confirm' => \Yii::t('yii', 'Are you sure to delete this deal?'),
                                        'data-method' => 'post',
                                        'data-pjax' => '0',
                            ]);
                        }
                            ],
                            'template' => '{edit} {delete} '
                        ];
                    
// echo $this->render('_search', ['model' => $searchModel]); 
                            ?>
                            <?=
                            GridView::widget([
                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    [

                                        'attribute' => 'Category',
                                        'format' => 'raw',
                                        'value' => function($model) {

                                            if (isset($model->category->name)) {
                                                $categoryname = $model->category->name;
                                                return $categoryname;
                                            }
                                            // 
                                        },
                                            ],
                                            // 'status',
                                            // 'created_at',
                                            // 'updated_at',
                                            GenxGridHelper::ActionColumn($button),
                                        ],
                                    ]);
                                    ?>

        </div>
    </div>



</div>

