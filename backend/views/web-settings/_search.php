<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\WebSettingsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="web-settings-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'website_title') ?>

    <?= $form->field($model, 'website_email') ?>

    <?= $form->field($model, 'website_mobile') ?>

    <?php // echo $form->field($model, 'website_fax') ?>

    <?php // echo $form->field($model, 'website_address') ?>

    <?php // echo $form->field($model, 'website_googlemap') ?>

    <?php // echo $form->field($model, 'slug') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
