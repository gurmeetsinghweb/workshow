<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WebSettings */

$this->title = 'Create Web Settings';
$this->params['breadcrumbs'][] = ['label' => 'Web Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="web-settings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
