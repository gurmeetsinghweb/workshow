<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\EnquirySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Enquiries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="enquiry-index">
<div style="overflow-x:auto;">
  
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Enquiry', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            
              /*  [
                     
                    'attribute' => 'Name',
                    'format'=>'raw',
                    'value' => function($model) {
                        
                        $name = $model->first_name." ".$model->last_name;
                        return $name;
                          
                    },
                    
                ],*/
                            
             
                
            
            
            
            
            'first_name' ,
            'last_name',
            'business_name',
            'email_id:email',                            
            'phone',
             'website',
           
            [
                    'attribute' => 'contact_type',
                    'filter' => common\models\Enquiry::getFormTypeForForm(),
            ],
            
            [
                     
                    'attribute' => 'State City',
                    'format'=>'raw',
                    'value' => function($model) {
                        
                        $name = $model->state." ".$model->city. " ".$model->postcode;
                        return $name;
                          
                    },
                    
                ],
              'description',
            // 'business_name',
            // 'business_email:email',
            // 'category_id',
            // 'website',
            // 'city',
            // 'description:ntext',
            // 'newsletter',
            // 'status',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>
