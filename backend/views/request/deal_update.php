<?php

use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = (($title) ? $title : 'Requests');
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
/*GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "New",
        "url" => Url::toRoute(['/request/create']),
        "icon" => "wb-plus",
    ],
    [
        "title" => "Refresh",
        "url" => Url::toRoute(['/request/index']),
        "icon" => "wb-refresh",
    ],
]);*/
?>
<div class="request-index panel">
    <div class="panel-body">
         <div class="table-responsive">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                GenxGridHelper::linkedID(),
                'from_id' => [
                    'label' => 'From',
                    'format'=>'raw',
                    'value' => function($data) {
            return Html::a($data->from->name, Url::toRoute(['/merchant/index', 'UserSearch[email]' => $data->from->email]));
                        //return $data->from->email;
                    }
                ],
                'object_id' => [
                    'label' => 'Deal ID',
                    'format'=>'raw',
                    'value' => function($data) {
                        $deal = $data->object_class;
                        //$deal = $deal::findOne($data->object_id);
                        return Html::a($data->object_id, Url::toRoute(['/deal/index', 'DealSearch[id]' => $data->object_id]));
                    }
                ],
                'deal' => [
                    'label' => 'Deal',
                    'format' => 'raw',
                    'value' => function($data) {
                        $deal_model = $data->object_class;
                        $deal = $deal_model::findOne($data->object_id);
                        if(!$deal) {
                            return '';
                        }
                        return $deal->title ;
                    }
                ],
               'remarks',
//                'assigned_id' => [
//                    'label' => 'Assigned to',
//                    'value' => function($data) {
//                        return $data->assigned->name;
//                    }
//                ],
//                 'remarks:ntext',
                // 'object_class',
                // 'data:ntext',
                // 'response:ntext',
                // 'updated_at',
                // 'created_at',
//            GenxGridHelper::status(),
                         [       
                     'attribute' => 'status',
                    'value' => function($model) {
                        $array = [
                            \common\models\RequestSearch::STATUS_ACTIVE => "Pending for Approval",
                         common\models\RequestSearch::STATUS_APPROVED => "Approved",
                         common\models\RequestSearch::STATUS_REJECTED => "Rejected",
                            
                        ];
                        return $array[$model->status];
                    },
                            'filter' => [
                                 common\models\RequestSearch::STATUS_ACTIVE => "Pending for Approval",
                         common\models\RequestSearch::STATUS_APPROVED => "Approved",
                                 common\models\RequestSearch::STATUS_REJECTED => "Rejected",
                            ]
                        ],      
                GenxGridHelper::ActionColumn([
                    'buttons' => [
                        'cancel' => function ($url, $model, $key) {
                    if(common\models\RequestSearch::STATUS_ACTIVE == $model->status){
                            $url = Url::toRoute(['deal-request-approve', 'id' => $model->id]);
                           /* return Html::a('<span class="glyphicon glyphicon-check" style="font-size:20px;"></span>', $url, [
                                        'title' => \Yii::t('yii', 'Approve'),
                                        'data-confirm' => \Yii::t('yii', 'Are you sure to approve this request? The amount will be credited in the user account instantly.'),
                                        'data-method' => 'post',
                                        'data-pjax' => '0',
                            ]);*/
                                 return Html::button('<span class="glyphicon glyphicon-check" style="font-size:20px;"></span>', [
                                'title' => \Yii::t('yii', 'Approve'),
                                'class' => 'sendapprove',
                                'value' => $model->id,
                    ]);
                    }
                        },
                                'reject' => function ($url, $model, $key) {
                            if(common\models\RequestSearch::STATUS_ACTIVE == $model->status){
                            $url = Url::toRoute(['deal-request-reject', 'id' => $model->id]);
                           /* return Html::a('<span class="glyphicon glyphicon-trash" style="font-size:20px;"></span>', $url, [
                                        'title' => \Yii::t('yii', 'Reject'),
                                        'data-confirm' => \Yii::t('yii', 'Are you sure to reject this request?'),
                                        'data-method' => 'post',
                                        'data-pjax' => '0',
                            ]);*/
                            return Html::button('<span class="glyphicon glyphicon-trash" style="font-size:20px;"></span>', [
                                'title' => \Yii::t('yii', 'Reject'),
                                'class' => 'sendreject',
                                'value' => $model->id,
                        ]);
                            }
                                }
                        
                            ],
                            'template' => '{cancel} {reject} '
                        ]),
                    ],
                ]);
                ?>
            </div>
    </div>
</div>


<div id="load_popup_modal_show_id" class="modal fade" tabindex="-1"></div>
<div id="load_popup_myreject_show_id" class="modal fade" tabindex="-1"></div>


<?php
$this->registerJs("
var dealApprove = $('#load_popup_modal_show_id');
$('.sendapprove').on('click', function(){
requestId=$(this).val(); 

dealApprove.load('" . \yii\helpers\Url::toRoute('send-request-deal-approve') . "',{'id': requestId},
function(){
dealApprove.modal('show');
});
});


var dealReject = $('#load_popup_myreject_show_id');
$('.sendreject').on('click', function(){
requestId=$(this).val(); 
dealReject.load('" . \yii\helpers\Url::toRoute('send-request-deal-reject') . "',{'id': requestId},
function(){
dealReject.modal('show');
});
});


      
");
?>