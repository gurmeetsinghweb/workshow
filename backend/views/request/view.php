<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\helpers\Url;
use common\components\GenXHelper;
use common\components\GenxGridHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Request */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
/*GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "Edit",
        "url" => Url::toRoute(['/request/update', 'id' => $model->id]),
        "icon" => "wb-edit",
    ],
    [
        "title" => "Delete",
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
        "url" => Url::toRoute(['/request/delete', 'id' => $model->id]),
        "icon" => "wb-trash",
    ],
    [
        "title" => "List",
        "url" => Url::toRoute(['/request/index']),
        "icon" => "wb-list",
    ]
]);*/
?>
<div class="request-view panel">
    <div class="panel-body">
        <h1><?= Html::encode($this->title) ?></h1>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
            'from_id',
            'type',
            'assigned_id',
            'remarks:ntext',
            'object_id',
            'object_class',
            'data:ntext',
            'response:ntext',
            'status',
            'updated_at',
            'created_at',
            ],
        ]) ?>
    </div>
</div>
