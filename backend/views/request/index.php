<?php

use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = (($title) ? $title : 'Requests');
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
/*GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "New",
        "url" => Url::toRoute(['/request/create']),
        "icon" => "wb-plus",
    ],
    [
        "title" => "Refresh",
        "url" => Url::toRoute(['/request/index']),
        "icon" => "wb-refresh",
    ],
]);*/
?>
<div class="request-index panel">
    <div class="panel-body">
         <div class="table-responsive">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                GenxGridHelper::linkedID(),
                'from_id' => [
                    'label' => 'From User',
                    'format'=>'raw',
                    'value' => function($data) {
                return Html::a($data->from->name, Url::toRoute(['/user/view', 'id' => $data->from_id]));
            
                     
                    }
                ],
                     'Merchant' => [
                    'label' => 'Merchant',
                    'format' => 'raw',
                    'value' => function($data) {
                        $coupon_class = $data->object_class;
                        $coupon = $coupon_class::findOne($data->object_id);
                        if(!$coupon) {
                            return '';
                        }
                        return Html::a($coupon->cart->merchant->name, Url::toRoute(['/merchant/index', 'UserSearch[email]' => $coupon->cart->merchant->email]));
            
                      //  return $coupon->cart->user->name . "<br /><br /><b>Option:</b>" . $coupon->cart->dealOption->name;
                    }
                ],
//                
                /*'object_id' => [
                    'label' => 'Coupon Code',
                    'value' => function($data) {
                        $coupon_class = $data->object_class;
                        $coupon = $coupon_class::findOne($data->object_id);
                        if(!$coupon) {
                            return '';
                        }
                        return $coupon->coupon_no;
                    }
                ],*/
                'deal' => [
                    'label' => 'Deal',
                    'format' => 'raw',
                    'value' => function($data) {
                        $coupon_class = $data->object_class;
                        $coupon = $coupon_class::findOne($data->object_id);
                        if(!$coupon) {
                            return '';
                        }
                        return $coupon->cart->deal->title . "<br /><br /><b>Option:</b>" . $coupon->cart->dealOption->name;
                    }
                ],
//                'assigned_id' => [
//                    'label' => 'Assigned to',
//                    'value' => function($data) {
//                        return $data->assigned->name;
//                    }
//                ],
                 
                 'remarks' => [
                    'label' => 'Remarks',
                    'format' => 'raw',
                     'filter'=>TRUE,
                    'value' => function($model) {
                    return $model->remarks;
                    }
                    ],
                // 'object_class',
                // 'data:ntext',
                // 'response:ntext',
                // 'updated_at',
                // 'created_at',
//            GenxGridHelper::status(),
                          [       
                     'attribute' => 'status',
                    'value' => function($model) {
                        $array = [
                            \common\models\RequestSearch::STATUS_ACTIVE => "Pending for Approval",
                         common\models\RequestSearch::STATUS_APPROVED => "Approved",
                         common\models\RequestSearch::STATUS_REJECTED => "Rejected",
                            
                        ];
                        return $array[$model->status];
                    },
                            'filter' => [
                                 common\models\RequestSearch::STATUS_ACTIVE => "Pending for Approval",
                         common\models\RequestSearch::STATUS_APPROVED => "Approved",
                                 common\models\RequestSearch::STATUS_REJECTED => "Rejected",
                            ]
                        ],      
                GenxGridHelper::ActionColumn([
                    'buttons' => [
                        'cancel' => function ($url, $model, $key) {
                             if(common\models\RequestSearch::STATUS_ACTIVE == $model->status){
                            $url = Url::toRoute(['c-c-approve', 'id' => $model->id]);
                            /*/return Html::a('<span class="glyphicon glyphicon-check" style="font-size:20px;"></span>','#', [
                                        'title' => \Yii::t('yii', 'Approve'),
                                        'class' => 'sendapprove',
                                        'value' => $model->id,
                            ]);*/
                            return Html::button('<span class="glyphicon glyphicon-check" style="font-size:20px;"></span>', [
                                'title' => \Yii::t('yii', 'Approve'),
                                'class' => 'sendapprove',
                                'value' => $model->id,
                    ]);
                             }
                        },
                                'reject' => function ($url, $model, $key) {
                            if(common\models\RequestSearch::STATUS_ACTIVE == $model->status){
                            $url = Url::toRoute(['c-c-reject', 'id' => $model->id]);
                            return Html::button('<span class="glyphicon glyphicon-trash" style="font-size:20px;"></span>', [
                                'title' => \Yii::t('yii', 'Reject'),
                                'class' => 'sendreject',
                                'value' => $model->id,
                    ]);
                            /*return Html::a('<span class="glyphicon glyphicon-trash" style="font-size:20px;"></span>', $url, [
                                        'title' => \Yii::t('yii', 'Reject'),
                                        'data-confirm' => \Yii::t('yii', 'Are you sure to reject this request?'),
                                        'data-method' => 'post',
                                        'data-pjax' => '0',
                            ]);*/
                            }
                            
                            }
                            ],
                     
                          
                            'template' => '{cancel} {reject} '
                        ]),
                    ],
                ]);
                ?>
            </div>
    </div>
</div>

<div id="load_popup_modal_show_id" class="modal fade" tabindex="-1"></div>
<div id="load_popup_myreject_show_id" class="modal fade" tabindex="-1"></div>




<?php
$this->registerJs("
var couponApprove = $('#load_popup_modal_show_id');
$('.sendapprove').on('click', function(){
requestId=$(this).val(); 

couponApprove.load('" . \yii\helpers\Url::toRoute('send-request-c-c-approve') . "',{'id': requestId},
function(){
couponApprove.modal('show');
});
});


var couponReject = $('#load_popup_myreject_show_id');
$('.sendreject').on('click', function(){
requestId=$(this).val(); 
couponReject.load('" . \yii\helpers\Url::toRoute('send-request-c-c-reject') . "',{'id': requestId},
function(){
couponReject.modal('show');
});
});


      
");
?>