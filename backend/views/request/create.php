<?php

use yii\helpers\Html;
use common\components\GenXHelper;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\Request */

$this->title = 'Create Request';
$this->params['breadcrumbs'][] = ['label' => 'Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "List",
        "url" => Url::toRoute(['/request/index']),
        "icon" => "wb-list",
    ]
]);
?>
<div class="request-create panel">
    <div class="panel-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    </div>
</div>
