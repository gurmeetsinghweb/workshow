<?php

use yii\helpers\Html;
use common\components\GenXHelper;
use yii\helpers\Url;
use yii\grid\GridView;
use common\components\GenxGridHelper;

/* @var $this yii\web\View */
/* @var $model common\models\DealOption */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
            'modelClass' => 'Deal Option',
        ]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Deal Options'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
//    [
//        "title" => "List",
//        "url" => Url::toRoute(['/deal-option/index']),
//        "icon" => "fa fa-list",
//    ],
    [
        "title" => "Delete",
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
        "url" => Url::toRoute(['/deal-option/delete', 'id' => $model->id]),
        "icon" => "fa fa-trash",
    ],
]);
?>
<div class="deal-option-update panel">
    <div class="panel-body">
        <?=
        $this->render('_form', [
            'model' => $model,
            'deal_model' => $deal_model,
        ])
        ?>
    </div>
</div>

<div class="deal-option-index panel">
    <div class="panel-body">
        <?php
        if(!isset($_REQUEST['update_id']))
{
    
   $button = [

                                    'buttons' => [
                                        'edit' => function ($url, $model, $key) {
                                            $url = Url::toRoute(['update', 'id' => $model->id]);
                                            return Html::a('<span class="glyphicon glyphicon-pencil" style="font-size:20px;"></span>', $url, [
                                                        'title' => \Yii::t('yii', 'Update'),
                                                        'data-pjax' => '0',
                                            ]);
                                        },
                                             
                                                
                                                
                                                
                                               
                                                'delete' => function ($url, $model, $key) {

                                            $url = Url::toRoute(['delete', 'id' => $model->id]);
                                            return Html::a('<span class="glyphicon glyphicon-trash" style="font-size:20px;"></span>', $url, [
                                                        'title' => \Yii::t('yii', 'Delete'),
                                                        'data-confirm' => \Yii::t('yii', 'Are you sure to delete this deal?'),
                                                        'data-method' => 'post',
                                                        'data-pjax' => '0',
                                            ]);
                                        }
                                            ],
                                            'template' => '{edit}  '
     ];
}
else{
    
    $button = [

                                    'buttons' => [
                                        'edit' => function ($url, $model, $key) {
                                            $url = Url::toRoute(['update', 'id' => $model->id,'update_id'=>1]);
                                            return Html::a('<span class="glyphicon glyphicon-pencil" style="font-size:20px;"></span>', $url, [
                                                        'title' => \Yii::t('yii', 'Update'),
                                                        'data-pjax' => '0',
                                            ]);
                                        },
                                             
                                                
                                                
                                                
                                               
                                                'delete' => function ($url, $model, $key) {

                                            $url = Url::toRoute(['delete', 'id' => $model->id,'update_id'=>1]);
                                            return Html::a('<span class="glyphicon glyphicon-trash" style="font-size:20px;"></span>', $url, [
                                                        'title' => \Yii::t('yii', 'Delete'),
                                                        'data-confirm' => \Yii::t('yii', 'Are you sure to delete this deal?'),
                                                        'data-method' => 'post',
                                                        'data-pjax' => '0',
                                            ]);
                                        }
                                            ],
                                            'template' => '{edit}  '
     ];
    
}

        ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                'list_price',
                'selling_price',
                'no_per_person',
                'total_sold',
                // 'status',
                // 'created_at',
                // 'updated_at',
                GenxGridHelper::ActionColumn(
                        
                       $button
                        
                        ),
            ],
        ]);
        ?>
        
         <?php
        if(!isset($_REQUEST['update_id'])){
       
        ?>
        <?= Html::a('Back - Update Deal', ['/deal/update', 'id' => $model->deal_id,], ['class' => 'pull-left btn btn-primary']) ?>
        <?= Html::a('Next - Update Gallery', ['/deal-address/create', 'deal_id' => $model->deal_id], ['class' => 'pull-right btn btn-primary']) ?>
        <?php
        
         }
        else{
           echo Html::a('Finish', ['/deal/index'], ['class' => 'pull-right btn btn-success']);  
         
        }   
        ?>
    </div>
</div>