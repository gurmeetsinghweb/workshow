<?php

use yii\helpers\Html;
use common\components\GenXHelper;
use yii\helpers\Url;
use yii\grid\GridView;
use common\components\GenxGridHelper;

/* @var $this yii\web\View */
/* @var $model common\models\DealOption */

$this->title = Yii::t('app', 'Create Deal Option');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Deal Options'), 'url' => ['deal/index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
//GenXHelper::contentHeaderButtons($this, [
//    [
//        "title" => "List",
//        "url" => Url::toRoute(['/deal-option/index']),
//        "icon" => "fa fa-list",
//    ]
//]);
?>
<div class="deal-option-create panel">
    <div class="panel-body">
        <div class="table-responsive">
        <?=
        $this->render('_form', [
            'model' => $model,
            'deal_model' => $deal_model,
        ])
        ?>
    </div>
</div>
</div>
<div class="deal-option-index panel">
    <div class="panel-body">
        <div class="table-responsive">
        <?php
        if (!isset($_REQUEST['update_id'])) {

            $button = [

                'buttons' => [
                    'edit' => function ($url, $model, $key) {
                        $url = Url::toRoute(['update', 'id' => $model->id]);
                        return Html::a('<span class="glyphicon glyphicon-pencil" style="font-size:20px;"></span>', $url, [
                                    'title' => \Yii::t('yii', 'Update'),
                                    'data-pjax' => '0',
                        ]);
                    },
                            'delete' => function ($url, $model, $key) {

                        $url = Url::toRoute(['delete', 'id' => $model->id]);
                        return Html::a('<span class="glyphicon glyphicon-trash" style="font-size:20px;"></span>', $url, [
                                    'title' => \Yii::t('yii', 'Delete'),
                                    'data-confirm' => \Yii::t('yii', 'Are you sure to delete this deal?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                        ]);
                    }
                        ],
                        'template' => '{edit} {delete} '
                    ];
                } else {

                    $button = [

                        'buttons' => [
                            'edit' => function ($url, $model, $key) {
                                $url = Url::toRoute(['update', 'id' => $model->id, 'update_id' => 1]);
                                return Html::a('<span class="glyphicon glyphicon-pencil" style="font-size:20px;"></span>', $url, [
                                            'title' => \Yii::t('yii', 'Update'),
                                            'data-pjax' => '0',
                                ]);
                            },
                                    'delete' => function ($url, $model, $key) {

                                $url = Url::toRoute(['delete', 'id' => $model->id, 'update_id' => 1]);
                                return Html::a('<span class="glyphicon glyphicon-trash" style="font-size:20px;"></span>', $url, [
                                            'title' => \Yii::t('yii', 'Delete'),
                                            'data-confirm' => \Yii::t('yii', 'Are you sure to delete this deal?'),
                                            'data-method' => 'post',
                                            'data-pjax' => '0',
                                ]);
                            }
                                ],
                                'template' => '{edit} {delete} '
                            ];
                        }

// echo $this->render('_search', ['model' => $searchModel]); 
                        ?>
                        <?=
                        GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'name',
                                'list_price',
                                'selling_price',
                                'no_per_person',
                                'total_sold',
                                'total_items',
                                // 'status',
                                // 'created_at',
                                // 'updated_at',
                                GenxGridHelper::ActionColumn(
                                        $defauls = [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}',
        ]
                                        ),
                            ],
                        ]);
                        ?>

                         </div>
                        <?php
                        if (!isset($_REQUEST['update_id'])) {
                            ?>
                            <?= Html::a('Back - Update Deal', ['/deal/update', 'id' => $model->deal_id], ['class' => 'pull-left btn btn-primary deal-address-btn']) ?>
                            <?php
                            if ($dataProvider->getCount()) {
                                echo Html::a('Next - Update Address', ['/deal-address/create', 'deal_id' => $model->deal_id], ['class' => 'pull-right btn btn-primary deal-address-btn']);
                            }
                        } else {
                            echo Html::a('Finish', ['/deal/index'], ['class' => 'pull-right btn btn-success']);
                        }
                        ?>


    </div>
</div>