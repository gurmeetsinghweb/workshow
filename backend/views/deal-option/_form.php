<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DealOption */
/* @var $form yii\widgets\ActiveForm */
?>
<!-- Panel Wizard Form -->
<h4>
    <?= $deal_model->title ?>
</h4>
<div class="panel">
    <div class="panel-body">
        <!-- Steps -->
        <div id="wizard" class="form_wizard wizard_horizontal">
            <ul class="wizard_steps">
                <li>
                    <a href="#step-1" class="done">
                        <span class="step_no">1</span>
                        <span class="step_descr">
                            Step 1<br />
                            <small>Update Deal</small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-2"  class="selected">
                        <span class="step_no">2</span>
                        <span class="step_descr">
                            Step 2<br />
                            <small>Deal options</small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-3">
                        <span class="step_no">3</span>
                        <span class="step_descr">
                            Step 3<br />
                            <small>Deal Addresses</small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-4">
                        <span class="step_no">4</span>
                        <span class="step_descr">
                            Step 4<br />
                            <small>Gallery for deal</small>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- End Steps -->

        <!-- Wizard Content -->
        <div class="wizard-content">
            <div class="wizard-pane active">
                <div class="deal-option-form">
                    <div class="row">
                        <?php $form = ActiveForm::begin(); ?>
                        <div class="col-xs-12 col-lg-12">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                        </div>
                         <?php
                            if(isset($_REQUEST['update_id'])){?>
                            <input type="hidden" name="update_id" value="1">
                            <?php }?>
                        <div class="col-xs-12 col-lg-3">
                            <?= $form->field($model, 'list_price')->textInput() ?>
                        </div>
                        <div class="col-xs-12 col-lg-3">
                            <?= $form->field($model, 'selling_price')->textInput() ?>
                        </div>
                       <div class="col-xs-12 col-lg-3">
                            <?= $form->field($model, 'no_per_person')->textInput() ?>
                        </div>
                        <div class="col-xs-12 col-lg-3">
                            <?= $form->field($model, 'total_items')->textInput() ?>
                        </div>
                        <div class="form-group pull-right">
                            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>

                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
        <!-- End Wizard Content -->

    </div>
</div>
