<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

?>
<div class="row">
    <div class="col-lg-4">{image} <br /><a id="refresh_captcha" href="javascript:;">Refresh</a></div>
    <div class="col-lg-6">{input}</div>
</div>
<?php
$this->registerJs('
    $(document).ready(function () {
        $("#refresh_captcha").click(function (event) {
            event.preventDefault();
            $(this).parent().children(\'img\').click();
        })
    });
    ');
?>