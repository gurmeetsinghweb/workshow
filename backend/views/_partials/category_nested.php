<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* @var $form yii\widgets\ActiveForm */

$services = common\models\ServiceType::getByParentID();
$service_types = yii\helpers\ArrayHelper::map($services, 'id', 'name');

$service_subtype = yii\helpers\ArrayHelper::map(common\models\ServiceType::getByParentID((int) $model->service_type_1_id), 'id', 'name');
?>
<script type="text/javascript">
    var changeSelectOptions = function (parent_id, select_id) {
        $("#" + select_id).html("");
        if (parent_id !== "") {
            $.get("<?= \yii\helpers\Url::toRoute(['/service-type/get-by-parent-id']) ?>?parent_id=" + parent_id, function (data) {
                response = JSON.parse(data);
                $.each(response, function (index, value) {
                    $("#" + select_id).append('<option value="' + value.id + '">' + value.name + '</options>');
                });
            });
        }
    }
</script>
<?php
echo $form->field($model, 'service_type_1_id')->dropDownList($service_types, [
    'prompt' => "Select Service",
    'onChange' => "changeSelectOptions(this.value, 'service_type_2_id');",
]);

echo $form->field($model, 'service_type_2_id')->dropDownList($service_subtype, [
    'prompt' => "Select Parent Service",
    'onChange' => "changeSelectOptions(this.value, 'service_type_2_id');",
    'id' => 'service_type_2_id',
]);

echo $form->field($model, 'service_type_3_id')->dropDownList($service_subtype, [
    'prompt' => "Select Parent Service",
    'id' => 'service_type_3_id',
])
?>