<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

echo $form->field($model, $attribute)->widget(\yii\redactor\widgets\Redactor::className(), [
    'clientOptions' => [
        'imageUpload' => \yii\helpers\Url::to(['/redactor/upload/image']),
    ],
]);
