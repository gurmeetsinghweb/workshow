<?php

use yii\helpers\Html;
use common\components\GenXHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\SiteSettings */

$this->title = Yii::t('app', 'Create Site Settings');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Site Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "List",
        "url" => Url::toRoute(['/site-settings/index']),
        "icon" => "fa fa-list",
    ]
]);
?>
<div class="site-settings-create panel">
    <div class="panel-body">
        <?=
        $this->render('_form', [
            'model' => $model,
            'attribute' => $attribute,
        ])
        ?>
    </div>
</div>
