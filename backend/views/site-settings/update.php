<?php

use yii\helpers\Html;
use common\components\GenXHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\SiteSettings */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
            'modelClass' => 'Site Settings',
        ]) . ' ' . $model->key;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Site Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update - ' . $model->key);
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "List",
        "url" => Url::toRoute(['/site-settings/index']),
        "icon" => "fa fa-list",
    ],
    [
        "title" => "Delete",
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
        "url" => Url::toRoute(['/site-settings/delete', 'id' => $model->id]),
        "icon" => "fa fa-trash",
    ],
]);
?>
<div class="site-settings-create panel">
    <div class="panel-body">
        <?=
        $this->render('_form', [
            'model' => $model,
            'attribute' => $attribute,
        ])
        ?>

    </div>
</div>
