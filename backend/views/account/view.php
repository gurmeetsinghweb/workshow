<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\City */

//$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Account Report'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);

backend\assets\ChartAsset::register($this);
backend\assets\FromAssets::register($this);
?>

<!-- top tiles -->
<div class="row top_tiles">
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats  btn-danger" style="background:#004a02">
                  <div class="icon"></div>
                    <div class="count"><i class="fa fa-usd"></i><?= $metrics['total_payment']?></div>
                    <h3>Total Sale</h3>
                    <p>Last <?= $number_of_days; ?> Days.</p>
                </div>
            </div>
            
            
       <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-dark" style="background:#585858" >
                  <div class="icon"><!--<i class="fa fa-sort-amount-desc"></i>--></div>
                    <div class="count"><i class="fa fa-usd"></i><?= $metrics['total_merchant'] ?></div>
                    <h3> Total Merchant Sale</h3>
                    <p>&nbsp;</p>
                </div>
            </div>
     <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-dark" style="background:#008803" >
                  <div class="icon"><!--<i class="fa fa-sort-amount-desc"></i>--></div>
                    <div class="count"><i class="fa fa-usd"></i><?= $metrics['total_merchantgiven'] ?></div>
                    <h3>Merchant Given</h3>
                    <p>&nbsp;</p>
                </div>
            </div>
     <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-dark" style="background:#ab0b0b" >
                  <div class="icon"><!--<i class="fa fa-sort-amount-desc"></i>--></div>
                    <div class="count"><i class="fa fa-usd"></i><?= $metrics['total_company'] ?></div>
                    <h3>VS Amount</h3>
                    <p>&nbsp;</p>
                </div>
            </div>
     <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-dark" style="background:#c79735" >
                  <div class="icon"><!--<i class="fa fa-sort-amount-desc"></i>--></div>
                    <div class="count"><i class="fa fa-usd"></i><?= $metrics['total_companygiven'] ?></div>
                    <h3>VS Given</h3>
                    <p>&nbsp;</p>
                </div>
            </div>
     
     <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats btn-dark" style="background:#2a3f54" >
                  <div class="icon"><!--<i class="fa fa-sort-amount-desc"></i>--></div>
                    <div class="count"><i class="fa fa-usd"></i><?= $metrics['total_companytax'] ?></div>
                    <h3>VS Tax</h3>
                    <p>&nbsp;</p>
                </div>
            </div>
     
            
 </div>



<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-12">
                    <?php if ($date_set): ?>
                        <h3>Last <?= $number_of_days; ?> Days' Sale<small> Report</small></h3>
                    <?php else: ?>
                        <h3>Sale<small> Report</small></h3>
                    <?php endif; ?>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
                <div style="width: 100%;">
                    <canvas id="sale-wise-chart"></canvas>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>




</div>

<div class="category-view panel">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
                <?php
                $form = \yii\widgets\ActiveForm::begin(['method' => 'GET', 'id' => 'search_form', 'action' => Url::toRoute(['/account/view'])]);
                ?>
                <div class="col-lg-3">
                    Choose Date to filter records
                    <input type="text" style="width: 200px" name="date_range" id="date_range" class="form-control" value="<?= $POST['date_range'] ?>" />
                </div>
                <div class="col-lg-3">
                    Choose Merchant filter records
                    <?php
                    echo yii\jui\AutoComplete::widget([
                        'clientOptions' => [
                            'source' => Url::toRoute(['/merchant/merchant-autocomplete']),
                            'select' => new yii\web\JsExpression('function( event, ui ) {
                            $("#merchant_id").val(ui.item.id);
                            $("#user_name").val(ui.item.value);
                          }'),
                        ],
                        'options' => ['class' => 'form-control', 'placeholder' => ((isset($_GET['user_name'])) ? $_GET['user_name'] : "")]
                    ]);
                    ?>
                    <input type="hidden" id="merchant_id" value="<?php if(isset($POST['merchant_id'])) { echo $POST['merchant_id']; }    ?>" name="merchant_id" />
                    <input type="hidden" id="user_name" value="<?php if(isset($POST['user_name'])) { echo $POST['user_name']; }    ?>"  name="user_name" />
                </div>
                <div class="col-lg-3">
                    <br />
                    <input type="submit" value="Filter Records" class="btn btn-success btn-sm"/>
                </div>
                <div class="col-lg-3 ">
                    <br />
                    <?=
                    yii\helpers\Html::a('Reset', ['account/view'], ['class' => 'btn btn-danger btn-sm']);
                    ?>
                </div>
                <?php
                yii\widgets\ActiveForm::end();
                ?>
            </div>
        </div>
    </div>
    
    
    
    
    
    
    
    <div class="panel-body account">
      <div style="overflow-x:auto;">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                
                [
                     
                    'attribute' => 'Payout ID',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        return "Payout ID ".$dataProvider->transaction_payment_id;
                          
                    },
                    
                ],
                
                [
                     
                    'attribute' => 'Merchant Income Paid',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        return "$".$dataProvider->total_merchant_amount;
                          
                    },
                    
                ],
               [
                     
                    'attribute' => 'Company Income Paid',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        return "$".$dataProvider->total_site_amount;
                          
                    },
                    
                ],
                [
                    'label' => 'Generated at',
                    'format' => 'date',
                    'attribute' => 'created_at',
                    'filter' => False,
                ],
                   [
                     
                    'attribute' => 'Action',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        if(isset($_REQUEST['merchant_id'])){
                            
                            $mervalue =  $_REQUEST['merchant_id'];
                        
                            
                        }
                        else{
                            $mervalue="";
                                    
                        }
                        
                        $moredetails = \yii\helpers\Html::a(
                                        '<i class="fa fa-list" aria-hidden="true"></i> Merchant Details', \yii\helpers\Url::toRoute([
                                            'merchant-income',
                                            'id' => $dataProvider->transaction_payment_id,'merchant_id'=> $mervalue
                                                
                                        ]), ['data-method' => 'post','class' => 'btn btn-success btn-sm']);
                        
                              $moredetails.= \yii\helpers\Html::a(
                                        '<i class="fa fa-list" aria-hidden="true"></i> Company Details', \yii\helpers\Url::toRoute([
                                            'company-income',
                                            'id' => $dataProvider->transaction_payment_id
                                        ]), ['data-method' => 'post','class' => 'btn btn-info btn-sm']);
                              
                             return $moredetails; 
                          
                    },
                    
                ],         
             
                        // 'slug',
                        // 'image_url:url',
                        // 'nationality',
                        // 'dob',
                        // 'yob',
                        // 'hint_question',
                        // 'hint_answer',
                        // 'agreed',
                        // 'created_at',
                        // 'updated_at',
                       // GenxGridHelper::ActionColumn()
                    ],
                ]);
                ?>

   
    </div>
    </div>
    
    
    
    
    
    
    
</div>



<?php
$cart_js = "
      
        var sale_wise_chart = new Chart(document.getElementById(\"sale-wise-chart\"), {
        type: 'bar',
        data: " . json_encode($merchant_company_sale) . ",
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
      
      $('#date_range').daterangepicker(null, function(start, end, label) {});
      
    ";

$this->registerJs($cart_js);
