<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use yii\grid\GridView;
use common\models\User;
use \common\components\GenXHelper;
use common\components\GenxGridHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Account Pending Payment');
//$this->params['breadcrumbs'][] = $model->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [

]);
?>

<!-- top tiles -->
<div class="row top_tiles">
     <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="tile-stats  btn-danger" style="background:#004a02">
                  <div class="icon"></div>
                    <div class="count"><i class="fa fa-usd"></i><?= $metrics['total_merchant']?></div>
                    <h3>Total Merchant</h3>
                    <p>&nbsp;</p>
                </div>
            </div>
            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="tile-stats btn-primary" style="background:#337ab7">
                    <div class="icon"></div>
                    <div class="count"><i class="fa fa-usd"></i><?= $metrics['total_company'] ?></div>
                    <h3>VS Amount</h3>
                    <p>&nbsp;</p>
                </div>
            </div>
    <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="tile-stats btn-dark" style="background:#7954e3" >
                  <div class="icon"><!--<i class="fa fa-sort-amount-desc"></i>--></div>
                    <div class="count"><i class="fa fa-usd"></i><?= $metrics['total_company_tax'] ?></div>
                    <h3>VS Tax </h3>
                    <p>&nbsp;</p>
                </div>
            </div>
   
    
</div>
<!-- /top tiles -->

<div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
                
                
                <div class="col-lg-3">
                    <br />
                    <?=
                        yii\helpers\Html::a('<i class="ti-list" aria-hidden="true"></i> Generate Income', ["create-payout"], ['data-method' => 'post', 'class' => 'btn btn-raised btn-primary ripple-effect btn-md','data-confirm'=>'Are you sure, you want to create income to merchant']);
                    ?> 
                </div>
               
                
            </div>
        </div>
    </div>

<div class="panel">
    <div class="user-index panel-body">
        <div class="table-responsive">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                
  //GenxGridHelper::linkedID(),
 //GenxGridHelper::linkedTitle('merchant_id'),
               
//            'auth_key',
//            'sms_otp',
//            'password_hash',
                // 'password_reset_token',
                //'email:email',
                [
                     
                    'attribute' => 'Merchant Name',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        return $dataProvider->merchant->name;
                          
                    },
                    
                ],
               [
                     
                    'attribute' => 'Merchant Amount',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        return $dataProvider->merchant_total_payment;
                          
                    },
                    
                ],
                             [
                     
                    'attribute' => 'Company Profit',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        return $dataProvider->site_amount_excluding_gst;
                          
                    },
                    
                ],
                                 [
                     
                    'attribute' => 'Company Tax',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        return $dataProvider->site_gst;
                          
                    },
                    
                ],
             
                        // 'slug',
                        // 'image_url:url',
                        // 'nationality',
                        // 'dob',
                        // 'yob',
                        // 'hint_question',
                        // 'hint_answer',
                        // 'agreed',
                        // 'created_at',
                        // 'updated_at',
                       // GenxGridHelper::ActionColumn()
                    ],
                ]);
                ?>
            </div>
    </div>
</div>