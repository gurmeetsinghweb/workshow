<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\City */

//$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Account Report'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);

backend\assets\ChartAsset::register($this);
backend\assets\FromAssets::register($this);
?>

<!-- top tiles -->

<!-- /top tiles -->


<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-12">
                    
                        <h3> Company <small> Report</small></h3>
                   
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
                <div style="width: 100%;">
                    <canvas id="sale-wise-chart"></canvas>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>




</div>

<div class="category-view panel">
    
    
    
    
    
    <div class="panel-body">
      
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                
                [
                     
                    'attribute' => 'Transaction ID',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        return "Transaction ID ".$dataProvider->id;
                          
                    },
                    
                ],
                [
                     
                    'attribute' => 'Merchant Name',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        return $dataProvider->merchant->name;
                          
                    },
                    
                ],
                [
                     
                    'attribute' => 'Company Income',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        return "$".$dataProvider->total_site_amount;
                          
                    },
                    
                ],
                
               [
                     
                    'attribute' => 'Company GST',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        return "$".$dataProvider->total_site_gst;
                          
                    },
                    
                ],
                            
               
                            
             
                        // 'slug',
                        // 'image_url:url',
                        // 'nationality',
                        // 'dob',
                        // 'yob',
                        // 'hint_question',
                        // 'hint_answer',
                        // 'agreed',
                        // 'created_at',
                        // 'updated_at',
                       // GenxGridHelper::ActionColumn()
                    ],
                ]);
                ?>

   
    </div>
    
    
    
    
    
    
    
</div>



<?php
$cart_js = "
      
        var sale_wise_chart = new Chart(document.getElementById(\"sale-wise-chart\"), {
        type: 'bar',
        data: " . json_encode($merchant_company_sale) . ",
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
      
      $('#date_range').daterangepicker(null, function(start, end, label) {});
      
    ";

$this->registerJs($cart_js);
