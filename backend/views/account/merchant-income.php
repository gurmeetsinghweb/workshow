<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\GenXHelper;
use common\components\GenxGridHelper;

use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\City */

//$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Account Report'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);

backend\assets\ChartAsset::register($this);
backend\assets\FromAssets::register($this);
?>

<!-- top tiles -->
<div class="row tile_count">
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-usd"></i> Total Merchant</span>
        <div class="count"><?= "$" . $metrics['total_merchant'] ?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-clock-o"></i> Merchant Given</span>
        <div class="count green"><?= "$" . $metrics['total_merchantgiven'] ?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-clock-o"></i> Merchant Pending</span>
        <div class="count green"><?= "$" . $metrics['total_merchantpending'] ?></div>
    </div>

    
</div>
<!-- /top tiles -->


<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-12">
                    <?php if ($date_set): ?>
                        <h3>Last <?= $number_of_days; ?> Days' Sale<small> Report</small></h3>
                    <?php else: ?>
                        <h3>Sale<small> Report</small></h3>
                    <?php endif; ?>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
                <div style="width: 100%;">
                    <canvas id="sale-wise-chart"></canvas>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>




</div>

<div class="category-view panel">
<?php
if (Yii::$app->user->isAdmin()) 
    {
  ?>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">
                <?php
                $form = \yii\widgets\ActiveForm::begin(['method' => 'GET', 'id' => 'search_form', 'action' => Url::toRoute(['/account/merchant-account'])]);
                ?>
                <div class="col-lg-3">
                    Choose Date to filter records
                    <input type="text" style="width: 200px" name="date_range" id="date_range" class="form-control" value="<?= $POST['date_range'] ?>" />
                </div>
                <div class="col-lg-3">
                    Choose Merchant filter records
                    <?php
                    echo yii\jui\AutoComplete::widget([
                        'clientOptions' => [
                            'source' => Url::toRoute(['/merchant/merchant-autocomplete']),
                            'select' => new yii\web\JsExpression('function( event, ui ) {
                            $("#merchant_id").val(ui.item.id);
                            $("#user_name").val(ui.item.value);
                          }'),
                        ],
                        'options' => ['class' => 'form-control', 'placeholder' => ((isset($_GET['user_name'])) ? $_GET['user_name'] : "")]
                    ]);
                    ?>
                    <input type="hidden" id="merchant_id" value="" name="merchant_id" />
                    <input type="hidden" id="user_name" value=""  name="user_name" />
                </div>
                <div class="col-lg-3">
                    <br />
                    <input type="submit" value="Filter Records" class="btn btn-success btn-sm"/>
                </div>
                <div class="col-lg-3 pull-right">
                    <br />
                    <?=
                    yii\helpers\Html::a('Reset', ['account/view'], ['class' => 'btn btn-danger btn-sm']);
                    ?>
                </div>
                <?php
                yii\widgets\ActiveForm::end();
                ?>
            </div>
        </div>
    </div>
<?php 

}
?>
    
    
    
    
    
    
    <div class="panel-body">
      
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                
                [
                     
                    'attribute' => 'Payout ID',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        return "Payout ID ".$dataProvider->id;
                          
                    },
                    
                ],
                
                [
                     
                    'attribute' => 'Merchant Income Paid',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        return "$".$dataProvider->total_merchant_amount;
                          
                    },
                    
                ],
             
                        // 'slug',
                        // 'image_url:url',
                        // 'nationality',
                        // 'dob',
                        // 'yob',
                        // 'hint_question',
                        // 'hint_answer',
                        // 'agreed',
                        // 'created_at',
                        // 'updated_at',
                       // GenxGridHelper::ActionColumn()
                    ],
                ]);
                ?>

   
    </div>
    
    
    
    
    
    
    
</div>



<?php
$cart_js = "
      
        var sale_wise_chart = new Chart(document.getElementById(\"sale-wise-chart\"), {
        type: 'bar',
        data: " . json_encode($merchant_company_sale) . ",
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
      
      $('#date_range').daterangepicker(null, function(start, end, label) {});
      
    ";

$this->registerJs($cart_js);
