<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\City */

//$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Account Report'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);

backend\assets\ChartAsset::register($this);
backend\assets\FromAssets::register($this);
?>

<!-- top tiles -->

<!-- /top tiles -->


<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-12">
                    
                        <h3>  Merchant <small> Report</small></h3>
                   
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
                <div style="width: 100%;">
                    <canvas id="sale-wise-chart"></canvas>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>




</div>

<div class="category-view panel">
    
    
    
    
    
    <div class="panel-body">
      
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                
                [
                     
                    'attribute' => 'Payout ID',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        return "Payout ID ".$dataProvider->transaction_payment_id;
                          
                    },
                    
                ],
                [
                     
                    'attribute' => 'Transaction ID',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        return "Transaction ID ".$dataProvider->id;
                          
                    },
                    
                ],
                [
                     
                    'attribute' => 'Merchant Name',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        return $dataProvider->merchant->name;
                          
                    },
                    
                ],
                [
                     
                    'attribute' => 'Merchant Income',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        return "$".$dataProvider->total_merchant_amount;
                          
                    },
                    
                ],
                [
                     
                    'attribute' => 'Merchant GST',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                        return "$".$dataProvider->total_merchant_gst;
                          
                    },
                    
                ],
                 [
                     
                    'attribute' => 'Created Date',
                    'format'=>'date',
                    'value' => function($dataProvider) {
                        return $dataProvider->created_at;
                          
                    },
                    
                ],
                 [
                     
                    'attribute' => 'Action',
                    'format'=>'raw',
                    'value' => function($dataProvider) {
                       
                        $moredetails = \yii\helpers\Html::a(
                                        '<i class="fa fa-list" aria-hidden="true"></i> View more', \yii\helpers\Url::toRoute([
                                            'coupon-code/index',
                                            'payout_id' => $dataProvider->transaction_payment_id,'merchant_id' => $dataProvider->merchant_id
                                                
                                        ]), ['data-method' => 'post','class' => 'btn btn-success btn-sm']);
                        
                             
                              
                             return $moredetails; 
                          
                    },
                    
                ],         
                       
                            
               
                            
             
                        // 'slug',
                        // 'image_url:url',
                        // 'nationality',
                        // 'dob',
                        // 'yob',
                        // 'hint_question',
                        // 'hint_answer',
                        // 'agreed',
                        // 'created_at',
                        // 'updated_at',
                       // GenxGridHelper::ActionColumn()
                    ],
                ]);
                ?>

   
    </div>
    
    
    
    
    
    
    
</div>



<?php
$cart_js = "
      
        var sale_wise_chart = new Chart(document.getElementById(\"sale-wise-chart\"), {
        type: 'bar',
        data: " . json_encode($merchant_company_sale) . ",
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
      
      $('#date_range').daterangepicker(null, function(start, end, label) {});
      
    ";

$this->registerJs($cart_js);
