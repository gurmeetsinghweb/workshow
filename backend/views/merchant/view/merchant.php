<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\widgets\DetailView;
$selected_tab = \Yii::$app->request->get('selected_tab', 'user-profile');
?>
<div class="col-md-9">
    <!-- Panel -->
    <div class="panel">
        <div class="panel-body">
            <ul class="nav nav-tabs nav-tabs-line" data-plugin="nav-tabs" role="tablist">
                <li <?= (($selected_tab == 'user-profile') ? 'class="active"': '');?> role="presentation">
                    <a data-toggle="tab" href="#user-profile" aria-controls="user-profile" role="tab">
                        User Profile
                    </a>
                </li>
                <li <?= (($selected_tab == 'company-profile') ? 'class="active"': '');?> role="presentation">
                    <a data-toggle="tab" href="#company-profile" aria-controls="company-profile" role="tab">
                        Company Profile
                    </a>
                </li>
               <!-- <li <?= (($selected_tab == 'deals') ? 'class="active"': '');?> role="presentation">
                    <a data-toggle="tab" href="#deals" aria-controls="deals" role="tab">
                        Deals
                    </a>
                </li>-->
            </ul>

            <div class="tab-content">
                <div class="tab-pane <?= (($selected_tab == 'user-profile') ? 'active': '');?>" id="user-profile" role="tabpanel">
                    <?=
                    DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'username',
                            'email:email',
                            'mobile',
                            'status',
                            'type',
                            'name',
                            'slug',
                             [
                                'attribute' => 'city_id',
                                'format' => 'raw',
//                                'value'=> yii\helpers\Html::a($model->city->name, ['/city/view', 'id' => $model->city->id])
                                'value' => $model->city_id
                            ],
                            'dob',
                        ],
                    ])
                    ?>
                </div>

                <div class="tab-pane <?= (($selected_tab == 'company-profile') ? 'active': '');?>" id="company-profile" role="tabpanel">
                    <?=
                    DetailView::widget([
                        'model' => $model->company,
                        'attributes' => [
                            'id',
                            'title',
                            'slug',
                            'logo',
                            'address',
                            'city_id',
                            'state_id',
                            'country_id',
                            'contact',
                            'website',
                            'facebook',
                            'twitter',
                            'youtube',
                            'gplus',
                            [
                                'attribute' => 'updated_at',
                                'label' => 'Last Updated',
                                'value' => $model->getUpdatedTime()
                            ],
                            'created_at:datetime',
                        ],
                    ])
                    ?>
                </div>

               <!-- <div class="tab-pane <?= (($selected_tab == 'deals') ? 'active': '');?>" id="deals" role="tabpanel">

                </div>-->
            </div>
        </div>
    </div>
    <!-- End Panel -->
</div>