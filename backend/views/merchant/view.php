<?php

use yii\widgets\DetailView;
use \common\components\GenXHelper;
use yii\helpers\Html;
use \yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Merchant'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "Edit",
        "url" => Url::toRoute(['/merchant/update', 'id' => $model->id]),
        "icon" => "fa fa-edit",
    ],
    [
        "title" => "Delete",
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
        "url" => Url::toRoute(['/merchant/delete', 'id' => $model->id]),
        "icon" => "fa fa-trash",
    ],
]);
?>

<div class="user-view">
    <div class="page-content container-fluid">
        <div class="row" style="margin-right:0px !important; margin-left:-43px !important;">
            <div class="col-md-3">
                <!-- Page Widget -->
                <div class="panel">
                    <div class="widget widget-shadow text-center">
                        <div class="widget-header">
                            <div class="widget-header-content">
                                <a class="avatar avatar-lg" href="javascript:void(0)">
                                    <img  title="<?= $model->name ?>" src="<?= $model->getIconURL('large') ?>">
                                </a></br>
                                <div class="profile-user">
                                    <?php echo $model->name; ?>
                                </div>
                                <div class="profile-job">
                                    <?php echo $model->email; ?>
                                </div>
                                <div class="profile-job">
                                    <?php echo $model->mobile; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Page Widget -->
                </div>
            </div>
            <?= $this->render('view/' . $model->type, ['model' => $model]); ?>
        </div>
    </div>
</div>

