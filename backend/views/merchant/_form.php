<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

foreach (range((date('Y') - 100), (date('Y') - 18)) as $yr)
    $yr_list[$yr] = $yr;

foreach (range(1, 12) as $mon)
    $mon_list[$mon] = $mon;

foreach (range(1, 31) as $day)
    $day_list[$day] = $day;
?>

<div class="user-form panel-body">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'password_repeat')->passwordInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'nationality')->dropDownList(common\models\Country::find()->select(['name', 'id'])->indexBy('id')->column(), ['maxlength' => true]) ?>
    <?= $this->render('@common/views/_partials/country', ['form' => $form, 'model' => $model, 'attribute' => 'country_id']); ?>
    <?= $this->render('@common/views/_partials/state', ['form' => $form, 'model' => $model, 'attribute' => 'state_id']); ?>
    <?= $this->render('@common/views/_partials/city', ['form' => $form, 'model' => $model, 'attribute' => 'city_id']); ?>
    <div class="form-group field-user-dob">
        <label class="control-label" for="user-dob">Date of Birth</label>
        <?= Html::activeDropDownList($model, "dob_d", $day_list, ['class' => 'form-control form_dob', 'prompt' => "Select Day"]); ?>
        <?= Html::activeDropDownList($model, "dob_m", $mon_list, ['class' => 'form-control form_dob', 'prompt' => "Select Month"]); ?>
        <?= Html::activeDropDownList($model, "dob_y", $yr_list, ['class' => 'form-control form_dob', 'prompt' => "Select Year"]); ?>
        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'hint_question')->dropDownList($model->getHintQuestions()) ?>
    <?= $form->field($model, 'hint_answer')->textInput(['maxlength' => true]) ?>

    <?php
    if (Yii::$app->user->can('admin')) {
        echo $form->field($model, 'status')->dropDownList($model->getUserStatusDropdownForForm());
        //echo $form->field($model, 'type')->dropDownList($model->getUserTypeDropdownForForm(), ['maxlength' => true]);
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
