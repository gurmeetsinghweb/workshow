<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use yii\grid\GridView;
use common\models\User;
use \common\components\GenXHelper;
use common\components\GenxGridHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Merchant Listing');
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "New",
        "url" => Url::toRoute(['/company/create']),
        "icon" => "fa fa-plus",
    ],
    [
        "title" => "Refresh",
        "url" => Url::toRoute(['/merchant']),
        "icon" => "fa fa-refresh",
    ],
]);
?>
<div class="panel">
    <div class="user-index panel-body">
        <div class="table-responsive">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                GenxGridHelper::linkedID(),
                GenxGridHelper::linkedTitle('name'),
              //  'username',
//            'auth_key',
//            'sms_otp',
//            'password_hash',
                // 'password_reset_token',
                'email:email',
                
                [
                     
                    'attribute' => 'Company Name',
                    'format'=>'raw',
                    'value' => function($model) {
                        
            if(isset($model->company->title))
            {
                        $company_name = $model->company->title;
                        return Html::a($company_name, Url::toRoute(['/company/update', 'id' => $model->company->id]));  
            }
            else{
                
            }
                       // 
                          
                    },
                    
                ],
                [
                     
                    'attribute' => 'Total Deals',
                    'format'=>'raw',
                    'value' => function($model) {
                        $deal_count = \common\models\Deal::find()->andWhere(['user_id' => $model->id])->count();
                        return Html::a($deal_count, Url::toRoute(['/deal/index', 'DealSearch[user_id]' => $model->id]));
                          
                    },
                    
                ],
             
                            [
                     
                    'attribute' => 'Total Sold',
                    'format'=>'raw',
                    'value' => function($model) {
                        $deal_sold = \common\models\Cart::find()->andWhere(['merchant_id' => $model->id])->count();
                        return $deal_sold;
                          
                    },
                    
                ],
                            [
                     
                    'attribute' => 'Total Redeemed',
                    'format'=>'raw',
                    'value' => function($model) {
                        $deal_redeemed =(int) \common\models\DealOption::find()->joinWith("deal")->andWhere(['deal.user_id' => $model->id])->sum("total_redeem");
                        return $deal_redeemed;
                          
                    },
                    
                ],
             
             
                            
//            [
//                'attribute' => 'country_id',
//                'value' => function($model) {
//                    return $model->country->name;
//                },
//                'filter' => \yii\helpers\ArrayHelper::map(common\models\Country::find()->asArray()->all(), 'id', 'name')
//            ],
                // 'mobile',
                [
                    'attribute' => 'status',
                    'value' => function($model) {
                        $array = [
                            User::STATUS_ACTIVE => "Active",
                            User::STATUS_PENDING_VALIDATION => "Not Validated",
                        ];
                        return $array[$model->status];
                    },
                            'filter' => [
                                User::STATUS_ACTIVE => "Active",
                                User::STATUS_PENDING_VALIDATION => "Not Validated",
                            ]
                        ],
                        // 'slug',
                        // 'image_url:url',
                        // 'nationality',
                        // 'dob',
                        // 'yob',
                        // 'hint_question',
                        // 'hint_answer',
                        // 'agreed',
                        // 'created_at',
                        // 'updated_at',
                        GenxGridHelper::ActionColumn()
                    ],
                ]);
                ?>

            </div>
    </div>
</div>