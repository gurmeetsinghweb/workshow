<?php

use yii\helpers\Html;
use common\components\GenXHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
            'modelClass' => 'Category',
        ]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "Delete",
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
        "url" => Url::toRoute(['/category/delete', 'id' => $model->id]),
        "icon" => "fa fa-trash",
    ],
    [
        "title" => "List",
        "url" => Url::toRoute(['/category']),
        "icon" => "fa fa-list",
    ]
]);
?>
<div class="panel">
    <div class="category-update panel-body">
        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>
    </div>
</div>
