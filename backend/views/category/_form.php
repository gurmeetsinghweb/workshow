<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'parent_id')->dropDownList($model->getTreeForDropdown(['blank' => True])); ?>
    <?= $this->render("@common/views/_partials/status", ['model' => $model, 'form' => $form]); ?>
    <?= $form->field($model, 'fa')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'showhome')->dropDownList(common\components\GenxBaseModel::getFeaturedDropdown());?>
    <?= $form->field($model, 'serial_order')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'meta_title')->textInput(['maxlength' => false]) ?>
    <?= $form->field($model, 'meta_description')->textarea(['maxlength' => false]) ?>
    <?= $form->field($model, 'meta_keyword')->textarea(['maxlength' => false]) ?>
    
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
