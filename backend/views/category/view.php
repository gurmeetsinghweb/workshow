<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\components\GenXHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "Add Child",
        "url" => Url::toRoute(['/category/create', 'parent_id' => $model->id]),
        "icon" => "fa fa-plus",
    ],
    [
        "title" => "Edit",
        "url" => Url::toRoute(['/category/update', 'id' => $model->id]),
        "icon" => "fa fa-edit",
    ],
    [
        "title" => "Delete",
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
        "url" => Url::toRoute(['/category/delete', 'id' => $model->id]),
        "icon" => "fa fa-trash",
    ],
    [
        "title" => "List",
        "url" => Url::toRoute(['/category']),
        "icon" => "fa fa-list",
    ]
]);
?>
<div class="category-view panel">
    <div class="panel-body">
        <h1>
            <?= $model->name; ?>

            <?php
            if ($model->parent_id) {
                ?>
                <i class="small">in</i> <?= Html::a($model->parent->name, $model->parent->getURL()) ?>
                <?php
            }
            ?>
        </h1>
        <hr />
        <h4>Child Categories</h4>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'id',
                [
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => function($model) {
                        return Html::a($model->name, $model->getURL());
                    }
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    </div>
</div>
