<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Url;
use common\components\GenXHelper;

$this->title = Yii::t('app', 'Nested Categories');
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile(Yii::$app->request->baseUrl . "/vendors/bootstrap-treeview/bootstrap-treeview.min.css", ['depends' => 'backend\assets\ReMarkAssets']);
$this->registerJsFile(Yii::$app->request->baseUrl . "/vendors/bootstrap-treeview/bootstrap-treeview.min.js", ['depends' => 'backend\assets\ReMarkAssets']);
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "New",
        "url" => Url::toRoute(['/category/create']),
        "icon" => "fa fa-plus",
    ],
    [
        "title" => "Refresh",
        "url" => Url::toRoute(['/category']),
        "icon" => "fa fa-refresh",
    ],
    [
        "title" => "List",
        "url" => Url::toRoute(['/category']),
        "icon" => "fa fa-list",
    ]
]);
?>
<div class="panel">
    <div class="panel-body">
        <div id="category_tree"></div>
    </div>
</div>

<?php
$category_json_data = [];
if (sizeof($data)) {
    foreach ($data as $parent_data) {
        foreach ($parent_data as $_d1) {
            $tmp_array = [
                'text' => $_d1['name'],
                'href' => "#" . $_d1['slug'],
                'tags' => [(string) $_d1['active_deals']]
            ];
            if (isset($_d1['children'])) {
                $tmp_array_2 = array();
                $tmp_array_2_ = array();
                foreach ($_d1['children'] as $_d2) {
                    $tmp_array_2_ = [
                        'text' => $_d2['name'],
                        'href' => Url::toRoute(["view", "id" => $_d2['id']]),
                        'tags' => [(string) $_d2['active_deals']]
                    ];
                    if (isset($_d2['children'])) {
                        $tmp_array_3 = array();
                        foreach ($_d2['children'] as $_d3) {
                            $tmp_array_3[] = [
                                'text' => $_d3['name'],
                                'href' => "#" . $_d3['slug'],
                                'tags' => [(string) $_d3['active_deals']]
                            ];
                        }
                        $tmp_array_2_['nodes'] = $tmp_array_3;
                    }

                    $tmp_array_2[] = $tmp_array_2_;
                }
                $tmp_array['nodes'] = $tmp_array_2;
            }
            $category_json_data[] = $tmp_array;
        }
    }
}

$this->registerJs("
    var getExampleTreeview = function() {
        return " . json_encode($category_json_data) . "
      };
    ", 1);

//$this->registerJs("
//    var getExampleTreeview = function() {
//        return [{
//          text: 'Parent 1',
//          href: '#parent1',
//          tags: [4],
//          nodes: [{
//            text: 'Child 1',
//            href: '#child1',
//            tags: ['2'],
//            nodes: [{
//              text: 'Grandchild 1',
//              href: '#grandchild1',
//              tags: ['0']
//            }, {
//              text: 'Grandchild 2',
//              href: '#grandchild2',
//              tags: ['0']
//            }]
//          }, {
//            text: 'Child 2',
//            href: '#child2',
//            tags: ['0']
//          }]
//        }, {
//          text: 'Parent 2',
//          href: '#parent2',
//          tags: ['0']
//        }, {
//          text: 'Parent 3',
//          href: '#parent3',
//          tags: ['0']
//        }, {
//          text: 'Parent 4',
//          href: '#parent4',
//          tags: ['0']
//        }, {
//          text: 'Parent 5',
//          href: '#parent5',
//          tags: ['0']
//        }];
//      };
//    ", 1);
$this->registerJs("
    $('#category_tree').treeview(
      {
        data: getExampleTreeview(), 
        showTags: true, 
        onNodeSelected: function(event, data) {
            window.location.href = data.href;
        },
      }
  );");
