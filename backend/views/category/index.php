<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "New",
        "url" => Url::toRoute(['/category/create']),
        "icon" => "fa fa-plus",
    ],
    [
        "title" => "Refresh",
        "url" => Url::toRoute(['/category']),
        "icon" => "fa fa-refresh",
    ],
]);
?>
<div class="panel">
    <div class="panel-body">
        <div class="table-responsive">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                GenxGridHelper::linkedID(),
                GenxGridHelper::linkedTitle('name'),
                [
                    "attribute" => "parent_id",
                    "label" => "Parent",
                    "filter" => (new \common\models\Category)->getTreeForDropdown(['blank' => True]),
                    "value" => function($model) {
                if ($model->parent_id && \common\models\Category::findOne(['id' => $model->parent_id])) {
                    return \common\models\Category::findOne(['id' => $model->parent_id])->name;
                }

                return "--None--";
            }
                ],
                [
                     
                    'attribute' => 'Font Awesome',
                    'format'=>'raw',
                    'value' => function($model) {
                        $fonticon = $model->fa;
                        if($fonticon){
                            return Html::a("<i class='$fonticon' style='font-size:20px'></i>");
                        }
                        else{
                            return $fonticon;
                        }
                        
                          
                    }
                    
                ],
                 'meta_title',
                 'meta_description',
                 'meta_keyword',       
                [
                    'label' => 'Show Homepage',
                    'attribute'=>'showhome',
                    'filter'=>array("1"=>"Enable","0"=>"Disable")
                ],
                GenxGridHelper::status(),
                GenxGridHelper::ActionColumn(),
            ],
        ]);
        ?>
    </div>
    </div>
</div>
