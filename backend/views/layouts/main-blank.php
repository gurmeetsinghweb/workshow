<?php

use yii\helpers\Html;
use dmstr\widgets\Alert;
use \yii\widgets\ActiveForm;
use \yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

backend\assets\AppAsset::register($this);
backend\assets\IndexAsset::register($this);
$bundle = yiister\gentelella\assets\Asset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="login">
        <?php $this->beginBody() ?>
        <div>
            <div class="login_wrapper">
                <?= Alert::widget() ?>
                <?= $content; ?>

                <div class="clearfix"></div>
                <br />

                <div>
                    <h1><i class="fa fa-shopping-cart"></i> classifr.com</h1>
                    <p>©<?= date("Y", time()); ?>  All Rights Reserved. classifr.com. Terms</p>
                </div>
            </div>

        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
