<?php

use yii\helpers\Html;
use dmstr\widgets\Alert;
use \yii\widgets\ActiveForm;
use \yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

backend\assets\AppAsset::register($this);
backend\assets\IndexAsset::register($this);
$bundle = yiister\gentelella\assets\Asset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="login">
        <?php $this->beginBody() ?>
        <div>
            <a class="hiddenanchor" id="signup"></a>
            <a class="hiddenanchor" id="signin"></a>
            <?= Alert::widget() ?>
            <div class="login_wrapper">
                <div class="animate form login_form">
                    <section class="login_content">
                        <?php $login_form = ActiveForm::begin(); ?>
                        <h1>Login Form</h1>
                        <div>
                            <?= $login_form->field($this->context->login_model, 'email')->label(False)->textInput(['placeholder' => $this->context->login_model->getAttributeLabel('email')]); ?>
                        </div>
                        <div>
                            <?= $login_form->field($this->context->login_model, 'password')->label(False)->passwordInput(['placeholder' => $this->context->login_model->getAttributeLabel('password')]); ?>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-default submit">Log in</button>
                            <!--<a class="reset_pass" href="#">Lost your password?</a>-->
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <div>
                                <h1><i class="fa fa-shopping-cart"></i> Classifr</h1>
                                <p>©<?= date("Y", time()); ?>  All Rights Reserved. Classifr.com</p>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </section>
                </div>
            </div>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
