<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */
use yii\helpers\Html;
use common\models\User;
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

$bundle = yiister\gentelella\assets\Asset::register($this);
backend\assets\AppAsset::register($this);
$logged_in_user = Yii::$app->user->getIdentity();
if (!Yii::$app->user->canAccessAdmin()) {
    header("Location: " . yii\helpers\Url::toRoute(['../']));
    exit;
}
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta charset="<?= Yii::$app->charset ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <style>
        .pop_child .conditions_listing{
            list-style: none;
        }
        .conditions_listing{
            padding-left:10px;
        }

    </style>

    <body class="nav-md">
        <?php $this->beginBody(); ?>
        <div class="container body">

            <div class="main_container">

                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">

                        <div class="navbar nav_title" style="border: 0;">
                            <a href="<?= yii\helpers\Url::toRoute(['../']); ?>" class="site_title" title="Go to main site."><i class="fa fa-cart-plus"></i>
                                <span>Classifr </span>
                            </a>
                        </div>
                        <div class="clearfix"></div>

                        <!-- menu prile quick info -->
                        <div class="profile">
                            <div class="profile_pic">
                                <img src="<?= $logged_in_user->getIconURL(); ?>" alt="<?= $logged_in_user->name; ?>" class="img-circle profile_img">
                            </div>
                            <div class="profile_info">
                                <span>Welcome,</span>
                                <h2><?= $logged_in_user->name; ?></h2>
                            </div>
                        </div>
                        <!-- /menu prile quick info -->

                        <br />

                        <!-- sidebar menu -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                            <div class="menu_section">
                                <?=
                                \yiister\gentelella\widgets\Menu::widget(
                                        [
                                            'items' => [
                                                [
                                                    'label' => '--- Admin ---',
                                                ],
                                                [
                                                    'label' => 'Dashboard',
                                                    'icon' => 'dashboard',
                                                    'url' => ['/site/index'],
                                                    'visible' => Yii::$app->user->canAccessAdmin(),
                                                ],
                                                [
                                                    'label' => 'Merchant Profile',
                                                    'icon' => 'copyright',
                                                    'url' => 'javascript:void(0)',                                                    
                                                    'visible' => Yii::$app->user->isOnlyMerchant(),
                                                   'data-slug' => "",
                                                    'items' => [
                                                        [
                                                            "label" => "Change Password",
                                                            "url" => ["/company/change-password"],
                                                            'icon' => '',
                                                            'data-slug' => "",
                                                        ],
                                                        [
                                                            "label" => "Company Profile",
                                                            'url' => ['/company/my'],
                                                            'icon' => '',
                                                            'data-slug' => "",
                                                        ],
                                                     ],   
                                                ],
//                                                [
//                                                    'label' => 'Search Coupon',
//                                                    'icon' => 'search',
//                                                    'url' => ['/coupon-code/index'],
//                                                    'visible' => Yii::$app->user->isOnlyMerchant(),
//                                                ],
                                                // Marchent Account 
                                                [
                                                    'label' => 'Merchant Account',
                                                    'icon' => 'tag',
                                                    'url' => 'javascript:void(0)',
                                                    'visible' => Yii::$app->user->isAdmin(),
                                                    'data-slug' => "",
                                                    'items' => [
                                                        [
                                                            "label" => "Business Add",
//                                                            'icon' => 'tag',
                                                            'url' => 'javascript:void(0)',
                                                            //"url" => ["/deal/create"],
                                                            'visible' => Yii::$app->user->canAccessAdmin(),
                                                            //'icon' => 'plus',
                                                            'data-slug' => "",
                                                            'items' => [
                                                                [
                                                                    "label" => "New Business",
                                                                    "url" => ["/deal/create"],
                                                                    'icon' => '',
                                                                    'data-slug' => "",
                                                                ],
                                                                [
                                                                    "label" => "View All",
                                                                    "url" => ["/deal/index"],
                                                                    'icon' => '',
                                                                    'data-slug' => "",
                                                                ],
                                                                [
                                                                    "label" => "Active Deal",
                                                                    "url" => ["/deal/active"],
                                                                    'icon' => '',
                                                                    'data-slug' => "",
                                                                ],
                                                                [
                                                                    "label" => "Inactive Deal",
                                                                    "url" => ["/deal/inactive"],
                                                                    'icon' => '',
                                                                    'data-slug' => "",
                                                                ]
                                                            ],
                                                        ],
                                                        [
                                                            "label" => "Create New Merchant",
                                                            "url" => ["/company/create"],
//                                                            'icon' => 'plus',
                                                            'data-slug' => "",
                                                        ],
                                                        [
                                                            "label" => "Merchant List",
                                                            "url" => ["/merchant/index"],
//                                                            'icon' => 'list',
                                                            'data-slug' => "",
                                                            'visible' => Yii::$app->user->isAdmin(),
                                                        ]
                                                    ],
                                                ],
                                                [
                                                    "label" => "My Business",
                                                    'icon' => 'tag',
                                                    'url' => 'javascript:void(0)',
                                                    'visible' => Yii::$app->user->isOnlyMerchant(),
                                                    'data-slug' => "",
                                                    'items' => [
                                                        [
                                                            "label" => "Add New",
                                                            "url" => ["/deal/create"],
                                                            'icon' => '',
                                                            'data-slug' => "",
                                                            'visible' => Yii::$app->user->isAdmin(),
                                                        ],
                                                        [
                                                            "label" => "View All",
                                                            "url" => ["/deal/index"],
                                                            'icon' => '',
                                                            'data-slug' => "",
                                                        ],
                                                        [
                                                            "label" => "Active Business",
                                                            "url" => ["/deal/active"],
                                                            'icon' => '',
                                                            'data-slug' => "",
                                                        ],
                                                        [
                                                            "label" => "Inactive Business",
                                                            "url" => ["/deal/inactive"],
                                                            'icon' => '',
                                                            'data-slug' => "",
                                                        ]
                                                    ],
                                                ],
                                                /*[
                                                    "label" => "Redeem Options",
                                                    'icon' => 'tag',
                                                    'url' => 'javascript:void(0)',
                                                    'visible' => Yii::$app->user->isMerchant(),
                                                    'data-slug' => "",
                                                    'items' => [
                                                        [
                                                            "label" => "Redeem Vouchers",
                                                            "url" => ["/coupon-code/multipal-coupon-redeemed"],
                                                            'icon' => '',
                                                            'data-slug' => "",
                                                        ]
                                                        
                                                    ],
                                                ],*/
                                                // sales report
                                                [
                                                    "label" => "Reports",
                                                    'icon' => 'line-chart',
                                                    'url' => 'javascript:void(0)',
                                                    'visible' => Yii::$app->user->isMerchant(),
                                                    'data-slug' => "",
                                                    'items' => [
                                                        [
                                                            "label" => "# Business Enquiry",
                                                            "url" => ["/reports/sold-deals"],
                                                            'icon' => '',
                                                            'data-slug' => "",
                                                        ],
                                                        [
                                                            "label" => "# Net Amount Business",
                                                            "url" => ["/reports/sold-amount"],
                                                            'icon' => '',
                                                            'data-slug' => "",
                                                        ],
                                                        [
                                                            "label" => "Customers",
                                                            "url" => ["/coupon-code/index"],
                                                            'icon' => '',
                                                            'data-slug' => "",
                                                        ]
                                                    ],
                                                ],
                                                // Account report
                                                [
                                                    "label" => "My Accounts",
                                                    'icon' => 'align-left',
                                                    'url' => 'javascript:void(0)',
                                                    'visible' => Yii::$app->user->isAdmin(),
                                                    'data-slug' => "",
                                                    'items' => [
                                                        [
                                                            "label" => "Pending Payments",
                                                            "url" => ["/account/index"],
                                                            'visible' => Yii::$app->user->isAdmin(),
                                                            'icon' => '',
                                                            'data-slug' => "",
                                                        ],
                                                        [
                                                            "label" => "Company Earnings",
                                                            "url" => ["/account/view"],
                                                            'visible' => Yii::$app->user->isAdmin(),
                                                            'icon' => '',
                                                            'data-slug' => "",
                                                        ],
                                                        [
                                                            "label" => "Merchant Account Report",
                                                            "url" => ["/account/merchant-account"],
                                                            'visible' => FALSE,
                                                            'icon' => '',
                                                            'data-slug' => "",
                                                        ]
                                                    ],
                                                ],
                                                // User Details
                                                [
                                                    'label' => 'Users',
                                                    'icon' => 'users',
                                                    'url' => 'javascript:void(0)',
                                                    'visible' => Yii::$app->user->onlyTeam(),
                                                    'data-slug' => "",
                                                    'items' => [
                                                        [
                                                            "label" => "New",
                                                            "url" => ["/user/create"],
//                                                            'icon' => 'plus',
                                                            'data-slug' => "",
                                                            'visible' => Yii::$app->user->can(User::ROLE_MANAGER),
                                                        ],
                                                        [
                                                            "label" => "View All",
                                                            "url" => ["/user/index"],
//                                                            'icon' => 'list',
                                                            'data-slug' => "",
                                                            'visible' => Yii::$app->user->onlyTeam(),
                                                        ],
                                                    ],
                                                ],
                                                // Add Enquiry
                                                [
                                                    'label' => 'Requests',
                                                    'icon' => 'question-circle',
                                                    'url' => 'javascript:void(0)',
                                                    'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                                                    'data-slug' => "",
                                                    'items' => [
                                                        [
                                                            "label" => "Coupon Cancel",
                                                            "url" => ["/request/cc"],
//                                                            'icon' => 'list',
                                                            'data-slug' => "",
                                                        ],
                                                        [
                                                            "label" => "Deal Update",
                                                            "url" => ["/request/deal-update"],
//                                                            'icon' => 'list',
                                                            'data-slug' => "",
                                                        ],
                                                    ],
                                                ],
                                                // Add Enquiry
                                                [
                                                    'label' => 'Enquiry',
                                                    'icon' => 'users',
                                                    'url' => 'javascript:void(0)',
                                                    'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                                                    'data-slug' => "",
                                                    'items' => [
                                                        [
                                                            "label" => "All List",
                                                            "url" => ["/enquiry/index"],
//                                                            'icon' => 'list',
                                                            'data-slug' => "",
                                                        ],
                                                    ],
                                                ],
                                                // Add Coupon
                                                [
                                                    'label' => 'Discount Coupon',
                                                    'icon' => 'gift',
                                                    'url' => 'javascript:void(0)',
                                                    'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                                                    'data-slug' => "",
                                                    'items' => [
                                                        [
                                                            "label" => "Create Coupon",
                                                            "url" => ["/discountcoupon-details/create"],
//                                                            'icon' => 'list',
                                                            'data-slug' => "",
                                                        ],
                                                        [
                                                            "label" => "View",
                                                            "url" => ["/discountcoupon-details/"],
//                                                            'icon' => 'list',
                                                            'data-slug' => "",
                                                        ],
                                                    ],
                                                ],
                                                [
                                                    'label' => 'Categories',
                                                    'icon' => 'tag',
                                                    'url' => 'javascript:void(0)',
                                                    'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                                                    'data-slug' => "",
                                                    'items' => [
                                                        [
                                                            "label" => "New",
                                                            "url" => ["/category/create"],
//                                                            'icon' => 'plus',
                                                            'data-slug' => "",
                                                        ],
                                                        [
                                                            "label" => "View All",
                                                            "url" => ["/category/index"],
//                                                            'icon' => 'list',
                                                            'data-slug' => "",
                                                        ],
//                                                        [
//                                                            "label" => "View Tree",
//                                                            "url" => ["/category/tree"],
////                                                            'icon' => 'list-alt',
//                                                            'data-slug' => "",
//                                                        ],
                                                    ],
                                                ],
                                                // Country State City
                                                [
                                                    'label' => 'Country / State / City',
                                                    'icon' => 'globe',
                                                    'url' => 'javascript:void(0)',
                                                    'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                                                    'data-slug' => "",
                                                    'items' => [
                                                        [
                                                            "label" => "Country",
                                                            "url" => ["/country/create"],
//                                                            'icon' => 'globe',
                                                            'data-slug' => "",
                                                            'url' => 'javascript:void(0)',
                                                            'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                                                            'items' => [
                                                                [
                                                                    "label" => "New",
                                                                    "url" => ["/country/create"],
//                                                                    'icon' => 'plus',
                                                                    'data-slug' => "",
                                                                ],
                                                                [
                                                                    "label" => "View All",
                                                                    "url" => ["/country/index"],
//                                                                    'icon' => 'list',
                                                                    'data-slug' => "",
                                                                ],
                                                            ],
                                                        ],
                                                        [
                                                            'label' => 'States',
//                                                            'icon' => 'globe',
                                                            'url' => 'javascript:void(0)',
                                                            'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                                                            'data-slug' => "",
                                                            'items' => [
                                                                [
                                                                    "label" => "New",
                                                                    "url" => ["/state/create"],
//                                                                    'icon' => 'plus',
                                                                    'data-slug' => "",
                                                                ],
                                                                [
                                                                    "label" => "View All",
                                                                    "url" => ["/state/index"],
//                                                                    'icon' => 'list',
                                                                    'data-slug' => "",
                                                                ],
                                                            ],
                                                        ],
                                                        [
                                                            'label' => 'Cities',
//                                                            'icon' => 'globe',
                                                            'url' => 'javascript:void(0)',
                                                            'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                                                            'data-slug' => "",
                                                            'items' => [
                                                                [
                                                                    "label" => "New",
                                                                    "url" => ["/city/create"],
//                                                                    'icon' => 'plus',
                                                                    'data-slug' => "",
                                                                ],
                                                                [
                                                                    "label" => "View All",
                                                                    "url" => ["/city/index"],
//                                                                    'icon' => 'list',
                                                                    'data-slug' => "",
                                                                ],
                                                            ],
                                                        ],
                                                    ],
                                                ],
                                                [
                                                    'label' => 'Settings',
                                                    'icon' => 'cogs',
                                                    'url' => 'javascript:void(0)',
                                                    'visible' => Yii::$app->user->can(User::ROLE_SYSADMIN),
                                                    'data-slug' => "",
                                                    'items' => [
                                                        [
                                                            "label" => "New",
                                                            "url" => ["/site-settings/create"],
//                                                            'icon' => 'plus',
                                                            'data-slug' => "",
                                                        ],
                                                        [
                                                            "label" => "View All",
                                                            "url" => ["/site-settings/index"],
//                                                            'icon' => 'list',
                                                            'data-slug' => "",
                                                        ]
                                                    ],
                                                ],
                                            ]
                                        ]
                                )
                                ?>
                            </div>

                        </div>
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->
                        <div class="sidebar-footer hidden-small">
                            <a data-toggle="tooltip" data-placement="top" title="Settings">
                                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                            </a>
                            <a data-toggle="tooltip" data-placement="top" title="Lock">
                                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                            </a>
                            <?=
                            Html::a(
                                    '<span class="glyphicon glyphicon-off" aria-hidden="true"></span>', ['/site/logout'], ['data-method' => 'post', 'data-placement' => 'top', 'data-toggle' => 'tooltip', 'title' => 'Logout']
                            )
                            ?>
                        </div>
                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">

                    <div class="nav_menu">
                        <nav class="" role="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="<?= $logged_in_user->getIconURL(); ?>" alt="<?= $logged_in_user->name; ?>"><?= $logged_in_user->name; ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <!-- <li><a href="javascript:;">  Profile</a>
                                         </li>
                                         <li>
                                             <a href="javascript:;">
                                                 <span class="badge bg-red pull-right">50%</span>
                                                 <span>Settings</span>
                                             </a>
                                         </li>
                                         <li>
                                             <a href="javascript:;">Help</a>
                                         </li>-->
                                        <li>
                                            <?=
                                            Html::a(
                                                    '<i class="fa fa-sign-out pull-right"></i> Log Out', ['/site/logout'], ['data-method' => 'post']
                                            );
                                            ?>
                                        </li>
                                    </ul>
                                </li>

                                <!--<li role="presentation" class="dropdown">
                                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                        <i class="fa fa-envelope-o"></i>
                                        <span class="badge bg-green">6</span>
                                    </a>
                                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                        <li>
                                            <a>
                                                <span class="image">
                                                    <img src="http://placehold.it/128x128" alt="Profile Image" />
                                                </span>
                                                <span>
                                                    <span>John Smith</span>
                                                    <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                                    Film festivals used to be do-or-die moments for movie makers. They were where...
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                                <span class="image">
                                                    <img src="http://placehold.it/128x128" alt="Profile Image" />
                                                </span>
                                                <span>
                                                    <span>John Smith</span>
                                                    <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                                    Film festivals used to be do-or-die moments for movie makers. They were where...
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                                <span class="image">
                                                    <img src="http://placehold.it/128x128" alt="Profile Image" />
                                                </span>
                                                <span>
                                                    <span>John Smith</span>
                                                    <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                                    Film festivals used to be do-or-die moments for movie makers. They were where...
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                                <span class="image">
                                                    <img src="http://placehold.it/128x128" alt="Profile Image" />
                                                </span>
                                                <span>
                                                    <span>John Smith</span>
                                                    <span class="time">3 mins ago</span>
                                                </span>
                                                <span class="message">
                                                    Film festivals used to be do-or-die moments for movie makers. They were where...
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <div class="text-center">
                                                <a href="/">
                                                    <strong>See All Alerts</strong>
                                                    <i class="fa fa-angle-right"></i>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>-->

                            </ul>
                        </nav>
                    </div>

                </div>
                <!-- /top navigation -->

                <!--popup-->



                <!-- page content -->
                <div class="right_col" role="main">
                    <?= Alert::widget() ?>
                    <?php
                    if ($this->context->show_page_header === True) {
                        ?>
                        <div class="page-title">
                            <h1><?= (($this->context->show_title) ? $this->title : "") ?></h1>
                            <div class="title_left">

                            </div>
                            <div class="title_right pull-right">
                                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                    <?php
                                    if (isset($this->blocks['content-header-buttons'])) {
                                        echo $this->blocks['content-header-buttons'];
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <?=
                        Breadcrumbs::widget(
                                [
                                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                ]
                        );
                        ?>
                        <div class="clearfix"></div>
                    <?php } ?>

                    <?= $content ?>
                </div>
                <!-- /page content -->
                <!--popup-->
                <?php
                if (Yii::$app->user->isOnlyMerchant()) {
                    ?>
                    <div class="popup"  style="display:none;">
                        <div id="overlay" style="display:block;"></div>
                        <div id="popup">
                            <div class="pop_child">
                                <h4>MERCHANT TERMS</h4>
                                <div class="terms_popup merchant_terms">

                                    <p>The Classifr platform is owned and operated by Classifr Group Pty LtdABN 83 615 223 099(referred to as the Classifr). 
                                    </p>

                                    <p>By accessing or using the Classifr’s platform, you agree to be bound by these Terms and Conditions (Terms). If you object to anything in, or otherwise do not agree to these Terms you are not authorised to use the Classifr platform. You must not use the Classifr platform in any way other than in accordance with these Terms.
                                    </p>

                                    <ul class="conditions_listing">
                                        <li><strong><span>1) </span> What we are and are not</strong><br>
                                            (a)  The Classifr’s platform allows businesses (Merchants) to advertise different deals on the products and services offered by the Merchant, whereby customers (Customers) can use the Classifr’s platform to find and purchase vouchers for the deals offered by Merchants. The Classifr’s platform is referred to as the Services. The Services are available at classifr.com (Site) and on the Classifr mobile application (App). By accessing or using the Site, Usersacknowledge that Merchants are not employees, contractors, partners or agents of the Classifr and the Classifr does not supply and is not a supplier of any of the products or services advertised through the Site, App or Services. </li>

                                        </li><br>
                                        <li><strong><span>2) </span>   Contract</strong><br>
                                            (a)  These Terms forma binding legal agreement between the Classifrand each person, organisation or entity using the Site, App and Services (referred to as aUser). By using the Site, App and Services, each User agrees to comply with and be legally bound by these Terms. Please read the Terms carefully.  If there are any questions, pleasecontact the Classifrusing the contact details at the end of these Terms.</li>
                                        (b)  The Merchant’s use of the Site, the App and the Services indicates that:<br>
                                        i.   the Merchant has had sufficient opportunity to access the Terms and contact the Classifr;<br>
                                        ii. the Merchant has read, accepted and will comply with the Terms; and<Br>
                                        iii.     the Merchant has legal capacity to enter into a contract for sale.<Br>
                                        If this is not correct, or if the Merchant does not agree to these Terms, the Merchant is not permitted to use any of the Services.<br>
                                        (c)  The Classifr may amend these Terms from time to time, and will give the Merchant 14 days’ notice of the varied Terms. Use of our Services following any such amendments will be deemed to be confirmation that the Merchant accepts those amendments. If a Merchant has reasonable grounds to believe that the varied Terms will be detrimental to their rights, the Merchant may terminate these Terms without penalty within 14 days’ of receiving the notice of varied Terms. The Classifr recommends that each Merchant check the current Terms, before continuing use of the Services. The Classifr’s agents, employees and third parties do not have authority to change the Terms.<Br>
                                        (d) These Terms supplement and incorporate:<Br>
                                        i.  The Classifr’s policies and terms and conditions, including without limitation the Website Terms of Use and Privacy Policy posted on the Site;<Br>
                                        ii. the PayPal Terms of Service including, without limitation, the User Agreement; and<br>
                                        iii.    Pin Payments’ Terms and Conditions available at https://pin.net.au/terms. <Br><Br>

                                        </li>
                                        <li><strong><span>3) </span>   The Classifr’s Introductory Service </strong><br>
                                            (a)  The Site, the App and the Services provide an online introductory platform for Merchants and Customers.</li>

                                        (b) Merchants are able to create listings (Listings) on the Site advertising different Deals for the  goods (Merchant Products) or services (Merchant Services) offered by that particular Merchant.<Br>
                                        (c) Customers can purchase Merchant Products or Merchant Services through the Site, App or Services (an Order).<Br>
                                        (d) When a Customer places an Order, the Customer accepts any terms and conditions set out in the relevant Listing.<Br>
                                        (e) The Merchant understands and agrees that the Site and the App is an online introductory platform only, and that the Classifr’s responsibilities are limited to facilitating the availability of the Site, the App and the Services.<br>
                                        (f) The Classifris not a party to any agreement entered into between Merchants and Customers. The Classifr has no control over the conduct of Merchants and Customers and any other users of the Site, the App and the Services.  The Classifr disclaims all liability in this regard, as set out in these Terms.<br>
                                        (g) Any arrangement between a Merchant and a Customer is solely between the relevant Customer and Merchant.  It is strictly and expressly not part of the Merchant’s agreement with the Classifr.<Br><Br>

                                        </li>

                                        <li><strong><span>4) </span>  Online Registration  </strong><br>
                                            (a) Merchants may advertise different deals (Deals) in relation to Merchant Products and/or Merchant Services sold, offered or provided by that Merchant.<br>
                                            (b) Merchants must register for an account (Account) on the Site in order to create a listing (Listing) advertising a Deal. <Br>
                                            (c) Merchants may only have one (1) Account on the Siteand App<Br>
                                            (d) Merchants must provide basic information when registering on the Site for an Account, including businessname, ABN, email address and location and select a username and password<br>
                                            (e) Merchants agree to provide accurate, current and complete information during the registration process and to update such information to keep it accurate, current and complete. The Classifrreserves the right to suspend or terminate any Merchant’s Account and itsaccess to the Site, App and Services if any information provided to the Classifr proves to be inaccurate, not current or incomplete.<br>
                                            (f) It is the Merchant’s responsibility to keep itsAccount details, username and password confidential and the Merchant is liable for all activity on itsAccount. The Merchant agrees that it will not disclose its password to any third party and that it will take sole responsibility for any activities or actions under its Account, whether or not it has authorised such activities or actions.<br>
                                            (g) The Merchant will immediately notifythe Classifrof any unauthorised use of its Account.<br><br>

                                        </li>

                                        <li><strong><span>5) </span>  Vouchers </strong><br>
                                            (a)  Customers will be provided with printable vouchers (Vouchers) for the Deal purchased on the Classifr’s Site or App. <br>
                                            (b) Merchantsare the sellers or providers of Merchant Products or Merchant Services, not the Classifr. <Br>
                                            (c)    Vouchers are valid until the expiry date listed on the relevant Voucher.
                                            (d) Vouchers which have expired are non-refundable either in whole or in part and are no longer valid. Merchants are not obliged to honour expired Vouchers.<br><Br>


                                        </li>

                                        <li><strong><span>6)</span>   Merchant Obligations </strong><br>
                                            (a)  Merchants must set out all relevant information in a Listing such as the price of a Merchant Service or Merchant Product, the availability of a Merchant Service, the delivery method and any delivery fees in respect of Merchant Products, the cancellation, refund and exchange policy in respect of Merchant Services or Merchant Products and any other terms and conditions applicable to the Deal.  <br>
                                            (b) It is a condition of advertising a Deal and creating a Listing or using the Classifr’s Site or App that the Deal is exclusive to the Classifr’s platform. Merchants must not offer the same Deal to other online deal websites, or in the Merchant’s retail stores <Br>
                                            (c)    Merchants agree to honour each Voucher presented by a Customer, for its full offer value and until the Voucher’s expiry date, where such Voucher is redeemed in accordance with these Terms and any terms set out in the Merchant’s Listing. Merchants are obliged to honour the terms they set out in the relevant Listing and must honour the Voucher irrespective of whether terms have been set out on the Listing.<Br><Br>



                                        </li>
                                        <li><strong><span>7) </span>     Delivery of Products </strong><br>
                                            (a)  Merchants agree to arrange delivery of any Merchant Products to the relevant Customer. The Classifr is not involved in the delivery process. Merchants are required to communicate with Customers regarding delivery areas, delivery fees, when the Merchant Product is likely to be dispatched, the method of delivery, when risk of loss, damage or deterioration to any Merchant Product will pass to the Customer and what to do if a Merchant Product is delayed, damaged or goes missing in the delivery process.<br><Br>



                                        </li>
                                        <li><strong><span>8) </span>     Users </strong><br>
                                            (a) Merchants agree that any communications entered into with a Customer or other User, whether through the Site, App or otherwise is at its own risk. <Br>
                                            (b) Merchants should report to the Classifr, any activities or requests of Customers which are, or which the Merchant reasonably believes to be:<Br>
                                            <ul>
                                                <li>i.  suspicious;</li>
                                                <li>ii. inconsistent; </li>
                                                <li>iii.    illegal; or </li>
                                                <li>iv. likely to have a negative effect on the reputation of the Classifr, the Site, the App, Services and/or a User. </li>
                                            </ul>
                                            (c) Each Merchantrepresents and warrants that any content that it providesand Listing that it posts:<Br>

                                            <ul>
                                                <li>i.  will not breach any agreements it has entered into with any third parties;<li>
                                                <li>ii. will be in compliance with all applicable laws, tax requirements, and rules and regulations that may apply to the Merchant in its local area and country; and</li>
                                                <li>iii.   will not conflict with the rights of third parties. </li><Br>
                                            </ul>
                                            (d) The Classifr assumes no responsibility for a Merchant’s compliance with any applicable laws, rules and regulations<Br>
                                            (e) The Classifr reserves the right, at any time and without prior notice, to remove or disable access to any Account and/or Listing for any reason, including Accounts and/or Listings that the Classifr, at its sole discretion, considers to be objectionable for any reason, in violation of these Terms or otherwise harmful to the Site, the App or Services.<br><Br>

                                        </li>
                                        <li><strong><span>9) </span>    Payments  </strong><br>
                                            (a)  Merchants are not required to pay fees for registering on the Site or App for an Account, nor are Merchants required to pay a listing fee to create a Listing <br>
                                            (b)  As the Merchant’s limited payment collection agent, the Classifr will collect from the Customer the voucher fee (Voucher Fee) payable by the Customer for the relevant Deal on behalf of the Merchant.<Br>
                                            (c) The Total Merchant Fee, defined below,becomes payable 15 days after a Customer has redeemed a voucher (Voucher) for a Deal with the relevant Merchant. <Br>
                                            (d)  Merchants are responsible for notifying the Classifr that a Voucher has been redeemed.<br>
                                            (e) Merchants are paid based on the number of Vouchers that have been redeemed with that Merchant, not on the total number of Vouchers purchased from the Classifr<Br>
                                            (f) The Classifr facilitates payment of the Voucher Fee less the Classifr’s charge for connecting Customers to that Merchant (Classifr Fee) at the end of each fortnight (Total Merchant Fee).<Br>
                                            (g) Process: 15 days after a Voucher is redeemed with the relevant Merchant, the redeemed Voucher is payable to the Merchant. The Classifr remits payments every fortnight and after the expiry of 15 days, the Classifr will pay the Total Merchant Fee to the Merchant in the next fortnightly payment run. <br>
                                            (h) The Classifr’s pricing structure or payment methods may be amended from time to time at its sole discretion. After a pricing change, each Merchant has the choice to continue using the Site, or to cease to use the Site without penalty. <br><br>

                                        </li>
                                        <li><strong><span>10) </span>  Limited Payment Collection Agent </strong><br>
                                            (a)  Each Merchant appoints the Classifr as the Merchant’s limited payment collection agent solely for the purpose of accepting the Voucher Fee from the Customer. <br>
                                            (b)  Each Merchantagrees that payment of the Voucher Fee to the Classifr in respect of a Merchant by a Customer, as that Merchant’s limited payment collection agent, shall be considered the same as a payment made directly by that Customer to the relevant Merchant and the Merchant will provide the relevant Merchant Services and/or Merchant Product to the Customer, as outlined in the relevant Listing, as if the Merchant had received payment directly. <Br>
                                            (c)  Merchants agree thatthe Classifr, as a Merchant’s limited payment collection agent, may permit a Customer to cancel an Order and provide the Customer with a refund in accordance with the cancellation and refund policy set out in the Merchant’s Listing.<br>
                                            (d)  The Classifr, as limited payment collection agent for the Merchant, agrees to facilitate the payment of the Total Merchant Fee for Merchant Services provided and/or Merchant Products delivered on a fortnightlybasis unless otherwise agreed between the Classifr and the Merchant. In the event that the Classifr does not remit such amounts, the Merchant will have recourse only against the Classifr.<br>
                                            (e) Notwithstanding the Classifr’s appointment as limited payment collection agent of each Merchant, the Classifr explicitly disclaims all liability for any acts or omissions of the Merchant or any other third parties.<Br><Br>

                                        </li>
                                        <li><strong><span>11) </span> Dispute Resolution </strong><br>
                                            (a)    By using the Classifr’s Site, App and Services, Merchants agree that any legal remedy or liability that it may seek to obtain for actions or omissions of a Customer,  or other third party, will be limited to a claim against the Customer or other third party, who caused harm to it.  The Classifrencourages Merchants to communicate directly with the relevant Customeror third party to resolve any disputes.<br>
                                            (b)  The Classifrwelcomes feedback from Merchants.  The Classifrseeks to resolve concerns quickly and effectively.  If any Merchant has any feedback or questions about the Services, please contact any member of the staff. <Br>
                                            (c)  If there are any complaints from a Merchant, the Classifrwill aim to respond and provide a suitable solution within 45 days.  If a Merchant is not satisfied with the Classifr’sresponse, the Merchant and the Classifragree to the following dispute resolution procedure:<br>
                                            <ul>
                                                <li>i.  The complainant must tell the respondent in writing, the nature of the dispute, what outcome the complainant wants and what action the complainant thinks will settle the dispute.  The Merchant and the Classifr agree to meet in good faith to seek to resolve the dispute by agreement between them (Initial Meeting).</li>
                                                <li>ii. If a resolution cannot be agreed upon at the Initial Meeting, either the Merchant or the Classifr may refer the matter to a mediator.  If the Merchant and the Classifr cannot agree on who the mediator should be, the complainant will ask the Law Society of Victoria to appoint a mediator.  The mediator will decide the time and place for mediation.  Each party to the dispute must attend the mediation in good faith, to seek to resolve the dispute. </li>

                                            </ul>
                                            (d)  Any attempts made by a party to resolve a dispute pursuant to this clause are without prejudice to other rights or entitlements of either party under these Terms, by law or in equity.<Br><Br>


                                        </li>

                                        <li><strong><span>12) </span>  Cancellation of Registration </strong><br>
                                            (a)  If a Merchant wishes to cancel its registration, it may do so by deleting its Account on the Site or App<br>
                                            (b)  The Classifr will pay the Merchant the Total Merchant Fees owing for any Vouchers redeemed prior to the cancellation.<Br>
                                            (c)  Merchants must honour all Vouchers purchased by Customers prior to a Merchant cancelling their registration. For the avoidance of doubt, if a Customer approaches a Merchant to redeem a Voucher, that the Customer purchased prior to a Merchant cancelling their registration, then the Merchant is obligated to honour that Voucher.<br><br>
                                        </li>

                                        <li><strong><span>13)</span> Refunds </strong><br>
                                            (a)  If a Customer contacts the Classifr for a refund or an exchange of a Merchant Product, the Classifr will provide the Customer with guidelines about how to return the product. Customers must contact the Classifr within seven days of receiving the Merchant Product. The Classifr will liaise with the Customer and determine whether the Merchant Product can be returned and provide a return authorisation if applicable. The Classifr requires all Merchant Products to be returned in a saleable condition. The Classifr will liaise with Merchants in regards to the return or exchange of Merchant products.<br>
                                            (b)  If a Customer contacts the Classifr within two days of redeeming a Voucher, the Classifr will liaise with the Customer regarding a refund or another remedy. If the Classifr determines that a refund should be provided, then the Classifr may not remit the relevant payment to the Merchant and the Classifr will liaise with the Merchant regarding the Merchant Service provided.<Br><Br>

                                        </li>
                                        <li><strong><span>14) </span>Classifr IntellectualProperty</strong><br>
                                            (a) (a) All logos, slogans, content, designs, diagrams, drawings, graphics, images, layouts, appearance, videos, ideas, methods, databases, codes, algorithms, software, fees, pricing, notes, documents, domain names, confidential information, copyright, rights in circuit layouts (or similar rights), registered or unregistered trade marks, trade names, patent, know-how, trade secrets and any other intellectual or industrial property whether such rights are capable of being registered or not (collectively Intellectual Property), including but not limited to copyright which subsists in all creative and literary works displayed on the Site, the App and Services, the layout, appearance and look of the Site and App, together with any applications for registration and any rights to registration or renewal of such rights anywhere in the world, whether created before or after the date of these Terms and whether used or contained in the Site is owned, controlled or licensed to the Classifr (or its affiliates and/or third party licensors as applicable).<br>
                                            (b) (b) The Merchant agrees that, as between the Merchant and the Classifr, the Classifrowns or holds the relevant licence to all Intellectual Property rights in the Site, the App and Services, and that nothing in these Terms constitutes a transfer of any Intellectual Property. <Br>
                                            (c) The Intellectual Property, Site, App and Services are protected by copyright, trademark, patent, trade secret, international treaties, laws and other proprietary or industrial rights whether such rights are capable of being registered or not, and also may have security components that protect digital information only as authorised by the Classifror the owner of the content.<Br>
                                            (d) Some Intellectual Property used in connection with the Site, App and Services are the trademarks of their respective owners (collectively Third Party Marks).<br>
                                            (e) The Classifr’s Intellectual Property and Third Party Marks may not be copied, imitated or used, in whole or in part, without the prior written permission of the Classifror the applicable trademark holder or Intellectual Property owner.
                                            (f) Users of the Site do not obtain any interest or licence in the Intellectual Property or Third Party Markswithout the prior written permission of the Classifror the applicable Intellectual Property owner. Merchants may not do anything which interferes with or breaches the Intellectual Property rights.<Br><Br>

                                        </li>
                                        <li><strong><span>15) </span>    User Licence </strong><br>
                                            (a) Subject to these Terms, the Classifr grants the Merchant a personal, non-exclusive, non-transferable, limited and revocable licence to use the Site, App and Services for its own personal and/or non-commercial use only on a computer or mobile device owned or controlled by the Merchantas permitted in accordance with these Terms (User Licence),and not to use the Site, App and Services in any other way or for any other purpose, apart from local fair dealing legislation in accordance with the Copyright Act 1968 (Cth).All other uses are prohibited without the Classifr’s prior written consent.<br>
                                            (b)  The right to use the Site, App and Services is licensed to the Merchant and not being sold to the Merchant. A Merchant has no rights in the Site, App and Services other than to use it in accordance with these Terms.<Br>
                                            (c) This Agreement and User Licence governs any updates to, or supplements or replacements for the Site, App and Services, unless separate Terms accompany such updates, supplements or replacements, in which case the separate Terms will apply.<Br><Br>

                                        </li>
                                        <li><strong><span>16) </span> Permitted and Prohibited Conduct</strong><br>
                                            (a)  The Merchant will not remove, alter or obscure any copyright, trademark, service mark or other proprietary rights notices incorporated in or accompanying the Site, App and Services, or Intellectual Property.<br>
                                            (b)  The Merchant is solely responsible for compliance with any and all laws, rules, regulations, including but not limited to tax obligations that may apply to its use of the Site, App and Services. <Br>
                                            (c) The Merchantmust not post, upload, publish, submit or transmit any content that:
                                            <ul>
                                                <li>i.  infringes, misappropriates or violates a third party’s patent, copyright, trademark, trade secret, moral rights or other intellectual property rights, or rights of publicity or privacy; </li>
                                                <li>ii. is fraudulent, false, misleading or deceptive;</li>
                                                <li>iii.    denigrates the Classifr, the Site, the App, Services, a Customer, or a Merchant;</li>
                                                <li>iv. violates, or encourages any conduct that would violate, any applicable law or regulation or would give rise to civil liability;</li>
                                                <li>v.  is defamatory, obscene, pornographic, vulgar, offensive, promotes discrimination, bigotry, racism, hatred, harassment or harm against any individual or group; </li>
                                                <li>vi. is violent or threatening or promotes violence or actions that are threatening to any other person; or</li>
                                                <li>vii.    promotes illegal or harmful activities or substances.</li>
                                            </ul><br>
                                            (d) In connection with the Merchant’s use of the Site, App and Services, the Merchant may not and agrees that it will not:
                                            <ul>
                                                <li>i.  use the Site, App or Services for any commercial or other purposes that are not expressly permitted by these Terms;</li>
                                                <li>ii. register for more than oneAccount or register for an Account on behalf of another individual and/or entity;</li>
                                                <li>iii.    upload any content (including but not limited to User Content) that is offensive, contains nudity or inappropriate language, contains racial or religious ranting or discrimination or defames another Useror Listing;</li>
                                                <li>iv. submit any false or misleading information;</li>
                                                <li>v.  violate any local, state, provincial, national, or other law or regulation, or any order of a court, including, without limitation, zoning restrictions and tax regulations;</li>
                                                <li>vi. copy, store or otherwise access any information contained on the Site, App and Services or content for purposes not expressly permitted by these Terms;</li>
                                                <li>vii.    infringe the rights of any person or entity, including without limitation, their intellectual property, privacy, publicity or contractual rights;</li>
                                                <li>viii.   use the Site, Appor Services to transmit, distribute, post or submit any information concerning any other person or entity, including without limitation, photographs of others without their permission, personal contact information or credit, debit, calling card or account numbers;</li>
                                                <li>ix. use the Site, App or Services in connection with the distribution of unsolicited commercial email, i.e. spam or advertisements;</li>
                                                <li>x.  stalk or harass any other user of the Services or collect or store any personally identifiable information about any other user other than for purposes of transacting as a Merchant on the Site or App; </li>
                                                <li>xi. use, display, mirror or frame the Site or App, or any individual element within the Site or App, Services, the Classifr’s name, any the Classifr trademark, logo or other Intellectual Property, information, or the layout and design of any page or form contained on a page, without the Classifr’s express written consent; or </li>
                                                <li>xii.    advocate, encourage, or assist any third party in doing any of the foregoing.</li>
                                            </ul>

                                        </li>

                                        <li><strong><span>17) </span>  User Content</strong><br>
                                            (a) Merchants are permitted to post, upload, publish, submit or transmit relevant information and content (User Content). By making available any User Content or any Intellectual Property on or through the Site, App and Services, the Merchant grants to the Classifr a worldwide, irrevocable, perpetual, non-exclusive, transferable, royalty-free licence to use the User Content and Intellectual Property, with the right to use, view, copy, adapt, modify, distribute, licence, sell, transfer, publicly display, publicly perform, transmit, stream, broadcast, access, or otherwise exploit such User Contentand Intellectual Property on, through, or by means of the Site, App and Services.<Br>
                                            (b) The Merchant agrees that it is solely responsible for all User Contentand Intellectual Property that it makes available through the Site, App and Services. The Merchant represents and warrants that: <br>
                                            <ul>
                                                <li>i.  it is either the sole and exclusive owner of all User Content and Intellectual Property that it makes available through the Site, App and Services, or that it has all rights, licences, consents and releases that are necessary to grant to the Classifrthe rights in such User Content or Intellectual Property, as contemplated under these Terms; and</li>
                                                <li>ii. neither the User Content nor the posting, uploading, publication, submission or transmittal of the User Content or the Classifr’s use of the User Content (or any portion thereof) on, through or by means of the Site, Appand the Services will infringe, misappropriate or violate a third party’s patent, copyright, trademark, trade secret, moral rights or other Intellectual Property rights, or rights of publicity or privacy, or result in the violation of any applicable law or regulation.</li>
                                            </ul>
                                            (c) The Classifr may at its sole discretion remove any User Content that is offensive or in breach of these Terms.<Br><Br>
                                        </li>

                                        <li><strong><span>18) </span>  Disclaimers</strong><br>
                                            (a)  Each Merchant who uses the Site, App and the Services does so at their own risk.The Classifr does not perform background checks of Customers and does not confirm any Customer’s identity.<Br>
                                            (b) The Classifr does not guarantee that Merchants Products and/orMerchantServices will be requested by any Customers. <br>
                                            (c) The Classifr accepts no responsibility for and makes no representations or warranties to the Merchant or to any other person or entity as to the reliability, accuracy or completeness of the information contained on the Site or the App.The Classifr disclaims any and all liability related to any and all Customers, Merchants, Listings,Merchant Services and Merchant Products<br>
                                            (d) By using the Site, App or Services, the Merchant agrees that any legal remedy or liability that it seeks to obtain for actions or omissions of other Customers or Merchants or other third parties will be limited to a claim against the Customeror Merchantor other third party who caused it harm. The Classifr encourages the Merchant to communicate directly with the relevant Customeron the Site, App and Services regarding any communications or arrangements made between them and to resolve any dispute between them.<Br>
                                            (e) To the fullest extent allowable under applicable law, the Classifr disclaims all warranties, representations and conditions, whether express or implied, including any warranties, representations and conditions that the Site, App or Services are merchantable, of satisfactory quality, reliable, accurate, complete, suitable or fit for a particular purpose or need, non-infringing or free of defects or errors.<Br>
                                            (f) the Classifr excludes all express and implied conditions and warrantiesto the fullest extent permitted by law, including but not limited to:<Br>
                                            <ul>
                                                <li>i.  the Classifr expressly disclaims any implied or express guarantees, warranties, representations or conditions of any kind, which are not stated in these Terms;</li>
                                                <li>ii. the Classifr does not warrant that the Site, the App, the Services, content on the Site or App (including pictures, videos, sound clips, resumes, links etc.), or the Merchant’s access to the Site, the Appor the Services will be error free, that any defects will be corrected or that the Site, the App or the server which stores and transmits material to the Merchant is free of viruses or any other harmful components;.</li>
                                                <li>iii.    the Classifr takes no responsibility for, and will not be liable for, the Site, the App, the Services or the Customers; and</li>
                                                <li>iv. the Classifr will not be liable for any loss, damage, costs or expense whether direct, indirect, incidental, special, consequential and/or incidental, exemplary or consequential damages, including lost profits, loss of data or loss of goodwill, service interruption, computer damage or system failure or the cost of substitute products or services, or for any damages for personal, bodily injury, death or emotional distress, loss of revenue, production, opportunity, access to markets, goodwill, reputation, use or any indirect, remote, abnormal or unforeseeable loss, or any loss or damage relating to business interruption, loss of programs or other data on the Merchant’s information systems or costs of replacement goods, or otherwise, suffered by the Merchant or claims made against the Merchant, arising out of or in connection with the Site, the App, Services, content on the Site or App, inability to access or use the Site, the App, the Services, any Listing, the Merchant Services, the Merchant Products or the Terms, even if the Classifr was expressly advised of the likelihood of such loss or damage.</li>

                                            </ul>
                                            (g)   The Merchantagrees not to attempt to impose liability on, or seek any legal remedy from the Classifr with respect to such actions or omissions<br><br>
                                        </li>

                                        <li><strong><span>19 </span>  Limitation of Liability</strong><br>
                                            (a) To the extent permitted by law, the Classifr’s total liability arising out of or in connection with the Site, the App, the Services or the Terms, however arising, including under contract, tort, negligence, in equity, under statute or otherwise, is limited to the Classifr re-supplying the Services to the Merchant, or, at the Classifr’s option, the Classifr refunding to the Merchant the amount it paid to the Classifr for the Services to which its claim relates. The Classifr’s total liability to the Merchant for all damages in connection with the Services will not exceed the price paid by the Merchant under these Terms for the 12 months period prior to the act which gave rise to the liability, or one hundred dollars (AUD$100) if no such payments have been made.<Br>
                                            (b) The limitations of damages set forth above are fundamental elements of the basis of the bargain between the Classifrand the Merchant. Some jurisdictions do not allow the exclusion or limitation of liability for consequential or incidental damages, so the above limitation may not apply to the Merchant. <Br>
                                            (c) This limitation of liability reflects the allocation of risk between the parties. The limitations specified in this section will survive and apply even if any limited remedy specified in these terms is found to have failed of its essential purpose. The limitations of liability provided in these terms inure to the benefit of the Classifr.<br><br>
                                        </li>

                                        <li><strong><span>20) </span>     Indemnity</strong><br>
                                            (a)  Each Merchantagrees to defend and indemnify and hold the Classifr (and the Classifr’s parent, related bodies corporate, officers, directors, contractors, employees and agents) harmless from and against any claims, actions, suits, demands, damages, liabilities, costs or expenses (including legal costs and expenses on a full indemnity basis), including in tort, contract or negligence, arising out of or connected to the Merchant’s use of or access to theServices; any breach by the Merchant of these Terms; any wilful, unlawful or negligent act or omission by the Merchant; and any violation by the Merchant of any applicable laws or the rights of any third party.<Br>
                                            (b)  The Classifr reserves the right to assume the exclusive defence and control of any matter otherwise subject to indemnification by the Merchant, in which event the Merchant will cooperate in asserting any available defences. <Br>
                                            (c)  This defence and indemnification obligation will survive these Terms and the Merchant’s use of the Site, App or Services. These Terms, and any rights and licences granted hereunder, may not be transferred or assigned by the Merchant, but may be assigned by the Classifr without restriction.<br><br>
                                        </li>

                                        <li><strong><span>21) </span>    General</strong><br>
                                            (a)     Accuracy: While the Classifr will endeavour to keep the information up to date and correct, the Classifrmakes no representations, warranties or guarantees, express or implied, about the completeness, accuracy, reliability, suitability or availability of any information, images, products, services, or related graphics contained on the Site or App for any particular purpose. The Merchant hereby acknowledges that such information and materials may contain mistakes, inaccuracies or errors and the Classifr expressly excludes any liability for such to the fullest extent permissible by law.<Br>
                                            (b)  Termination: the Classifrreserves the right to refuse supply of the Services requiredby any Merchant, terminate any Merchant’s Account, terminate its contract with any Merchant, and remove or edit content on the Site or App, if the Merchant commits a non-remediable breach or a remediable breach that is not remedied within 5 days, in its sole discretion. If the Classifr decides to terminate a Merchant’s Account, with or without notice to the Merchant the Merchant’s Account will be deactivated, its password will be disabled and it will not be able to access the Site, App, Services, its Account or its User Content.<Br>
                                            (c) Fraudulent Activities: Each Merchant acknowledges and agrees that, in the event the Classifr reasonably suspects that there are fraudulent activities occurring within the Site, App and Services, the Classifr reserves the right to immediately terminate any Accounts involved in such activities, contact the relevant authorities and provide all necessary information to assist in proceedings and investigations.<br>
                                            (d) Force Majeure:the Classifr will not be liable for any delay or failure to perform its obligations under the Terms if such delay is due to any circumstance beyond its reasonable control. <Br>
                                            (e) Notice: Any notice in connection with the Terms will be deemed to have been duly given when made in writing and delivered or sent by email or post to the party to whom such notice is intended to be given or to such other address or email address as may from time to time be notified in writing to the other party.<br>
                                            (f) Waiver: Any failure by a party to insist upon strict performance by the other of any provision in the Terms will not be taken to be a waiver of any existing or future rights in relation to the provision. No waiver by the Classifr of any of the Terms shall be effective unless the Classifr expressly states that it is a waiver and the Classifrcommunicates it to the Merchant in writing.<Br>
                                            (g) Assignment: A Merchant must not assign any rights and obligations under the Terms whether in whole or in part without the Classifr’s prior written consent<br>
                                            (h) Severability:If any of the Terms are determined by any competent authority to be invalid, unlawful or unenforceable to any extent, such term, condition or provision will to that extent be severed from the remaining terms, conditions and provisions which will continue to be valid to the fullest extent permitted by law.<Br>
                                            (i) Jurisdiction and Applicable Law: These Terms, use of this Site, the App, the Servicesand any dispute arising out of any Merchant’s use of the Site, App or Services is subject to the laws of Victoria, Australia, and subject to the exclusive jurisdiction of Victorian courts. The Site and App may be accessed throughout Australia and overseas.The Classifrmakes no representation that the content of the Site and App complies with the laws (including intellectual property laws) of any country outside Australia.If a Merchant accesses the Site or App from outside Australia, it does so at its own risk and isresponsible for complying with the laws in the place where it accesses the Site or App.<Br>
                                            (j) Entire Agreement:These Terms and any document expressly referred to in them represent the entire agreement between the Classifr and each Merchant, and supersede any prior agreement, understanding or arrangement between the Classifr and each Merchant, whether oral or in writing.<Br><br>
                                        </li>
                                        <li><strong>For questions and notices, please contact</strong>
                                            <ul>
                                                <li>Classifr Group Pty. Ltd. trading as Classifr ABN 83 615 223 099</li>
                                                <li>99 Coventry Street, Southbank VIC 3006</li>
                                                <li>contact@classifr.com</li>
                                            </ul>

                                        </li>



                                    </ul>
                                    <div class="popup_btn">
                                        <a class="close-menu" href="javascript:void(0)" onclick="w3_close2()">I Accept the Terms and Conditions</a>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                    <?php
                }
                ?>

                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                        © <?= date("Y", time()) . " " . Yii::$app->name; ?>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>

        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>
        <!-- /footer content -->
        <?php $this->endBody(); ?>
    </body>
</html>

<script>
    function w3_close2() {
        document.getElementById("popup").style.display = "none";
        document.getElementById("overlay").style.display = "none";
    }
    // remove this
    // document.getElementById("popup").style.display = "none";
    // document.getElementById("overlay").style.display = "none";
</script>
<?php $this->endPage(); ?>
