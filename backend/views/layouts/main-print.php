<?php

use yii\helpers\Html;
use dmstr\widgets\Alert;
use \yii\widgets\ActiveForm;
use \yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

backend\assets\AppAsset::register($this);
backend\assets\IndexAsset::register($this);
$bundle = yiister\gentelella\assets\Asset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body onload="window.print(); window.close();">
        <?php $this->beginBody() ?>
        <div class="col-lg-8">
            <?= $content; ?>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
