<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CountrySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Countries');
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "New",
        "url" => Url::toRoute(['/country/create']),
        "icon" => "fa fa-plus",
    ],
    [
        "title" => "Refresh",
        "url" => Url::toRoute(['/country/index']),
        "icon" => "fa fa-refresh",
    ],
]);
?>
<div class="country-index panel">
    <div class="panel-body"> 
        <div class="table-responsive">
        <?php
        $dataProvider->sort = [
            'attributes' => ['id', 'name']
        ];
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                GenxGridHelper::linkedID(),
                GenxGridHelper::linkedTitle('name'),
                'iso_2_char',
                'iso_3_char',
                GenxGridHelper::status(),
                GenxGridHelper::ActionColumn(),
            ],
        ]);
        ?>
    </div>
    </div>
</div>