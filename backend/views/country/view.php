<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Country */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Countries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "Add State",
        "url" => Url::toRoute(['/state/create', 'country_id' => $model->id]),
        "icon" => "fa fa-plus",
    ],
    [
        "title" => "Edit",
        "url" => Url::toRoute(['/country/update', 'id' => $model->id]),
        "icon" => "fa fa-edit",
    ],
    [
        "title" => "Delete",
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
        "url" => Url::toRoute(['/country/delete', 'id' => $model->id]),
        "icon" => "fa fa-trash",
    ],
    [
        "title" => "List",
        "url" => Url::toRoute(['/country/index']),
        "icon" => "fa fa-list",
    ]
]);
?>

<div class="category-view panel">
    <div class="panel-body">
        <h1>
            <?= $model->name; ?>
        </h1>
        <hr />
        <h4>States</h4>
        <?=
        GridView::widget([
            'dataProvider' => $stateDataProvider,
            'filterModel' => $stateSearchModel,
            'columns' => [
                GenxGridHelper::linkedID(),
                GenxGridHelper::linkedTitle('name'),
                GenxGridHelper::ActionColumn()
            ],
        ]);
        ?>
    </div>
</div>
