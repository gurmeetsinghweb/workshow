<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\GenxGridHelper;
use common\components\GenXHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Companies');
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "New",
        "url" => Url::toRoute(['/company/create']),
        "icon" => "fa fa-plus",
    ],
    [
        "title" => "Refresh",
        "url" => Url::toRoute(['/company/index']),
        "icon" => "fa fa-refresh",
    ],
]);
?>
<div class="panel">
    <div class="company-index panel-body">

        <?php
        $dataProvider->sort = [
            'attributes' => ['id', 'title', 'updated_at']
        ];
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                GenxGridHelper::linkedID(),
                GenxGridHelper::linkedTitle(),
                GenxGridHelper::users(),
//                'geo_lat',
                // 'geo_long',
                // 'logo',
                // 'address',
                // 'city_id',
                // 'state_id',
                // 'country_id',
                // 'contact',
                // 'website',
                // 'facebook',
                // 'twitter',
                // 'youtube',
                // 'gplus',
                GenxGridHelper::status(),
                GenxGridHelper::updatedTime(),
                GenxGridHelper::ActionColumn(),
            ],
        ]);
        ?>
    </div>
</div>
