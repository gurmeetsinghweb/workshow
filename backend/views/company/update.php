<?php

use yii\helpers\Url;
use common\components\GenXHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
            'modelClass' => 'Company',
        ]) . $model->title;

if(Yii::$app->user->isAdmin()){
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
 }
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
GenXHelper::contentHeader($this);
if (\Yii::$app->user->isAdmin()) {
    GenXHelper::contentHeaderButtons($this, [
        [
            "title" => "List",
            "url" => Url::toRoute(['/merchant/index']),
            "icon" => "fa fa-list",
        ]
    ]);
}
?>
<div class="country-create panel">
    <div class="panel-body"><?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>

    </div>
</div>
