<?php

use common\components\GenXHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$this->title = Yii::t('app', 'Create Merchant');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
if (\Yii::$app->user->isAdmin()) {
    GenXHelper::contentHeaderButtons($this, [
        // [
        //     "title" => "List",
        //     "url" => Url::toRoute(['/country/index']),
        //     "icon" => "fa fa-list",
        // ]
    ]);
}
?>
<div class="country-create panel">
    <div class="panel-body">
        <?=
        $this->render('_form', [
            'model' => $model,
            'UserModel' => $UserModel,
        ])
        ?>
    </div>
</div>
