<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $UserModel common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
//    if ($model->isNewRecord && \Yii::$app->user->isAdmin()) {
//        echo '<label class="control-label">Search Merchant (if Merchant already have company, page will redirect to update from)</label>';
//        echo yii\jui\AutoComplete::widget([
//            'clientOptions' => [
//                'source' => Url::toRoute(['/merchant/merchant-autocomplete']),
//                'select' => new yii\web\JsExpression('function( event, ui ) {
//                            // if merchant already have company, then go to edit mode
//                            if(ui.item.company_id > 0) {
//                                window.location.href = "' . Url::toRoute('update') . '?id="+ui.item.company_id;
//                            }
//                            $("#company-user_id").val(ui.item.id);
//                          }'),
//            ],
//            'options' => ['class' => 'form-control']
//        ]);
//        echo $form->field($model, 'user_id')->hiddenInput()->label(False);
//    }
    ?>
    <div class="row">
        <div class="col-sm-6 col-xs-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label("Company Name") ?>
        </div>
        <div class="col-sm-6 col-xs-6">
            <?= $form->field($model, 'trading_name')->textInput(['maxlength' => true])->label("Trading Name") ?>
        </div>
    </div>
    
    
    <?= $this->render('@common/views/_partials/text_editor', ['form' => $form, 'model' => $model, 'attribute' => 'description','label'=>"Company Description"]); ?>

    <?= $form->field($model, 'logo')->fileInput() ?>

    <?= $this->render('@common/views/_partials/city', ['form' => $form, 'model' => $model, 'attribute' => 'city_id']); ?>
    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'geo_lat')->hiddenInput()->label(False); ?>
    <?= $form->field($model, 'geo_long')->hiddenInput()->label(False); ?>
    <div class="row">
        <div class="col-sm-12 col-xs-12">
            <label class="control-label">Select your Location</label>
              

        <?=
         $this->render('@common/views/_partials/gmap_geolocation', [
                'form' => $form,
                'model' => $model,
                'options' => [
                    'address_input_id' => 'company-address',
                    'lat_input_id' => 'company-geo_lat',
                    'long_input_id' => 'company-geo_long',
                ]
            ]);
         ?>
            
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'contact')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'facebook')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'twitter')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'youtube')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'gplus')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($model, 'abn_number')->textInput(['maxlength' => true]) ?>
        </div>
        
    </div>

    <?php if (isset($UserModel)) { ?>
        <div>
            <div class="col-lg-12">
                <h3>Login Details <span>for merchant to access his/her account</span></h3>
            </div>
        </div>

        <div class="row">
             <div class="col-lg-12">
                 Select Existing user to Merchant
                  <?php 
                
                     echo yii\jui\AutoComplete::widget([
                            'clientOptions' => [
                                'source' => Url::toRoute(['/user/user-autocomplete']),
                                'select' => new yii\web\JsExpression('function( event, ui ) {
                            $("#user-name").val(ui.item.name);
                            $("#user-username").val(ui.item.username);
                            $("#user-id").val(ui.item.id);
                            $("#user-email").val(ui.item.email);
                            $("#user-password").val(ui.item.password);
                            
                            
                            
                          }'),
                            ],
                            'options' => ['class' => 'form-control']
                        ]);
                     ?>
                 <input type="hidden" name="User[id]" id="user-id">
             </div> 
            
            <div class="col-lg-6">
                <?= $form->field($UserModel, 'name')->textInput(['maxlength' => true])->label('Contact Name') ?>
            </div>
            <div class="col-lg-6">
               
                <?= $form->field($UserModel, 'username')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($UserModel, 'email')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($UserModel, 'password')->passwordInput(['maxlength' => true]) ?>
            </div>
        </div>
    <?php } ?>

    <div class="form-group here">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        

    </div>



    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs("
        $('#company-address').geocomplete();
");
?>


<!-- <script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initMap" async defer></script> -->

