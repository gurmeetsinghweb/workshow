<?php

use yii\helpers\Html;
use common\components\GenXHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
            'modelClass' => 'User',
        ]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "List",
        "url" => Url::toRoute(['/user']),
        "icon" => "fa fa-list",
    ]
]);
?>
<div class="panel">
    
    <div class="user-form panel-body">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true])->label('New Password') ?>
    <?= $form->field($model, 'password_repeat')->passwordInput(['maxlength' => true])->label('Repeat New Password') ?>
        
    <?php
    if (Yii::$app->user->can(common\models\User::ROLE_ADMIN)) {
        echo $form->field($model, 'status')->dropDownList($model->getUserStatusDropdownForForm());
        echo $form->field($model, 'type')->dropDownList($model->getUserTypeDropdownForForm(), ['maxlength' => true]);
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
    

</div>
