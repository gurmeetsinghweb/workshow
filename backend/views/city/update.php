<?php

use yii\helpers\Html;
use common\components\GenXHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\City */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
            'modelClass' => 'City',
        ]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "List",
        "url" => Url::toRoute(['/city/index']),
        "icon" => "fa fa-list",
    ],
    [
        "title" => "Delete",
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
        "url" => Url::toRoute(['/city/delete', 'id' => $model->id]),
        "icon" => "fa fa-trash",
    ],
]);
?>
<div class="city-update panel">
    <div class="panel-body">
        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>
    </div>
</div>
