<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\City */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-form">

    <?php $form = ActiveForm::begin([
                                'options' => [
                                    'enctype' => 'multipart/form-data'
                                ]
                    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $this->render("@common/views/_partials/country", ["model" => $model, "form" => $form]); ?>
    <?= $this->render("@common/views/_partials/state", ["model" => $model, "form" => $form]); ?>
    <?= $form->field($model, 'iso_2_char')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'iso_3_char')->textInput(['maxlength' => true]) ?>
    <?= $this->render("@common/views/_partials/status", ['model' => $model, 'form' => $form]); ?>
    <?= $form->field($model, 'featured')->dropDownList(common\components\GenxBaseModel::getFeaturedDropdown());?>
    <?= $form->field($model, 'featured_img')->fileInput(); ?>
     <?= $form->field($model, 'show_home')->dropDownList(common\components\GenxBaseModel::getFeaturedDropdown());?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
