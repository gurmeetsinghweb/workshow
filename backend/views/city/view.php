<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\City */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "Edit",
        "url" => Url::toRoute(['/city/update', 'id' => $model->id]),
        "icon" => "fa fa-edit",
    ],
    [
        "title" => "Delete",
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
        "url" => Url::toRoute(['/city/delete', 'id' => $model->id]),
        "icon" => "fa fa-trash",
    ],
    [
        "title" => "List",
        "url" => Url::toRoute(['/city/index']),
        "icon" => "fa fa-list",
    ]
]);
?>

<div class="category-view panel">
    <div class="panel-body">
        <h1>
            <?= Html::a($model->country->name, $model->country->getURL());?> : <?= Html::a($model->state->name, $model->state->getURL());?> : <?= $model->name; ?>
        </h1>
    </div>
</div>
