<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\GenxGridHelper;
use common\components\GenXHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cities');
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "New",
        "url" => Url::toRoute(['/city/create']),
        "icon" => "fa fa-plus",
    ],
    [
        "title" => "Refresh",
        "url" => Url::toRoute(['/city/index']),
        "icon" => "fa fa-refresh",
    ],
]);
?>
<div class="city-index panel">
    <div class="panel-body">
        <div class="table-responsive">
        <?php

        $dataProvider->sort = [
            'attributes' => ['id', 'name']
        ];
       echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                GenxGridHelper::linkedID(),
                GenxGridHelper::linkedTitle('name'),
                'zip_code',
                GenxGridHelper::states(),
                GenxGridHelper::status(),
                GenxGridHelper::featured(),
                 [
                    'label' => 'Featured Image',
                    'attribute' => 'featured_img',
                    
                ],
                [
                    'label' => 'Show Homepage',
                    'attribute'=>'show_home',
                    'filter'=>array("1"=>"Active","0"=>"InActive")
                ],
                
                GenxGridHelper::ActionColumn(),
            ],
        ]);
        ?>
    </div>
    </div>
</div>
