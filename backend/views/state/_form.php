<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\State */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="state-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $this->render("@common/views/_partials/country", ["model" => $model, "form" => $form]); ?>
    <?= $form->field($model, 'iso_2_char')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'iso_3_char')->textInput(['maxlength' => true]) ?>
    <?= $this->render("@common/views/_partials/status", ['model' => $model, 'form' => $form]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
