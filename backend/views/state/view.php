<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\State */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'States'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "Add State",
        "url" => Url::toRoute(['/city/create', 'state_id' => $model->id, 'country_id' => $model->country_id]),
        "icon" => "fa fa-plus",
    ],
    [
        "title" => "Edit",
        "url" => Url::toRoute(['/state/update', 'id' => $model->id]),
        "icon" => "fa fa-edit",
    ],
    [
        "title" => "Delete",
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
        "url" => Url::toRoute(['/state/delete', 'id' => $model->id]),
        "icon" => "fa fa-trash",
    ],
    [
        "title" => "List",
        "url" => Url::toRoute(['/state/index']),
        "icon" => "fa fa-list",
    ]
]);
?>

<div class="category-view panel">
    <div class="panel-body">
        <h1>
            <?= Html::a($model->country->name, $model->country->getURL()); ?> : <?= $model->name; ?>
        </h1>
        <hr />
        <h4>Cities</h4>
        <?=
        GridView::widget([
            'dataProvider' => $cityDataProvider,
            'filterModel' => $citySearchModel,
            'columns' => [
                GenxGridHelper::linkedID(),
                GenxGridHelper::linkedTitle('name'),
                GenxGridHelper::ActionColumn()
            ],
        ]);
        ?>
    </div>
</div>
