<?php

use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DealShippingAmountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deal Shipping Amounts';
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "New",
        "url" => Url::toRoute(['/deal-shipping-amount/create']),
        "icon" => "wb-plus",
    ],
    [
        "title" => "Refresh",
        "url" => Url::toRoute(['/deal-shipping-amount/index']),
        "icon" => "wb-refresh",
    ],
]);
?>
<div class="deal-shipping-amount-index panel">
    <div class="panel-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

              GenxGridHelper::linkedID(),
            'deal_id',
            'state_id',
            'shipping_amount',
            // 'status',
            // 'created_at',
            // 'updated_at',

            GenxGridHelper::status(),
            GenxGridHelper::ActionColumn(),
        ],
    ]); ?>
    </div>
</div>
