<?php

use yii\helpers\Html;
use common\components\GenXHelper;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use common\components\GenxGridHelper;


/* @var $this yii\web\View */
/* @var $model common\models\DealShippingAmount */

$this->title = 'Create Deal Shipping Amount';
$this->params['breadcrumbs'][] = ['label' => 'Deal Shipping Amounts'];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
//    [
//        "title" => "List",
//        "url" => Url::toRoute(['/deal-shipping-amount/index']),
//        "icon" => "fa fa-list",
//    ]
    
]);


 if(!isset($_REQUEST['update_id']))
{
     $button = [

                                    'buttons' => [
                                        'edit' => function ($url, $model, $key) {
                                            $url = Url::toRoute(['update', 'id' => $model->id]);
                                            return Html::a('<span class="glyphicon glyphicon-pencil" style="font-size:20px;"></span>', $url, [
                                                        'title' => \Yii::t('yii', 'Update'),
                                                        'data-pjax' => '0',
                                            ]);
                                        },
                                             
                                                
                                                
                                                
                                               
                                                'delete' => function ($url, $model, $key) {

                                            $url = Url::toRoute(['delete', 'id' => $model->id]);
                                            return Html::a('<span class="glyphicon glyphicon-trash" style="font-size:20px;"></span>', $url, [
                                                        'title' => \Yii::t('yii', 'Delete'),
                                                        'data-confirm' => \Yii::t('yii', 'Are you sure to delete this deal?'),
                                                        'data-method' => 'post',
                                                        'data-pjax' => '0',
                                            ]);
                                        }
                                            ],
                                            'template' => '{edit} {delete} '
     ];
}
else{
 $button = [

                                    'buttons' => [
                                        'edit' => function ($url, $model, $key) {
                                            $url = Url::toRoute(['update', 'id' => $model->id,'update_id'=>1]);
                                            return Html::a('<span class="glyphicon glyphicon-pencil" style="font-size:20px;"></span>', $url, [
                                                        'title' => \Yii::t('yii', 'Update'),
                                                        'data-pjax' => '0',
                                            ]);
                                        },
                                             
                                                
                                                
                                                
                                               
                                                'delete' => function ($url, $model, $key) {

                                            $url = Url::toRoute(['delete', 'id' => $model->id,'update_id'=>1]);
                                            return Html::a('<span class="glyphicon glyphicon-trash" style="font-size:20px;"></span>', $url, [
                                                        'title' => \Yii::t('yii', 'Delete'),
                                                        'data-confirm' => \Yii::t('yii', 'Are you sure to delete this deal?'),
                                                        'data-method' => 'post',
                                                        'data-pjax' => '0',
                                            ]);
                                        }
                                            ],
                                            'template' => '{edit} {delete} '
     ];
}

?>
<div class="deal-shipping-amount-create panel">
    <div class="panel-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    </div>
</div>



<div class="deal-shipping-amount-view panel">
    <div class="panel-body">
        <h1><?= Html::encode($this->title) ?></h1>

       <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

              GenxGridHelper::linkedID(),
            
            [
		  'attribute' => 'state_id',
		  'label' => 'State',
		  'format' => 'raw',
		  'value' => function ($model) {

		      return $model->state->name;
		  }
	      ],
            'shipping_amount',
            // 'status',
            // 'created_at',
            // 'updated_at',

            GenxGridHelper::status(),
           GenxGridHelper::ActionColumn($button),
	     
        ],
    ]); ?>
	 <?php
                        if (!isset($_REQUEST['update_id'])) {
                            ?>
                            <?= Html::a('Back - Update Deal', ['/deal/update', 'id' => $model->deal_id], ['class' => 'pull-left btn btn-primary deal-address-btn']) ?>
                            
				<?php
                                 if ($dataProvider->getCount()) {
                                echo Html::a('Next - Update option', ['/deal-option/create', 'deal_id' => $model->deal_id], ['class' => 'pull-right btn btn-primary deal-address-btn']);
                            }
                                
                                
	 			   }else{
                                       
                                       ?>
         <?= Html::a('Back - Update Deal', ['/deal/update', 'id' => $model->deal_id], ['class' => 'pull-left btn btn-primary deal-address-btn']) ?>
                            
                                       <?php 
                                       
					echo Html::a('Finish', ['/deal/index'], ['class' => 'pull-right btn btn-success']);
				
			   }
                           
                        
                        ?>
    </div>
</div>

