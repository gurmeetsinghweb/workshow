<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DealShippingAmount */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="deal-shipping-amount-form">

	
    <?php $form = ActiveForm::begin(); ?>

    <?= $this->render('@common/views/_partials/state', ['form' => $form, 'model' => $model, 'attribute' => 'state_id']); ?>

    <?= $form->field($model, 'shipping_amount')->textInput() ?>
<?php
                            if(isset($_REQUEST['update_id'])){
                                ?>
                            <input type="hidden" name="update_id" value="1">
                            <?php
                          }
                            ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
