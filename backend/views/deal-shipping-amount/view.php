<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\helpers\Url;
use common\components\GenXHelper;
use common\components\GenxGridHelper;

/* @var $this yii\web\View */
/* @var $model common\models\DealShippingAmount */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Deal Shipping Amounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "Edit",
        "url" => Url::toRoute(['/deal-shipping-amount/update', 'id' => $model->id]),
        "icon" => "wb-edit",
    ],
    [
        "title" => "Delete",
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
        "url" => Url::toRoute(['/deal-shipping-amount/delete', 'id' => $model->id]),
        "icon" => "wb-trash",
    ],
    [
        "title" => "List",
        "url" => Url::toRoute(['/deal-shipping-amount/index']),
        "icon" => "wb-list",
    ]
]);
?>
<div class="deal-shipping-amount-view panel">
    <div class="panel-body">
        <h1><?= Html::encode($this->title) ?></h1>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
            'deal_id',
            'state_id',
            'shipping_amount',
            'status',
            'created_at',
            'updated_at',
            ],
        ]) ?>
    </div>
</div>
