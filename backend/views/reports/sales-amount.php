<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\grid\GridView;
use common\components\GenxGridHelper;

backend\assets\ChartAsset::register($this);
backend\assets\FromAssets::register($this);
$this->title = Yii::t('app', 'Sale Report Net price');
$this->params['breadcrumbs'][] = $this->title;

$deals_array = \yii\helpers\ArrayHelper::map($deals, 'id', 'title');
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">
            <?php
            $form = \yii\widgets\ActiveForm::begin(['method' => 'GET']);
            ?>
            <div class="col-xs-12 col-md-6 col-lg-3">
                Choose Date to filter records
                <input type="text" style="" name="date_range" id="date_range" class="form-control" value="<?= $POST['date_range'] ?>" />
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3">
                Choose Deal to filter records<br />
                <select id="deals_list" name="deal_id" class="form-control">
                    <option></option>
                    <?php
                    if (sizeof($deals_array)) {
                        foreach ($deals_array as $id => $title) {
                            echo "<option value='{$id}' " . ((isset($POST['deal_id']) && $POST['deal_id'] == $id) ? "SELECTED" : "") . ">{$title}</option>";
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3 filter_btn">
                <br />
                <input type="submit" value="Filter Records" class="btn btn-success btn-md"/>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3  filter_btn">
                <br />
                <?=
                yii\helpers\Html::a('Reset', ['reports/sold-deals'], ['class' => 'btn btn-danger btn-md']);
                ?>
            </div>
            <?php
            yii\widgets\ActiveForm::end();
            ?>
        </div>
    </div>
</div>
<hr />
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-xs-12 col-md-6">
                    <h3><?= $this->title; ?><small> [<?= $POST['date_range']; ?>]</small></h3>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
                <div style="width: 100%;">
                    <canvas id="sale-wise-chart"></canvas>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

</div>
<br />
<div class="country-index panel">
    <div class="panel-body"> 
        <?php
        $dataProvider->sort = [
            'attributes' => ['id', 'name']
        ];
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'Date',
                    'value' => function($model) {
                        return $model->getCreatedTime();
                    }
                ],
                'total_sale',
                [
                    'attribute' => 'actions',
                    'format' => 'raw',
                    'value' => function($model) {
                        $start_time = mktime(0, 0, 0, date("n", $model->created_at), date("j", $model->created_at), date("Y", $model->created_at));
                        $end_time = mktime(23, 59, 59, date("n", $model->created_at), date("j", $model->created_at), date("Y", $model->created_at));
                        return \yii\helpers\Html::a(
                                        '<i class="fa fa-list" aria-hidden="true"></i> View Details', \yii\helpers\Url::toRoute([
                                            '/coupon-code/index',
                                            'time_range' => $start_time . "-" . $end_time
                                        ]), ['class' => 'btn btn-success btn-sm']);
                    }
                        ]
                    ],
                ]);
                ?>

            </div>
        </div>
        <?php
        $cart_js = "
      
        var sale_wise_chart = new Chart(document.getElementById(\"sale-wise-chart\"), {
        type: 'bar',
        data: " . json_encode($chart_data) . ",
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              }
            }]
          }
        }
      });
      
        $('#date_range').daterangepicker(null, function(start, end, label) {});
        $('#deals_list').select2();
    ";

        $this->registerJs($cart_js);
        ?>