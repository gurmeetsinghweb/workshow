<?php

use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

backend\assets\ChartAsset::register($this);
backend\assets\FromAssets::register($this);

/* @var $this yii\web\View */
/* @var $searchModel common\models\CouponCodeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Redeemed Coupon Codes');
$this->params['breadcrumbs'][] = $this->title;
$deals_array = \yii\helpers\ArrayHelper::map($deals, 'id', 'title');
?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">
            <?php
            $form = \yii\widgets\ActiveForm::begin(['method' => 'GET']);
            ?>
            <div class="col-lg-3">
                Choose Date to filter records
                <input type="text" style="width: 200px" name="date_range" id="date_range" class="form-control" value="<?= $POST['date_range'] ?>" />
            </div>
            <div class="col-lg-3">
                Choose Deal to filter records<br />
                <select id="deals_list" name="deal_id" class="form-control">
                    <option></option>
                    <?php
                    if (sizeof($deals_array)) {
                        foreach ($deals_array as $id => $title) {
                            echo "<option value='{$id}' " . ((isset($POST['deal_id']) && $POST['deal_id'] == $id) ? "SELECTED" : "") . ">{$title}</option>";
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-3">
                <br />
                <input type="submit" value="Filter Records" class="btn btn-success btn-sm"/>
            </div>
            <div class="col-lg-3 pull-right">
                <br />
                <?=
                yii\helpers\Html::a('Reset', ['reports/pending-coupons'], ['class' => 'btn btn-danger btn-sm']);
                ?>
            </div>
            <?php
            yii\widgets\ActiveForm::end();
            ?>
        </div>
    </div>
</div>
<hr />
<div class="coupon-code-index panel">
    <div class="panel-body">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'Custtomer',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->cart->user->name;
                    },
                ],
                [
                    'label' => 'Deal',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model->cart->deal->title;
                    },
                ],
                'coupon_no',
                [
                    'label' => 'Generated at',
                    'format' => 'datetime',
                    'attribute' => 'created_at'
                ]
            ],
        ]);
        ?>
    </div>
</div>
<?php
$cart_js = "
        $('#date_range').daterangepicker(null, function(start, end, label) {});
        $('#deals_list').select2();
    ";

$this->registerJs($cart_js);
?>
