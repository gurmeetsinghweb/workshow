<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\grid\GridView;
use common\components\GenxGridHelper;

backend\assets\ChartAsset::register($this);
backend\assets\FromAssets::register($this);
$this->title = Yii::t('app', 'Customer Wise Sale');
$this->params['breadcrumbs'][] = $this->title;

$deals_array = \yii\helpers\ArrayHelper::map($deals, 'id', 'title');
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">
            <?php
            $form = \yii\widgets\ActiveForm::begin(['method' => 'GET']);
            ?>
            <div class="col-lg-3">
                Choose Date to filter records
                <input type="text" style="width: 200px" name="date_range" id="date_range" class="form-control" value="<?= $POST['date_range'] ?>" />
            </div>
            <div class="col-lg-3">
                Choose Deal to filter records<br />
                <select id="deals_list" name="deal_id" class="form-control">
                    <option></option>
                    <?php
                    if (sizeof($deals_array)) {
                        foreach ($deals_array as $id => $title) {
                            echo "<option value='{$id}' " . ((isset($POST['deal_id']) && $POST['deal_id'] == $id) ? "SELECTED" : "") . ">{$title}</option>";
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-3">
                <br />
                <input type="submit" value="Filter Records" class="btn btn-success btn-sm"/>
            </div>
            <div class="col-lg-3 pull-right">
                <br />
                <?=
                yii\helpers\Html::a('Reset', ['reports/customers'], ['class' => 'btn btn-danger btn-sm']);
                ?>
            </div>
            <?php
            yii\widgets\ActiveForm::end();
            ?>
        </div>
    </div>
</div>
<hr />
<!--<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="dashboard_graph">

            <div class="row x_title">
                <div class="col-md-6">
                    <h3><?= $this->title; ?><small> [<?= $POST['date_range']; ?>]</small></h3>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
                <div style="width: 100%;">
                    <canvas id="sale-wise-chart"></canvas>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

</div>
<br />-->
<div class="country-index panel">
    <div class="panel-body"> 
        <?php
        $dataProvider->sort = [
            'attributes' => ['id', 'name']
        ];
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'Date',
                    'value' => function($model) {
                        return $model->getCreatedTime();
                    }
                ],
                [
                    'label' => 'Customer',
                    'attribute' => 'user',
                    'format' => 'raw',
                    'value' => function($model) {
                        return $model->user->name;
                    }
                ],
                [
                    'attribute' => 'total_sale',
                    'label' => 'Total Purchased amount'
                ],
            ],
        ]);
        ?>

    </div>
</div>
<?php
$cart_js = "
        $('#date_range').daterangepicker(null, function(start, end, label) {});
        $('#deals_list').select2();
    ";

$this->registerJs($cart_js);
?>