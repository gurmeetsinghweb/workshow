<?php

use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $order \common\models\OrderSearch */
?>
<div class="">
    <?php if (!$print) { ?>
        <div class="page-title no-print">
            <div class="title_left">
                <h3>Detail Invoice <small>#<?= $order->id ?></small></h3>
            </div>
        </div>
        <div class="clearfix"></div>
    <?php } ?>
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <?php if (!$print) { ?>
                    <div class="x_title">
                        <h2>Detail Invoice <small>#<?= $order->id ?></small></h2>
                        <!--                    <ul class="nav navbar-right panel_toolbox">
                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                                </li>
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="#">Settings 1</a>
                                                        </li>
                                                        <li><a href="#">Settings 2</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                                </li>
                                            </ul>-->
                        <div class="clearfix"></div>
                    </div>
                <?php } ?>
                <div class="x_content">

                    <section class="content invoice">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-xs-12 invoice-header">
                                <h1>
                                    <i class="fa fa-globe"></i> <?= (($print) ? "VoucherStore " : ""); ?>Invoice
                                    <small class="pull-right">Date: <?= $order->getCreatedTime(); ?></small>
                                </h1>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- info row -->
                        <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
                                To
                                <address>
                                    <strong><?= $order->user->name; ?></strong>
                                    <?php
                                    if ($order->user->address) {
                                        echo "<br />{$order->user->address}";
                                    }
                                    ?>
                                    <?php
                                    if ($order->user->city) {
                                        echo "<br />{$order->user->city->name}";
                                    }
                                    ?>
                                    <?php
                                    if ($order->user->mobile) {
                                        echo "<br />Phone: {$order->user->mobile}";
                                    }
                                    ?>
                                    <br />Email: <?= $order->user->email; ?>
                                </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                                <b>Invoice #<?= $order->id ?></b>
                                <br>
                                <br>
                                <b>Payment On:</b> <?= $order->getCreatedDate() ?>
                                <br>
                                <b>Transaction ID:</b> #968-34567
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <!-- Table row -->
                        <div class="row">
                            <div class="col-xs-12 table">
                                <table class="table table-striped table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Qty</th>
                                            <th>Product</th>
<!--                                            <th>Voucher Id#</th>
                                            <th>Voucher Code#</th>-->
                                            
                                            <th style="width: 40%">Description</th>
                                            <th>Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $total_amount = 0;
                                        /* @var $cart \common\models\Cart */
                                        foreach ($order->cart as $key => $cart) {
                                            $total_amount += $cart->net_price;
                                            ?>
                                            <tr>
                                                <td><?= $cart->qty; ?></td>
                                                <td><?= $cart->deal->title; ?></td>
<!--                                                <td><?= $cart->couponCode->id; ?></td>
                                                <td><?= $cart->couponCode->coupon_no; ?></td>-->
                                                <td><?= $cart->dealOption->name; ?></td>
                                                <td>&#x20B9;<?= $cart->net_price; ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <!-- accepted payments column -->
                            <div class="col-xs-12 col-sm-6">
                               <!-- <p class="lead">Payment Methods: PayPal</p>
                                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                    Transaction ID: #968-34567
                                </p>-->
                            </div>
                            <!-- /.col -->
                            <div class="col-xs-12 col-sm-6">
                                <p class="lead">Amount </p>
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th>Total:</th>
                                                <td>&#x20B9;<?= $total_amount; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                        <?php if (!$print) { ?>
                            <!-- this row will not appear when printing -->
<!--                            <div class="row no-print">
                                <div class="col-xs-12">
                                    <button class="btn btn-default" 
                                            onclick="window.open('<?= Url::toRoute(['invoice-print', 'id' => $order->id], True) ?>', 'Invoice Print', 'width=1000,height=800');">
                                        <i class="fa fa-print"></i> Print
                                    </button>
                                    <button class="btn btn-primary pull-right" 
                                            onclick="window.open('<?= Url::toRoute(['invoice-pdf', 'id' => $order->id], True) ?>', 'Invoice Print', 'width=1000,height=800');" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
                                </div>
                            </div>-->
                        <?php } ?>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>