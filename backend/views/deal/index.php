<?php

use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DealSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
backend\assets\FromAssets::register($this);
$this->title = Yii::t('app', $title);
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "New",
        "url" => Url::toRoute(['/deal/create']),
        "icon" => "fa fa-plus",
    ],
    [
        "title" => "Refresh",
        "url" => Url::toRoute(['/deal/index']),
        "icon" => "fa fa-refresh",
    ],
]);
?>
<div class="deal-index panel">
    <?php
    if (Yii::$app->user->isAdmin()) {
        ?>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph">
                    <?php
                    $form = \yii\widgets\ActiveForm::begin(['method' => 'GET', 'action' => Url::toRoute(["index"])]);
                    ?>
                    <div class="col-lg-2">
                        Choose Date filter
                        <input type="text"  name="date_range" id="date_range" class="form-control" value="<?= $POST['date_range'] ?>" />
                    </div>

                    <div class="col-lg-2">
                        Select Merchant
                        <?php
                        echo yii\jui\AutoComplete::widget([
                            'clientOptions' => [
                                'source' => Url::toRoute(['/merchant/merchant-autocomplete']),
                                'select' => new yii\web\JsExpression('function( event, ui ) {
                            $("#merchant").val(ui.item.id);
                                
                            
                            
                          }'),
                            ],
                            'options' => ['class' => 'form-control']
                        ]);
                        ?>
                        <input type="hidden" name="DealSearch[user_id]" id="merchant" value="<?= (isset($POST['DealSearch']['user_id'])) ? $POST['DealSearch']['user_id'] : "" ?>">
                    </div>
                    <div class="col-lg-2">
                        Choose Category


                        <select id="deals_list" name="deal_category_id" class="form-control">
                            <option value=""></option>
                            <?php
                            foreach ($category->getTreeForDropdown() as $key => $categorylist) {
                                echo "<option value='$key'";
                                if (isset($POST['deal_category_id'])) {
                                    if ($key == $POST['deal_category_id']) {

                                        echo " selected='selected'";
                                    }
                                }
                                echo " >" . $categorylist . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-2">
                        Status
                        <?php
                        if (isset($POST['status'])) {
                            $dealStatus = $POST['status'];
                        } else {
                            $dealStatus = "";
                        }
                        ?>


                        <select id="deals_list" name="status" class="form-control">
                            <option value="" >All Status</option>
                            <option value="active" <?php if ($dealStatus == 'active') { ?> selected="selected" <?php } ?> >Active</option>
                            <option value="inactive" <?php if ($dealStatus == 'inactive') { ?> selected="selected" <?php } ?>>In-active</option>
                            <option value="expired" <?php if ($dealStatus == 'expired') { ?> selected="selected" <?php } ?>>Expired</option>
                            <option value="incomplete" <?php if ($dealStatus == 'incomplete') { ?> selected="selected" <?php } ?>>Incomplete Deals</option>
                             <option value="pendingapproval" <?php if ($dealStatus == 'pendingapproval') { ?> selected="selected" <?php } ?>>Wait for Admin Approval</option>
                            <option value="approved"  <?php if ($dealStatus == 'approved') { ?> selected="selected" <?php } ?>>Wait for publish</option>




                        </select>
                    </div>
                    <div class="col-lg-4">
                        <br />
                        <input type="submit" value="Filter Records" class="btn btn-success btn-sm"/>
    <?=
    yii\helpers\Html::a('Reset', ['index'], ['class' => 'btn btn-danger btn-sm']);
    ?>
                    </div>

                        <?php
                        yii\widgets\ActiveForm::end();
                        ?>
                </div>
            </div>
        </div>
    <?php
}
?>

    <div class="panel-body">
        <div class="table-responsive table_inactive">
<?php // echo $this->render('_search', ['model' => $searchModel]);   ?>
<?php
$dataProvider->sort = [
    'attributes' => ['id', 'title']
];


echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        GenxGridHelper::linkedID(),
        //'company_id',
        GenxGridHelper::linkedTitle(),
             [
            'visible' => Yii::$app->user->isAdmin(),
            'attribute' => 'user_id',
            'format' => 'raw',
            'label' => "Merchant",
            'filter' => false,
            'value' => function ($model) {
                return Html::a($model->user->name, Url::toRoute(['/merchant/index', 'UserSearch[email]' => $model->user->email]));
            },
        ],
        [
                    'label' => 'Category',
                    'attribute' => 'deal',
                    'format' => 'raw',
                    'value' => function ($model) {
                        if(count($model->dealCategory)){
                            $delcat="";
                            foreach($model->dealCategory as $dealcat){
                                $delcat.=$dealcat->category->name .", ";
                            }
                          $delcat= substr($delcat,0, -1);
                        return $delcat;
                            
                        }
                        else{
                            return "";
                        }
                    },
                ],
                    
                    [
            'attribute' => 'totaldealoption',
            'format' => 'raw',
            'label' => 'Deals Combination',
            'value' => function ($model) {
                return Html::a("# " . $model->getDealOptions()->count(), ["deal-option/create", 'deal_id' => $model->id]);
            }
        ],
        [
            'attribute' => 'Total Sold',
            'label' => 'Sold',
            'format' => 'raw',
            'value' => function ($model) {

                $totalsold = \common\models\Cart::find()->andWhere(['deal_id' => $model->id])->count();
                //return $totalsold;
                if($model->getAvgRating()!=''){ 
                    $rating = "<img src=".Url::toRoute(['../themes/main/images/'. $model->getAvgRating() .'stars.png'],true)." />";
                }
                else 
                {
                    $rating ='';
                }
                return Html::a($totalsold, Url::toRoute(["coupon-code/index", 'deal_id' => $model->id, 'merchant_id' => $model->user->id, 'username' => $model->user->name]))."<br>". $rating;
            }
        ],
        [
            'attribute' => 'Total Redeemed',
            'label' => 'Redeemed',
            'value' => function ($model) {

                $totalredeemed = \common\models\DealOption::find()->andWhere(['deal_id' => $model->id])->sum("total_redeem");
                return $totalredeemed;
                //return Html::a($totalsold, Url::toRoute(['/deal/index', 'marchant_id' => $model->id]));
            }
        ],
        GenxGridHelper::statusToggle(),
        [
            'attribute' => 'expire_time',
            'format' => 'raw',
            'value' => function($model) {

                $newtime = intval(($model->expire_time - time()) / 86400);
                return GenXHelper::getDateFormat($model->expire_time) . "<br>"
                        . $newtime . " Days are left";
            }
        ],
        GenxGridHelper::createdTime(),
        [
            'attribute' => 'Request',
            'label' => 'Request',
            'format' => 'raw',
            'visible' => \Yii::$app->user->isOnlyMerchant(),
            'value' => function ($model) {

                if ($model->status == 10) {
                    return Html::button('<span class="glyphicon glyphicon-pencil" style="font-size:20px;"></span>', [
                                'title' => \Yii::t('yii', 'Request'),
                                'class' => 'sendrequest',
                                'value' => $model->id,
                    ]);
                }
            }
        ],
        [
            'attribute' => 'MyRequest',
            'label' => 'MyRequest',
            'format' => 'raw',
            'visible' => \Yii::$app->user->isOnlyMerchant(),
            'value' => function ($model) {
                if ($model->status == 10) {
                    return Html::button('<span class="glyphicon glyphicon-comment" style="font-size:20px;"></span>', [
                                'title' => \Yii::t('yii', 'My Request'),
                                'class' => 'myrequest',
                                'value' => $model->id,
                    ]);
                }
            }
        ],
        GenxGridHelper::ActionColumn([
            'visible' => Yii::$app->user->isMerchant(),
            'buttons' => [
                'edit' => function ($url, $model, $key) {
                    if (Yii::$app->user->isMerchant() || $model->status == \common\models\Deal::STATUS_INCOMPLETE) {
                        $url = Url::toRoute(['/deal/update', 'id' => $model->id, 'update_id' => 1]);
                        return Html::a('<span class="glyphicon glyphicon-pencil" style="font-size:20px;"></span>', $url, [
                                    'title' => \Yii::t('yii', 'Deal Update'),
                                    'data-pjax' => '0',
                        ]);
                    } else {
                        return false;
                    }
                },
                'price' => function ($url, $model, $key) {

                    if (Yii::$app->user->isMerchant() || $model->status == \common\models\Deal::STATUS_INCOMPLETE) {


                        $url = Url::toRoute(['/deal-option/create', 'deal_id' => $model->id, 'update_id' => 1]);
                        return Html::a('<span class="glyphicon glyphicon-usd" style="font-size:20px;"></span>', $url, [
                                    'title' => \Yii::t('yii', 'Deal Price Option'),
                                    'data-pjax' => '0',
                        ]);
                    }
                },
                'address' => function ($url, $model, $key) {
                     if (Yii::$app->user->isAdmin() || $model->status == \common\models\Deal::STATUS_INCOMPLETE) {


                    $url = Url::toRoute(['/deal-address/create', 'deal_id' => $model->id, 'update_id' => 1]);
                    return Html::a('<span class="glyphicon glyphicon-map-marker" style="font-size:20px;"></span>', $url, [
                                'title' => \Yii::t('yii', 'Deal Maps'),
                                'data-pjax' => '0',
                    ]);
                     }
                },
                'gallery' => function ($url, $model, $key) {
                    
 if (Yii::$app->user->isMerchant() || $model->status == \common\models\Deal::STATUS_INCOMPLETE) {

                    $url = Url::toRoute(['/deal-gallery/create', 'deal_id' => $model->id, 'update_id' => 1]);
                    return Html::a('<span class="glyphicon glyphicon-camera" style="font-size:20px;"></span>', $url, [
                                'title' => \Yii::t('yii', 'Deal Gallery'),
                                'data-pjax' => '0',
                    ]);
 }
                },
                'delete' => function ($url, $model, $key) {
 if (Yii::$app->user->isAdmin() || $model->status == \common\models\Deal::STATUS_INCOMPLETE) {

                    $url = Url::toRoute(['/deal/delete', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-trash" style="font-size:20px;"></span>', $url, [
                                'title' => \Yii::t('yii', 'Delete'),
                                'data-confirm' => \Yii::t('yii', 'Are you sure to delete this deal?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                    ]);
                }
                },
                'view' => function ($url, $model, $key) {
                    
 if (Yii::$app->user->isMerchant()) {
        
                    $url = Url::toRoute(['../deal/'.$model->slug, 'authhash' => $model->user->auth_key]);
                    return Html::a('<span class="glyphicon glyphicon-eye-open" style="font-size:20px;"></span>', $url, [
                                'title' => \Yii::t('yii', 'Deal View'),
                                'data-pjax' => '0',
                    ]);
 }
                },
            ],
            'template' => '{edit} {price} {address} {gallery} {view} '
        ]),
    ],
]);
?>
        </div>
    </div>
</div>
<div id="load_popup_modal_show_id" class="modal fade" tabindex="-1"></div>
<div id="load_popup_myrequest_show_id" class="modal fade" tabindex="-1"></div>




<?php
$this->registerJs("
var cancelPop = $('#load_popup_modal_show_id');
$('.sendrequest').on('click', function(){
requestId=$(this).val(); 
cancelPop.load('" . \yii\helpers\Url::toRoute('send-request') . "',{'id': requestId},
function(){
cancelPop.modal('show');
});
});

var myrequestPop = $('#load_popup_myrequest_show_id');
$('.myrequest').on('click', function(){
requestId=$(this).val(); 
cancelPop.load('" . \yii\helpers\Url::toRoute('myrequest') . "',{'id': requestId},
function(){
cancelPop.modal('show');
});
});

$('#date_range').daterangepicker(null, function(start, end, label) {});
      
");
?>