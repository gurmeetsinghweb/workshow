<?php

use yii\helpers\Html;
use common\components\GenXHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Deal */

$this->title = Yii::t('app', 'Create Business');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Deals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "List",
        "url" => Url::toRoute(['/deal/index']),
        "icon" => "fa fa-list",
    ]
]);
?>


<div class="deal-create panel">
    <div class="panel-body">
        <?php
        if (\Yii::$app->user->isAdmin() || Yii::$app->user->merchant()->company) {
            echo $this->render('_form', [
                'model' => $model
            ]);
        } else {
            echo "<h2>You have not created Company yet. " . Html::a("Create now", ['/company/my']) . "</h2>";
        }
        ?>
    </div>
</div>