    <?php
use yii\helpers\Url;
use yii\bootstrap\Html;
?>
  <div id="load_popup_modal_contant" class="" role="dialog">

  <div class="modal-dialog modal-md">
<?php
//\common\components\GenXHelper::c($cart);
//echo $id1 = $_POST["id1"];
//echo $id2 = $_POST["id2"];
?>
      <form  action="<?php echo Url::toRoute('deal/update-request') ?>" method="post">

        <?php
        $request = Yii::$app->getRequest();
        echo Html::hiddenInput($request->csrfParam, $request->getCsrfToken());
        ?>
    <!-- Start: Modal content-->
     <div class="modal-content cancel-request"> 
    <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
              &times;
            </span>
          </button>
          <h4 class="modal-title" id="myModalLabel">
            Deal Update/ Cancellation Request 
          </h4>
        </div>
         
    <div class="modal-body">
        <h3> <?=$deal->title; ?></h3>
          <div class="credit-card-div">
            <div class="panel panel-default">
              <div class="panel-heading">
                
                <!--/ row -->
                
                <!-- / row -->
                
                
                
                <div class="row">
                  <div class="col-xs-12">
                       <label>Submit your update or cancellation information</label>
                       <textarea class="col-xs-12" rows="5" placeholder="Submit valid information" name="remarks" required=""></textarea>
                  </div>
                </div>
                
                <div class="row ">
                  <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust green-btn-2">
                    <input type="submit" class="btn btn-danger btn-raised ripple-effect" value="Cancel"  data-dismiss="modal" aria-label="Close">
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust  green-btn">
                    <input type="submit" class="btn btn-success btn-raised ripple-effect btn-block" value="Submit">
                    <input type="hidden" value="<?= $deal->id; ?>" name="deal">
                  </div>
                </div>
                
              </div>
            </div>
          </div>
          
        </div>
     </div>
    </form>
    
  </div>
  </div>



