<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Deal */
/* @var $form yii\widgets\ActiveForm */
?>
<!-- Panel Wizard Form -->
<div class="panel">
    <div class="panel-body">
        <!-- Steps -->
        <div id="wizard" class="form_wizard wizard_horizontal">
            <ul class="wizard_steps">
                <li>
                    <a href="#step-1" class="selected">
                        <span class="step_no">1</span>
                        <span class="step_descr">
                            Step 1<br />
                            <small>Create Deal</small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-2">
                        <span class="step_no">2</span>
                        <span class="step_descr">
                            Step 2<br />
                            <small>Deal options</small>
                        </span>
                    </a>
                </li>
                 <li>
                    <a href="#step-3">
                        <span class="step_no">3</span>
                        <span class="step_descr">
                            Step 3<br />
                            <small>Deal Addresses</small>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#step-4">
                        <span class="step_no">4</span>
                        <span class="step_descr">
                            Step 4<br />
                            <small>Gallery for deal</small>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- End Steps -->

        <!-- Wizard Content -->
        <div class="wizard-content">
            <div class="wizard-pane active">
                <div class="deal-form">
                    <?php
                    $form = ActiveForm::begin([]);
                    ?>
                    <?php
                    if ($model->isNewRecord && \Yii::$app->user->isAdmin()) {
                        echo '<label class="control-label">Search Merchant<span id="deal-user_id-label"></span></label>';
                        echo yii\jui\AutoComplete::widget([
                            'clientOptions' => [
                                'source' => Url::toRoute(['/merchant/merchant-autocomplete']),
                                'select' => new yii\web\JsExpression('function( event, ui ) {
                            $("#deal-user_id").val(ui.item.id);
                            $("#deal-user_id-label").html(" - <b>"+ui.item.label+" #"+ui.item.id+"</b>");
                            if(ui.item.company_id > 0) {
                                $("#deal-company_id-search_box").hide();
                                $("#deal-company_id").val(ui.item.company_id);
                                $.get("' . Url::toRoute(['/company/view']) . '?id=" + ui.item.company_id, function(company) {
                                        if(company.id > 0) {
                                            $("#deal-address").val(company.address);
                                            document.getElementById("deal-address").dispatchEvent(new Event("change"));
                                            $("#deal-city_id").val(company.city_id);
                                        }
                                    });
                            } else {
                                $("#deal-company_id-search_box").show();
                            }
                          }'),
                            ],
                            'options' => ['class' => 'form-control']
                        ]);
                        echo $form->field($model, 'user_id')->hiddenInput()->label(False);

                         //echo '<span id="deal-company_id-search_box"><label class="control-label">Search Company<span id="deal-company_id-label"></span></label>';
                        /*echo yii\jui\AutoComplete::widget([
                            'clientOptions' => [
                                'source' => Url::toRoute(['/company/autocomplete']),
                                'select' => new yii\web\JsExpression('function( event, ui ) {
                            $("#deal-company_id").val(ui.item.id);
                            $("#deal-company_id-label").html(" - <b>"+ui.item.value+" #"+ui.item.id+"</b>");
                          }'),
                            ],
                            'options' => ['class' => 'form-control']
                        ]);
                        echo "</span>";
                        echo $form->field($model, 'company_id')->hiddenInput()->label(False);*/
                    }
                    ?>

                    <div class="row">
                        <div class="col-sm-8 col-xs-12">
                            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                        </div>

                        <div class="col-sm-4 col-xs-12">
  <?php   

                   //   common\components\GenXHelper::c((new common\models\Category)->getCategoryTreeForDropdown());
  
echo $form
        ->field($model, 'category_id[]')
        ->label(isset($label) ? $label : Null)
        ->dropDownList((new common\models\Category)->getCategoryTreeForDropdown(), ['prompt' => '', 'id' => 'category_id'],['options' =>
                    [                        
                      153 => ['selected' => true]
                    ]
          ]
)->label("Select Category");


$this->registerJs('
    $(function () {
    $("#category_id").select2({
        multiple: true,
        delay: 250,
        
    });
});
');
?>
     <?php
//\common\components\GenXHelper::c($model->dealCategory);
foreach ($model->dealCategory as $getcat)
{
    echo $getcat->category->name." - ";
    
}   
     ?>
                            
                            
                            <?php // $form->field($model, 'category_id')->dropDownList((new common\models\Category)->getTreeForDropdown(['blank' => False, 'level' => 2]), ['prompt' => 'Select Category'])->label("Select Category"); ?>
                        </div>

                    </div>
                    <?php
                    if(Yii::$app->user->isAdmin()){
                        ?>

                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <?php
                            if(isset($model->start_time)){
                                $startdate=$model->start_time;
                                $enddate=$model->expire_time;
                             
                            }
                            else{
                                $startdate=  time();
                                $enddate=  time()+ Yii::$app->params['deal_expire_time'];
                            }
                            echo '<label class="control-label">Start time</label>';
                            echo yii\jui\DatePicker::widget([
                                'clientOptions' => [
                                    'altField' => "#deal-start_time",
                                    'altFormat' => "@",
                                    
                                          
                                ],
                                'options' => ['class' => 'form-control'],
                                'value' =>  date('M d Y', ($startdate))
                            ]);
                            echo $form->field($model, 'start_time')->hiddenInput(['value'=> $startdate])->label(False);
                            ?>
                        </div>
                      <div class="col-sm-4 col-xs-12">
                            <?php
                            echo '<label class="control-label">Expire time</label>';
                            echo yii\jui\DatePicker::widget([
                                
                                'clientOptions' => [
                                    
                                    'altField' => "#deal-expire_time",
                                    'altFormat' => "@",
                                    'minDate'=> 0,     
                                ],
                                'options' => ['class' => 'form-control'],
                                'value' => date('M d Y', ($enddate))
                            ]);
                            echo $form->field($model, 'expire_time')->hiddenInput(['value'=> $enddate])->label(False);
                            ?>
                        </div>
                        

                        <div class="col-sm-4 col-xs-12">
                            <?= $this->render('@common/views/_partials/city', ['form' => $form, 'model' => $model, 'attribute' => 'city_id']); ?>
                        </div>
                    </div>
                    
                    <?php
                    }
                    if(Yii::$app->user->isAdmin()){
                        ?>
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <?= $form->field($model, 'deal_commission')->textInput(['maxlength' => true,'value' => ($model->deal_commission)  ?  $model->deal_commission : Yii::$app->params['deal_commission']]) ?>
                            
                        </div>

                        <div class="col-sm-4 col-xs-12">
                            <?= $form->field($model, 'deal_credit')->textInput(['maxlength' => true,'value' => ($model->deal_credit)  ?  $model->deal_credit : Yii::$app->params['deal_credit']]) ?>
                        </div>
                        

                        <div class="col-sm-4 col-xs-12">
                            
                                    <?= $form->field($model, 'slider')->checkBox(['label' => 'Show Top Deals']) ?>
                                 
                        </div>
			   
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <?= $form->field($model, 'coupon_expire')->textInput(['maxlength' => true,'value' => ($model->coupon_expire)  ?  $model->coupon_expire : 90]) ?>
                            
                        </div>

                        <div class="col-sm-4 col-xs-12">
                            
                                    <?= $form->field($model, 'shipping_applied')->checkBox(['label' => 'Apply Shipping']) ?>
                                 
                        </div>
                        
                    </div>
                        <?php
                    }
                    ?>

                  <!--  <div class="row">
                        <div class="col-lg-12">
                            <?php // $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                            <?php // $form->field($model, 'geo_lat')->hiddenInput()->label(False); ?>
                            <?php // $form->field($model, 'geo_long')->hiddenInput()->label(False); ?>
                            <label class="control-label">Select your Location</label>
                            <?php 
                            //$this->render('@common/views/_partials/gmap_geolocation', [
                             //   'form' => $form,
                              //  'model' => $model,
                               // 'options' => [
                                //    'address_input_id' => 'deal-address',
                                 //   'lat_input_id' => 'deal-geo_lat',
                                  //  'long_input_id' => 'deal-geo_long',
                                //]
                            //]);
                            ?>
                        </div>
                    </div>-->

                    <br />
                    <div class="row">
                        <div class="col-lg-12">
                            <?= $this->render('@common/views/_partials/text_editor', ['form' => $form, 'model' => $model, 'attribute' => 'description','label'=>"Business Description"]); ?>
                        </div>
                        <div class="col-lg-12">
                            <?= $this->render('@common/views/_partials/text_editor', ['form' => $form, 'model' => $model, 'attribute' => 'terms','label'=>"Services or About More Business"]); ?>
                        </div>
                           <div class="col-lg-12">
                            <?= $this->render('@common/views/_partials/text_editor', ['form' => $form, 'model' => $model, 'attribute' => 'how_redeemed','label'=>"Any Terms and Condition"]); ?>
                        </div>
                    </div>
                    
                    
                                            <div class="col-lg-12">
                                              <?= $form->field($model, 'meta_title')->textInput(['maxlength' => false,'value' => ($model->meta_title)  ?  $model->meta_title : '' ]) ?>
                        </div>

                                  <div class="col-lg-12">
                                     <?= $form->field($model, 'meta_description')->textarea(['maxlength' => false,'value' => ($model->meta_description)  ?  $model->meta_description : '' ]) ?>
                        </div>

                                <div class="col-lg-12">                                                         <?= $form->field($model, 'meta_keyword')->textarea(['maxlength' => false,'value' => ($model->meta_keyword)  ?  $model->meta_keyword : '' ]) ?>
                             </div>


                    <div class="form-group pull-right">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Next - Add Options') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                    <?php
                            if(isset($_REQUEST['update_id'])){?>
                            <input type="hidden" name="update_id" value="1">
                            <?php }?>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
        <!-- End Wizard Content -->

    </div>
</div>
<!-- End Panel Wizard One Form -->
<?php
$this->registerJs("$(document).on('pjax:complete', function(data, status, xhr, options) {
  console.log(data);
  console.log(status);
  console.log(xhr);
  console.log(options);
})");
