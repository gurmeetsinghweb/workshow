<?php

use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = (($title) ? $title : 'Requests');
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
/* GenXHelper::contentHeaderButtons($this, [
  [
  "title" => "New",
  "url" => Url::toRoute(['/request/create']),
  "icon" => "wb-plus",
  ],
  [
  "title" => "Refresh",
  "url" => Url::toRoute(['/request/index']),
  "icon" => "wb-refresh",
  ],
  ]); */
?>
<div id="load_popup_myrequest_show_id" class="" role="dialog">

    <div class="modal-dialog modal-md">
        <div class="request-index panel">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        
<tr><th>From</th><th>Remarks</th><th>Request Date</th><th>Update Date</th><th>Status</th></tr>
<?php
$color="#";
foreach ($dataProvider as $request)
    {
    $status=$request->status;
    if($status == 10){
        $vals="Pending for approval";
        $color="#cccccc";
    }
    else if($status==5){
        $vals="Approved";
        $color="#baffc9";
    }
    else if($status==2){
        $vals="Rejected";
        $color="#ffafaf";
    }
?>
<tr bgcolor='<?= $color?>' ><td><?= $request->from->name ?></td><td><?= $request->remarks; ?></td><td><?= date('d M Y', $request->created_at)?></td><td><?= date('d M Y', $request->updated_at)?></td><td><?=  $vals;?></td></tr>
<?php
}
?>
</tbody></table>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                   
                </div>
            </div>
        </div>
    </div>
</div>