<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\components\GenXHelper;
use common\components\GenxGridHelper;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Deal */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Deals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "Edit",
        "url" => Url::toRoute(['/deal/update', 'id' => $model->id]),
        "icon" => "fa fa-edit",
    ],
    [
        "title" => "Delete",
        'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this Deal?'),
            'method' => 'post',
        ],
        "url" => Url::toRoute(['/deal/delete', 'id' => $model->id]),
        "icon" => "fa fa-trash",
    ],
    [
        "title" => "List",
        "url" => Url::toRoute(['/deal/index']),
        "icon" => "fa fa-list",
    ]
]);
?>
<div class="deal-view panel">
    <div class="panel-body">
      <!--   <h1><?= Html::encode($this->title) ?></h1> -->
        <p>
        </p>
        <h3>Deal Details</h3>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td><b>Category</b>: <?php // $model->mainCategory->name; ?> : <?php // ($model->sub_category_id) ? $model->subCategory->name : "" ?> : <?php // ($model->sub_sub_category_id) ? $model->subSubCategory->name : "" ?></td>
                    <td><b>Deal Start</b> : <?= GenXHelper::getDateFormat($model->start_time); ?>
                    </td>
                    <td><b>Deal Expire</b> : <?= GenXHelper::getDateFormat($model->expire_time); ?>
                    </td>
                </tr>
                <tr><td colspan="3"><b>Description:</b><br><?= $model->description; ?>
                    </td>
                </tr>
                <tr><td colspan="3"><b>Fine Prints </b><br><?= $model->terms; ?>
                    </td>
                </tr>
                
                    
            </thead>
        </table>  
        
        <h3>Options</h3>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>List Price</th>
                    <th>Selling Price</th>
                    <th>Total Discount</th>
                    
                    <th>Number of Persons Allow</th>
                    <th>Total Sold</th>
                    <th>Total Items</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($model->dealOptions as $option) {
                    ?>
                    <tr>
                        <td><?= $option->name; ?></td>
                        <td><?= $option->list_price; ?></td>
                        <td><?= $option->selling_price; ?></td>
                        <td><?= $option->discount." %"; ?></td>
                        
                        <td><?= $option->no_per_person; ?></td>
                        <td><?= (int) $option->total_sold; ?></td>
                        <td><?php $total=(int) $option->total_items; echo ($total) ? $total : "Unlimted"; ?></td>
                        <!--<td class="text-nowrap">
                            <a href="<?= Url::toRoute(['/deal-option/update', 'id' => $option->id]) ?>">
                                <button type="button" class="btn btn-sm btn-icon btn-flat btn-default" data-toggle="tooltip"
                                        data-original-title="Edit">
                                    <i class="icon fa fa-wrench" aria-hidden="true"></i>
                                </button>
                            </a>
                        </td>-->
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        
        <h3>Gallery</h3>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Description</th>
                  <!--  <th class="text-nowrap">Action</th> -->
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($model->dealGalleries as $gallery) {
                    ?>
                    <tr>
                        <td><?= Html::img($gallery->getIconURL('small')); ?></td>
                        <td><?= $gallery->description; ?></td>
                        <!--<td class="text-nowrap">
                            <a href="<?= Url::toRoute(['/deal-gallery/update', 'id' => $option->id]) ?>">
                                <button type="button" class="btn btn-sm btn-icon btn-flat btn-default" data-toggle="tooltip"
                                        data-original-title="Edit">
                                    <i class="icon fa fa-wrench" aria-hidden="true"></i>
                                </button>
                            </a>
                        </td>-->
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        
              <h3>Deal Addresses</h3>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Address</th>
                     <th>longitude</th>
                     <th>Latitude</th>
                     
                 
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($model->dealAddress as $address) {
                    ?>
                    <tr>
                        <td><?= $address->name ?></td>
                        <td><?= $address->address; ?></td>
                         <td><?= $address->geo_long; ?></td>
                          <td><?= $address->geo_lat; ?></td>
                       
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
