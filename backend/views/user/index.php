<?php

use yii\helpers\Html;
use \yii\helpers\Url;
use yii\grid\GridView;
use common\models\User;
use \common\components\GenXHelper;
use common\components\GenxGridHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
GenXHelper::contentHeader($this);
GenXHelper::contentHeaderButtons($this, [
    [
        "title" => "New",
        "url" => Url::toRoute(['/user/create']),
        "icon" => "fa fa-plus",
    ],
    [
        "title" => "Refresh",
        "url" => Url::toRoute(['/user']),
        "icon" => "fa fa-refresh",
    ],
]);
?>
<div class="panel">
    <div class="user-index panel-body">
        <div style="overflow-x:auto;">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                GenxGridHelper::linkedID(),
                GenxGridHelper::linkedTitle('name'),
                'username',
//            'auth_key',
//            'sms_otp',
//            'password_hash',
                // 'password_reset_token',
                'email:email',
                [
                    'attribute' => 'type',
                    'filter' => (new User)->getUserTypeDropdownForForm()
                ],
//            [
//                'attribute' => 'country_id',
//                'value' => function($model) {
//                    return $model->country->name;
//                },
//                'filter' => \yii\helpers\ArrayHelper::map(common\models\Country::find()->asArray()->all(), 'id', 'name')
//            ],
                // 'mobile',
                [
                    'label' => 'Joining Date',
                    'format' => 'date',
                    'attribute' => 'created_at',
                    'filter' => False,
                ],
                [
                    'attribute' => 'status',
                    'value' => function($model) {
                        $array = [
                            User::STATUS_ACTIVE => "Active",
                            User::STATUS_PENDING_VALIDATION => "Not Validated",
                        ];
                        return $array[$model->status];
                    },
                            'filter' => [
                                User::STATUS_ACTIVE => "Active",
                                User::STATUS_PENDING_VALIDATION => "Not Validated",
                            ]
                        ],
                        // 'slug',
                        // 'image_url:url',
                        // 'nationality',
                        // 'dob',
                        // 'yob',
                        // 'hint_question',
                        // 'hint_answer',
                        // 'agreed',
                        // 'created_at',
                        // 'updated_at',
                        GenxGridHelper::ActionColumn()
                    ],
                ]);
                ?>
        </div>
    </div>
</div>