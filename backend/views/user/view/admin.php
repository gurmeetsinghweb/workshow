<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


use yii\widgets\DetailView;
?>
<div class="col-md-9">
    <!-- Panel -->
    <div class="panel">
        <div class="panel-body">
            <ul class="nav nav-tabs nav-tabs-line" data-plugin="nav-tabs" role="tablist">
                <li class="active" role="presentation">
                    <a data-toggle="tab" href="#profile" aria-controls="profile" role="tab">
                        User Profile
                    </a>
                </li>
                <li role="presentation">
                    <a data-toggle="tab" href="#account" aria-controls="account" role="tab">
                        Account
                    </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="profile" role="tabpanel">
                    <?=
                    DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'username',
                            'email:email',
                            'mobile',
                            'status',
                            'type',
                            'name',
                            'slug',
                            [
                                'attribute' => 'city_id',
                                'format' => 'raw',
//                                'value'=> yii\helpers\Html::a($model->city->name, ['/city/view', 'id' => $model->city->id])
                                'value' => ($model->city_id) ? $model->city->name : "",
                            ],
                        ],
                    ])
                    ?>
                </div>

                <div class="tab-pane" id="account" role="tabpanel">

                </div>
            </div>
        </div>
    </div>
    <!-- End Panel -->
</div>