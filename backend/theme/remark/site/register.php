<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';
$this->registerCssFile(Yii::$app->request->baseUrl . "/css/pages/register.css", ['depends' => 'backend\assets\ReMarkAssets']);
\common\components\GenXHelper::setPageVar('body_class', 'page-register layout-full');
?>
<p>Sign up as Merchant to find interesting thing.</p>
<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'name')->label(False)->textInput(['placeholder' => $model->getAttributeLabel('name')]); ?>
<?= $form->field($model, 'username')->label(False)->textInput(['placeholder' => $model->getAttributeLabel('username')]); ?>
<?= $form->field($model, 'email')->label(False)->textInput(['placeholder' => $model->getAttributeLabel('email')]); ?>
<?= $form->field($model, 'password')->label(False)->passwordInput(['placeholder' => $model->getAttributeLabel('password')]); ?>
<?= $form->field($model, 'password_repeat')->label(False)->passwordInput(['placeholder' => $model->getAttributeLabel('password_repeat')]); ?>
<?=
$form->field($model, 'verificationCode')->widget(yii\captcha\Captcha::className(), [
    'template' => $this->render("@common/views/_partials/captcha_template"),
])->label(False);
?>
<?= $form->field($model, 'agreed')->checkbox()->label("I Agree all terms and conditions " . Html::a("Terms", "#") . ""); ?>
<?= Html::submitButton("Register", ['class' => 'btn btn-primary btn-block']); ?>
<?php ActiveForm::end(); ?>
<p>Have account? Please go to <?= Html::a("Login", ['/site/login']); ?></p>