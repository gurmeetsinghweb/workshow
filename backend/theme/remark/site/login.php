<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';
$this->registerCssFile(Yii::$app->request->baseUrl . "/css/pages/login.css", ['depends' => 'backend\assets\ReMarkAssets']);
\common\components\GenXHelper::setPageVar('body_class', "page-login layout-full");
?>

<p>Sign into your account</p>
<?php $form = ActiveForm::begin(); ?>

<div class="form-group">
    <label class="sr-only" for="inputEmail">Email</label>
    <?=
    $form->field($model, 'email')->label(False)->textInput(['placeholder' => $model->getAttributeLabel('email')]);
    ?>
</div>
<div class="form-group">
    <label class="sr-only" for="inputPassword">Password</label>
    <?=
    $form->field($model, 'password')->label(False)->passwordInput(['placeholder' => $model->getAttributeLabel('password')]);
    ?>
</div>
<div class="form-group clearfix">
    <div class="checkbox-custom checkbox-inline pull-left">
        <input type="checkbox" id="inputCheckbox" name="LoginForm[rememberMe]" value=1>
        <label for="inputCheckbox">Remember me</label>
    </div>
    <?= Html::a("Forget password", ['/site/request-password-reset'], ['class' => "pull-right"]); ?>
</div>
<button type="submit" class="btn btn-primary btn-block">Sign in</button>
<?php ActiveForm::end(); ?>
<p>Still no account? Please go to <?= Html::a("Register", ['/site/register']); ?></p>

