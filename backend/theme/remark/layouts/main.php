<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

backend\assets\AppAsset::register($this);
backend\assets\ReMarkBreakpoint::register($this);
backend\assets\ReMarkAssets::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/remark-admin/assets');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <script>
            Breakpoints();
        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <?php $this->beginBody() ?>
        <?php
        if (Yii::$app->user->canAccessAdmin()) {
            echo $this->render('header.php', ['directoryAsset' => $directoryAsset]);
            echo $this->render('left.php', ['directoryAsset' => $directoryAsset]);
        }
        ?>
        <?= $this->render('content.php', ['content' => $content, 'directoryAsset' => $directoryAsset]) ?>
        <footer class="site-footer">
            <span class="site-footer-legal">© <?= date("Y", time()) . " " . Yii::$app->name; ?> </span>
        </footer>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
