<?php

use yii\helpers\Html;
use dmstr\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

\backend\assets\ReMarkAssets::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="<?= (\common\components\GenXHelper::getPageVar('body_class')) ? \common\components\GenXHelper::getPageVar('body_class') : "page-login layout-full"; ?>">
        <?php $this->beginBody() ?>
        <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
            <div class="page-content vertical-align-middle">
                <div class="brand">
                    <a href="<?= yii\helpers\Url::toRoute(["/"]); ?>">
                        <img class="brand-img" src="<?= \yii\helpers\Url::toRoute('../themes/main/images/logo.png'); ?>" width="200px">
                    </a>
                </div>
                <?= Alert::widget() ?>
                <?= $content ?>
                <footer class="page-copyright">
                    <p>© <?= date("Y"); ?>. All RIGHT RESERVED.</p>
                </footer>
            </div>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
