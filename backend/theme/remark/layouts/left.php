<?php

use \common\models\User;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */
$current_route = "/" . trim(\Yii::$app->controller->getRoute());
?>
<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>
                <?php
                $side_menu = [
                    [
                        'label' => 'Dashboard',
                        'icon' => 'wb-dashboard',
                        'url' => ['/site/index'],
                        'active' => (strstr($current_route, 'site/') ? "active open" : ""),
                        'visible' => Yii::$app->user->canAccessAdmin(),
                    ],
                    [
                        'label' => 'Company Profile',
                        'icon' => 'glyphicon glyphicon-copyright-mark',
                        'url' => ['/company/my'],
                        'active' => (strstr($current_route, 'company/') ? "active open" : ""),
                        'visible' => Yii::$app->user->isOnlyMerchant(),
                    ],
     
                    // Marchent Account 
                                   [
                        'label' => 'Merchant Account',
                        'icon' => 'wb-tag',
                        'url' => 'javascript:void(0)',
                        'visible' => Yii::$app->user->canAccessAdmin(),
                        'active' => ((strstr($current_route, 'merchant/') || strstr($current_route, 'deal/')) ? "active open" : ""),
                        'data-slug' => "",
                        'items' => [
                            [
                                "label" => "Business Add",
                                 'icon' => 'wb-tag',
                                 'url' => 'javascript:void(0)',
                                //"url" => ["/deal/create"],
                                'visible'=>  Yii::$app->user->canAccessAdmin(),
                                //'icon' => 'wb-plus',
                                'data-slug' => "",
                                'active' => (strstr($current_route, 'deal/') ? "active open" : ""),
                                
                                'itemsub'=>[
                                   [
                                       "label" => "New Business",
                                    "url" => ["/deal/create"],
                                    'icon' => '',
                                    'data-slug' => "", 
                                     
                                   ],
                                    [
                                        "label" => "View All",
                                        "url" => ["/deal/index"],
                                        'icon' => '',
                                        'data-slug' => "",
                                    ]
                                     
                                ],
                                
                                
                            ],
                            [
                                "label" => "Marchent List",
                                "url" => ["/merchant/index"],
                                'icon' => 'wb-list',
                                'data-slug' => "",
                                'visible'=>  Yii::$app->user->isAdmin(),
                            ],
                        ],
                    ],
                    
                    
                    
                    
                    
                    
                    // User Details
                    [
                        'label' => 'Users',
                        'icon' => 'wb-users',
                        'url' => 'javascript:void(0)',
                        'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                        'active' => (strstr($current_route, 'user/') ? "active open" : ""),
                        'data-slug' => "",
                        'items' => [
                            [
                                "label" => "New",
                                "url" => ["/user/create"],
                                'icon' => 'wb-plus',
                                'data-slug' => "",
                            ],
                            [
                                "label" => "View All",
                                "url" => ["/user/index"],
                                'icon' => 'wb-list',
                                'data-slug' => "",
                            ],
                        ],
                    ],
                    [
                        'label' => 'Companies',
                        'icon' => 'wb-users',
                        'url' => 'javascript:void(0)',
                        'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                        'active' => (strstr($current_route, 'company/') ? "active open" : ""),
                        'data-slug' => "",
                        'items' => [
                            [
                                "label" => "New",
                                "url" => ["/company/create"],
                                'icon' => 'wb-plus',
                                'data-slug' => "",
                            ],
                            [
                                "label" => "View All",
                                "url" => ["/company/index"],
                                'icon' => 'wb-list',
                                'data-slug' => "",
                            ],
                        ],
                    ],
                    [
                        'label' => 'Categories',
                        'icon' => 'wb-tag',
                        'url' => 'javascript:void(0)',
                        'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                        'active' => (strstr($current_route, 'category/') ? "active open" : ""),
                        'data-slug' => "",
                        'items' => [
                            [
                                "label" => "New",
                                "url" => ["/category/create"],
                                'icon' => 'wb-plus',
                                'data-slug' => "",
                            ],
                            [
                                "label" => "View All",
                                "url" => ["/category/index"],
                                'icon' => 'wb-list',
                                'data-slug' => "",
                            ],
                            [
                                "label" => "View Tree",
                                "url" => ["/category/tree"],
                                'icon' => 'glyphicon glyphicon-list-alt',
                                'data-slug' => "",
                            ],
                        ],
                    ],
                    
                        // enquiry
                        [
                        'label' => 'Enquiry',
                        'icon' => 'wb-tag',
                        'url' => 'javascript:void(0)',
                        'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                        'active' => (strstr($current_route, 'enquiry/') ? "active open" : ""),
                        'data-slug' => "",
                        'items' => [
                            [
                                "label" => "New",
                                "url" => ["/category/create"],
                                'icon' => 'wb-plus',
                                'data-slug' => "",
                            ],
                            [
                                "label" => "View All",
                                "url" => ["/category/index"],
                                'icon' => 'wb-list',
                                'data-slug' => "",
                            ],
                            [
                                "label" => "View Tree",
                                "url" => ["/category/tree"],
                                'icon' => 'glyphicon glyphicon-list-alt',
                                'data-slug' => "",
                            ],
                        ],
                    ],
                    
                    
                    
                    // Country State City
                    [
                        'label' => 'Country/State/City',
                        'icon' => 'glyphicon glyphicon-globe',
                        'url' => 'javascript:void(0)',
                        'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                        'active' => (strstr($current_route, 'country/') ? "active open" : ""),
                        'data-slug' => "",
                        'items' => [
                            [
                                "label" => "Country",
                                "url" => ["/country/create"],
                                'icon' => 'glyphicon glyphicon-globe',
                                'data-slug' => "",
                                'url' => 'javascript:void(0)',
                                'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                                'active' => (strstr($current_route, 'country/') ? "active open" : ""),
                                
                                'itemsub' => [
                                [
                                    "label" => "New",
                                    "url" => ["/country/create"],
                                    'icon' => 'wb-plus',
                                    'data-slug' => "",
                                ],
                                [
                                    "label" => "View All",
                                    "url" => ["/country/index"],
                                    'icon' => 'wb-list',
                                    'data-slug' => "",
                                ],
                                
                                
                            ],
                         ],       
                        [
                        'label' => 'States',
                        'icon' => 'glyphicon glyphicon-globe',
                        'url' => 'javascript:void(0)',
                        'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                        'active' => (strstr($current_route, 'state/') ? "active open" : ""),
                        'data-slug' => "",
                        'itemsub' => [
                            [
                                "label" => "New",
                                "url" => ["/state/create"],
                                'icon' => 'wb-plus',
                                'data-slug' => "",
                            ],
                            [
                                "label" => "View All",
                                "url" => ["/state/index"],
                                'icon' => 'wb-list',
                                'data-slug' => "",
                            ],
                        ],
                    ],
                    [
                        'label' => 'Cities',
                        'icon' => 'glyphicon glyphicon-globe',
                        'url' => 'javascript:void(0)',
                        'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                        'active' => (strstr($current_route, 'city/') ? "active open" : ""),
                        'data-slug' => "",
                        'itemsub' => [
                            [
                                "label" => "New",
                                "url" => ["/city/create"],
                                'icon' => 'wb-plus',
                                'data-slug' => "",
                            ],
                            [
                                "label" => "View All",
                                "url" => ["/city/index"],
                                'icon' => 'wb-list',
                                'data-slug' => "",
                            ],
                        ],
                    ],
                                
                                
                                
                                
                                
                        ],
                    ],
                    
                    
                    
                    [
                        'label' => 'Settings',
                        'icon' => 'glyphicon glyphicon-cog',
                        'url' => 'javascript:void(0)',
                        'visible' => Yii::$app->user->can(User::ROLE_SYSADMIN),
                        'active' => (strstr($current_route, 'site-settings/') ? "active open" : ""),
                        'data-slug' => "",
                        'items' => [
                            [
                                "label" => "New",
                                "url" => ["/site-settings/create"],
                                'icon' => 'wb-plus',
                                'data-slug' => "",
                            ],
                            [
                                "label" => "View All",
                                "url" => ["/site-settings/index"],
                                'icon' => 'wb-list',
                                'data-slug' => "",
                            ]
                        ],
                    ],
                ];
                ?>
                <ul class="site-menu">
                    <li class="site-menu-category">
                        <?= Yii::t("app", "Welcome ") . Yii::$app->user->getName() ?>
                    </li>
                    <?php
                    foreach ($side_menu as $menu_item) {
                        if (!isset($menu_item['visible']) || !$menu_item['visible']) {
                            continue;
                        }
                        if (!isset($menu_item['active'])) {
                            $menu_item['active'] = (($current_route == $menu_item['url']) ? "active" : "");
                        }
                        if (!isset($menu_item['data-slug']) || !$menu_item['data-slug']) {
                            $menu_item['data-slug'] = '';
                        }
                        
                        ?>
                        <li class="site-menu-item <?= ((isset($menu_item['items'])) ? " has-sub  " : ""); ?> <?= $menu_item['active'] ?>">
                            <a href="<?= ((isset($menu_item['items'])) ? $menu_item['url'] : Url::toRoute($menu_item['url'])); ?>" data-slug="<?= $menu_item['data-slug'] ?>">
                                <i class="site-menu-icon <?= $menu_item['icon'] ?>" aria-hidden="true"></i>
                                <span class="site-menu-title"><?= $menu_item['label'] ?></span>
                                <?php
                                if (isset($menu_item['items'])) {
                                    ?>
                                    <span class="site-menu-arrow"></span>
                                    <?php
                                }
                                ?>
                            </a>
                            <?php
                            if (isset($menu_item['items'])) {
                                echo '<ul class="site-menu-sub">';
                                foreach ($menu_item['items'] as $sub_menu_item) {
                                    $sub_menu_item['visible'] = ((isset($sub_menu_item['visible'])) ? $sub_menu_item['visible'] : $menu_item['visible']);
                                    if (!$sub_menu_item['visible']) {
                                        continue;
                                    }
                                    if (!isset($sub_menu_item['active'])) {
                                        $sub_menu_item['active'] = (($current_route == $sub_menu_item['url'][0]) ? "active" : "");
                                    }
                                    ?>
                             <li class="site-menu-item <?= ((isset($sub_menu_item['itemsub'])) ? " has-sub  " : ""); ?> <?= $sub_menu_item['active'] ?>">   
                            
                                    <a href="<?= ((isset($sub_menu_item['itemsub'])) ? $sub_menu_item['url'] : Url::toRoute($sub_menu_item['url'])); ?>" data-slug="<?= $sub_menu_item['data-slug'] ?>">
                                       <i class="site-menu-icon <?= $sub_menu_item['icon'] ?>" aria-hidden="true"></i>
                                       <span class="site-menu-title"><?= $sub_menu_item['label'] ?></span>
                                       <?php
                                       if (isset($sub_menu_item['itemsub'])) {
                                           ?>
                                           <span class="site-menu-arrow"></span>
                                           <?php
                                       }
                                       ?>
                                   </a>
                                   <?php
                                    if (isset($sub_menu_item['itemsub'])) {
                                        echo '<ul class="site-menu-sub">';
                                        foreach ($sub_menu_item['itemsub'] as $sub_menu_items) {
                                            //$sub_menu_item['visible'] = ((isset($sub_menu_item['visible'])) ? $sub_menu_item['visible'] : $menu_item['visible']);
                                            //$sub_menu_items['visible'] = ((isset($sub_menu_items['visible'])) ? $sub_menu_items['visible'] : $sub_menu_item['visible']);
                                            //if (!$sub_menu_items['visible']) {
                                            //    continue;
                                            //}
                                            if (!isset($sub_menu_items['active'])) {
                                                $sub_menu_items['active'] = (($current_route == $sub_menu_items['url'][0]) ? "active" : "");
                                            }
                                            ?>
                                           
                                    <li class="site-menu-item <?= $sub_menu_items['active'] ?>">
                                   
                                    <a class="animsition-link" href="<?= Url::toRoute($sub_menu_items['url']); ?>" data-slug="<?= $sub_menu_items['data-slug'] ?>">
                                        <i class="site-menu-icon <?= $sub_menu_items['icon'] ?>" aria-hidden="true"></i>
                                        <span class="site-menu-title"><?= $sub_menu_items['label'] ?></span>
                                    </a>
                                    </li>
                                    <?php
                                        }
                                         echo "</ul>";
                                      
                                    }  
                                     ?>
                                    
                                    
                      
                                    
                                    
                                </li>
                                <?php
                            }
                            echo '</ul>';
                        }
                        ?>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="site-menubar-footer">
        <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip"
           data-original-title="Settings">
            <span class="icon wb-settings" aria-hidden="true"></span>
        </a>
        <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock">
            <span class="icon wb-eye-close" aria-hidden="true"></span>
        </a>
        <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
            <span class="icon wb-power" aria-hidden="true"></span>
        </a>
    </div>
</div>
<div class="site-gridmenu">
    <ul>
        <li>
            <a href="apps/mailbox/mailbox.html">
                <i class="icon wb-envelope"></i>
                <span>Mailbox</span>
            </a>
        </li>
        <li>
            <a href="apps/calendar/calendar.html">
                <i class="icon wb-calendar"></i>
                <span>Calendar</span>
            </a>
        </li>
        <li>
            <a href="apps/contacts/contacts.html">
                <i class="icon wb-user"></i>
                <span>Contacts</span>
            </a>
        </li>
        <li>
            <a href="apps/media/overview.html">
                <i class="icon wb-camera"></i>
                <span>Media</span>
            </a>
        </li>
        <li>
            <a href="apps/documents/categories.html">
                <i class="icon wb-order"></i>
                <span>Documents</span>
            </a>
        </li>
        <li>
            <a href="apps/projects/projects.html">
                <i class="icon wb-image"></i>
                <span>Project</span>
            </a>
        </li>
        <li>
            <a href="apps/forum/forum.html">
                <i class="icon wb-chat-group"></i>
                <span>Forum</span>
            </a>
        </li>
        <li>
            <a href="index.html">
                <i class="icon wb-dashboard"></i>
                <span>Dashboard</span>
            </a>
        </li>
    </ul>
</div>