<?php

use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
?>
<div class="page">

    <?php
    if ($this->context->show_page_header === True) {
        ?>

        <div class="page-header">
            <?php if (isset($this->blocks['content-header'])) { ?>
                <h1 class="page-title"><?= $this->blocks['content-header'] ?></h1>
            <?php } else { ?>
                <h1>
                    <?php
                    if ($this->title !== null) {
                        //echo \yii\helpers\Html::encode($this->title);
                    } else {
                        echo \yii\helpers\Inflector::camel2words(
                                \yii\helpers\Inflector::id2camel($this->context->module->id)
                        );
                        echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                    }
                    ?>
                </h1>
            <?php } ?>

            <?=
            Breadcrumbs::widget(
                    [
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]
            );

            if (isset($this->blocks['content-header-buttons'])) {
                echo $this->blocks['content-header-buttons'];
            }
            ?>
        </div>
    <?php } ?>
    <?= Alert::widget() ?>
    <div class="page-content">
        <?= $content ?>
    </div>
</div>