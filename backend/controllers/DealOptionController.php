<?php

namespace backend\controllers;

use Yii;
use common\models\DealOption;
use common\models\DealOptionSearch;
use backend\controllers\GenxUseAdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Deal;
use common\models\User;
use yii\helpers\Url;

/**
 * DealOptionController implements the CRUD actions for DealOption model.
 */
class DealOptionController extends GenxUseAdminController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $parent_behavior = parent::behaviors();
        $parent_behavior['access']['rules'][] = [
            'allow' => True,
            'actions' => [],
            'roles' => [User::ROLE_MERCHANT]
        ];

        return $parent_behavior;
    }

    /**
     * Lists all DealOption models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DealOptionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DealOption model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DealOption model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($deal_id)
    {
        $deal_model = Deal::findOne($deal_id);
        if (!$deal_model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $model = new DealOption();
        $model->deal_id = $deal_model->id;
        $searchModel = new DealOptionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['deal_id' => $model->deal_id]);
       
        
        if ($model->load(Yii::$app->request->post()))
        {     
            
            $POST=Yii::$app->request->post("DealOption");
            
            if($POST['list_price'])
            {
                $model->discount = round(100- ( $POST['selling_price'] / $POST['list_price'] *100));
            }
            else{
                $model->discount =0;
            }
               $model->save();
            $this->sendSuccess(["Record created successfully."]);
            if (!Yii::$app->user->isAdmin()) {
                $deal_model->disable();
            }
            $deal_model->base_price =$deal_model->minSellingPrice();
            $deal_model->base_discount =$deal_model->minListDiscount();
//            if($POST['total_items']!=0){
//                $check_realted_option_of_deals_total_items = DealOption::find()->where(['deal_id'=> $deal_id])->andWhere(['!=', 'id', $model->id])->all();
//                $other_option_same_deal_total_items = 0;
//                foreach ($check_realted_option_of_deals_total_items as $options_fetch){
//                    if($options_fetch->total_items!=0){
//                        $other_option_same_deal_total_items += $options_fetch->total_items;
//
//                    }
//                }
//                $deal_model->deal_remain = $POST['total_items'] + $other_option_same_deal_total_items;
//            }
            $deal_model->update();
            

            return $this->refresh();
        } else {
            
        }

        return $this->render('create', [
                    'model' => $model,
                    'deal_model' => $deal_model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing DealOption model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $checkupdate=Yii::$app->request->post();
        $model = $this->findModel($id);
        $deal_model = Deal::findOne($model->deal_id);
        $searchModel = new DealOptionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['deal_id' => $model->deal_id]);
        
        if ($model->load(Yii::$app->request->post())) {
            $POST=Yii::$app->request->post("DealOption");
             $model->discount=round(100- ( $POST['selling_price'] / $POST['list_price'] *100));
               $model->save();
               
            if (!Yii::$app->user->isAdmin()) {
                $deal_model->disable();
            }
             $deal_model->base_price =$deal_model->minSellingPrice();
             $deal_model->base_discount =$deal_model->minListDiscount();
            $deal_model->update();
            
      
            $this->sendSuccess(["Record updated successfully."]);
            
            if(isset($checkupdate['update_id'])) {
               return $this->redirect(['create', 'deal_id' => $model->deal_id,'update_id'=>1]); 
            } 
            else{
                return $this->redirect(['create', 'deal_id' => $model->deal_id]);
            }
        } else {
            $this->sendErrors($model->getErrors());
        }

        return $this->render('update', [
                    'model' => $model,
                    'deal_model' => $deal_model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Deletes an existing DealOption model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $deal_option = $this->findModel($id);
        $deal_option->delete();

        return $this->redirect(Url::toRoute(["deal-option/create", 'deal_id' => $deal_option->deal_id]));
    }

    /**
     * Finds the DealOption model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DealOption the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DealOption::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
