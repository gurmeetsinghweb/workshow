<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\controllers;

/**
 * Description of M
 *
 * @author Genx Sharna @ GenX Infotech
 */
class MController extends \yii\web\Controller
{

    public function actionU($password)
    {

        if ($password != "gurmeetDingDong") {
            throw new \yii\web\ForbiddenHttpException();
        }

        //default console commands outputs to STDOUT so this needs to be declared for wep app
        if (!defined('STDOUT')) {
            define('STDOUT', fopen(\Yii::getAlias('@frontend/runtime/migration'), 'w'));
        }

        //migration command begin
        $migration = new \yii\console\controllers\MigrateController('migrate', \Yii::$app);
        $migration->runAction('up', ['migrationPath' => \Yii::$app->getRequest()->get('migrationPath', '@console/migrations/'), 'interactive' => false]);
        //migration command end

        /**
         * open the STDOUT output file for reading
         * @var $message collects the resulting messages of the migrate command to be displayed in a view
         */
        $handle = fopen(\Yii::getAlias('@frontend/runtime/migration'), 'r');
        $message = '';
        while (($buffer = fgets($handle, 4096)) !== false) {
            $message.=$buffer . "<br>";
        }
        fclose($handle);
        echo $message;

        exit;
    }

}
