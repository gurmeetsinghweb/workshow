<?php

namespace backend\controllers;

use Yii;
use common\models\Deal;
use common\models\DealSearch;
use common\models\DealGallery;
use common\models\DealOption;
use backend\controllers\GenxUseAdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Model;
use common\models\User;

/**
 * DealController implements the CRUD actions for Deal model.
 */
class DealController extends GenxUseAdminController {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        $parent_behavior = parent::behaviors();
        $parent_behavior['access']['rules'][] = [
            'allow' => True,
            'actions' => [],
            'roles' => [User::ROLE_MERCHANT]
        ];
        $parent_behavior['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'delete' => ['POST'],
                'complete' => ['POST'],
            ],
        ];

        return $parent_behavior;
    }

    /**
     * Lists all Deal models.
     * @return mixed
     */
    public function actionIndex($search_options = Null) {
        $searchModel = new DealSearch();
        $categoryModel = new \common\models\Category();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $end_time = time();
        $start_time = time() - (60 * 60 * 24 * 30);
        if (Yii::$app->request->get()) {
            $POST = Yii::$app->request->get();



            if (isset($POST['time_range'])) {
                $posted_time = explode('-', $POST['time_range']);

                $end_time = str_replace("+", " ", $posted_time[1]);
                $start_time = str_replace("+", " ", $posted_time[0]);
            }
            if (isset($POST['deal_category_id']) and ! empty($POST['deal_category_id'])) {
                // gurmeet 13/7/2017 remove or function ['or', ['deal.main_category_id' => $POST['deal_category_id']], ['deal.sub_category_id' => $POST['deal_category_id']], ['deal.sub_sub_category_id' => $POST['deal_category_id']]] and working deal category table
                $dataProvider->query->andWhere(['deal_category.category_id' => $POST['deal_category_id']]);
                // gurmeet 13/7/2017 join apply
                $dataProvider->query->joinWith('dealCategory');
                
            }


            if (isset($POST['date_range']) and ! empty($POST['date_range'])) {

                $posted_date = explode('-', $POST['date_range']);
                $end_time = str_replace("+", " ", $posted_date[1]);
                $start_time = str_replace("+", " ", $posted_date[0]);
                $end_time = (int) strtotime($end_time) + (60 * 60 * 24);
                $start_time = (int) strtotime($start_time);


                $dataProvider->query->andWhere(['between', 'deal.created_at', $start_time, $end_time]);


                //$dataProvider->query->andWhere(['between', 'cart.created_at', $start_time, $end_time]);
                $where['andWhere'][] = ['between', 'cart.created_at', $start_time, $end_time]
                ;

                $POST['date_range'] = urldecode($posted_date[0]) . "-" . urldecode($posted_date[1]);
            } else {
                $POST['date_range'] = "";
            }
        } else {
            $POST['date_range'] = "";
        }

        if (isset($POST['status'])) {
            if ($POST['status'] == 'active') {

                $dataProvider->query->andWhere(['deal.status' => Deal::STATUS_ACTIVE]);
            }
            if ($POST['status'] == 'inactive') {

                $dataProvider->query->andWhere(['deal.status' => Deal::STATUS_DISABLED]);
            }
            if ($POST['status'] == 'incomplete') {

                $dataProvider->query->andWhere(['deal.status' => Deal::STATUS_INCOMPLETE]);
            }
            if ($POST['status'] == 'approved') {

                $dataProvider->query->andWhere(['deal.status' => Deal::STATUS_APPROVED]);
            }
            if ($POST['status'] == 'pendingapproval') {

                $dataProvider->query->andWhere(['deal.status' => Deal::STATUS_PENDINGAPPROVAL]);
            }
            if ($POST['status'] == 'expired') {
                $dataProvider->query->andWhere(['<', 'deal.expire_time', time()]);
            }
        }




        if (is_array($search_options) && sizeof($search_options)) {
            if (is_array($search_options['andWhere']) && sizeof($search_options['andWhere'])) {
                foreach ($search_options['andWhere'] as $condition) {
                    $dataProvider->query->andWhere($condition);
                }
            }
        }
        if (!\yii::$app->request->get("sort", FALSE)) {
            $dataProvider->query->orderBy("id desc");
        }
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'POST' => $POST,
                    'category' => $categoryModel,
                    'title' => ((isset($search_options['title'])) ? $search_options['title'] : "All Business")
        ]);
    }

    public function actionActive() {
        return $this->actionIndex([
                    'andWhere' => [
                        ['deal.status' => Deal::STATUS_ACTIVE]
                    ],
                    'title' => "Active Business"
        ]);
    }

    public function actionInactive() {
        return $this->actionIndex([
                    'andWhere' => [
                        ['deal.status' => Deal::STATUS_DISABLED]
                    ],
                    'title' => "Inactive Business"
        ]);
    }

    public function actionIncomplete() {
        return $this->actionIndex([
                    'andWhere' => [
                        ['deal.status' => Deal::STATUS_INCOMPLETE]
                    ],
                    'title' => "Incomplete Business"
        ]);
    }

    /**
     * Displays a single Deal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }
    
    /**
     * Creates a new Deal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        
        if(Yii::$app->user->isAdmin())
        {    
        $model = new Deal();

        $getdeal = Yii::$app->request->post();


        $merchant = Yii::$app->user->merchant();
        if ($merchant) {

            $model->city_id = $merchant->company->city_id;
        } else {
            if ($getdeal) {
                $model->company_id = User::findOne($getdeal['Deal']['user_id'])->company->id;
            }
        }
       
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            if(isset($getdeal['Deal']['category_id']))
            {
            $dealcatid = $getdeal['Deal']['category_id'];
            }
            else{
                $dealcatid="";
            }
            //\common\components\GenXHelper::c($dealcatid);
            
            if (is_array($dealcatid)) {
                foreach ($dealcatid as $value) {
                    if($value!=''){
                    $dealcategory = new \common\models\DealCategory();
                    $dealcategory->deal_id = $model->id;
                    $dealcategory->category_id = $value;
                    $dealcategory->save();
                    }
                }
            }

	     \common\models\ActivityRiver::logActivity('saved_deal', $model);
	     
	     
	     if ($model->shipping_applied)  {
		      return $this->redirect(\yii\helpers\Url::toRoute(['/deal-shipping-amount/create', 'deal_id' => $model->id]));
		  } 
		  else {
		      return $this->redirect(\yii\helpers\Url::toRoute(['/deal-option/create', 'deal_id' => $model->id]));
		  }
	     
          // return $this->redirect(\yii\helpers\Url::toRoute(['/deal-option/create', 'deal_id' => $model->id]));
        } else {
            $this->sendErrors($model->getErrors());
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
        }
        else{
            $this->redirect(['index']);
        }
         //   
         //   
//                return $this->actionIndex([
//                        'andWhere' => [
//                            ['deal.status' => Deal::STATUS_ACTIVE]
//                        ],
//                        'title' => "Active Deals"
//            ]);
        //}
    }

    /**
     * Updates an existing Deal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {

        $checkupdate = Yii::$app->request->post();
        $model = $this->findModel($id);

        $getdeal = Yii::$app->request->post();

        if ($model->load(Yii::$app->request->post())) {
            if(isset($getdeal['Deal']['category_id']))
            {
            $dealcatid = $getdeal['Deal']['category_id'];
            if($dealcatid[0]=='')
            {
                    $dealcatid="";
            }   
            }
            else{
                $dealcatid="";
            }
            
//\common\components\GenXHelper::c($checkupdate);exit;

            if (!Yii::$app->user->isAdmin()) {

                //$model->status = Deal::STATUS_PENDINGAPPROVAL;
                //$model->disable();
            }
            if ($model->save()) {

                if (is_array($dealcatid)) {
                    \common\models\DealCategory::deleteAll(['deal_id' => $id]);

                    foreach ($dealcatid as $value) {
                        if($value!=''){
                        $dealcategory = new \common\models\DealCategory();

                        $dealcategory->deal_id = $model->id;
                        $dealcategory->category_id = $value;
                        $dealcategory->save();
                        }
                    }
                }
                // lets save activity river
                \common\models\ActivityRiver::logActivity('updated_deal', $model);
		 if ($model->shipping_applied)  {
		      return $this->redirect(\yii\helpers\Url::toRoute(['/deal-shipping-amount/create', 'deal_id' => $model->id,'update_id'=>1]));
		  } 
		  else {
		      return $this->redirect(\yii\helpers\Url::toRoute(['/deal/index', 'deal_id' => $model->id]));
		  }
//                if (isset($checkupdate['update_id'])) {
//                    $this->sendSuccess(['Deal updated successfully.']);
//                    return $this->redirect(\yii\helpers\Url::toRoute(['index']));
//                } else {
//                    return $this->redirect(\yii\helpers\Url::toRoute(['/deal-option/create', 'deal_id' => $model->id]));
//                }
            } else {
                \common\components\GenXHelper::c($model->getErrors());
                echo "not updated";
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Deal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->delete();
        \common\models\ActivityRiver::logActivity('deleted_deal', $model);
        return $this->redirect(['index']);
    }

    public function actionComplete($id) {
        $deal = $this->findModel($id);

        // only pre-activate deal if its admin
        if (Yii::$app->user->isAdmin()) {
            $deal_options = $deal->getDealOptions()->count();
            $deal_gallery = $deal->getDealGalleries()->count();

            if ($deal_options && $deal_gallery) {
                $deal->status = Deal::STATUS_APPROVED;
                $this->actionSendForPublish($id);
            }
        } else {
            $deal->status = Deal::STATUS_PENDINGAPPROVAL;
        }

        if ($deal->save()) {

            \common\models\ActivityRiver::logActivity('completed_deal', $deal);
            return $this->redirect(['index']);
        }
        return $this->goBack();
    }

    public function actionResendActivationCode($id) {
        //$deal = $this->findModel($id);
        $this->actionSendForPublish($id);
        $this->sendSuccess(['Activation code sent successfully, once merchant will approved then deal will published.']);
        return $this->redirect(['index']);
    }

    public function actionSendForPublish($dealid) {

        $deal = $this->findModel($dealid);
        \common\components\GenXMailer::start()->sendAssociateToViewPublish($deal);
    }

    /**
     * Finds the Deal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Deal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        $find = Deal::find();
        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $find->andWhere(['user_id' => Yii::$app->user->getLoggedinID()]);
        }
        $find->andWhere(['id' => $id]);
        if (($model = $find->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSendRequest() {
        $dealId = Yii::$app->request->post();
        $dealdetails = $this->findModel($dealId);
        //\common\components\GenXHelper::c($dealdetails);

        return $this->renderAjax("cancel_request", ['deal' => $dealdetails]);

        /* if (!\common\components\GenXHelper::checkDealCancelRequest($coupon->id)) {
          return $this->renderAjax("/user/cancel_request", ['cart' => $cartdetails]);
          } else {
          $this->sendErrors(['request_error' => ['Request is already sent']]);
          return $this->redirect(\yii\helpers\Url::toRoute("/user"));
          }
         * 
         */
    }

    public function actionMyrequest() {

        $dealId = Yii::$app->request->post();
        $searchModel = new \common\models\RequestSearch();
        $dataProvider = $searchModel->search([]);
        $dataProvider->query->andWhere(['type' => 'deal_update']);
        $dataProvider->query->andWhere(['from_id' => Yii::$app->user->id]);
        $dataProvider->query->andWhere(['object_id' => $dealId['id']]);
        $dataProvider->query->orderBy('created_at desc');

        $dataprov = $dataProvider->getModels();
        // \common\components\GenXHelper::c($dataprov);
        return $this->renderAjax('myrequest', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataprov,
                    'title' => "deal Request"
        ]);
        //\common\components\GenXHelper::c($dataProvider);
        // exit();
        // $dealdetails = $this->findModel($dealId);
        //\common\components\GenXHelper::c($dealdetails);
        // return $this->renderAjax("myrequest", ['deal' => $dealdetails]);

        /* if (!\common\components\GenXHelper::checkDealCancelRequest($coupon->id)) {
          return $this->renderAjax("/user/cancel_request", ['cart' => $cartdetails]);
          } else {
          $this->sendErrors(['request_error' => ['Request is already sent']]);
          return $this->redirect(\yii\helpers\Url::toRoute("/user"));
          }
         * 
         */
    }

    public function actionUpdateRequest() {

        $dealrequest = Yii::$app->request->post();

//$cartModel=new Cart();
        $getDeal = Deal::findOne($dealrequest['deal']);
        if (!\common\components\GenXHelper::checkDealUpdateRequest($getDeal->id)) {
            $request = new \common\models\Request();
            $request->from_id = Yii::$app->user->id;
            $request->type = 'deal_update';
            $request->remarks = $dealrequest['remarks'];
            $request->object_id = $getDeal->id;
            $request->object_class = Deal::className();
            $request->data = json_encode($getDeal->getAttributes());
            $request->assigned_id = 2;
            if ($request->save()) {
                $this->sendSuccess(['Deal Update request sent successfully, Wait for approval.']);
            } else {
                $this->sendErrors($request->getErrors());
            }
        } else {
            $this->sendErrors(['request_error' => [' Your previous updation is still pending for approval.']]);
        }

        //  \common\components\GenXHelper::c($request->getErrors());
        return $this->redirect(\yii\helpers\Url::toRoute("index"));
    }
    
    public function actionSendEmailNewsletter($email = ""){
        
        $getnewsletter=\common\models\Newsletter::findOne(['sendemail'=>0,'subscribed'=>1]);
        $to_email= $getnewsletter->email;
        echo $to_email;
       /* foreach ($getnewsletter as $newsletter)
        {   
            $newsmodel=new \common\models\Newsletter();
            
            echo $newsletter->email."<br>";
            $newsmodel->save()
            
        }*/
        
       // $deals= Deal::find()->andWhere(['deal.slider'=>1])->andWhere(['>=', 'deal.expire_time', time()])->limit(8, 16)->all();
     
        //echo  $to_email="gaurav@classifr.com";
      //  $to_email = [
          //  1 => 'kdsingh49p@gmail.com',
         //   2 => 'shubham@yopmail.com',
        //    3 => $email,
         //   4 => 'kuldeep12345677@outlook.com',
        
        $deals= Deal::find()->andWhere(['deal.status'=>  Deal::STATUS_ACTIVE])->andWhere(['>=', 'deal.expire_time', time()])->limit(16)->all();
     
         // $to_email="gurmeetgogi@gmail.com";
        /*$to_email = [
            1 => 'kdsingh49p@gmail.com',
            2 => 'shubham@yopmail.com',
            3 => 'kuldeep@genx-infotech.com',
            4 => 'kuldeep12345677@outlook.com',
           // 5 => 'rimpysharma9404@gmail.com',
          //  6 => 'akashsharma091990@gmail.com',
          //  7=> 'gurmeetgogi@gmail.com',
            
        ];*/
     /*   foreach($to_email as $key => $email){
            echo $to_email[$key];
      * 
      */
            $subject="Big saving alert: Melbourne deals of the day - Classifr ";
            \common\components\GenXMailer::start()->sendNewsletterSubscribedUser($deals,$to_email,$subject);
       // }
        
       
        $getnewsletter->sendemail=1;
        $getnewsletter->save();
        echo  '<meta http-equiv="refresh" content="10;url=https://www.classifr.com/admin/deal/send-email-newsletter">';
        
    }
    public function actionSendDealExpireNotificationToMerchant( $deal_id = ""){
        
       
//       "<a 'class' = 'btn btn-success btn-sm' href='".\yii\helpers\Url::toRoute([
//                                            '/deal/send-deal-expire-notification-to-merchant',
//                                            'deal_id' => $model->cart->deal->id, 'merchant_id' => $model->cart->merchant->id
//                                        ])."'>Send Deal Expire Notificaiton Email</a>
//                                       ".
        //$deals= Deal::find()->where(['id' => $deal_id])->one();
        //$count_deals = Deal::find()->andWhere(['deal.status'=>  Deal::STATUS_ACTIVE])->andWhere(['<=', 'deal.expire_time', time()])->limit(16)->count();
        
        $deals_query = Deal::find()->andWhere(['deal.status'=>  Deal::STATUS_ACTIVE])->andWhere(['<=', 'deal.expire_time', time()])->all();
        foreach($deals_query as $deals){
        $Merchant_detail = \common\models\User::findOne($deals->user_id);
        $to_email= $Merchant_detail->email;
        echo $to_email;
        $subject="Deal Expired Notification - Classifr ";
            \common\components\GenXMailer::start()->sendExpireDealNotification($deals, $Merchant_detail, $to_email, $subject);
            echo  '<meta http-equiv="refresh" content="10;url=https://www.classifr.com/admin/deal/send-email-newsletter">';
        }
            
       // }
        
       
       
        
        
    }

}
