<?php

namespace backend\controllers;

use Yii;
use common\models\City;
use common\models\CitySearch;
use backend\controllers\GenxUseAdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;
use yii\web\UploadedFile;

/**
 * CityController implements the CRUD actions for City model.
 */
class CityController extends GenxUseAdminController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $parent_behavior = parent::behaviors();
        $parent_behavior['access']['rules'][] = [
            'allow' => True,
            'actions' => [],
            'roles' => [User::ROLE_ADMIN]
        ];
        $parent_behavior['access']['rules'][] = [
            'allow' => True,
            'actions' => ['autocomplete'],
            'roles' => [User::ROLE_MERCHANT]
        ];

        return $parent_behavior;
    }

    /**
     * Lists all City models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
       // \common\components\GenXHelper::c($dataProvider);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single City model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new City model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new City();
        $model->country_id = Yii::$app->request->get('country_id', 0);
        $model->state_id = Yii::$app->request->get('state_id', 0);
        
        
        if ($model->load(Yii::$app->request->post())) {
        
        $statedata= \common\models\State::findOne($model->state_id);
        
            
            $model->imageFile = UploadedFile::getInstance($model, 'featured_img');
            if($model->imageFile){
            $model->featured_img = $model->imageFile->getBaseName();
            if ($featured_img = $model->upload()) {
                $model->featured_img = $featured_img;
                
                }
            }
            // google api work
            
            $address =  $model->name.", ".$statedata->name;
            $address = urlencode($address);
            $googleApi = new \common\components\GoogleApiEx($address);
            $data=$googleApi->getInfo();
            $model->latitude = $data['results'][0]['geometry']['location']['lat'];
            $model->longitude =  $data['results'][0]['geometry']['location']['lng'];
            if(!$model->save()){
                    $this->sendErrors($model->getErrors());
                }
          
                
            return $this->redirect(['view', 'id' => $model->id]);
        
            
            } 
        else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }
    

    /**
     * Updates an existing City model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            
            $model->imageFile = UploadedFile::getInstance($model, 'featured_img');
            if($model->imageFile){
            $featured_img = $model->imageFile->getBaseName();
            if ($featured_img = $model->upload()) {
                $model->featured_img = $featured_img;
                
                }
            }
            else{
                $model->featured_img=$model->getOldAttribute("featured_img");
            }
            
            $address= $model->search_name;
            $address = urlencode($address);
            // google api work
            $googleApi = new \common\components\GoogleApiEx($address);
            $data=$googleApi->getInfo();
            $model->latitude = $data['results'][0]['geometry']['location']['lat'];
            $model->longitude =  $data['results'][0]['geometry']['location']['lng'];
            
                if(!$model->save()){
                    $this->sendErrors($model->getErrors());
                }
            
            
            return $this->redirect(['index', 'CitySearch[id]' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionAllCityUpdated()
    {
        $allrecord = City::find()
                ->Where(['done'=>0,
                    'status'=>10])
                //->andWhere(['status'=>10])
                ->limit(25)->all();
        foreach ($allrecord as $citymodel)
        {
            
             $address= $citymodel->search_name;
            $address = urlencode($address);
            
            $googleApi = new \common\components\GoogleApiEx($address);
            $data=$googleApi->getInfo();
            $citymodel->latitude = $data['results'][0]['geometry']['location']['lat'];
            $citymodel->longitude =  $data['results'][0]['geometry']['location']['lng'];
            $citymodel->done=1;
             if($citymodel->save())
             {   
            \common\components\GenXHelper::c($citymodel['search_name']);
             }
        }
    }

    /**
     * Deletes an existing City model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the City model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return City the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = City::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAutocomplete($term = Null)
    {

        $city_search_options = [];
        $term = trim($term);
        if (empty($term)) {
            $city_search_options[] = ['featured' => 1];
        }

        $city_data_query = City::find()->andFilterWhere(['like', 'search_name', $term]);
        if (sizeof($city_search_options)) {
            foreach ($city_search_options as $filter) {
                $city_data_query->andWhere($filter);
            }
        }

        $city_data = $city_data_query->all();
        $return = [];
        if (sizeof($city_data)) {
            foreach ($city_data as $city) {
                $return['results'][] = [
                    'text' => $city->search_name,
                    'id' => $city->id
                ];
            }

            $return['more'] = False;
        }

        // send with json headers
        header('Content-Type: application/json');
        echo json_encode($return);
    }

}
