<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use backend\controllers\GenxUseAdminController;
use yii\web\NotFoundHttpException;
use common\models\CartSearch;
use common\models\CouponCodeSearch;

/**
 * Description of ReportsController
 *
 * @author Genx Sharna @ GenX Infotech
 */
class ReportsController extends GenxUseAdminController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $parent_behavior = parent::behaviors();
        $parent_behavior['access']['rules'][] = [
            'allow' => True,
            'actions' => [],
            'roles' => [User::ROLE_ADMIN]
        ];
        $parent_behavior['access']['rules'][] = [
            'allow' => True,
            'actions' => ['sold-deals', 'sold-amount', 'invoice'],
            'roles' => [User::ROLE_MERCHANT]
        ];

        return $parent_behavior;
    }

    public $show_title = False;
    public $show_page_header = False;

    public function actionSoldDeals()
    {
        $end_time = time();
        $start_time = time() - (60 * 60 * 24 * 30);
        if (Yii::$app->request->get()) {
            $POST = Yii::$app->request->get();
            if (isset($POST['date_range'])) {
                $posted_date = explode('-', $POST['date_range']);
                $end_time = (int) strtotime($posted_date[1]) + (60 * 60 * 24);
                $start_time = (int) strtotime($posted_date[0]);
            } else {
                $POST['date_range'] = date('m/d/Y', $start_time) . " - " . date('m/d/Y', $end_time);
            }
        } else {
            $POST['date_range'] = date('m/d/Y', $start_time) . " - " . date('m/d/Y', $end_time);
        }

        // sales Chart
        $sale_wise_data = new CartSearch();
        $sale_data_provider = $sale_wise_data->search([]);
        if (isset($POST['deal_id']) && $POST['deal_id']) {
            $sale_data_provider->query->andWhere(['cart.deal_id' => $POST['deal_id']]);
        }
        $sale_data_provider
                ->query
                ->select("cart.*, sum(cart.qty) as total_sale")
                ->andWhere(['between', 'cart.created_at', $start_time, $end_time])
                ->groupBy('DATE(FROM_UNIXTIME(cart.created_at))')
                ->orderBy('cart.created_at ASC');

        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $sale_data_provider->query->andWhere(['cart.merchant_id' => Yii::$app->user->getLoggedinID()]);
        }

        $models = $sale_data_provider->getModels();

        $chart_data = [
            'labels' => [],
            'datasets' => [
                [
                    'label' => 'Total Sold Deals',
                    'backgroundColor' => '#26B99A',
                ]
            ]
        ];

        foreach ($models as $data_model) {
            $data_subset[$data_model->dd . "-" . $data_model->mm . "-" . $data_model->yy] = $data_model->total_sale;
        }

        while ($start_time <= $end_time) {
            $sale_labels[] = date('j M', $start_time);
            $sale_data[] = (int) ((isset($data_subset[date('j-n-Y', $start_time)])) ? $data_subset[date('j-n-Y', $start_time)] : 0);

            $start_time += (60 * 60 * 24);
        }

        $chart_data['labels'] = array_values($sale_labels);
        $chart_data['datasets'][0]['data'] = array_values($sale_data);

        // Deals data
        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $deals = \common\models\Deal::find()->andWhere(['user_id' => Yii::$app->user->getLoggedinID()])->all();
        } else {
            $deals = \common\models\Deal::find()->all();
        }

        return $this->render('sold-deals', [
                    'chart_data' => $chart_data,
                    'deals' => $deals,
                    'POST' => $POST,
                    'dataProvider' => $sale_data_provider
        ]);
    }

    public function actionSoldAmount()
    {
        $end_time = time();
        $start_time = time() - (60 * 60 * 24 * 30);
        if (Yii::$app->request->get()) {
            $POST = Yii::$app->request->get();
            if (isset($POST['date_range'])) {
                $posted_date = explode('-', $POST['date_range']);
                $end_time = (int) strtotime($posted_date[1]) + (60 * 60 * 24);
                $start_time = (int) strtotime($posted_date[0]);
            } else {
                $POST['date_range'] = date('m/d/Y', $start_time) . " - " . date('m/d/Y', $end_time);
            }
        } else {
            $POST['date_range'] = date('m/d/Y', $start_time) . " - " . date('m/d/Y', $end_time);
        }

        // sales Chart
        $sale_wise_data = new CartSearch();
        $sale_data_provider = $sale_wise_data->search([]);
        if (isset($POST['deal_id']) && $POST['deal_id']) {
            $sale_data_provider->query->andWhere(['cart.deal_id' => $POST['deal_id']]);
        }
        $sale_data_provider
                ->query
                ->select("cart.*, sum(cart.net_price) as total_sale")
                ->andWhere(['between', 'cart.created_at', $start_time, $end_time])
                ->groupBy('DATE(FROM_UNIXTIME(cart.created_at))')
                ->orderBy('cart.created_at ASC');

        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $sale_data_provider->query->andWhere(['cart.merchant_id' => Yii::$app->user->getLoggedinID()]);
        }

        $models = $sale_data_provider->getModels();

        $chart_data = [
            'labels' => [],
            'datasets' => [
                [
                    'label' => 'Total Sale',
                    'backgroundColor' => '#2A3F54',
                ]
            ]
        ];

        foreach ($models as $data_model) {
            $data_subset[$data_model->dd . "-" . $data_model->mm . "-" . $data_model->yy] = $data_model->total_sale;
        }

        while ($start_time <= $end_time) {
            $sale_labels[] = date('j M', $start_time);
            $sale_data[] = (int) ((isset($data_subset[date('j-n-Y', $start_time)])) ? $data_subset[date('j-n-Y', $start_time)] : 0);

            $start_time += (60 * 60 * 24);
        }

        $chart_data['labels'] = array_values($sale_labels);
        $chart_data['datasets'][0]['data'] = array_values($sale_data);

        // Deals data
        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $deals = \common\models\Deal::find()->andWhere(['user_id' => Yii::$app->user->getLoggedinID()])->all();
        } else {
            $deals = \common\models\Deal::find()->all();
        }

        return $this->render('sales-amount', [
                    'chart_data' => $chart_data,
                    'deals' => $deals,
                    'POST' => $POST,
                    'dataProvider' => $sale_data_provider
        ]);
    }

    public function actionCustomers()
    {
        $this->show_page_header = True;
        $this->show_title = True;
        $end_time = time();
        $start_time = time() - (60 * 60 * 24 * 30);
        if (Yii::$app->request->get()) {
            $POST = Yii::$app->request->get();
            if (isset($POST['date_range'])) {
                $posted_date = explode('-', $POST['date_range']);
                $end_time = (int) strtotime($posted_date[1]) + (60 * 60 * 24);
                $start_time = (int) strtotime($posted_date[0]);
            } else {
                $POST['date_range'] = date('m/d/Y', $start_time) . " - " . date('m/d/Y', $end_time);
            }
        } else {
            $POST['date_range'] = date('m/d/Y', $start_time) . " - " . date('m/d/Y', $end_time);
        }

        // sales Chart
        $sale_wise_data = new CartSearch();
        $sale_data_provider = $sale_wise_data->search([]);
        if (isset($POST['deal_id']) && $POST['deal_id']) {
            $sale_data_provider->query->andWhere(['cart.deal_id' => $POST['deal_id']]);
        }
        $sale_data_provider
                ->query
                ->select("cart.*, sum(cart.net_price) as total_sale")
                ->andWhere(['between', 'cart.created_at', $start_time, $end_time])
                ->groupBy('DATE(FROM_UNIXTIME(cart.created_at)), user_id')
                ->orderBy('cart.created_at ASC');

        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $sale_data_provider->query->andWhere(['cart.merchant_id' => Yii::$app->user->getLoggedinID()]);
        }

        // Deals data
        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $deals = \common\models\Deal::find()->andWhere(['user_id' => Yii::$app->user->getLoggedinID()])->all();
        } else {
            $deals = \common\models\Deal::find()->all();
        }

        return $this->render('customers', [
//                    'chart_data' => $chart_data,
                    'deals' => $deals,
                    'POST' => $POST,
                    'dataProvider' => $sale_data_provider
        ]);
    }

    public function actionPendingCoupons()
    {
        $this->show_page_header = True;
        $this->show_title = True;
        $end_time = time();
        $start_time = time() - (60 * 60 * 24 * 30);
        if (Yii::$app->request->get()) {
            $POST = Yii::$app->request->get();
            if (isset($POST['date_range'])) {
                $posted_date = explode('-', $POST['date_range']);
                $end_time = (int) strtotime($posted_date[1] + (60 * 60 * 24));
                $start_time = (int) strtotime($posted_date[0]);
            } else {
                $POST['date_range'] = date('m/d/Y', $start_time) . " - " . date('m/d/Y', $end_time);
            }
        } else {
            $POST['date_range'] = date('m/d/Y', $start_time) . " - " . date('m/d/Y', $end_time);
        }

        $searchModel = new CouponCodeSearch();
        $dataProvider = $searchModel->search([]);
        $dataProvider->query->joinWith('cart');
        if (isset($POST['deal_id']) && $POST['deal_id']) {
            $dataProvider->query->andWhere(['cart.deal_id' => $POST['deal_id']]);
        }
        $dataProvider->query
                ->andWhere(['coupon_code.status' => \common\models\CouponCode::STATUS_ACTIVE])
                ->andWhere(['between', 'coupon_code.created_at', $start_time, $end_time]);

        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $dataProvider->query->andWhere(['cart.merchant_id' => Yii::$app->user->getLoggedinID()]);
        }

        // Deals data
        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $deals = \common\models\Deal::find()->andWhere(['user_id' => Yii::$app->user->getLoggedinID()])->all();
        } else {
            $deals = \common\models\Deal::find()->all();
        }

        return $this->render('pending-coupons', [
                    'deals' => $deals,
                    'POST' => $POST,
                    'dataProvider' => $dataProvider
        ]);
    }

    public function actionRedeemedCoupons()
    {
        $this->show_page_header = True;
        $this->show_title = True;
        $end_time = time();
        $start_time = time() - (60 * 60 * 24 * 30);
        if (Yii::$app->request->get()) {
            $POST = Yii::$app->request->get();
            if (isset($POST['date_range'])) {
                $posted_date = explode('-', $POST['date_range']);
                $end_time = (int) strtotime($posted_date[1]) + (60 * 60 * 24);
                $start_time = (int) strtotime($posted_date[0]);
            } else {
                $POST['date_range'] = date('m/d/Y', $start_time) . " - " . date('m/d/Y', $end_time);
            }
        } else {
            $POST['date_range'] = date('m/d/Y', $start_time) . " - " . date('m/d/Y', $end_time);
        }

        $searchModel = new CouponCodeSearch();
        $dataProvider = $searchModel->search([]);
        $dataProvider->query->joinWith('cart');
        if (isset($POST['deal_id']) && $POST['deal_id']) {
            $dataProvider->query->andWhere(['cart.deal_id' => $POST['deal_id']]);
        }
        $dataProvider->query
                ->andWhere(['coupon_code.status' => \common\models\CouponCode::STATUS_REDEEMED])
                ->andWhere(['between', 'coupon_code.created_at', $start_time, $end_time]);

        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $dataProvider->query->andWhere(['cart.merchant_id' => Yii::$app->user->getLoggedinID()]);
        }

        // Deals data
        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $deals = \common\models\Deal::find()->andWhere(['user_id' => Yii::$app->user->getLoggedinID()])->all();
        } else {
            $deals = \common\models\Deal::find()->all();
        }
        return $this->render('redeemed-coupons', [
                    'deals' => $deals,
                    'POST' => $POST,
                    'dataProvider' => $dataProvider
        ]);
    }

    public function actionInvoice($id, $print = False)
    {
        $order = \common\models\OrderSearch::findOne($id);
        return $this->render(
                        'invoice', ['order' => $order, 'print' => $print]
        );
    }

    public function actionInvoicePrint($id)
    {
        $this->layout = "//main-print";

        return $this->actionInvoice($id, True);
    }

    public function actionInvoicePdf($id)
    {
        $this->layout = "//main-pdf";
        Yii::$app->response->format = 'pdf';

        return $this->actionInvoice($id, True);
    }

}
