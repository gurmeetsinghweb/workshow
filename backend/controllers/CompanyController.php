<?php

namespace backend\controllers;

use Yii;
use common\models\Company;
use common\models\User;
use common\models\CompanySearch;
use backend\controllers\GenxUseAdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\GenXHelper;

/**
 * CompanyController implements the CRUD actions for Company model.
 */
class CompanyController extends GenxUseAdminController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors ['access']['rules'] = [
            [
                'allow' => True,
                'actions' => ['index', 'delete', 'create', 'update', 'autocomplete', 'view'],
                'roles' => [
                    \common\models\User::ROLE_ADMIN
                ],
            ],
            [
                'allow' => True,
                'actions' => ['my','change-password'],
                'roles' => [\common\models\User::ROLE_MERCHANT]
            ],
        ];

        return $behaviors;
    }

    public function actionMy()
    {
        if (Yii::$app->user->merchant()->company && Yii::$app->user->merchant()->company->id) {
            return $this->actionUpdate(Yii::$app->user->merchant()->company->id, ['my']);
        }

        return $this->actionCreate(['my']);
    }
    
    //merchant password change
    
        
    public  function actionChangePassword()
    {
        $model=  User::findOne(Yii::$app->user->id);
        //GenXHelper::c($model);
        
        if ($model->load(Yii::$app->request->post())) {
            
          $pass=  Yii::$app->request->post();
          if(!empty($pass['User']['password']))
          {
            $model->password=  $pass['User']['password'];
            $model->save();
            $this->sendSuccess(["Password Updated successfully."]);
            return $this->redirect(['change-password']);
          }
          else
          {
                 $this->sendErrors(['Error' => ['Fill complete form.']]);
          } 
            //GenXHelper::c($model);
            
        } else {
            if (Yii::$app->request->post()) {
            
                $this->sendErrors(['Error' => [' Fill complete form.']]);
            }
        } 
        return $this->render('changepwd', [
                        'model' => $model,
            ]);
        /*  $model
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->sendSuccess(["User Updated successfully"]);
            return $this->redirect(['index']);
        } else {
            if (Yii::$app->request->post()) {
                $this->sendErrors(['Error' => [' Fill complete form']]);
            }
            return $this->render('update', [
                        'model' => $model,
            ]);
        }*/
    }        


    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Company model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model->attributes;
        }
        return $this->render('view', [
                    'model' => $model,
        ]);
    }

    /**
     * Creates a new Company model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($redirect = False)
    {
        $model = new Company();
        $UserModel = new User();
        if (!$redirect) {
            if ($model->user) {
                $redirect = array('/user/view', 'id' => $model->user->id, 'selected_tab' => 'company-profile');
            }
            else{
                
                $redirect = array('/merchant/index');
            }
        }
        
      /* var_dump(Yii::$app->request->isAjax); 
      if (Yii::$app->request->isAjax && $UserModel->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($UserModel);
        }
       * 
       */
        if($model->load(Yii::$app->request->post()))
        {    
        
        if (Yii::$app->request->post()) {
            
            if(empty($_POST['User']['id']))
            {    
            $UserModel->email = $_POST['User']['email'];
            $UserModel->username = $_POST['User']['username'];
            $UserModel->name = $_POST['User']['name'];
            $UserModel->password_repeat = $_POST['User']['password'];
            $UserModel->password = $_POST['User']['password'];
            $UserModel->type = User::ROLE_MERCHANT;
            $UserModel->city_id = $_POST['Company']['city_id'];
            
            
            }
            else{
                
                $UserModel =  User::findOne($_POST['User']['id']);
                $UserModel->type = User::ROLE_MERCHANT;
            
                
            }
            if ($UserModel->save()) {
                
                $model->user_id = $UserModel->id;
            }
            else {
                
                $this->sendErrors($UserModel->getErrors());
            }
            
            unset($_POST['User']);
        }
       
        if($model->save())
        {   
        $this->sendSuccess(["Company Created Successfully."]);
            return $this->redirect($redirect);
        
            
        } else {
            $this->sendErrors($model->getErrors());
           return $this->render('create', [
                        'model' => $model,
                        'UserModel' => $UserModel,
            ]); 
        }
        }
        else{
            $this->sendErrors($model->getErrors());
            return $this->render('create', [
                        'model' => $model,
                        'UserModel' => $UserModel,
            ]);
        }
    }

    /**
     * Updates an existing Company model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $redirect = False)
    {
        $model = $this->findModel($id);
        if (!$redirect) {
            $redirect = array('/user/view', 'id' => $model->user->id, 'selected_tab' => 'company-profile');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->sendSuccess(["Company Updated Successfully."]);
            return $this->redirect($redirect);
        } else {
            $this->sendErrors($model->getErrors());
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Company model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
        $this->sendErrors(['Warning'=>['This function is disabled.']]);
        return $this->redirect(['index']);
    }

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAutocomplete($term)
    {
        $term = trim($term);
        if (!strlen($term)) {
            echo json_encode([]);
        }

        $company_search = new CompanySearch();
        $company_search->title = $term;
        $company_data = $company_search->search([])->getModels();

        $return = [];
        if (sizeof($company_data)) {
            foreach ($company_data as $company) {
                $label = $company->title;
                $return[] = [
                    'label' => $label,
                    'value' => $company->title,
                    'id' => $company->id
                ];
            }
        }
        echo json_encode($return);
    }

}
