<?php

namespace backend\controllers;

use Yii;
use common\models\CouponCode;
use common\models\CouponCodeSearch;
use backend\controllers\GenxUseAdminController;
use yii\web\NotFoundHttpException;
use common\models\User;
use common\models\DealOption;
use common\components\GenXHelper;
use common\models\Request;


/**
 * CouponCodeController implements the CRUD actions for CouponCode model.
 */
class CouponCodeController extends GenxUseAdminController
{

    public function behaviors()
    {
        $behaviours = parent::behaviors();
        $behaviours['access']['rules'][] = [
            'allow' => True,
            'actions' => ['create', 'update', 'view', 'delete'],
            'roles' => [User::ROLE_ADMIN]
        ];
        $behaviours['access']['rules'][] = [
            'allow' => True,
            'actions' => ['index', 'un-redeem-coupon', 'redeem-coupon', 'multipal-coupon-redeemed', 'extend-coupon-exp-date', 'extend-coupon-expiry-action','cart-tracking','cart-tracking-action'],
            'roles' => [User::ROLE_MERCHANT]
        ];

        return $behaviours;
    }

    /**
     * Lists all CouponCode models.
     * @return mixed
     */
    public function actionIndex()
    {

        /* switch (Yii::$app->user->identity->type) {
          case User::ROLE_ADMIN:
          return $this->asdf();
          break;

          } */
        //echo time();
        $this->show_page_header = False;
        $end_time = time();
        $start_time = time() - (60 * 60 * 24 * 30);
        $where = array();

        $searchModel = new CouponCodeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        //\common\components\GenXHelper::c(Yii::$app->request->queryParams);      
        if (Yii::$app->request->get()) {
            $POST = Yii::$app->request->get();

            if (isset($POST['time_range'])) {
                $posted_time = explode('-', $POST['time_range']);

                $end_time = str_replace("+", " ", $posted_time[1]);
                $start_time = str_replace("+", " ", $posted_time[0]);
            }

            if (isset($POST['date_range']) and ! empty($POST['date_range'])) {

                $posted_date = explode('-', $POST['date_range']);
                $end_time = str_replace("+", " ", $posted_date[1]);
                $start_time = str_replace("+", " ", $posted_date[0]);
                $end_time = (int) strtotime($end_time) + (60 * 60 * 24);
                $start_time = (int) strtotime($start_time);
                $dataProvider->query->andWhere(['between', 'cart.created_at', $start_time, $end_time]);
                $where['andWhere'][] = ['between', 'cart.created_at', $start_time, $end_time]
                ;

                $POST['date_range'] = urldecode($posted_date[0]) . "-" . urldecode($posted_date[1]);
            } else {
                $POST['date_range'] = date('m/d/Y', $start_time) . " - " . date('m/d/Y', $end_time);
            }
        } else {
            $POST['date_range'] = date('m/d/Y', $start_time) . " - " . date('m/d/Y', $end_time);
        }





        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $dataProvider->query->andWhere(['cart.merchant_id' => Yii::$app->user->getLoggedinID()]);
        }
// 13/4/17 gurmeet remove main_category_id and replace with deal_category table with join command
      /*  if (isset($POST['deal_category_id']) and ! empty($POST['deal_category_id'])) {
            $dataProvider->query->andWhere(['deal_category.category_id' => $POST['deal_category_id']]);
            $where['andWhere'][] = ['deal_category.category_id' => $POST['deal_category_id']];
        }*/

        if (isset($POST['merchant_id']) and ! empty($POST['merchant_id'])) {
            $dataProvider->query->andWhere(['cart.merchant_id' => $POST['merchant_id']]);
            $where['andWhere'][] = ['cart.merchant_id' => $POST['merchant_id']]
            ;
        }

        $dataProvider->query->orderBy('coupon_code.id DESC');
        if (isset($POST['deal_id']) && $POST['deal_id']) {
            $dataProvider->query->andWhere(['cart.deal_id' => $POST['deal_id']]);
            $where['andWhere'][] = ['cart.deal_id' => $POST['deal_id']]
            ;
        }
        if (isset($POST['payout_id']) && $POST['payout_id']) {
            $dataProvider->query->andWhere(['cart.transaction_payment_id' => $POST['payout_id']]);
            $where['andWhere'][] = ['cart.transaction_payment_id' => $POST['payout_id']]
            ;
        }


        $models = GenXHelper::totalDealSoldAount($where);
        // canceled deal
        $canceldeals = [
            'andWhere' => [
                ['coupon_code.status' => \common\components\GenxBaseModel::STATUS_CANCLED]
            ]
        ];

        $canceldeals = $this->mergeArrayAndWhere($canceldeals, $where);
        $canceldeal = GenXHelper::totalDealSoldAount($canceldeals);

        // payment given

        $dealredemed = [
            'andWhere' => [
                ['coupon_code.status' => \common\components\GenxBaseModel::STATUS_REDEEMED],
                ['is not', 'cart.transaction_log_id', NULL]
            ]
        ];
        $dealredemed = $this->mergeArrayAndWhere($dealredemed, $where);
        $redemeddeal = GenXHelper::totalDealSoldAount($dealredemed);

        //payment unclear but already redeemed
        $uncleapayment = [
            'andWhere' => [
                ['coupon_code.status' => \common\components\GenxBaseModel::STATUS_REDEEMED],
                ['is', 'cart.transaction_log_id', NULL]
            ]
        ];
        $uncleapayment = $this->mergeArrayAndWhere($uncleapayment, $where);
        $paymentunclear = GenXHelper::totalDealSoldAount($uncleapayment);

        //payment not redeemed
        $paymentnotredeemed = [
            'andWhere' => [
                ['coupon_code.status' => \common\components\GenxBaseModel::STATUS_ACTIVE],
                ['>', 'coupon_code.expires_at', time()]
            ]
        ];

        $paymentnotredeemed = $this->mergeArrayAndWhere($paymentnotredeemed, $where);
        $notredeemed = GenXHelper::totalDealSoldAount($paymentnotredeemed);


        $metrics = [
            'total_sale' => (float) round($models[0]['total_sale'], 2),
            'total_deals_sold' => (int) $models[0]['total_deals'],
            'total_my_earning' => (float) round($models[0]['merchant_sale'], 2),
            'company_sale' => (float) round($models[0]['company_sale'], 2),
            //'canceled_deal' => (float) round($canceldeal[0]['merchant_sale'], 2),
           // 'payment_given' => (float) round($redemeddeal[0]['merchant_sale'], 2),
           // 'payment_not_clear' => (float) round($paymentunclear[0]['merchant_sale'], 2),
           // 'non_redeemed' => (float) round($notredeemed[0]['merchant_sale'], 2),
        ];

        $category = \common\models\CategorySearch::find()->andWhere(['parent_id' => 0])->all();
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'POST' => $POST,
                    'category' => $category,
                    'metrics' => $metrics,
        ]);
    }

    public function mergeArrayAndWhere($newArray, $mergeArray)
    {
        if (isset($mergeArray['andWhere']) && is_array($mergeArray['andWhere']) && sizeof($mergeArray['andWhere'])) {

            foreach ($mergeArray['andWhere'] as $value) {
                $newArray['andWhere'][] = $value;
            }
        }
        return $newArray;
    }

    /**
     * Displays a single CouponCode model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CouponCode model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CouponCode();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CouponCode model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    public function actionRedeemCoupon($id)
    {
        $model = $this->findModel($id);
        $totalredeem = $model->cart->dealOption->total_redeem;
        $qty = $model->cart->qty;

        //\common\components\GenXHelper::c($model); 
        //exit();
        // if coupon has not expired and it belongs to the logged in merchant
        if ($model->expires_at > time() && ($model->cart->merchant_id == Yii::$app->user->merchant()->id )) {

            if ($model->status == CouponCode::STATUS_REDEEMED) {
                $this->sendErrors(["alreadyredeem" => ["This Coupon is already redeemed."]]);
                return $this->redirect(['coupon-code/index']);
            } else if ($model->status == CouponCode::STATUS_CANCLED) {
                $this->sendErrors(["alreadyredeem" => ["This Coupon is canceled."]]);
                return $this->redirect(['coupon-code/index']);
            } else {

                $totalredeem = $totalredeem + $qty;
                $model->cart->dealOption->total_redeem = $totalredeem;
                $model->cart->dealOption->update();

                $model->status = CouponCode::STATUS_REDEEMED;
                $model->update();
                // add credit

                if ($model->cart->deal->deal_credit) {

                    // add credit to user account

                    $creditamount = round((($model->cart->net_price * $model->cart->deal->deal_credit) / 100), 2);

                    $user_account = new \common\models\UserAccount();
                    $user_account->cart_id = $model->cart->id;
                    $user_account->user_id = $model->cart->user_id;
                    $user_account->order_id = $model->id;
                    $user_account->transaction_id = "add_credit_redeemed_coupon " . $model->id;
                    $user_account->debit = 0;
                    $user_account->remarks = "Credit against voucher redeemed";
                    $user_account->credit = $creditamount;
                    if ($user_account->save()) {
                        //return True;
                    }
                }
                // if request send for cancelation on that case request rejected automatically

                $cancelRequest = Request::find()->andWhere(['object_id' => $record->id])->andWhere(['status' => Request::STATUS_ACTIVE])->andWhere(['type' => 'cancel_coupon'])->one();
                if (!$cancelRequest) {
                    
                } else {

                    if (isset($cancelRequest->id)) {
                        $cancelRequest->status = Request::STATUS_REJECTED;
                        $cancelRequest->remarks = $cancelRequest->remarks . "<br> Coupon Redeemed Successfully, Coupon Cancellation request rejected";
                        $cancelRequest->update();
                    }
                }

                $this->sendSuccess(["Voucher Redeemed Successfully !"]);
                //\common\components\GenXHelper::c($model);    
                return $this->redirect(['coupon-code/index']);
            }
        } else {
            $this->sendErrors(["checkmerchent" => ["Your Voucher Code is Invalid or Wrong Merchant User."]]);
            return $this->redirect(['coupon-code/index']);
        }
    }

   public function actionMultipalCouponRedeemed()
    {
//        var_dump(Yii::$app->request->post());
//        exit();
//        GenXHelper::c(Yii::$app->request->post());
        if (Yii::$app->user->isAdmin() && Yii::$app->request->post())
        {

            $couponRedeemed = Yii::$app->request->post();
            if ($couponRedeemed['couponredeemed']) {
                $coupon = explode(",", $couponRedeemed['couponredeemed']);
                $flag = 1;
                $error = "";
                
                
               foreach ($coupon as $couponValue) {
                    if (trim($couponValue) != "") {
                        //echo $couponValue;
                        $record = CouponCode::find()->andWhere(['coupon_no' => trim($couponValue)])->andWhere(['status' => CouponCode::STATUS_ACTIVE])->andWhere(['>', 'expires_at', time()])->one();

                        if (!$record) {
                            $flag = 0;
                            $error .= " $couponValue is in-valid<br>";
                        }
                    }
                    else{
                        
                         $flag = 0;
                         $error .= "Fill Voucher No<br>";
                    }
                }

                if ($flag) {
                    $loop = 0;
                    $count = 0;
                    foreach ($coupon as $couponValue) {
                      $count += count($couponValue);
                        if (trim($couponValue) != "") {
                            //echo $couponValue;
                            $record = CouponCode::find()->andWhere(['coupon_no' => trim($couponValue)])->andWhere(['status' => CouponCode::STATUS_ACTIVE])->andWhere(['>', 'expires_at', time()])->one();

                            if ($record) {

                                $totalredeem = $record->cart->dealOption->total_redeem;
                                $qty = $record->cart->qty;


                                $totalredeem = $totalredeem + $qty;
                                $record->cart->dealOption->total_redeem = $totalredeem;
                                $record->cart->dealOption->update();

                                $record->status = CouponCode::STATUS_REDEEMED;
                                $record->update();
                                $error .= " $couponValue - Voucher Redeemed Successfully<br>";
                                // add credit
                                $flag =2;
                                if ($record->cart->deal->deal_credit > 0) {

                                    // add credit to user account
                                    $creditamount = round((($record->cart->net_price * $record->cart->deal->deal_credit) / 100), 2);

                                    $user_account = new \common\models\UserAccount();
                                    $user_account->cart_id = $record->cart->id;
                                    $user_account->user_id = $record->cart->user_id;
                                    $user_account->order_id = $record->id;
                                    $user_account->transaction_id = "add_credit_redeemed_coupon " . $record->id;
                                    $user_account->debit = 0;
                                    $user_account->remarks = "Credit against coupon";
                                    $user_account->credit = $creditamount;
                                    if ($user_account->save()) {
                                        
                                    }
                                }else{
                                   /*  Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                                   Yii::$app->response->statusCode = 411;
                    
                                $flag = 0;
                                $error .= "Deal Credit Less than<br>";
                                return $error;*/
                                }
                                
                                
                                // sene email to merchant 
                                \common\components\GenXMailer::start()->sendMerchantAfterRedeemed($record->cart);
                                // send email to customer
                                \common\components\GenXMailer::start()->sendCustomerAfterRedeemed($record->cart,$record->cart->user->email);                              
                        

//$url=GenXHelper::encryptUserLink($record->cart->user_id,  \yii\helpers\Url::toRoute(["../user/rating","cart_id"=>$record->id,"user_id"=>$record->cart->user_id],TRUE),"../user/encrypted-link" );



                                $cancelRequest = Request::find()->andWhere(['object_id' => $record->id])->andWhere(['status' => Request::STATUS_ACTIVE])->andWhere(['type' => 'cancel_coupon'])->one();
                                if (!$cancelRequest) {
                                    
                                } else {

                                    if (isset($cancelRequest->id)) {
                                        $cancelRequest->status = Request::STATUS_REJECTED;
                                        $cancelRequest->remarks = $cancelRequest->remarks . "<br> Coupon Redeemed Successfully, Coupon Cancellation request rejected";
                                        $cancelRequest->update();
                                    }
                                }
                            }
//                            else{
//                                 $flag = 3; 
//                            }
                        }
                          ++$loop;
                    }
                    
                    
                    
                    if($flag==2){
                              Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                              Yii::$app->response->statusCode = 400;
                                return $error;
                                exit();
                               
                    }
//                    if($flag==3){
//                            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//                                   Yii::$app->response->statusCode = 411;
//                    
//                                $error .= " $couponValue is in-Valid<br>".$count;
//                                return $error;
//                    }
                    //$this->sendSuccess(['Voucher Redeemed Successfully ! ']);
                } else {
                    
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    Yii::$app->response->statusCode = 411;
                    substr($error, 0, -1);
                    return $error;
                }
            
            } else {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                Yii::$app->response->statusCode = 411;
                return "Coupon Can't Blank";
            }
        
          
            
        }    
        //(Yii::$app->request->post() && Yii::$app->user->merchant()->id) 
      else if(Yii::$app->request->post() && Yii::$app->user->merchant()->id){
          
          //echo Yii::$app->user->merchant()->id;
            $couponRedeemed = Yii::$app->request->post();
            // GenXHelper::c($couponRedeemed);
            if ($couponRedeemed['couponredeemed']) {
                $coupon = explode(",", $couponRedeemed['couponredeemed']);
                $flag = 1;
                $error = "";
                foreach ($coupon as $couponValue) {
                    if (trim($couponValue) != "") {
                        //echo $couponValue;
                        $record = CouponCode::find()->andWhere(['coupon_no' => trim($couponValue)])->andWhere(['status' => CouponCode::STATUS_ACTIVE])->andWhere(['>', 'expires_at', time()])->one();

                        if ($record) {
                            if ($record->cart->merchant_id != Yii::$app->user->merchant()->id) {
                                $flag = 0;
                                $error .= " $couponValue Merchant " . Yii::$app->user->merchant()->id . $record->cart->merchant_id . "  is in-valid<br>";
                            }
                        } else {
                            $flag = 0;
                            $error .= " $couponValue is in-valid<br>";
                        }
                    }
                }

                if ($flag) {


                    foreach ($coupon as $couponValue) {
                        if (trim($couponValue) != "") {
                            //echo $couponValue;
                            $record = CouponCode::find()->andWhere(['coupon_no' => trim($couponValue)])->andWhere(['status' => CouponCode::STATUS_ACTIVE])->andWhere(['>', 'expires_at', time()])->one();
                                
                            if ($record) {

                                $totalredeem = $record->cart->dealOption->total_redeem;
                                $qty = $record->cart->qty;


                                $totalredeem = $totalredeem + $qty;
                                $record->cart->dealOption->total_redeem = $totalredeem;
                                $record->cart->dealOption->update();

                                $record->status = CouponCode::STATUS_REDEEMED;
                                $record->update();
                                // add credit

                                if ($record->cart->deal->deal_credit > 0) {

                                    // add credit to user account
                                    $creditamount = round((($record->cart->net_price * $record->cart->deal->deal_credit) / 100), 2);

                                    $user_account = new \common\models\UserAccount();
                                    $user_account->cart_id = $record->cart->id;
                                    $user_account->user_id = $record->cart->user_id;
                                    $user_account->order_id = $record->id;
                                    $user_account->transaction_id = "add_credit_redeemed_coupon " . $record->id;
                                    $user_account->debit = 0;
                                    $user_account->remarks = "Credit against coupon";
                                    $user_account->credit = $creditamount;
                                    if ($user_account->save()) {
                                        
                                    }
                                }
                                
                                $error .= " $couponValue - Voucher Redeemed Successfully<br>";
                                // sene email to merchant 
                                \common\components\GenXMailer::start()->sendMerchantAfterRedeemed($record->cart);
                                // send email to customer
                                \common\components\GenXMailer::start()->sendCustomerAfterRedeemed($record->cart,$record->cart->user->email);
                                
                                

                                $cancelRequest = Request::find()->andWhere(['object_id' => $record->id])->andWhere(['status' => Request::STATUS_ACTIVE])->andWhere(['type' => 'cancel_coupon'])->one();
                                if (!$cancelRequest) {
                                    
                                } else {

                                    if (isset($cancelRequest->id)) {
                                        $cancelRequest->status = Request::STATUS_REJECTED;
                                        $cancelRequest->remarks = $cancelRequest->remarks . "<br> Coupon Redeemed Successfully, Coupon Cancellation request rejected";
                                        $cancelRequest->update();
                                    }
                                }
                            }
                        }
                    }
                   // $this->sendSuccess(['Voucher Redeemed Successfully ! ']);
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    //$error .= " Voucher Redeemed Successfully<br>";
                    Yii::$app->response->statusCode = 400;
                    substr($error, 0, -1);
                    return $error;
                    exit();
                } else {
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    Yii::$app->response->statusCode = 411;
                    substr($error, 0, -1);
                    return $error;
                }
            } else {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                Yii::$app->response->statusCode = 411;
                return "Coupon Can't Blank";
            }
        } 
        else {


            return $this->render('merchant_redeemed');
        }
    }

   public function actionEncryptedLink()
    {

        $get = \Yii::$app->request->get();
        $ts = $get['t'];
        //$hash = $get['h'];
        $random_1 = $get['r1'];
        $random_2 = $get['r2'];
        $data = $get['d'];

        if (time() > $random_1) {
            throw new \yii\web\ForbiddenHttpException("Expired");
        }

        //$pass_token = base64_decode($hash);
        //$user = User::findByPasswordResetToken($pass_token);
//        if (!$user) {
//            throw new \yii\web\NotFoundHttpException("Invalid Hash");
//        }
//        $user->generatePasswordResetToken();
//        $user->update(False);

        $redirect_url = base64_decode($data);
        return $this->redirect($redirect_url);
    }

    public function actionUnRedeemCoupon($id)
    {
        $model = $this->findModel($id);

        if ($model->cart->merchant_id == Yii::$app->user->merchant()->id) {
            $model->status = CouponCode::STATUS_ACTIVE;
            $model->update();
            // Url::toRoute(['coupon-code/redeem-coupon', 'id' => $data->id]);
            // Url::toRoute(['coupon-code/un-redeem-coupon', 'id' => $data->id])  
            return $this->redirect(['coupon-code/index']);
        } else {
            throw new \yii\web\ForbiddenHttpException();
        }
    }
    public function actionExtendCouponExpDate(){
        $post = Yii::$app->request->post();
        $id = $post['id'];
        $model = CouponCode::findOne($id);
        $cartModel = \common\models\Cart::findOne($model->cart_id);
        $dealModel = \common\models\Deal::findOne($cartModel->deal_id);
        
        return $this->renderAjax("get-modal", ['model' => $model, 'cart_id'=> $cartModel, 'dealModel' => $dealModel]);
    }
    public function actionExtendCouponExpiryAction(){
        $post = Yii::$app->request->post();
       
        $model = CouponCode::findOne($post['id']);
        $model->expires_at = strtotime($post['expire_time']);
        if($model->save()){
            return $this->redirect('index');
        }else{
            var_dump($model->getErrors());
            
        }
    }
    public function actionCartTracking(){
	 
	 $post = Yii::$app->request->post();
        $id = $post['cart_id'];

	    $model = \common\models\TrackCart::findOne(['cart_id'=>$id]);
         if (!$model) {
          $model = new \common\models\TrackCart();
         }
        //$model = CouponCode::findOne($id);

        $cartModel = \common\models\Cart::findOne($id);
        //print_r($cartModel->carttrack);
        //exit();
        $dealModel = \common\models\Deal::findOne($cartModel->deal_id);

        return $this->renderAjax("cart-track-popup", ['trackdata'=> $model, 'cart_id'=> $cartModel, 'dealModel' => $dealModel]);
    }
    
    public function actionCartTrackingAction(){
     	if($post = Yii::$app->request->post())
            {                
                if($post['TrackCart']['id']!=''){
                    $model = \common\models\TrackCart::findOne($post['TrackCart']['id']);
                }
                else{
                    $model = new \common\models\TrackCart();
                }
                $model->cart_id = $post['TrackCart']['cart_id'];
                $model->company_name = $post['TrackCart']['company_name'];
                $model->track_id = $post['TrackCart']['track_id'];
                $model->url = $post['TrackCart']['url'];
                if ($model->save()){
                    Yii::$app->session->setFlash('success', "Cart Traking Added");
                    return $this->redirect('index');
                }
                else{
                    Yii::$app->session->setFlash('error', "Opp's! Cart Traking Not Added");
                    return $this->redirect('index');
                }
            }
            else{
                var_dump($model->getErrors());
            } 
    }
    /**
     * Deletes an existing CouponCode model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CouponCode model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CouponCode the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $couponQuery = CouponCode::find();
        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $couponQuery->joinWith('cart');
            $couponQuery->andWhere(['cart.merchant_id' => Yii::$app->user->getLoggedinID()]);
            //$couponQuery->andWhere(['cart.merchant_id' => Yii::$app->user->getLoggedinID()]);
        }
        $couponQuery->andWhere(['coupon_code.id' => $id]);

        if (($model = $couponQuery->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
