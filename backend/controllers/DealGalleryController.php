<?php

namespace backend\controllers;

use Yii;
use common\models\Deal;
use common\models\DealGallery;
use common\models\DealGallerySearch;
use backend\controllers\GenxUseAdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Url;
use common\models\User;

/**
 * DealGalleryController implements the CRUD actions for DealGallery model.
 */
class DealGalleryController extends GenxUseAdminController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $parent_behavior = parent::behaviors();
        $parent_behavior['access']['rules'][] = [
            'allow' => True,
            'actions' => [],
            'roles' => [User::ROLE_MERCHANT]
        ];

        return $parent_behavior;
    }

    /**
     * Lists all DealGallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DealGallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DealGallery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DealGallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($deal_id)
    {
        $deal_model = Deal::findOne($deal_id);
        if (!$deal_model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model = new DealGallery();
        $model->deal_id = $deal_model->id;

        $searchModel = new DealGallerySearch();
        $dataProvider = $searchModel->search([]);
        $dataProvider->query->andWhere(['deal_id' => $model->deal_id])->orderBy("sort_order asc");

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $model->name = $model->imageFile->getBaseName();
            if ($name = $model->upload()) {
                $model->name = $name;
                if (!$model->save(False)) {
                    $this->sendErrors($model->getErrors());
                }else{
                    if (!Yii::$app->user->isAdmin()) {
                             $deal_model->disable();
                }
            $this->sendSuccess(["Deal photo uploaded successfully."]);
                }
                
            }

            return $this->refresh();
        }

        return $this->render('create', [
                    'model' => $model,
                    'deal_model' => $deal_model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing DealGallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
       
        $checkupdate=Yii::$app->request->post();
        $model = $this->findModel($id);
        $deal_model=$model->deal;
        //$deal_model = Deal::findOne($model->deal_id);
        $searchModel = new DealGallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['deal_id' => $model->deal_id])->orderBy("sort_order asc");

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($name = $model->upload()) {
                $model->name = $name;
            }

            if (!$model->save(False)) {
                $this->sendErrors($model->getErrors());
            } else {
                if (!Yii::$app->user->isAdmin()) {
                    
                 $deal_model->disable();
            }
            
            $this->sendSuccess(["Deal photo uploaded successfully."]);
            
             if(isset($checkupdate['update_id'])) {
               return $this->redirect(['create', 'deal_id' => $model->deal_id,'update_id'=>1]); 
            } 
            else{
                return $this->redirect(['create', 'deal_id' => $model->deal_id]);
            }
            }
        }

        return $this->render('update', [
                    'model' => $model,
                    'deal_model' => $deal_model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Deletes an existing DealGallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $dealgallery = $this->findModel($id);
        //echo $dealgallery->deal_id;
        //$deal=$dealgallery->deal;
        //    \common\components\GenXHelper::c($dealgallery);   
        $dealgallery->delete();
        return $this->redirect(Url::toRoute(["deal-gallery/create", 'deal_id' => $dealgallery->deal_id]));
    }

    /**
     * Finds the DealGallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DealGallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DealGallery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
