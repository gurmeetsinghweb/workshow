<?php

namespace backend\controllers;

use Yii;
use common\models\Category;
use common\models\CategorySearch;
use backend\controllers\GenxUseAdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends GenxUseAdminController
{

    public function behaviors()
    {
        $behaviours = parent::behaviors();
        $behaviours['access']['rules'][] = [
            'allow' => True,
            'actions' => [],
            'roles' => [User::ROLE_ADMIN]
        ];

        return $behaviours;
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $slug = Null, $city = Null)
    {
        $model = $this->findModel($id);
        $view_vars['model'] = $model;
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere(['parent_id' => $model->id]);
        $view_vars['searchModel'] = $searchModel;
        $view_vars['dataProvider'] = $dataProvider;
        return $this->render('view', $view_vars);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();
        $model->parent_id = Yii::$app->request->get('parent_id', 0);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->sendSuccess(['Category updated successfully.']);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionTree()
    {
        $category_model = new Category;
        return $this->render("tree", ["data" => $category_model->getTree()]);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
    
    public function actionAutocomplete($term)
    {
        $term = trim($term);
        if (!strlen($term)) {
            echo json_encode([]);
        }

        $catSearch = new Category();
        $user_data=Category::findAll(['parent_id'=>0]);
        $return = [];
        if (sizeof($user_data)) {
            foreach ($user_data as $user) {
                if(!$user) {
                    continue;
                }
                $label = $user->name;
                $return[] = [
                    'label' => $label,
                    'name' => $user->name,
                    'id' => $user->id,
                ];
            
             }
        }
        echo json_encode($return);
    }

}
