<?php

namespace backend\controllers;

use Yii;
use common\models\TransactionLog;
use common\models\TransactionLogSearch;
use common\models\TransactionPayment;
use common\models\Cart;
use common\models\CartSearch;
use common\models\CategorySearch;
use backend\controllers\GenxUseAdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;
use yii2tech\csvgrid\CsvGrid;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class AccountController extends GenxUseAdminController
{

    public function behaviors()
    {
        $behaviours = parent::behaviors();
        $behaviours['access']['rules'][] = [
            'allow' => True,
            'actions' => [],
            'roles' => [User::ROLE_ADMIN]
        ];

        return $behaviours;
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        
        // $totalmetchant = "count(cart.merchant_total_payment) as total_merchant";
        $totalmetchantpending = "round(sum(if(cart.transaction_log_id is NULL,cart.merchant_total_payment,0)),2) as merchant_total_payment";
        $totalcompanyprofit = "round(sum(if(cart.transaction_log_id is  NULL,cart.site_amount_excluding_gst,0)),2) as site_amount_excluding_gst";
        $total_company_tax = "round(sum(if(cart.transaction_log_id is  NULL,cart.site_gst,0)),2) as site_gst";
        
        
        $search_options['select'] = [
            "cart.id",
            "cart.merchant_id",
            "cart.deal_id",
            $totalmetchantpending,
            $totalcompanyprofit,
            $total_company_tax
          
           
        ];
        
        
      
            // payment slab
            $paymentgiven= \yii::$app->params['payment_given']; 
            $getdate=date('d');
            if($getdate > $paymentgiven[0] and $getdate <= $paymentgiven[1])
            { 
                $day= date('d',strtotime('last day of previous month'));
             
                $slab=1;
                $dateslab = mktime(23,59,59,date('m')-1,$day,date('Y'));
                //date("Y-n-j G:i:s", mktime(23,59,59,date('m')-1,$day,date('Y')));
                
              
              }
            else{
                
                $slab=2;
                $dateslab= mktime(23,59,59,date('m'),15,date('Y'));
                //date("Y-n-j G:i:s", mktime(23,59,59,date('m')-1,15,date('Y')));
               
              }
               
             
           $search_options['andWhere']=[
            
            ['is','cart.transaction_log_id', null],
            ['coupon_code.status' => \common\components\GenxBaseModel::STATUS_REDEEMED],
            ['<', 'coupon_code.updated_at', $dateslab]
        ];
        
        $search_options['groupby']=['cart.merchant_id'];
        $search_options['all']=1;
        $search_options['dataprovider']=1;
        $dataModel = \common\components\GenXHelper::accountSum($search_options);
        
        $search_options['all']=0;
        unset($search_options['groupby']);
        
        $models = \common\components\GenXHelper::accountSum($search_options);
          
            
            
          /*  $dataModel->query
                  ->select($search_options['select'])
                 ->andWhere(['is','cart.transaction_log_id', null])
                 ->andWhere(['coupon_code.status' => \common\components\GenxBaseModel::STATUS_REDEEMED])
                 ->andWhere(['<', 'coupon_code.updated_at', time()])
                 ->groupby(['cart.merchant_id']);
                 
           $dataModel->getModels();
          */ 
        //\common\components\GenXHelper::c($model);
        
        $metrics = [
            'total_merchant' => $models[0]['merchant_total_payment'],
            'total_company' => $models[0]['site_amount_excluding_gst'],
            'total_company_tax' => $models[0]['site_gst'],
            
        ];

        $cart_search = new CartSearch();
       /*
        $dataProvider = $cart_search->search([]);
        $dataProvider
                ->query
                ->select("cart.merchant_id,sum(cart.merchant_total_payment) as total_sale")
                ->andWhere(['IS', 'cart.transaction_log_id', null])
                ->groupby(['cart.merchant_id'])
                ->limit(0)
                ->all();

        $cart = $dataProvider->getModels();
        * 
        */

       //  \common\components\GenXHelper::c($cart);
        return $this->render('index', [
                    'dataProvider' => $dataModel,
                    'searchModel' => $cart_search,
                    'metrics' => $metrics,
        ]);
    }

    public function actionCreatePayout()
    {

        $model = new CartSearch();
        $cartModel = new Cart();

        $transactionPaymentModel = new TransactionPayment();
       
        if (Yii::$app->request->post()) {
            
            //Gurmeet Payment Slab
            // payment slab
            $paymentgiven= \yii::$app->params['payment_given']; 
            $getdate=date('d');
            if($getdate > $paymentgiven[0] and $getdate <= $paymentgiven[1])
            { 
                $day= date('d',strtotime('last day of previous month'));
             
                $slab=1;
                $dateslab = mktime(23,59,59,date('m')-1,$day,date('Y'));
                //date("Y-n-j G:i:s", mktime(23,59,59,date('m')-1,$day,date('Y')));
                
              
              }
            else{
                
                $slab=2;
                $dateslab= mktime(23,59,59,date('m')-1,15,date('Y'));
                //date("Y-n-j G:i:s", mktime(23,59,59,date('m')-1,15,date('Y')));
               
              }
              
            

            /*$dataProvider = $model->search([]);
            $dataProvider
                    ->query
                    ->select(["cart.deal_id", "cart.id", "cart.merchant_id", "sum(cart.merchant_total_payment) as merchant_total_payment", "sum(cart.merchant_amout_excluding_gst) as merchant_amout_excluding_gst", "sum(cart.merchant_gst) as merchant_gst",
                        "sum(cart.site_total_payment) as site_total_payment", "sum(cart.site_amount_excluding_gst) as site_amount_excluding_gst", "sum(cart.site_gst) as site_gst",
                        "sum(cart.net_price) as net_price", "sum(cart.deal_amout_excluding_gst) as deal_amout_excluding_gst", "sum(cart.deal_gst) as deal_gst"])
                    ->andWhere(['IS', 'cart.transaction_log_id', null])
                    ->groupby(['cart.merchant_id'])
                    ->limit(0)
                    ->all();
            $cart = $dataProvider->getModels();*/
            
          $search_options['select']=["cart.deal_id", "cart.id", "cart.merchant_id", "sum(cart.merchant_total_payment) as merchant_total_payment", "sum(cart.merchant_amout_excluding_gst) as merchant_amout_excluding_gst", "sum(cart.merchant_gst) as merchant_gst",
                        "sum(cart.site_total_payment) as site_total_payment", "sum(cart.site_amount_excluding_gst) as site_amount_excluding_gst", "sum(cart.site_gst) as site_gst",
                        "sum(cart.net_price) as net_price", "sum(cart.deal_amout_excluding_gst) as deal_amout_excluding_gst", "sum(cart.deal_gst) as deal_gst"];
          $search_options['andWhere']=[
            ['is','cart.transaction_log_id', null],
            ['coupon_code.status' => \common\components\GenxBaseModel::STATUS_REDEEMED],
            ['<', 'coupon_code.updated_at', $dateslab]
          ];
          $search_options['groupby']=['cart.merchant_id'];
          $search_options['all']=1;
          
         $cart = \common\components\GenXHelper::accountSum($search_options);
         
           if (is_array($cart) and sizeof($cart)) {

                if ($transactionPaymentModel->save()) {
                    
                    $transaction_payment_id = $transactionPaymentModel->id;
                } else {
                    \common\components\GenXHelper::DevErrors($transactionPaymentModel->getErrors());
                    return $this->redirect(['/account/']);
                }
                foreach ($cart as $key => $values) {

                    $transactionModel = new TransactionLog();
                    if ($transaction_payment_id) {

                        $transactionModel->merchant_id = $values->merchant_id;
                        $transactionModel->transaction_payment_id = $transaction_payment_id;
                        $transactionModel->total_merchant_amount = $values->merchant_total_payment;
                        $transactionModel->total_merchant_amout_excluding_gst = $values->merchant_amout_excluding_gst;
                        $transactionModel->total_merchant_gst = $values->merchant_gst;
                        $transactionModel->total_site_amount = $values->site_total_payment;
                        $transactionModel->total_site_amount_excluding_gst = $values->site_amount_excluding_gst;
                        $transactionModel->total_site_gst = $values->site_gst;
                        $transactionModel->total_amount = $values->net_price;
                        $transactionModel->total_amount_excluding_gst = $values->deal_amout_excluding_gst;
                        $transactionModel->total_amount_gst = $values->deal_gst;
                        if ($transactionModel->save()) {
                            
                       $sql="update `cart` LEFT JOIN `deal` ON `cart`.`deal_id` = `deal`.`id` LEFT JOIN `coupon_code` ON `cart`.`id` = `coupon_code`.`cart_id` set cart.transaction_payment_id = $transaction_payment_id, cart.transaction_log_id = $transactionModel->id, cart.transaction_log_time_created = ".time()."   WHERE (`cart`.`transaction_log_id` IS NULL) AND (`cart`.`merchant_id`=".$values->merchant_id.") and   (`coupon_code`.`status`=".\common\components\GenxBaseModel::STATUS_REDEEMED.") AND (`coupon_code`.`updated_at` < ".$dateslab.") AND (`deal`.`status` != 0) AND (`coupon_code`.`status` != 0)";     
                       $command = \Yii::$app->db->createCommand($sql);
                       $command->execute();  
                       
                            /*$condition=['and',
                                ['is','cart.transaction_log_id', null],
                                ['cart.merchant_id' => $values->merchant_id],
                                ['coupon_code.status' => \common\components\GenxBaseModel::STATUS_REDEEMED],
                                ['<', 'coupon_code.updated_at', time()]
                                ];
                                
                        
                            $cartModel->updateAll(['transaction_payment_id' => $transaction_payment_id, "transaction_log_id" => $transactionModel->id, "transaction_log_time_created" => time()], $condition);
                             * 
                             */
                        } else {
                            \common\components\GenXHelper::DevErrors($transactionModel->getErrors());
                        }
                    }
                }

                \common\components\GenXHelper::DevErrors($cartModel->getErrors());
                \common\components\GenXHelper::DevErrors($transactionPaymentModel->getErrors());
                return $this->redirect(\yii\helpers\Url::toRoute("view"));
            } else {
                $this->sendErrors(['carterror' => ['No Records Available.']]);
                return $this->redirect(\yii\helpers\Url::toRoute("index"));
            }
        } else {
            return $this->redirect(\yii\helpers\Url::toRoute("/site/index"));
        }
    }

    public function actionView()
    {
        $number_of_days = 30;
        $end_time = time();
        $start_time = time() - (60 * 60 * 24 * $number_of_days);
        $date_set = False;
        if (Yii::$app->request->get()) {
            $POST = Yii::$app->request->get();
            if (isset($POST['time_range'])) {
                $posted_time = explode('-', $POST['time_range']);
                $end_time = $posted_time[1];
                $start_time = $posted_time[0];
            }
            if (isset($POST['date_range']) && !empty($POST['date_range'])) {
                $date_set = True;
                $posted_date = explode('-', $POST['date_range']);
                //url 
                 $end_time = str_replace("+"," ",$posted_date[1]);
                $start_time = str_replace("+"," ",$posted_date[0]);
                
                $end_time = (int) strtotime($end_time) + (60 * 60 * 24);
                $start_time = (int) strtotime($start_time);
            } else {
                $POST['date_range'] = date('m/d/Y', $start_time) . " - " . date('m/d/Y', $end_time);
            }
        } else {
            $POST['date_range'] = date('m/d/Y', $start_time) . " - " . date('m/d/Y', $end_time);
        }
        // if merchant 
       // \common\components\GenXHelper::($POST)
          if(isset($POST['merchant_id']) and !empty($POST['merchant_id'])){
            $search_options['andWhere'][]=
                        ['cart.merchant_id' => $POST['merchant_id']];
                   }

        if ($date_set) {
            $search_options['andWhere'][]=['between', 'cart.created_at', $start_time, $end_time];
            $search_options['andWhere'][]=['coupon_code.status' => \common\components\GenxBaseModel::STATUS_REDEEMED];
               
            
        } else {
            $search_options['andWhere'][]= 
                
                    ['coupon_code.status' => \common\components\GenxBaseModel::STATUS_REDEEMED];
                
            
        }
      //  \common\components\GenXHelper::c($search_options);
       // exit();
        $models = \common\components\GenXHelper::accountSum($search_options);
        $metrics = [
            'total_payment' => $models[0]['total_payment'],
            'total_merchant' => $models[0]['total_merchant'],
            'total_merchantgiven' => $models[0]['total_merchantgiven'],
            'total_company' => $models[0]['total_company'],
            'total_companygiven' => $models[0]['total_companygiven'],
            'total_companytax' => $models[0]['total_companytax'],
        ];
// Graph
        $transactionlogModel = new TransactionLogSearch();
        $dataProvider = $transactionlogModel->search([]);

        // sales Chart
        $dataProvider
                ->query
                ->select("transaction_log.id,transaction_log.created_at,transaction_log.transaction_payment_id, sum(transaction_log.total_merchant_amount) as total_merchant_amount, sum(transaction_log.total_site_amount) as total_site_amount");
        if ($date_set) {
            $dataProvider->query->andWhere(['between', 'transaction_log.created_at', $start_time, $end_time]);
        }
        if(isset($POST['merchant_id']) and !empty($POST['merchant_id'])){
            $dataProvider->query->andWhere(['transaction_log.merchant_id' =>$POST['merchant_id']]);
           
        }
        
        
        $dataProvider->query->groupBy('transaction_log.transaction_payment_id')
                ->orderBy('transaction_log.transaction_payment_id DESC');

        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $dataProvider->query->andWhere(['transaction_log.merchant_id' => Yii::$app->user->getLoggedinID()]);
        }


        $models = $dataProvider->getModels();
        $sale_labels = [];
        $merchant_data = [];
        $company_data = [];
        $merchant_company_sale = [
            'labels' => [],
            'datasets' => [
                [
                    'label' => 'Merchant Income',
                    'backgroundColor' => '#26B99A',
                ],
                [
                    'label' => 'Company  Income',
                    'backgroundColor' => '#5bc0de',
                ]
            ]
        ];

        if ($models) {
            foreach ($models as $data_model) {
                $sale_labels[] = date('j M', $data_model->created_at);
                $merchant_data[] = $data_model->total_merchant_amount;
                $company_data[] = $data_model->total_site_amount;
            }

            $merchant_company_sale['labels'] = array_values($sale_labels);
            $merchant_company_sale['datasets'][0]['data'] = array_values($merchant_data);
            $merchant_company_sale['datasets'][1]['data'] = array_values($company_data);
        }
        // \common\components\GenXHelper::c($models);
        //exit;
        return $this->render('view', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $transactionlogModel,
                    'merchant_company_sale' => $merchant_company_sale,
                    'metrics' => $metrics,
                    'number_of_days' => $number_of_days,
                    'POST' => $POST,
                    'date_set' => $date_set
        ]);
    }

    public function actionMerchantIncome($id)
    {

// Graph
        $transactionlogModel = new TransactionLogSearch();
        $dataProvider = $transactionlogModel->search([]);

        // sales Chart
        $dataProvider
                ->query
                ->select("transaction_log.*")
                ->andWhere(['transaction_log.transaction_payment_id' => $id])
                ->orderBy('transaction_log.id')
                ->limit(0);
           if(Yii::$app->request->get("merchant_id")){
               $dataProvider->query->andWhere(['transaction_log.merchant_id'=>Yii::$app->request->get("merchant_id")]);
               
           }     



        $models = $dataProvider->getModels();
        $sale_labels = [];
        $merchant_data = [];
        $company_data = [];
        $merchant_company_sale = [
            'labels' => [],
            'datasets' => [
                [
                    'label' => 'Merchant Income',
                    'backgroundColor' => '#26B99A',
                ],
            ]
        ];

        if ($models) {
            foreach ($models as $data_model) {
                $sale_labels[] = $data_model->merchant->name;
                $merchant_data[] = $data_model->total_merchant_amount;
            }

            $merchant_company_sale['labels'] = array_values($sale_labels);
            $merchant_company_sale['datasets'][0]['data'] = array_values($merchant_data);
        }
        // \common\components\GenXHelper::c($models);
        //exit;
        return $this->render('incomedetails', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $transactionlogModel,
                    'merchant_company_sale' => $merchant_company_sale
        ]);
    }
     
    public function  actionExcelDownload($id){
        
        
        
    }

    
    public function actionCompanyIncome($id)
    {

// Graph
        $transactionlogModel = new TransactionLogSearch();
        $dataProvider = $transactionlogModel->search([]);

        // sales Chart
        $dataProvider
                ->query
                ->select("transaction_log.*")
                ->andWhere(['transaction_log.transaction_payment_id' => $id])
                ->orderBy('transaction_log.id')
                ->limit(0)
                ->all();



        $models = $dataProvider->getModels();
        $sale_labels = [];
        $merchant_data = [];
        $company_data = [];
        $merchant_company_sale = [
            'labels' => [],
            'datasets' => [
                [
                    'label' => 'Company Income',
                    'backgroundColor' => '#5bc0de',
                ],
            ]
        ];

        if ($models) {
            foreach ($models as $data_model) {
                $sale_labels[] = $data_model->merchant->name;
                $merchant_data[] = $data_model->total_site_amount;
            }

            $merchant_company_sale['labels'] = array_values($sale_labels);
            $merchant_company_sale['datasets'][0]['data'] = array_values($merchant_data);
        }
        // \common\components\GenXHelper::c($models);
        //exit;
        return $this->render('companyincome', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $transactionlogModel,
                    'merchant_company_sale' => $merchant_company_sale
        ]);
    }

    // merchant company

    public function actionMerchantAccount()
    {

        // Graph
        $number_of_days = 30;
        $end_time = time();
        $start_time = time() - (60 * 60 * 24 * $number_of_days);
        $date_set = False;
        if (Yii::$app->request->get()) {
            $POST = Yii::$app->request->get();
            if (isset($POST['time_range'])) {
                $posted_time = explode('-', $POST['time_range']);
                $end_time = $posted_time[1];
                $start_time = $posted_time[0];
            }
            if (isset($POST['date_range'])) {
                $date_set = True;
                $posted_date = explode('-', $POST['date_range']);
                $end_time = (int) strtotime($posted_date[1]) + (60 * 60 * 24);
                $start_time = (int) strtotime($posted_date[0]);
            } else {
                $POST['date_range'] = date('m/d/Y', $start_time) . " - " . date('m/d/Y', $end_time);
            }
        } else {
            $POST['date_range'] = date('m/d/Y', $start_time) . " - " . date('m/d/Y', $end_time);
        }

        if ($date_set) {
            $andWhere = [
                'andWhere' => [
                    ['between', 'cart.created_at', $start_time, $end_time]
                ]
            ];
        } else {
            $andWhere = [];
        }

        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $andWhere = ['andWhere' => [['cart.merchant_id' => Yii::$app->user->getLoggedinID()]]];
        }

        if (isset($POST['merchant_id'])) {
            $andWhere = [
                'andWhere' => [
                    ['cart.merchant_id' => $POST['merchant_id']]
                ]
            ];
        }


        $totalmetchant = "sum(cart.merchant_total_payment) AS total_merchant";
        $totalmetchantpending = "sum(if(cart.transaction_log_id is NULL,cart.merchant_total_payment,0)) AS total_merchantpending";
        $totalmetchantgiven = "sum(if(cart.transaction_log_id is not NULL,cart.merchant_total_payment,0)) AS total_merchantgiven";


        $andWhere['select'] = [
            "cart.*",
            $totalmetchant,
            $totalmetchantpending,
            $totalmetchantgiven
        ];

        $models = \common\components\GenXHelper::accountSum($andWhere);

        $metrics = [
            'total_merchant' => (float) $models[0]['total_merchant'],
            'total_merchantgiven' => (float) $models[0]['total_merchantgiven'],
            'total_merchantpending' => (float) $models[0]['total_merchantpending'],
        ];
// Graph
        $transactionlogModel = new TransactionLogSearch();
        $dataProvider = $transactionlogModel->search([]);

        // sales Chart

        $dataProvider
                ->query
                ->select("transaction_log.id,transaction_log.transaction_payment_id, sum(transaction_log.total_merchant_amount) as total_merchant_amount, sum(transaction_log.total_site_amount) as total_site_amount");
        if ($date_set) {
            $dataProvider->query->andWhere(['between', 'transaction_log.created_at', $start_time, $end_time]);
        }
        $dataProvider->query->groupBy('transaction_log.transaction_payment_id')
                ->orderBy('transaction_log.transaction_payment_id DESC');

        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $dataProvider->query->andWhere(['transaction_log.merchant_id' => Yii::$app->user->getLoggedinID()]);
        }
        if (isset($POST['merchant_id'])) {

            $dataProvider->query->andWhere(['transaction_log.merchant_id' => $POST['merchant_id']]);
        }


        $models = $dataProvider->getModels();
        $sale_labels = [];
        $merchant_data = [];
        $company_data = [];
        $merchant_company_sale = [
            'labels' => [],
            'datasets' => [
                [
                    'label' => 'Merchant Income',
                    'backgroundColor' => '#26B99A',
                ]
            ]
        ];

        if ($models) {
            foreach ($models as $data_model) {
                $sale_labels[] = date('j M', $data_model->created_at);
                $merchant_data[] = (float) $data_model->total_merchant_amount;
            }

            $merchant_company_sale['labels'] = array_values($sale_labels);
            $merchant_company_sale['datasets'][0]['data'] = array_values($merchant_data);
        }
        // \common\components\GenXHelper::c($models);
        //exit;
        return $this->render('merchant-income', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $transactionlogModel,
                    'merchant_company_sale' => $merchant_company_sale,
                    'metrics' => $metrics,
                    'number_of_days' => $number_of_days,
                    'POST' => $POST,
                    'date_set' => $date_set
        ]);
    }

    public function actionCsv()
    {
        $exporter = new CsvGrid([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => Cart::find(),
                'pagination' => [
                    'pageSize' => 100, // export batch size
                ],
                    ]),
        ]);
//        echo $exporter->export()->getResultFileName();exit;
        $saved_file = $exporter->export();
        $filecsvpath = \Yii::getAlias('@image_uploads/csv/text.csv');
        \common\components\GenXHelper::c($saved_file->saveAs($filecsvpath));exit;
    }

}
