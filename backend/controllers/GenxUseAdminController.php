<?php

namespace backend\controllers;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;

/**
 * Description of GenxUseAdminController
 *
 * @author Genx
 */
class GenxUseAdminController extends \common\components\GenxBaseController
{
    /*
     * To show page header in admin panel
     */

    public $show_page_header = True;
    public $show_title = True;
    public $merchant_company = Null;

    public function __construct($id, $module, $config = array())
    {
        parent::__construct($id, $module, $config);
        try {
            // if is merchant login, then check if he/she has filled the company details
            if (\Yii::$app->user->isOnlyMerchant()) {
                
            }
        } catch (Exception $ex) {
            
        }
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => True,
                        'actions' => [],
                        'roles' => [
                            User::ROLE_SYSADMIN
                        ]
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionActivateModel($id, $return_url)
    {
        $model = $this->findModel($id);
        // verify access
        $model->status = \common\components\GenxBaseModel::STATUS_ACTIVE;
        if (!$model->save()) {
            \common\components\GenXHelper::DevErrors($model->getErrors());
        }

        return $this->redirect($return_url);
    }

    public function actionDeactivateModel($id, $return_url)
    {
        $model = $this->findModel($id);

        $model->status = \common\components\GenxBaseModel::STATUS_DISABLED;
        $model->save();

        return $this->redirect($return_url);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fontFile' => '@backend/views/_partials/coolvetica_rg.ttf',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

}
