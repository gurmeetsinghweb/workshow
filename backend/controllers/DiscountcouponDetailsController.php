<?php

namespace backend\controllers;

use Yii;
use backend\controllers\GenxUseAdminController;
use common\models\DiscountcouponDetails;
use common\models\DiscountcouponDetailsSearch;
use common\models\DiscountcouponCategory;
use common\models\DiscountcouponDeal;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\User;

/**
 * DiscountcouponDetailsController implements the CRUD actions for DiscountcouponDetails model.
 */
class DiscountcouponDetailsController extends GenxUseAdminController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $parent_behavior = parent::behaviors();
        $parent_behavior['access']['rules'][] = [
            'allow' => True,
            'actions' => [],
            'roles' => [User::ROLE_ADMIN]
        ];
        return $parent_behavior;
    }

    /**
     * Lists all DiscountcouponDetails models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DiscountcouponDetailsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DiscountcouponDetails model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DiscountcouponDetails model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DiscountcouponDetails();

        if ($model->load(Yii::$app->request->post())) {

            $model->imageFile = UploadedFile::getInstance($model, 'coupon_img');
            if ($model->imageFile) {
                $model->coupon_img = $model->imageFile->getBaseName();
                if ($coupon_img = $model->upload()) {
                    $model->coupon_img = $coupon_img;
                }
            }
            if (!$model->save()) {
                $this->sendErrors($model->getErrors());
            } else {
                $this->sendSuccess(['Coupon Created Successfully.']);
            }


            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            $this->sendErrors($model->getErrors());
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DiscountcouponDetails model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->imageFile = UploadedFile::getInstance($model, 'coupon_img');
            if ($model->imageFile) {
                $model->coupon_img = $model->imageFile->getBaseName();
                if ($coupon_img = $model->upload()) {
                    $model->coupon_img = $coupon_img;
                }
            } else {
                $model->coupon_img = $model->getOldAttribute("coupon_img");
            }
            if (!$model->save()) {
                $this->sendErrors($model->getErrors());
            }
            $this->sendSuccess(['Coupon Updated Successfully.']);
            return $this->redirect('index');
        } else {
            return $this->render('update', [
                        'model' => $model
            ]);
        }
    }

    public function actionCategoryCreate($discount_id)
    {
        // $model = $this->findModel($id);
        $model = new DiscountcouponCategory();
        $searchModel = new \common\models\DiscountcouponCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['discount_id' => $discount_id]);

        if (Yii::$app->request->post()) {

            $categoryvalue = Yii::$app->request->post();
            if (DiscountcouponCategory::find()
                            ->andWhere(['category_id' => $categoryvalue['category']['id']])
                            ->andWhere(['discount_id' => $categoryvalue['discount-id']])
                            ->exists()) {

                $this->sendErrors(['Exist' => ['This category is already available.']]);
                //return $this->redirect(\yii\helpers\Url::toRoute(['category-create', 'id' => $categoryvalue['discount-id']]));
            } else {
                $model->category_id = $categoryvalue['category']['id'];
                $model->discount_id = $discount_id;
                if ($model->save()) {
                    $discountModel = $this->findModel($discount_id);
                    $discountModel->category = 1;
                    $discountModel->save();


                    $this->sendSuccess(['Record added successfully.']);
                    //return $this->redirect(\yii\helpers\Url::toRoute(['category-create', 'id' => $categoryvalue['discount-id']]));
                } else {
                    \common\components\GenXHelper::c($model->getErrors());
                    exit();
                }
            }
        }

        return $this->render('category-create', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider
        ]);




        //  $ccmodel=new DiscountcouponCategory();
        //\common\components\GenXHelper::c(\Yii::$app->request->post());
        //      return $this->redirect(\yii\helpers\Url::toRoute(['update', 'id' => $categoryvalue['discount-id'], 'selected_tab' => 'coupon-category']));
    }

    // category Update
    public function actionCategoryUpdate($id, $discount_id)
    {
        $model = DiscountcouponCategory::findOne($id);
        // $model = new DiscountcouponCategory();
        $searchModel = new \common\models\DiscountcouponCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['discount_id' => $discount_id]);

        if (Yii::$app->request->post()) {

            $categoryvalue = Yii::$app->request->post();
            if (DiscountcouponCategory::find()
                            ->andWhere(['category_id' => $categoryvalue['category']['id']])
                            ->andWhere(['discount_id' => $categoryvalue['discount-id']])
                            ->exists()) {

                $this->sendErrors(['Exist' => ['Change your category.']]);
                //return $this->redirect(\yii\helpers\Url::toRoute(['category-create', 'id' => $categoryvalue['discount-id']]));
            } else {

                $model->category_id = $categoryvalue['category']['id'];
                $model->discount_id = $discount_id;
                if ($model->save()) {
                    $this->sendSuccess(['Record updated successfully.']);
                    //return $this->redirect(\yii\helpers\Url::toRoute(['category-create', 'id' => $categoryvalue['discount-id']]));
                } else {
                    \common\components\GenXHelper::c($model->getErrors());
                }
            }
        }

        return $this->render('category-create', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider
        ]);




        //  $ccmodel=new DiscountcouponCategory();
        //\common\components\GenXHelper::c(\Yii::$app->request->post());
        //return $this->redirect(\yii\helpers\Url::toRoute(['update', 'id' => $categoryvalue['discount-id'], 'selected_tab' => 'coupon-category']));
    }

    public function actionCategoryDelete($id, $discount_id, $selected_tab)
    {
        DiscountcouponCategory::findOne($id)->delete();

        $checkifexist = DiscountcouponCategory::find()->andWhere(['discount_id' => $discount_id])->exists();
        if (!$checkifexist) {
            $discountModel = $this->findModel($discount_id);
            $discountModel->category = 0;
            $discountModel->save();
        }
        $this->sendSuccess(['Record Deleted successfully.']);
        $this->redirect(\yii\helpers\Url::toRoute(['category-create', 'discount_id' => $discount_id, 'selected_tab' => $selected_tab]));
    }

    public function actionDealCreate($discount_id)
    {
        $models = new DiscountcouponDeal();
        // $model = $this->findModel($id);

        $searchModel = new \common\models\DiscountcouponDealSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['discount_id' => $discount_id]);

        if (Yii::$app->request->post()) {
            $merchant = Yii::$app->request->post();
            $merchantDeal = \common\models\Deal::find()->andWhere(['user_id' => $merchant['merchant-id']])->all();
            $merchantdetail = \common\models\User::findOne($merchant['merchant-id']);

            if (isset($merchant['dealid'])) {
                $discountdealsModel = DiscountcouponDeal::find()->andWhere(['discount_id' => $discount_id])->all();
                $getDeal = [];
                foreach ($discountdealsModel as $dealid) {
                    $getDeal[] = $dealid->deal_id;
                }

                $discountModel = $this->findModel($discount_id);
                foreach ($merchant['dealid'] as $postDeal) {
                    $model = new DiscountcouponDeal();
                    if (!in_array($postDeal, $getDeal)) {
                        $model->deal_id = $postDeal;
                        $model->discount_id = $discount_id;
                        if ($model->save()) {
                            if ($discountModel->deal != 1) {
                                $discountModel->deal = 1;
                                $discountModel->save();
                            }
                        } else {
                            \common\components\GenXHelper::c($model->getErrors());
                            $this->sendErrors($model->getErrors());
                        }
                    }
                }
            }
        } else {
            $merchantDeal = 0;
            $merchantdetail = 0;
        }

        return $this->render('deal-create', [
                    'model' => $models,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'merchant_deal' => $merchantDeal,
                    'merchant_detail' => $merchantdetail
        ]);
    }

    public function actionDealDelete($id, $discount_id, $selected_tab)
    {
        DiscountcouponDeal::findOne($id)->delete();

        $checkifexist = DiscountcouponDeal::find()->andWhere(['discount_id' => $discount_id])->exists();
        if (!$checkifexist) {
            $discountModel = $this->findModel($discount_id);
            $discountModel->deal = 0;
            $discountModel->save();
        }

        $this->sendSuccess(['Record Deleted successfully.']);
        $this->redirect(\yii\helpers\Url::toRoute(['deal-create', 'discount_id' => $discount_id, 'selected_tab' => $selected_tab]));
    }

    /**
     * Deletes an existing DiscountcouponDetails model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DiscountcouponDetails model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DiscountcouponDetails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DiscountcouponDetails::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
