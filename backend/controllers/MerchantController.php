<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use backend\controllers\GenxUseAdminController;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class MerchantController extends GenxUseAdminController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $parent_behavior = parent::behaviors();
        $parent_behavior['access']['rules'][] = [
            'allow' => True,
            'actions' => [],
            'roles' => [User::ROLE_ADMIN]
        ];

        return $parent_behavior;
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['type' => User::ROLE_MERCHANT]);
        $dataProvider->query->orderBy("id desc");
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);
        
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $this->sendErrors($model->getErrors());
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            // $dealModel= new \common\models\Deal();
            $mymerchant = Yii::$app->request->post();
            if ($mymerchant['User']['status'] == 1) {
                \common\models\Deal::updateAll(['status' => \common\components\GenxBaseModel::STATUS_DISABLED], ['user_id' => $id]);
            }
            /* else{
              \common\models\Deal::updateAll(['status' =>\common\components\GenxBaseModel::STATUS_ACTIVE],['user_id'=>$id]);
              } */
            $model->save();

            $this->sendSuccess(["User Updated successfully."]);

            // \common\components\GenXHelper::c($model);
            //exit();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            if (Yii::$app->request->post()) {
                $this->sendErrors($model->getErrors());
            }

            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    public function actionMerchantAutocomplete($term)
    {
        $term = trim($term);
        if (!strlen($term)) {
            echo json_encode([]);
        }
        
        $userSearch = new UserSearch();
        $userSearch->name = $term;
        $userSearch->type = User::ROLE_MERCHANT;
        $user_data = $userSearch->search([])->getModels();
       
        $return = [];
        if (sizeof($user_data)) {
            foreach ($user_data as $user) {
                if (!$user) {
                    continue;
                }
                $label = $user->name;
                
                if ($user->company) {
                    $label .= " (Company: {$user->company->title})";

                    $return[] = [
                        'label' => $label,
                        'value' => $user->name,
                        'company_id' => (($user->company) ? (int) $user->company->id : 0),
                        'company_title' => (($user->company) ? (int) $user->company->title : 0),
                        'id' => $user->id,
                    ];
                }
            }
        }
 //       \common\components\GenXHelper::c($return);
 //       exit();
        echo json_encode($return);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
