<?php

namespace backend\controllers;

use Yii;
use common\models\SiteSettings;
use yii\data\ActiveDataProvider;
use backend\controllers\GenxUseAdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;

/**
 * SiteSettingsController implements the CRUD actions for SiteSettings model.
 */
class SiteSettingsController extends GenxUseAdminController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $parent_behavior = parent::behaviors();
        $parent_behavior['access']['rules'][] = [
            'allow' => True,
            'actions' => [],
            'roles' => [User::ROLE_ADMIN]
        ];
        return $parent_behavior;
    }

    public function beforeAction($action)
    {
        $parent = parent::beforeAction($action);
        if (!\Yii::$app->user->isAdmin()) {
            throw new \yii\web\ForbiddenHttpException();
        }
        return $parent;
    }

    /**
     * Lists all SiteSettings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => SiteSettings::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SiteSettings model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new SiteSettings();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->sendSuccess(["Created successfully."]);
            return $this->redirect(['index']);
        } else {
            $this->sendErrors($model->getErrors());
            return $this->render('create', [
                        'model' => $model,
                        'attribute' => 'all',
            ]);
        }
    }

    /**
     * Updates an existing SiteSettings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $attribute = Null)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->addFlash('success', "Updated successfully");
            return $this->redirect(['index']);
        } else {
            $this->sendErrors($model->getErrors());
            return $this->render('update', [
                        'model' => $model,
                        'attribute' => $attribute,
            ]);
        }
    }

    /**
     * Deletes an existing SiteSettings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SiteSettings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SiteSettings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SiteSettings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
