<?php

namespace backend\controllers;

use Yii;
use common\models\Request;
use common\models\RequestSearch;
use backend\controllers\GenxUseAdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;

/**
 * RequestController implements the CRUD actions for Request model.
 */
class RequestController extends GenxUseAdminController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $parent_behavior = parent::behaviors();
        $parent_behavior['access']['rules'][] = [
            'allow' => True,
            'actions' => [],
            'roles' => [User::ROLE_ADMIN]
        ];
        return $parent_behavior;
    }

    /**
     * Lists all Request models.
     * @return mixed
     */
    public function actionIndex($type, $title = Null)
    {
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['type' => $type])->orderBy(['id'=>SORT_DESC]);
       // $dataProvider->query->andWhere(['status' => RequestSearch::STATUS_ACTIVE]);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'title' => $title
        ]);
    }

    public function actionCc()
    {
        return $this->actionIndex('cancel_coupon', 'Coupon Cancellation Requests');
    }

    public function actionDealUpdate()
    {
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['type' => 'deal_update'])->orderBy(['id'=>SORT_DESC]);
       // $dataProvider->query->andWhere(['status' => RequestSearch::STATUS_ACTIVE]);
        return $this->render('deal_update', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'title' => 'Deal Request from Merchant'
        ]);
    }

    /**
     * Displays a single Request model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Request model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Request();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Request model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Request model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    // 
     public function actionSendRequestCCApprove() {
        $POST = Yii::$app->request->post();
      
      $model = $this->findModel($POST['id']);
         return $this->renderAjax("couponaccept_request", ['model' => $model]);

       
    }
    
    public function actionCCApprove()
    {
        $POST = Yii::$app->request->post();
       
        $model = $this->findModel($POST['id']);
        $coupon_class = $model->object_class;
        $coupon = $coupon_class::findOne($model->object_id);
        
        // Update Request Model
        
        // delete coupon
        $coupon_transacion = \common\models\CouponCode::getDb()->beginTransaction();
        $coupon->status = \common\models\CouponCode::STATUS_CANCLED;
        $coupon->update();

        // delete cart
        $cart_transacion = \common\models\Cart::getDb()->beginTransaction();
        $coupon->cart->status = \common\models\Cart::STATUS_CANCLED;
        $coupon->update();

        // add credit to user account
        $user_account = new \common\models\UserAccount();
        $user_account->cart_id = $coupon->cart->id;
        $user_account->user_id = $coupon->cart->user_id;
        $user_account->order_id = $coupon->cart->order_id;
        $user_account->transaction_id = "request_cancel_coupon_" . $model->id;
        $user_account->debit = 0;
            if(!empty($coupon->cart->discountcoupon_id) && $coupon->cart->discountcoupon_id!='')
            {
                $user_account->credit = ($coupon->cart->net_price -$coupon->cart->discount_amount);
            }
            else{
                $user_account->credit = $coupon->cart->net_price;
                
            }
        
        if ($user_account->save()) {
            $this->sendSuccess(['Voucher cancelled successfully, amount has been credited to the user account']);
            $coupon_transacion->commit();
            $cart_transacion->commit();
            $model->status = Request::STATUS_APPROVED;
            $model->remarks = $POST['premarks']."<br>".$POST['remarks'];
            
            $model->update();
            if($coupon->cart->dealOption->total_sold){
                
            $coupon->cart->dealOption->total_sold = $coupon->cart->dealOption->total_sold -  $coupon->cart->qty;
            $coupon->cart->dealOption->total_items = ($coupon->cart->dealOption->total_items +  $coupon->cart->qty);
            if($coupon->cart->deal->deal_remain!=0){
                $coupon->cart->deal->deal_remain = ($coupon->cart->deal->deal_remain +  $coupon->cart->qty);
            }
            $coupon->cart->dealOption->save();
            $coupon->cart->deal->save();
            
            
            }
            // send request to user 
            \common\components\GenXMailer::start()->sendCustomerVoucherCancelRequestAccept($coupon->cart, $model);
            // send email to admin
            \common\components\GenXMailer::start()->sendCustomerVoucherCancelRequestAccept($coupon->cart, $model, Yii::$app->params['supportEmail']);
            
            
        } else {
            $coupon_transacion->rollBack();
            $cart_transacion->rollBack();
            $this->sendErrors($user_account->getErrors());
        }

        return $this->redirect(['cc']);
       
    }
    
    // rejection coupon
         public function actionSendRequestCCReject() {
        $POST = Yii::$app->request->post();
      
        $model = $this->findModel($POST['id']);
         return $this->renderAjax("couponcancel_request", ['model' => $model]);

       
    }
    
    public function actionCCReject()
    {
        $POST = Yii::$app->request->post();
        $model = $this->findModel($POST['id']);
        $coupon_class = $model->object_class;
        $coupon = $coupon_class::findOne($model->object_id);
        
        $model->status = Request::STATUS_REJECTED;
        $model->remarks = $POST['premarks']."<br><br>".$POST['remarks'];
        $model->update();
        
        // send request to customer
        \common\components\GenXMailer::start()->sendCustomerVoucherCancelRequestReject($coupon->cart, $model);
        // send message to admin
           \common\components\GenXMailer::start()->sendCustomerVoucherCancelRequestReject($coupon->cart, $model,  Yii::$app->params['supportEmail']);
        
        
        $this->sendSuccess(['Request rejected successfully.']);
        return $this->redirect(['cc']);
    }

    // Deal Request Accept
     
         public function actionSendRequestDealApprove() {
        $POST = Yii::$app->request->post();
      
        $model = $this->findModel($POST['id']);
         return $this->renderAjax("dealaccept_request", ['model' => $model]);

       
    }
    public function actionDealRequestApprove()
    {
        $POST = Yii::$app->request->post();
        $model = $this->findModel($POST['id']);
        $model->status = Request::STATUS_APPROVED;
        $model->remarks = $POST['premarks']."\n\r ".$POST['remarks'];
        $model->update();
        //$coupon_class = $model->object_class;
        //$coupon = $coupon_class::findOne($model->object_id);
        // delete coupon
        //$coupon_transacion = \common\models\CouponCode::getDb()->beginTransaction();
        //$coupon->status = \common\models\CouponCode::STATUS_CANCLED;
        //$coupon->update();
        // delete cart
        //$cart_transacion = \common\models\Cart::getDb()->beginTransaction();
        //$coupon->cart->status = \common\models\Cart::STATUS_CANCLED;
        // $coupon->update();
        // add credit to user account
        //$user_account = new \common\models\UserAccount();
        //$user_account->cart_id = $coupon->cart->id;
        //$user_account->user_id = $coupon->cart->user_id;
        //$user_account->order_id = $coupon->cart->order_id;
        //$user_account->transaction_id = "request_cancel_coupon_" . $model->id;
        //$user_account->debit = 0;
        //$user_account->credit = $coupon->cart->net_price;
        //if ($user_account->save()) {
        //   $this->sendSuccess(['Coupon Cancled successfully, Amount has been credited to the user account']);
        //  $coupon_transacion->commit();
        // $cart_transacion->commit();
        //} else {
        //   $coupon_transacion->rollBack();
        //  $cart_transacion->rollBack();
        // $this->sendErrors($user_account->getErrors());
        // }
        $this->sendSuccess([' Deal content updated successfully.']);
        return $this->redirect(['deal-update']);
    }
        // deal request rejection
        public function actionSendRequestDealReject() {
        $POST = Yii::$app->request->post();
        $model = $this->findModel($POST['id']);
         
          return $this->renderAjax("dealcancel_request", ['model' => $model]);

       
    }
    //
    public function actionDealRequestReject()
    {
        $POST = Yii::$app->request->post();
        $model = $this->findModel($POST['id']);
        $model->status = Request::STATUS_REJECTED;
         $model->remarks = $POST['premarks']."<br> ".$POST['remarks'];
        $model->update();
        $this->sendSuccess(['Deal request rejected successfully.']);
        return $this->redirect(['deal-update']);
    }

    /**
     * Finds the Request model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Request the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Request::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
