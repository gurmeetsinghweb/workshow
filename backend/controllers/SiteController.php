<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use common\models\User;
use common\models\PasswordResetRequestForm;
use common\models\ResetPasswordForm;
use common\components\GenXHelper;
use common\models\CartSearch;

/**
 * Site controller
 */
class SiteController extends GenxUseAdminController
{

    public $login_model;
    public $signup_model;

    public function __construct($id, $module, $config = array())
    {
        parent::__construct($id, $module, $config);

        $this->login_model = new LoginForm();
        $this->signup_model = new \backend\models\MerchantRegisterForm();
        $this->signup_model->setScenario('signup');
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'][] = [
            'actions' => [
                'login',
                'error',
                'captcha',
                'register',
                'merchant-signup-success',
                'request-password-reset',
                'validate-email',
                'reset-password',
                'encrypted-link',
            ],
            'allow' => True
        ];
        $behaviors['access']['rules'][] = [
            'allow' => True,
            'actions' => ['index', 'logout'],
            'roles' => [User::ROLE_MERCHANT, User::ROLE_SR, User::ROLE_CSR]
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $rolebased = Yii::$app->user->identity;

        /* switch ($rolebased->type){

          case User::ROLE_MERCHANT:

          break;

          case User::ROLE_ADMIN:

          break;
          default :
          echo "default value";
          break;

          }
         * 
         */



        $this->show_page_header = False;

        $number_of_days = 15;
        $current_time = time();
        $start_time = $current_time - (60 * 60 * 24 * $number_of_days);

        // sales Chart
        // check sale provide
        $saleDate['start_time'] = $start_time;
        $saleDate['current_time'] = $current_time;

        // get Sale gragh Report
        $models = GenXHelper::totalDealSoldAount(0, $saleDate);
        
        $sales_chart_data = [
            'labels' => [],
            'datasets' => [
                [
                    'label' => 'Total Sale',
                    'backgroundColor' => '#2A3F54',
                ]
            ]
        ];
        if($models)
        {   
        foreach ($models as $data_model) {
            $data_subset[$data_model['dd'] . "-" . $data_model['mm'] . "-" . $data_model['yy']] = $data_model['total_sale'];
        }
        }

        while ($start_time <= $current_time) {
            $sale_labels[] = date('j M', $start_time);
            $sale_data[] = (int) ((isset($data_subset[date('j-n-Y', $start_time)])) ? $data_subset[date('j-n-Y', $start_time)] : 0);

            $start_time += (60 * 60 * 24);
        }

        $sales_chart_data['labels'] = array_values($sale_labels);
        $sales_chart_data['datasets'][0]['data'] = array_values($sale_data);


        $start_time = $current_time - (60 * 60 * 24 * $number_of_days);
        if($models)
        { 
        foreach ($models as $data_model) {
            $data_subset[$data_model['dd'] . "-" . $data_model['mm'] . "-" . $data_model['yy']] = $data_model['total_deals'];
        }
        }

        while ($start_time <= $current_time) {
            $deal_labels[] = date('j M', $start_time);
            $deal_data[] = (int) ((isset($data_subset[date('j-n-Y', $start_time)])) ? $data_subset[date('j-n-Y', $start_time)] : 0);

            $start_time += (60 * 60 * 24);
        }

        // get total deal chart report
        $deals_chart_data = [
            'labels' => [],
            'datasets' => [
                [
                    'label' => 'Total Sold Deals',
                    'backgroundColor' => '#26B99A',
                ]
            ]
        ];

        $deals_chart_data['labels'] = array_values($deal_labels);
        $deals_chart_data['datasets'][0]['data'] = array_values($deal_data);

        unset($deal_wise_data, $deal_data_provider);

        // customer
        $start_time = $current_time - (60 * 60 * 24 * $number_of_days);
        $customer_wise_data = new CartSearch();
        $customer_wise_data_provider = $customer_wise_data->search([]);
        $customer_wise_data_provider
                ->query
                ->select("cart.*")
                ->andWhere(['between', 'cart.created_at', $start_time, $current_time])
                ->orderBy('cart.created_at DESC');

        $customer_wise_data_provider->query->joinWith('dealOption');
        //$customer_wise_data_provider->query->joinWith('couponCode');
        if (Yii::$app->user->isOnlyMerchant() && Yii::$app->id == 'app-backend') {
            $customer_wise_data_provider->query->andWhere(['cart.merchant_id' => Yii::$app->user->getLoggedinID()]);
        }

//        GenXHelper::c($customer_wise_data_provider->getModels());exit;
        $metrics = new CartSearch();
        // overall deals
        $models = GenXHelper::totalDealSoldAount();
        // total redeemed Amount
        $dealredemed = [
            'andWhere' => [
                ['coupon_code.status' => \common\components\GenxBaseModel::STATUS_REDEEMED],
                ['is not', 'cart.transaction_log_id', NULL]
            ]
        ];

        $redemeddeal = GenXHelper::totalDealSoldAount($dealredemed);


// total cancel deals  
        $canceldeals = [
            'andWhere' => [
                ['coupon_code.status' => \common\components\GenxBaseModel::STATUS_CANCLED]
            ]
        ];
        $canceldeal = GenXHelper::totalDealSoldAount($canceldeals);
        // total paid amount
        //$paidamount['andWhere']=['<>','cart.transaction_log_id','null'];
        // expire deals
        $expireddeal = [
            'andWhere' => [
                ['!=', 'coupon_code.status', \common\components\GenxBaseModel::STATUS_REDEEMED],
                ['<', 'coupon_code.expires_at', time()]
            ]
        ];
        $expireddeals = GenXHelper::totalDealSoldAount($expireddeal);
        //unpaid balance $total merchant invoice - $merchant cancel - $merchantpaid - $merchantexpire   
        $uncleapayment = [
            'andWhere' => [
                ['coupon_code.status' => \common\components\GenxBaseModel::STATUS_REDEEMED],
                ['is', 'cart.transaction_log_id', NULL]
            ]
        ];

        $unclearpayment = GenXHelper::totalDealSoldAount($uncleapayment);




        $metrics = [
            'total_sale_30_days' => (float) round(array_sum($sale_data), 2),
            'total_deals_sold_30_days' => (int) array_sum($deal_data),
            'total_sale' => (int) round($models[0]['total_sale'], 2),
            'total_deals_sold' => (int) $models[0]['total_deals'],
            //'total_my_earning' => (float) round($models[0]['merchant_sale'], 2),
            //'total_deals_redemed' => (float) round($redemeddeal[0]['merchant_sale'], 2),
            //'total_deals_canceled' => (float) round($canceldeal[0]['merchant_sale'], 2),
            //'total_unclear' => (float) round($unclearpayment[0]['merchant_sale'], 2)
            'total_my_earning'=>0,
            'total_deals_redemed'=>0,
            'total_deals_canceled'=>0,
            'total_unclear'=>0
            
        ];

        return $this->render('index', [
                    'sales_chart_data' => $sales_chart_data,
                    'deals_chart_data' => $deals_chart_data,
                    'metrics' => $metrics,
                    'number_of_days' => $number_of_days,
                    'dataProvider' => $customer_wise_data_provider
        ]);
    }

    public function actionLogin()
    {
        $this->layout = '//main-login';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if ($this->login_model->load(Yii::$app->request->post())) {
            if (!$this->login_model->getUser()) {
                $user = User::findByEmail($this->login_model->email, User::STATUS_PENDING_VALIDATION);
                if (!$user) {
                    $this->sendErrors(['login' => ['User not found.']]);
                } else {
                    if ((new \common\components\GenXMailer())->sendReEmailVerification($user)) {
                        $this->sendSuccess(["Verification email sent again."]);
                    }
                    $this->sendErrors(['login' => ['Email not validated yet, Please verify to login.']]);
                }

                return $this->redirect(\yii\helpers\Url::toRoute(['/site/login']));
            }

            if ($this->login_model->login()) {
                return $this->redirect(\yii\helpers\Url::toRoute(['/']));
            }
        }

        $this->getView()->title = GenXHelper::buildTitle("Login or Register");
        return $this->renderContent('');
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionChangePassword()
    {
        $model = new \common\models\ChangePasswordForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->changePassword()) {
                $this->sendSuccess(["Password Updated Successfully."]);
                return $this->goHome();
            } else {
                $this->sendErrors(["Error Updating Password."]);
                GenXHelper::DevErrors($model->getErrors());
            }
        }
        return $this->render('change-password', ["model" => $model]);
    }

    // Merchant registration
    public function actionRegister()
    {
        $this->layout = '//main-login';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if ($this->signup_model->load(\Yii::$app->request->post())) {
            if ($this->signup_model->register()) {
                \common\components\GenXMailer::start()->sendSignup($this->signup_model->getUser());
                $this->redirect(['/site/merchant-signup-success', 'id' => $this->signup_model->getUser()->id]);
            } else {
                $this->sendErrors(['signup_model' => ['Error Registering new user.']]);
                GenXHelper::DevErrors($this->signup_model->getErrors());
            }
        }

        return $this->renderContent('');
    }

    public function actionMerchantSignupSuccess()
    {
        $this->layout = '//main-blank';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        return $this->render('register-success');
    }

    public function actionValidateEmail($hash)
    {
        /* @var $user User */
        $user = User::findByAuthKey($hash, User::STATUS_PENDING_VALIDATION);
        if (!$user || $user->type != User::ROLE_MERCHANT) {
            throw new \yii\web\NotFoundHttpException();
        }

        $user->status = User::STATUS_ACTIVE;
        // lets update auth key now for using link only once
        $user->generateAuthKey();
        if ($user->save()) {
            $this->sendSuccess(["Email Verified successfully."]);
        } else {
            $this->sendErrors(["Sorry, we are unable to verify you."]);
            GenXHelper::DevErrors($user->getErrors());
        }

        return $this->redirect(\yii\helpers\Url::toRoute(['/site/login']));
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = '//main-login';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                $this->sendSuccess(["Check your email for further instructions."]);
                return $this->goHome();
            } else {
                $this->sendErrors(["Sorry, we are unable to reset password for email provided."]);
                GenXHelper::DevErrors($model->getErrors());
            }
        }

        return $this->render('requestPasswordResetToken', [
                    'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = '//main-login';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            $this->sendSuccess(["New password was saved."]);
            return $this->goHome();
        }

        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

    public function actionEncryptedLink()
    {
        $get = \Yii::$app->request->get();
        $ts = $get['t'];
        $hash = $get['h'];
        $random_1 = $get['r1'];
        $random_2 = $get['r2'];
        $data = $get['d'];

        if (time() > $random_2) {
            throw new \yii\web\ForbiddenHttpException("Expired");
        }

        $pass_token = base64_decode($hash);
        $user = User::findByPasswordResetToken($pass_token);
        if (!$user) {
            throw new \yii\web\ForbiddenHttpException("Invalid Hash");
        }

        $user->generatePasswordResetToken();
        $user->update(False);

        $redirect_url = base64_decode($data);
        return $this->redirect($redirect_url);
    }

}
