<?php

namespace backend\controllers;

use Yii;
use common\models\DealAddress;
use common\models\DealAddressSearch;
use backend\controllers\GenxUseAdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Deal;
use common\models\User;
use yii\helpers\Url;

/**
 * DealOptionController implements the CRUD actions for DealOption model.
 */
class DealAddressController extends GenxUseAdminController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $parent_behavior = parent::behaviors();
        $parent_behavior['access']['rules'][] = [
            'allow' => True,
            'actions' => [],
            'roles' => [User::ROLE_MERCHANT]
        ];

        return $parent_behavior;
    }

    /**
     * Lists all DealOption models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DealAddressSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DealOption model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DealOption model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($deal_id)
    {
        if(Yii::$app->user->isAdmin())
        {    

        // exit;
        $deal_model = Deal::findOne($deal_id);
        if (!$deal_model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $model = new DealAddress();
        
        $merchant = Yii::$app->user->merchant();
        if ($merchant && $merchant->company) {
            $model->geo_lat = $merchant->company->geo_lat;
            $model->geo_long = $merchant->company->geo_long;
        }
        $model->deal_id = $deal_model->id;
        $searchModel = new DealAddressSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['deal_id' => $model->deal_id]);
        // gurmeet: Setting it default to one, so that we can show it checked
        // by default in form
        if (!Yii::$app->request->post()) {
            $model->show_address = 1;
            $model->show_name = 1;
        }
        else{
        // save city and state    
            $getdeal = Yii::$app->request->post();
            $cityvalue= \common\models\City::findOne($getdeal['DealAddress']['city_id']);
            $model->state_id = $cityvalue->state_id;
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if (!Yii::$app->user->isAdmin()) {
                $deal_model->disable();
            }
            $this->sendSuccess(["Deal Address created successfully."]);
            return $this->refresh();
        } else {
            \common\components\GenXHelper::DevErrors($model->getErrors());
        }

        return $this->render('create', [
                    'model' => $model,
                    'deal_model' => $deal_model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
        }
        else{
		      return $this->redirect(\yii\helpers\Url::toRoute(['/deal/index']));
        }
    }

    /**
     * Updates an existing DealOption model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->isAdmin())
        {    

        $checkupdate = Yii::$app->request->post();
        $model = $this->findModel($id);
        $deal_model = Deal::findOne($model->deal_id);
        $searchModel = new DealAddressSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['deal_id' => $model->deal_id]);

        //\common\components\GenXHelper::c($checkupdate);
        if($checkupdate)
            {
            $cityvalue= \common\models\City::findOne($checkupdate['DealAddress']['city_id']);
            $model->state_id = $cityvalue->state_id;
            }
            
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (!Yii::$app->user->isAdmin()) {
                $deal_model->disable();
            }
            $this->sendSuccess(["Deal Address updated successfully."]);
            if (isset($checkupdate['update_id'])) {
                return $this->redirect(['create', 'deal_id' => $model->deal_id, 'update_id' => 1]);
            } else {
                return $this->redirect(['create', 'deal_id' => $model->deal_id]);
            }
        } else {
            $this->sendErrors($model->getErrors());
        }

        return $this->render('update', [
                    'model' => $model,
                    'deal_model' => $deal_model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
        
        
        }
         else{
		      return $this->redirect(\yii\helpers\Url::toRoute(['/deal/index']));
        }
    }

    /**
     * Deletes an existing DealOption model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $deal_address = $this->findModel($id);
        $deal_address->delete();

        return $this->redirect(Url::toRoute(["deal-address/create", 'deal_id' => $deal_address->deal_id]));
    }

    /**
     * Finds the DealOption model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DealOption the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DealAddress::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
