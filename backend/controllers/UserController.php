<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use backend\controllers\GenxUseAdminController;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends GenxUseAdminController
{

    public function behaviors()
    {
        $behaviours = parent::behaviors();
        $behaviours['access']['rules'][] = [
            'allow' => True,
            'actions' => [],
            'roles' => [User::ROLE_ADMIN]
        ];
        $behaviours['access']['rules'][] = [
            'allow' => True,
            'actions' => ['create'],
            'roles' => [
                User::ROLE_MANAGER
            ]
        ];
        $behaviours['access']['rules'][] = [
            'allow' => True,
            'actions' => ['index', 'view'],
            'roles' => [
                User::ROLE_CSM,
                User::ROLE_CSR,
                User::ROLE_SM,
                User::ROLE_SR,
            ]
        ];

        return $behaviours;
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['<>', 'type', User::ROLE_SYSADMIN]);
        $dataProvider->query->andWhere(['<>', 'type', User::ROLE_MERCHANT]);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
       $model = User::find()->where(['id'=>$id])->one();
       return $this->render('view', [
                    'model' => $model,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->sendSuccess(['User Created successfully.']);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            //$model->getErrors();
            if (Yii::$app->request->post()) {
                $this->sendErrors(['Error' => ['Fill complete form.']]);
            }
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->sendSuccess(["User Updated successfully."]);
            return $this->redirect(['index']);
        } else {
            if (Yii::$app->request->post()) {
                $this->sendErrors(['Error' => [' Fill complete form.']]);
            }
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        $this->findModel($id)->delete();
        $this->sendSuccess(['User Deleted Successfully.']);
        return $this->redirect(['index']);
    }

    public function actionMerchantAutocomplete($term)
    {
        $term = trim($term);
        if (!strlen($term)) {
            echo json_encode([]);
        }

        $userSearch = new UserSearch();
        $userSearch->name = $term;
        $user_data = $userSearch->search([])->getModels();

        $return = [];
        if (sizeof($user_data)) {
            foreach ($user_data as $user) {
                if (!$user) {
                    continue;
                }
                $label = $user->name;
                if ($user->company) {
                    $label .= " (Company: {$user->company->title})";
                }
                $return[] = [
                    'label' => $label,
                    'value' => $user->name,
                    'company_id' => (($user->company) ? (int) $user->company->id : 0),
                    'id' => $user->id
                ];
            }
        }
        echo json_encode($return);
    }

    public function actionUserAutocomplete($term)
    {
        $term = trim($term);
        if (!strlen($term)) {
            echo json_encode([]);
        }

        $userSearch = new UserSearch();
        $userSearch->name = $term;
        $userSearch->type = User::ROLE_USER;
        $user_data = $userSearch->search([])->getModels();

        $return = [];
        if (sizeof($user_data)) {
            foreach ($user_data as $user) {
                if (!$user) {
                    continue;
                }
                $label = $user->name;
                if ($user->email) {
                    $label .= "({$user->email})";

                    $return[] = [
                        'label' => $label,
                        'name' => $user->name,
                        'username' => $user->username,
                        'password' => $user->password_hash,
                        'email' => $user->email,
                        'id' => $user->id,
                    ];
                }
            }
        }
        echo json_encode($return);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
