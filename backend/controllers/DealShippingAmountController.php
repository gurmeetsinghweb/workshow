<?php

namespace backend\controllers;

use Yii;
use common\models\DealShippingAmount;
use common\models\DealShippingAmountSearch;
use backend\controllers\GenxUseAdminController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * DealShippingAmountController implements the CRUD actions for DealShippingAmount model.
 */
class DealShippingAmountController extends GenxUseAdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DealShippingAmount models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DealShippingAmountSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DealShippingAmount model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DealShippingAmount model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($deal_id)
    {
        $model = new DealShippingAmount();
	 $deal_model = \common\models\Deal::findOne($deal_id);
        if (!$deal_model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
       
	 $model->deal_id = $deal_model->id;
        $searchModel = new DealShippingAmountSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['deal_id' => $model->deal_id]);
       
	  if ($model->load(Yii::$app->request->post()))
        {     
            
               $model->save();
            $this->sendSuccess(["Record created successfully."]);
            if (!Yii::$app->user->isAdmin()) {
                $deal_model->disable();
            }
            
            return $this->refresh();
        }

	 
      /*  if ($model->load(Yii::$app->request->post()) && $model->save()) {
	     
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }*/
	 
	 
	  return $this->render('create', [
                    'model' => $model,
                    'deal_model' => $deal_model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing DealShippingAmount model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	 $checkupdate=Yii::$app->request->post();
           $model = $this->findModel($id);
	 $deal_model = \common\models\Deal::findOne($model->deal_id);       
	 $model->deal_id = $deal_model->id;
        $searchModel = new DealShippingAmountSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['deal_id' => $model->deal_id]);
       
	  if ($model->load(Yii::$app->request->post()))
        {     
               $model->save();
	             
            $this->sendSuccess(["Record created successfully."]);
            if (!Yii::$app->user->isAdmin()) {  
                $deal_model->disable();
            }
           if(isset($checkupdate['update_id'])) {
               return $this->redirect(['create', 'deal_id' => $model->deal_id,'update_id'=>1]); 
            } 
            else{
                return $this->redirect(['create', 'deal_id' => $model->deal_id]);
            }
        }
        else {
            $this->sendErrors($model->getErrors());
        }

	 
      /*  if ($model->load(Yii::$app->request->post()) && $model->save()) {
	     
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }*/
	 
	 
	  return $this->render('update', [
                    'model' => $model,
                    'deal_model' => $deal_model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Deletes an existing DealShippingAmount model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	 $deal_option = $this->findModel($id);
        $this->findModel($id)->delete();

        return $this->redirect(Url::toRoute(["deal-shipping-amount/create", 'deal_id' => $deal_option->deal_id]));

    }

    /**
     * Finds the DealShippingAmount model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DealShippingAmount the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DealShippingAmount::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
