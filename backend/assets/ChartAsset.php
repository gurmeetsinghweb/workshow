<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\assets;

class ChartAsset extends \yii\web\AssetBundle
{

    public $sourcePath = '@vendor/bower-asset/gentelella/';
    public $css = [
        
    ];
    public $js = [
        'vendors/Chart.js/dist/Chart.min.js',
        'vendors/Flot/jquery.flot.js',
        'vendors/Flot/jquery.flot.pie.js',
        'vendors/Flot/jquery.flot.time.js',
        'vendors/Flot/jquery.flot.stack.js',
        'vendors/Flot/jquery.flot.resize.js',
        'production/js/flot/jquery.flot.orderBars.js',
        'production/js/flot/date.js',
        'production/js/flot/jquery.flot.spline.js',
        'production/js/flot/curvedLines.js',
        'production/js/moment/moment.min.js',
        'production/js/datepicker/daterangepicker.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
    ];

}
