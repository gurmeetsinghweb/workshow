<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\assets;

class DashboardAsset extends \yii\web\AssetBundle
{

    public $sourcePath = '@vendor/bower-asset/gentelella/';
    public $css = [
        'vendors/iCheck/skins/flat/green.css',
        'vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css',
        'production/css/maps/jquery-jvectormap-2.0.3.css',
    ];
    public $js = [
        'vendors/fastclick/lib/fastclick.js',
        'vendors/nprogress/nprogress.js',
        'vendors/bernii/gauge.js/dist/gauge.min.js',
        'vendors/bootstrap-progressbar/bootstrap-progressbar.min.js',
        'vendors/iCheck/icheck.min.js',
        'vendors/skycons/skycons.js',
        'production/js/maps/jquery-jvectormap-2.0.3.min.js',
        'production/js/maps/jquery-jvectormap-world-mill-en.js',
        'production/js/maps/jquery-jvectormap-us-aea-en.js',
        'production/js/maps/gdp-data.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
        'backend\assets\ChartAsset',
        'backend\assets\FromAssets',
    ];

}
