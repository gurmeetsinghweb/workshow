<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\assets;

class IndexAsset extends \yii\web\AssetBundle
{

    public $sourcePath = '@vendor/bower-asset/gentelella/build/';
    public $css = [
        'css/animate.min.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
    ];

}
