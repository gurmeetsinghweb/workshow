<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\assets;

class FromAssets extends \yii\web\AssetBundle
{

    public $sourcePath = '@vendor/bower-asset/gentelella/';
    public $css = [
        '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css',
        
    ];
    public $js = [
        'production/js/moment/moment.min.js',
        'production/js/datepicker/daterangepicker.js',
        '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js',
       
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
    ];
}
