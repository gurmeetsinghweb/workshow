<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\WebSettings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="web-settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'website_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'website_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'website_mobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'website_fax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'website_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'website_googlemap')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
